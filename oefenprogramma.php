<?php session_start();
// oefenprogramma.php 1.9.7.5 (11-02-2013) Bugfix mbt wijzigen thuis team. Kon max 5 karakters aan.
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl 
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl 
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van 
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel
 

include("config.php");
global $dbprefix;
    $filter = "";
    $filterfield = "";
    $wholeonly = "";
 

//error_reporting(E_ALL);
error_reporting(E_ERROR | E_WARNING | E_PARSE);


  //if (isset($_GET["orderop"])) $orderop = @$_GET["orderop"];
  //if (isset($_GET["typeop"])) $ordtypeop = @$_GET["typeop"];

  //if (isset($_POST["filter"])) $filter = @$_POST["filter"];
  //if (isset($_POST["filter_field"])) $filterfield = @$_POST["filter_field"];
  $wholeonly = false;
  //if (isset($_POST["wholeonly"])) $wholeonly = @$_POST["wholeonly"];

  //if (!isset($orderop) && isset($_SESSION["orderop"])) $orderop = $_SESSION["orderop"];
  //if (!isset($ordtypeop) && isset($_SESSION["typeop"])) $ordtypeop = $_SESSION["typeop"];
  //if (!isset($filter) && isset($_SESSION["filter"])) $filter = $_SESSION["filter"];
  //if (!isset($filterfield) && isset($_SESSION["filter_field"])) $filterfield = $_SESSION["filter_field"];


?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head> 
<title>Oefenwedstrijden toevoegen</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />  
<?php echo "<link rel=\"stylesheet\" type=\"text/css\" href='$CSSadmin'>"; ?>
</head>
<body>
<table class="bdadmin" style="width:100%"><tr><td class="hr"><h2>Oefenwedstrijden toevoegen/uitslagen invoeren</h2></td></tr></table>
<?php
  if (!login()) exit;
?>
<div style="float: right"><a href="oefenprogramma.php?a=logout">[ Uitloggen ]</a></div>
<br>
<?php 
  $con = connect();
  $showrecs = 30;
  $pagerange = 10;

  $a = @$_GET["a"];
  $recid = @$_GET["recid"];
  $page = @$_GET["page"];
  if (!isset($page)) $page = 1;

  $sql = @$_POST["sql"];

  switch ($sql) {
    case "insert":
      sql_insert();
      break;
    case "update":
      sql_update();
      break;
    case "delete":
      sql_delete();
      break;
  }

  switch ($a) {
    case "add":
      addrec();
      break;
    case "view":
      viewrec($recid);
      break;
    case "edit":
      editrec($recid);
      break;
    case "del":
      deleterec($recid);
      break;
    default:
      select();
      break;
  }

  if (isset($orderop)) $_SESSION["orderop"] = $orderop;
  if (isset($ordtypeop)) $_SESSION["typeop"] = $ordtypeop;
  if (isset($filter)) $_SESSION["filter"] = $filter;
  if (isset($filterfield)) $_SESSION["filter_field"] = $filterfield;
  if (isset($wholeonly)) $_SESSION["wholeonly"] = $wholeonly;

  mysqli_close($con);
?>
<table class="bdadmin" style="width:100%"><tr><td class="hr">Opties - Gemaakt door Yarro/Johnvs</td></tr></table>
<div style="float: left"><a href="dashboard.php">[ DashBoard ]</a></div><br />
<div style="float: left"><a href="instellingen.php">[ Instellingen ]</a></div><br>
<div style="float: left"><a href="admin.php">[ Beheer Teams ]</a></div><br>
<div style="float: left"><a href="groupid.php">[ Group ID accounts ]</a></div><br>
<div style="float: left"><a href="extra.php">[ Extra informatie Club Programma ]</a></div><br>
<div style="float: left"><a href="uitslag.php">[ Uitslagen invoeren voor Club / Team ]</a></div><br>
<div style="float: left"><a href="kantine.php">[ Kantinebezetting beheren ]</a></div><br>

  
</body>
</html>

<?php function select()
  {
  global $a;
  global $showrecs;
  global $page;
  global $filter;
  global $filterfield;
  global $wholeonly;
  global $orderop;
  global $ordtypeop;
  global $dbprefix;


  if ($a == "reset") {
    $filter = "";
    $filterfield = "";
    $wholeonly = "";
    $orderop = "";
    $ordtypeop = "";
  }

  $checkstr = "";
  if ($wholeonly) $checkstr = " checked";
  if ($ordtypeop == "asc") { $ordtypeopstr = "desc"; } else { $ordtypeopstr = "asc"; }
  $res = sql_select();
  $count = sql_getrecordcount();
  if ($count % $showrecs != 0) {
    $pagecount = intval($count / $showrecs) + 1;
  }
  else {
    $pagecount = intval($count / $showrecs);
  }
  $startrec = $showrecs * ($page - 1);
  if ($startrec < $count) {mysqli_data_seek($res, $startrec);}
  $reccount = min($showrecs * $page, $count);
?>
<table class="bdadmin">
<tr><td>Tabel: oefenprogramma</td></tr>
<tr><td>Getoonde wedstrijden <?php echo $startrec + 1 ?> - <?php echo $reccount ?>  van  <?php echo $count ?></td></tr>
</table>
<hr />
<form action="oefenprogramma.php" method="post">
<table class="bdadmin">
<tr>
<td><b>Filter</b>&nbsp;</td>
<td><input type="text" name="filter" value="<?php echo $filter ?>"></td>
<td><select name="filter_field">
<option value="">Alle velden</option>
<option value="<?php echo "Datum" ?>"<?php if ($filterfield == "Datum") { echo "selected"; } ?>><?php echo htmlspecialchars("Datum") ?></option>
<option value="<?php echo "Tijd" ?>"<?php if ($filterfield == "Tijd") { echo "selected"; } ?>><?php echo htmlspecialchars("Tijd") ?></option>
<option value="<?php echo "Thuis" ?>"<?php if ($filterfield == "Thuis") { echo "selected"; } ?>><?php echo htmlspecialchars("Thuis") ?></option>
<option value="<?php echo "Uit" ?>"<?php if ($filterfield == "Uit") { echo "selected"; } ?>><?php echo htmlspecialchars("Uit") ?></option>
</select></td>
<td><input type="checkbox" name="wholeonly"<?php echo $checkstr ?>>Alleen hele woorden</td>

<tr>
<td>&nbsp;</td>
<td><input type="submit" name="action" value="Filter toepassen"></td>
<td><a href="oefenprogramma.php?a=reset">Reset Filter</a></td>
</tr>
</table>
</form>
<hr />
<?php showpagenav($page, $pagecount); ?>
<br>
<table class="tbl" style="width:100%">
<tr>
<td class="hr">&nbsp;</td>
<td class="hr">&nbsp;</td>
<td class="hr">&nbsp;</td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "Datum" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Datum") ?></a></td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "Tijd" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Tijd") ?></a></td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "Thuis" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Thuis") ?></a></td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "Uit" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Uit") ?></a></td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "Uitslag" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Uitslag") ?></a></td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "Status" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Status") ?></a></td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "Wedstrijdnr" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Wedstrijdnr") ?></a></td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "Scheidsrechter" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Scheidsrechter") ?></a></td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "Vertrekverzameltijd" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Vertrekverzameltijd") ?></a></td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "KlkThuis" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Klk Thuis") ?></a></td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "KlkUit" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Klk Uit") ?></a></td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "Veld" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Veld") ?></a></td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "Accommodatie" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Accommodatie") ?></a></td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "Adres" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Adres") ?></a></td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "Postcode" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Postcode") ?></a></td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "Plaats" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Plaats") ?></a></td>
<td class="hr"><a class="hr" href="oefenprogramma.php?orderop=<?php echo "Telefoon" ?>&amp;typeop=<?php echo $ordtypeopstr ?>"><?php echo htmlspecialchars("Telefoon") ?></a></td>

</tr>
<?php
  for ($i = $startrec; $i < $reccount; $i++)
  {
    $row = mysqli_fetch_assoc($res);
    $style = "dr";
    if ($i % 2 != 0) {
      $style = "sr";
    }
?>
<tr>
<td class="<?php echo $style ?>"><a href="oefenprogramma.php?a=view&amp;recid=<?php echo $i ?>">Bekijk</a></td>
<td class="<?php echo $style ?>"><a href="oefenprogramma.php?a=edit&amp;recid=<?php echo $i ?>">Wijzig</a></td>
<td class="<?php echo $style ?>"><a href="oefenprogramma.php?a=del&amp;recid=<?php echo $i ?>">Wissen</a></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Datum"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Tijd"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Thuis"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Uit"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Uitslag"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Status"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Wedstrijdnr"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Scheidsrechter"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Vertrekverzameltijd"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["KlkThuis"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["KlkUit"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Veld"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Accommodatie"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Adres"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Postcode"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Plaats"]) ?></td>
<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Telefoon"]) ?></td>

</tr>
<?php
  }
  mysqli_free_result($res);
?>
</table>
<br>
<?php showpagenav($page, $pagecount); ?>
<?php } ?>

<?php function login() 
{  
    include("config.php"); 
  global $_POST; 
  global $_SESSION; 

  global $_GET; 
  if (isset($_GET["a"]) && ($_GET["a"] == 'logout')) $_SESSION["logged_in_oefen"] = false; 
  if (!isset($_SESSION["logged_in_oefen"])) $_SESSION["logged_in_oefen"] = false; 
  if (!$_SESSION["logged_in_oefen"]) { 
    $login = ""; 
    $password2 = ""; 

    if (isset($_POST["login"])) $login = @$_POST["login"]; 
    if (isset($_POST["password"])) $password2 = @$_POST["password"]; 

    if (($login != "") && ($password2 != "")) { 
      if (($login == $Oefenusername) && ($password2 == $Oefenpassword)) { 
        $_SESSION["logged_in_oefen"] = true; 
    } 
    else { 
?> 
<p><b><font color="-1">De combinatie gebruikersnaam/wachtwoord is onjuist</font></b></p>
<?php } } }if (isset($_SESSION["logged_in_oefen"]) && (!$_SESSION["logged_in_oefen"])) { ?>
<form action="oefenprogramma.php" method="post">
<table class="bdadmin">
<tr>
<td>Gebruikersnaam</td>
<td><input type="text" name="login" value="<?php echo $login ?>"></td>
</tr>
<tr>
<td>Wachtwoord</td>
<td><input type="password" name="password" value="<?php echo $password2 ?>"></td>
</tr>
<tr>
<td><input type="submit" name="action" value="Inloggen"></td>
</tr>
</table>
</form>
<?php
  }
  if (!isset($_SESSION["logged_in_oefen"])) $_SESSION["logged_in_oefen"] = false;
  return $_SESSION["logged_in_oefen"];
} ?>

<?php function showrow($row, $recid)
  {
?>
<table class="tbl" style="width:50%">
<tr>
<td class="hr"><?php echo htmlspecialchars("Datum")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Datum"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Tijd")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Tijd"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Thuis")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Thuis"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Uit")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Uit"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Uitslag")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Uitslag"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Status")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Status"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Wedstrijdnr")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Wedstrijdnr"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Scheidsrechter")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Scheidsrechter"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Vertrekverzameltijd")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Vertrekverzameltijd"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Kleedkamer Thuis")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["KlkThuis"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Kleedkamer Uit")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["KlkUit"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Veld")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Veld"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Accommodatie")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Accommodatie"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Adres")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Adres"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Postcode")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Postcode"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Plaats")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Plaats"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Telefoon")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Telefoon"]) ?></td>
</tr>

</table>
<?php } ?>

<?php function showroweditor($row, $iseditmode)
  {
  global $con;
?>
<table class="tbl" style="width:50%">
<tr>
<td class="hr"><?php echo htmlspecialchars("Datum")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Datum" value="<?php echo str_replace('"', '&quot;', trim($row["Datum"])) ?>"> Formaat: yyyy-mm-dd</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Tijd")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Tijd" maxlength="11" value="<?php echo str_replace('"', '&quot;', trim($row["Tijd"])) ?>"> Formaat: hh:mm</td></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Thuis")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="Thuis" maxlength="300"><?php echo str_replace('"', '&quot;', trim($row["Thuis"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Uit")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="Uit" maxlength="300"><?php echo str_replace('"', '&quot;', trim($row["Uit"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Uitslag")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Uitslag" maxlength="300" value="<?php echo str_replace('"', '&quot;', trim($row["Uitslag"])) ?>"> * - *</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Afgelast")."&nbsp;" ?></td>
<?php if ($row["Status"] == "afgelast") { ?>
<td class="dr"><input type="checkbox" CHECKED name="Status" maxlength="10" value="<?php echo str_replace('"', '&quot;', trim($row["Status"])) ?>"> Indien aangevinkt is wedstrijd afgelast door Club.  </td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Status" maxlength="10" value="<?php echo str_replace('"', '&quot;', trim($row["Status"])) ?>"> Indien aangevinkt is wedstrijd afgelast door Club.  </td>
<?php } ?>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Wedstrijdnr")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Wedstrijdnr" maxlength="300" value="<?php echo str_replace('"', '&quot;', trim($row["Wedstrijdnr"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Scheidsrechter")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Scheidsrechter" maxlength="300" value="<?php echo str_replace('"', '&quot;', trim($row["Scheidsrechter"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Vertrekverzameltijd")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Vertrekverzameltijd" maxlength="5" value="<?php echo str_replace('"', '&quot;', trim($row["Vertrekverzameltijd"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Kleedkamer Thuis")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="KlkThuis" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["KlkThuis"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Kleedkamer Uit")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="KlkUit" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["KlkUit"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Veld")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Veld" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Veld"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Accommodatie")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Accommodatie" maxlength="300" value="<?php echo str_replace('"', '&quot;', trim($row["Accommodatie"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Adres")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Adres" maxlength="100" value="<?php echo str_replace('"', '&quot;', trim($row["Adres"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Postcode")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Postcode" maxlength="10" value="<?php echo str_replace('"', '&quot;', trim($row["Postcode"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Plaats")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Plaats" maxlength="30" value="<?php echo str_replace('"', '&quot;', trim($row["Plaats"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Telefoon")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Telefoon" maxlength="15" value="<?php echo str_replace('"', '&quot;', trim($row["Telefoon"])) ?>"></td>
</tr>


</table>
<?php } ?>

<?php function showroweditor2($row, $iseditmode)
  {
  global $con;
?>
<table class="tbl" style="width:50%">
<tr>
<td class="hr"><?php echo htmlspecialchars("Datum")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Datum" value="<?php echo str_replace('"', '&quot;', trim($row["Datum"])) ?>"> Formaat: yyyy-mm-dd</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Tijd")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Tijd" maxlength="11" value="<?php echo str_replace('"', '&quot;', trim($row["Tijd"])) ?>"> Formaat: hh:mm</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Thuis")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="Thuis" maxlength="300"><?php echo str_replace('"', '&quot;', trim($row["Thuis"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Uit")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="4" name="Uit" maxlength="300"><?php echo str_replace('"', '&quot;', trim($row["Uit"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Uitslag")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Uitslag" maxlength="300" value="<?php echo str_replace('"', '&quot;', trim($row["Uitslag"])) ?>"> * - *</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Afgelast")."&nbsp;" ?></td>
<?php if ($row["Status"] == "afgelast") { ?>
<td class="dr"><input type="checkbox" CHECKED name="Status" maxlength="10" value="<?php echo str_replace('"', '&quot;', trim($row["Status"])) ?>"> Indien aangevinkt is wedstrijd afgelast door Club.  </td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Status" maxlength="10" value="<?php echo str_replace('"', '&quot;', trim($row["Status"])) ?>"> Indien aangevinkt is wedstrijd afgelast door Club.  </td>
<?php } ?>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Wedstrijdnr")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Wedstrijdnr" maxlength="30" value="<?php echo str_replace('"', '&quot;', trim($row["Wedstrijdnr"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Scheidsrechter")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Scheidsrechter" maxlength="30" value="<?php echo str_replace('"', '&quot;', trim($row["Scheidsrechter"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Vertrekverzameltijd")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Vertrekverzameltijd" maxlength="5" value="<?php echo str_replace('"', '&quot;', trim($row["Vertrekverzameltijd"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Kleedkamer Thuis")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="KlkThuis" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["KlkThuis"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Kleedkamer Uit")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="KlkUit" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["KlkUit"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Veld")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Veld" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Veld"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Accommodatie")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Accommodatie" maxlength="300" value="<?php echo str_replace('"', '&quot;', trim($row["Accommodatie"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Adres")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Adres" maxlength="100" value="<?php echo str_replace('"', '&quot;', trim($row["Adres"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Postcode")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Postcode" maxlength="10" value="<?php echo str_replace('"', '&quot;', trim($row["Postcode"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Plaats")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Plaats" maxlength="30" value="<?php echo str_replace('"', '&quot;', trim($row["Plaats"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Telefoon")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Telefoon" maxlength="15" value="<?php echo str_replace('"', '&quot;', trim($row["Telefoon"])) ?>"></td>
</tr>
</table>
<?php } ?>

<?php function showpagenav($page, $pagecount)
{
?>
<table class="bdadmin">
<tr>
<td><a href="oefenprogramma.php?a=add">Wedstrijden toevoegen</a>&nbsp;</td>
<?php if ($page > 1) { ?>
<td><a href="oefenprogramma.php?page=<?php echo $page - 1 ?>">&lt;&lt;&nbsp;Vorige</a>&nbsp;</td>
<?php } ?>
<?php
  global $pagerange;

  if ($pagecount > 1) {

  if ($pagecount % $pagerange != 0) {
    $rangecount = intval($pagecount / $pagerange) + 1;
  }
  else {
    $rangecount = intval($pagecount / $pagerange);
  }
  for ($i = 1; $i < $rangecount + 1; $i++) {
    $startpage = (($i - 1) * $pagerange) + 1;
    $count = min($i * $pagerange, $pagecount);

    if ((($page >= $startpage) && ($page <= ($i * $pagerange)))) {
      for ($j = $startpage; $j < $count + 1; $j++) {
        if ($j == $page) {
?>
<td><b><?php echo $j ?></b></td>
<?php } else { ?>
<td><a href="oefenprogramma.php?page=<?php echo $j ?>"><?php echo $j ?></a></td>
<?php } } } else { ?>
<td><a href="oefenprogramma.php?page=<?php echo $startpage ?>"><?php echo $startpage ."..." .$count ?></a></td>
<?php } } } ?>
<?php if ($page < $pagecount) { ?>
<td>&nbsp;<a href="oefenprogramma.php?page=<?php echo $page + 1 ?>">Volgende&nbsp;&gt;&gt;</a>&nbsp;</td>
<?php } ?>
</tr>
</table>
<?php } ?>

<?php function showrecnav($a, $recid, $count)
{
?>
<table class="bdadmin">
<tr>
<td><a href="oefenprogramma.php">Index Pagina</a></td>
<?php if ($recid > 0) { ?>
<td><a href="oefenprogramma.php?a=<?php echo $a ?>&recid=<?php echo $recid - 1 ?>">Vorige wedstrijd</a></td>
<?php } if ($recid < $count - 1) { ?>
<td><a href="oefenprogramma.php?a=<?php echo $a ?>&recid=<?php echo $recid + 1 ?>">Volgende wedstrijd</a></td>
<?php } ?>
</tr>
</table>
<hr />
<?php } ?>

<?php function addrec()
{
?>
<table class="bdadmin">
<tr>
<td><a href="oefenprogramma.php">Index Pagina</a></td>
</tr>
</table>
<hr />
<form enctype="multipart/form-data" action="oefenprogramma.php" method="post">
<p><input type="hidden" name="sql" value="insert"></p>
<?php
$row = array(
  "ClubProgrammaID" => "",
  "Datum" => "",
  "Tijd" => "",
  "Thuis" => "",
  "Uit" => "",
  "typeop" => "",
  "Accommodatie" => "",
  "Wedstrijdnr" => "",
  "Scheidsrechter" => "",
  "Vertrekverzameltijd" => "",
  "KlkThuis" => "",
  "KlkUit" => "",
  "Veld" => "",
  "Uitslag" => "",
  "Status" => "",
  "Adres" => "",
  "Postcode" => "",
  "Plaats" => "",
  "Telefoon" => "");
showroweditor($row, false);
?>
<p><input type="submit" name="action" value="Toevoegen"></p>
</form>
<?php } ?>

<?php function viewrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  mysqli_data_seek($res, $recid);
  $row = mysqli_fetch_assoc($res);
  showrecnav("view", $recid, $count);
?>
<br>
<?php showrow($row, $recid) ?>
<br>
<hr />
<table class="bdadmin">
<tr>
<td><a href="oefenprogramma.php?a=add">Wedstrijden toevoegen</a></td>
<td><a href="oefenprogramma.php?a=edit&recid=<?php echo $recid ?>">Wedstrijd wijzigen</a></td>
<td><a href="oefenprogramma.php?a=del&recid=<?php echo $recid ?>">Wedstrijd wissen</a></td>
</tr>
</table>
<?php
  mysqli_free_result($res);
} ?>

<?php function editrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  mysqli_data_seek($res, $recid);
  $row = mysqli_fetch_assoc($res);
  showrecnav("edit", $recid, $count);
?>
<br>
<form enctype="multipart/form-data" action="oefenprogramma.php" method="post">
<input type="hidden" name="sql" value="update">
<input type="hidden" name="xClubProgrammaID" value="<?php echo $row["ClubProgrammaID"] ?>">
<?php showroweditor2($row, true); ?>
<p><input type="submit" name="action" value="Toevoegen"></p>
</form>
<?php
  mysqli_free_result($res);
} ?>

<?php function deleterec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  mysqli_data_seek($res, $recid);
  $row = mysqli_fetch_assoc($res);
  showrecnav("del", $recid, $count);
?>
<br>
<form action="oefenprogramma.php" method="post">
<input type="hidden" name="sql" value="delete">
<input type="hidden" name="xClubProgrammaID" value="<?php echo $row["ClubProgrammaID"] ?>">
<?php showrow($row, $recid) ?>
<p><input type="submit" name="action" value="Bevestigen"></p>
</form>
<?php
  mysqli_free_result($res);
} ?>

<?php function connect()
{
  include("config.php"); 

  return $con;
}

function sqlvalue($val, $quote)
{
  if ($quote)
    $tmp = sqlstr($val);
  else
    $tmp = $val;
  if ($tmp == "")
    $tmp = "NULL";
  elseif ($quote)
    $tmp = "'".$tmp."'";
  return $tmp;
}

function sqlstr($val)
{
  return str_replace("'", "''", $val);
}

function sql_select()
{
  global $con;
  global $orderop;
  global $ordtypeop;
  global $filter;
  global $filterfield;
  global $wholeonly;
  global $dbprefix;

  $filterstr = sqlstr($filter);
  if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
  $sql = "SELECT * FROM `".$dbprefix."oefenprogramma`";
  if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
    $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
  } elseif (isset($filterstr) && $filterstr!='') {
    $sql .= " where (`Datum` like '" .$filterstr ."') or (`Tijd` like '" .$filterstr ."') or (`Thuis` like '" .$filterstr ."') or (`Uit` like '" .$filterstr ."')";
  }
  if (isset($orderop) && $orderop!='') $sql .= " order by `" .sqlstr($orderop) ."`";
  if (isset($ordtypeop) && $ordtypeop!='') $sql .= " " .sqlstr($ordtypeop);
  $res = mysqli_query($con,$sql) or die(mysqli_error());
  return $res;

}

function sql_getrecordcount()
{
  global $con;
  global $orderop;
  global $ordtypeop;
  global $filter;
  global $filterfield;
  global $wholeonly;
  global $dbprefix;

  $filterstr = sqlstr($filter);
  if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
  $sql = "SELECT COUNT(*) FROM `".$dbprefix."oefenprogramma`";
  if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
    $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
  } elseif (isset($filterstr) && $filterstr!='') {
    $sql .= " where (`Datum` like '" .$filterstr ."') or (`Tijd` like '" .$filterstr ."') or (`Thuis` like '" .$filterstr ."') or (`Uit` like '" .$filterstr ."')";
  }
  $res = mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
  $row = mysqli_fetch_assoc($res);
  reset($row);
  return current($row);
}

function sql_insert()
{
if(isset($_POST["Status"])){  
    $_POST["Status"] = "afgelast";  
  }  
  else{  
    $_POST["Status"] = ""; 
  }
  global $con;
  global $_POST;
  global $dbprefix;

  $sql = "insert into `".$dbprefix."oefenprogramma` (`Datum`, `Tijd`, `Thuis`, `Uit`, `Wedstrijdnr`,  `Scheidsrechter`, `Vertrekverzameltijd`,  `KlkThuis`, `KlkUit`, `Veld`, `Accommodatie`,`Adres`, `Postcode`, `Plaats`, `Telefoon`) values (" .sqlvalue(@$_POST["Datum"], true).", " .sqlvalue(@$_POST["Tijd"], true).", " .sqlvalue(@$_POST["Thuis"], true).", " .sqlvalue(@$_POST["Uit"], true).", " .sqlvalue(@$_POST["Wedstrijdnr"], true).", " .sqlvalue(@$_POST["Scheidsrechter"], true).", " .sqlvalue(@$_POST["Vertrekverzameltijd"], true).", " .sqlvalue(@$_POST["KlkThuis"], true).", " .sqlvalue(@$_POST["KlkUit"], true).", " .sqlvalue(@$_POST["Veld"], true).", " .sqlvalue(@$_POST["Accommodatie"], true).", " .sqlvalue(@$_POST["Adres"], true).", " .sqlvalue(@$_POST["Postcode"], true).", " .sqlvalue(@$_POST["Plaats"], true).", " .sqlvalue(@$_POST["Telefoon"], true).")";
//  mysql_query($sql, $con) or die(mysql_error());
$res = mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
}

function sql_update()
{
if(isset($_POST["Status"])){  
    $_POST["Status"] = "afgelast";  
  }  
  else{  
    $_POST["Status"] = ""; 
  }
  global $con;
  global $_POST;
  global $dbprefix;

  $sql = "update `".$dbprefix."oefenprogramma` set `Datum`=" .sqlvalue(@$_POST["Datum"], true).", `Tijd`=" .sqlvalue(@$_POST["Tijd"], true).", `Thuis`=" .sqlvalue(@$_POST["Thuis"], true).", `Uit`=" .sqlvalue(@$_POST["Uit"], true) .", `Uitslag`=" .sqlvalue(@$_POST["Uitslag"], true) .", `Status`=" .sqlvalue(@$_POST["Status"], true) .",  `Wedstrijdnr`=" .sqlvalue(@$_POST["Wedstrijdnr"], true) .", `Scheidsrechter`=" .sqlvalue(@$_POST["Scheidsrechter"], true) .", `Vertrekverzameltijd`=" .sqlvalue(@$_POST["Vertrekverzameltijd"], true).", `KlkThuis`=" .sqlvalue(@$_POST["KlkThuis"], true) .", `KlkUit`=" .sqlvalue(@$_POST["KlkUit"], true) .", `Veld`=" .sqlvalue(@$_POST["Veld"], true) .", `Accommodatie`=" .sqlvalue(@$_POST["Accommodatie"], true).", `Adres`=" .sqlvalue(@$_POST["Adres"], true).", `Postcode`=" .sqlvalue(@$_POST["Postcode"], true).", `Plaats`=" .sqlvalue(@$_POST["Plaats"], true).", `Telefoon`=" .sqlvalue(@$_POST["Telefoon"], true) ." where " .primarykeycondition();
//  mysql_query($sql, $con) or die(mysql_error());
$res = mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
}

function sql_delete()
{
  global $con;
  global $dbprefix;

  $sql = "delete from `".$dbprefix."oefenprogramma` where " .primarykeycondition();
  $res = mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
}
function primarykeycondition()
{
  global $_POST;
  $pk = "";
  $pk .= "(`ClubProgrammaID`";
  if (@$_POST["xClubProgrammaID"] == "") {
    $pk .= " IS NULL";
  }else{
  $pk .= " = " .sqlvalue(@$_POST["xClubProgrammaID"], false);
  };
  $pk .= ")";
  return $pk;
}
 ?>
