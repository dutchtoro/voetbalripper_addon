<?php
/// Dit simpele update script wordt gebruikt voor eventuele updates aan de database.
/// Hierin worden alle nieuwe rijen toegevoegd als deze nodig zijn voor volledig gebruik.

function checkVersion() {
	global $con, $dbprefix, $scriptDBVersion;


	$checktable = mysqli_query($con, "SHOW TABLES LIKE '%{$dbprefix}%'") or die(mysqli_error($con));
	$exists = (_mysqli_num_rows($checktable))?TRUE:FALSE;
	if(!$exists)
	{
		print("Database is leeg of bestaat niet!<br />\n");
		include_once('tabellenmaken.php');
		die("Database wordt aangemaakt. Probeer het over enkele ogenblikken nogmaals.<br />\n");
	}

	$result = mysqli_query($con, "SHOW COLUMNS FROM `${dbprefix}instellingen` LIKE 'dbVersion'") or die(mysqli_error($con));
	$exists = (_mysqli_num_rows($result))?TRUE:FALSE;
	if (!$exists)
	{
		print("Database niet up-to-date voor deze aanpassingen! Bezig met updaten!<br />\n");
		mysqli_query($con, "ALTER TABLE `${dbprefix}instellingen` ADD COLUMN dbVersion VARCHAR(10) NOT NULL DEFAULT '0'") or die(mysqli_error($con));
		mysqli_query($con, "UPDATE `${dbprefix}instellingen` SET `dbVersion`='1'") or die(mysqli_error($con));
		checkVersion();
	}
	else
	{
		$query = "SELECT * FROM `${dbprefix}instellingen`";
		$result = mysqli_query($con, $query);
		$row = mysqli_fetch_assoc($result);
		$currentDBVersion = $row['dbVersion'];
		if ($currentDBVersion < $scriptDBVersion)
		{
			doUpdate($currentDBVersion);
		}
		else if ($currentDBVersion > $scriptDBVersion)
		{
			echo "De database is nieuwer dan het script, dit kan voor problemen zorgen. Download een nieuwe versie van het script!";
		}
	}

}


function doUpdate($currentDBVersion) {
	global $con, $dbprefix;
	$sql ="";
	echo "Huidige DB versie is $currentDBVersion<br />\n";

	switch($currentDBVersion) {
		case 0:
		case 1:
			echo "Updaten naar versie 2<br />\n";
			$sql .= "UPDATE `${dbprefix}instellingen` SET `dbVersion`='2';
			ALTER TABLE `${dbprefix}programma` ADD COLUMN Pagina CHAR(50) NULL;
			ALTER TABLE `${dbprefix}teamlinks` ADD UNIQUE INDEX `Index` (`Teamcode`(100));
			ALTER TABLE `${dbprefix}teamlinks` CHANGE COLUMN `Wedstrijdtype` `Wedstrijdtype` VARCHAR(15) NULL DEFAULT 'Competitie_NJ' AFTER `Klasse`;
			ALTER TABLE `${dbprefix}clubprogramma`
				ALTER `Type` DROP DEFAULT,
				ALTER `Accommodatie` DROP DEFAULT,
				ALTER `Wedstrijdnr` DROP DEFAULT,
				ALTER `Scheidsrechter` DROP DEFAULT,
				ALTER `Status` DROP DEFAULT;
			ALTER TABLE `${dbprefix}clubprogramma`
				CHANGE COLUMN `Type` `Type` VARCHAR(2) NULL AFTER `Uit`,
				CHANGE COLUMN `Accommodatie` `Accommodatie` VARCHAR(300) NULL AFTER `Type`,
				CHANGE COLUMN `Wedstrijdnr` `Wedstrijdnr` VARCHAR(6) NULL AFTER `Accommodatie`,
				CHANGE COLUMN `Scheidsrechter` `Scheidsrechter` VARCHAR(30) NULL AFTER `Wedstrijdnr`,
				CHANGE COLUMN `Status` `Status` VARCHAR(30) NULL AFTER `Scheidsrechter`;";
		//Voorbeeld cases voor updates
		/*case 2:
			echo "Updaten naar versie 3<br />\n";
			$sql .= "UPDATE `${dbprefix}instellingen` SET `dbVersion`='3';"; */
		default:
			echo "Updaten voltooid<br />\n";
			break;
	}
	mysqli_multi_query($con, $sql) or die(mysqli_error($con));
	mysqli_reset();
	$con = connect_db();
	//unset($con);
}


?>
