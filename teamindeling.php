<?php
// teamindeling.php
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel


// Weergave van team gegevens behorende bij Voetbal.nl Ripper

include("config.php");
include("functies.php");

mysql_connect($server,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");


$teamID = filter_var($_GET['teamID'], FILTER_VALIDATE_INT); //alleen numerieke waarde toegstaan - voorkomt mogelijkheid tot sql injection

//Query om de uitslagen per team op te halen

$query1 = "SELECT * FROM ".$dbprefix."teamindeling WHERE teamID=$teamID";

$result1=mysql_query($query1);
$num1=mysql_numrows($result1);




//Query om teamnaam en klasse op te halen
$query3 = "SELECT * FROM ".$dbprefix."teamlinks WHERE teamID=$teamID";
$result3=mysql_query($query3);
$checkid=mysql_num_rows($result3);
$klasse=mysql_result($result3,0,"Klasse");
$naam=mysql_result($result3,0,"Naam");
$wedstrijdduur=mysql_result($result3,0,"Wedstrijdduur");
//$sponsor=mysql_result($result3,$i,"Sponsor");

//Controle op juiste teamID
if ($checkid < 1)
{
	echo "Team ID niet gevonden. Gebruik team.php?teamID=* op de plaats van * moet een geldige teamID staan.";
	die;
}
else
{
}




//Aantal teams per indeling
$result5 = mysql_query("SELECT * FROM ".$dbprefix."teamindeling WHERE teamID=$teamID");
$numrows5 = mysql_num_rows($result5);

//Queries om teamindeling op te halen
$a = round($numrows5/2); //Bepalen van aantal team die in de linkertabel moeten komen
$query6 = "SELECT * FROM ".$dbprefix."teamindeling WHERE teamID=$teamID";
$result6 = mysql_query($query6);
$num6=mysql_numrows($result6);


$result5a = mysql_query("SELECT * FROM ".$dbprefix."speelronde WHERE teamID=$teamID and Wedstrijd like '%speelronde%'");
$numrows5a = mysql_num_rows($result5a);
if($numrows5a % 2) $numrows5a = $numrows5a+"1";
$aantalteams= round($num1/2);
$aantalteams= $aantalteams + "1";
$berek= $numrows5a * $aantalteams;

$a2 = round($berek/2); //Bepalen van aantal indelingen die in de linkertabel moeten komen
$query6a = "SELECT * FROM ".$dbprefix."speelronde WHERE teamID=$teamID";
$result6a = mysql_query($query6a);
$num6a=mysql_numrows($result6a);

// Query om de datum van laatste update op te halen
$query7 = "SHOW TABLE STATUS from ".$database." LIKE ".$dbprefix."teamindeling";
$result7=mysql_query($query7);

//Query and Result
$updateTimeQuery = "show table status from $database like '".$dbprefix."teamindeling'";
$updateTimeResult = @mysql_query($updateTimeQuery)
or die("Couldn't execute Query.");
//Get the Result
while ($row = mysql_fetch_array($updateTimeResult, MYSQL_ASSOC)) {
	$Number_rows = $row["Rows"];
	$Date_created = $row["Create_time"];
	$Update_time = $row["Update_time"];

}

//print " Last Updated on: $Update_time";

mysql_close();

?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?php echo $naam; ?></title>


<?php echo "<link rel=\"stylesheet\" type=\"text/css\" href='$CSSweergave' />"; ?>

</head>

<body>
<div style="text-align:center"><br/>
<h2><?php echo $naam; ?></h2> </div>
<h3><?php echo $klasse; ?></h3>







<h3>Team Indeling</h3>
<table class="alle">

<tr><td style="width:350px; vertical-align:top;">
<table style="width:350px">
<tr>
<th class="left" style="width:25px">nr</th>
<th class="left" style="width:300px">Team</th>
<th class="center" style="width:50px">Voorkeurstijd</th>
</tr>
<?php
$i=0;
while ($i < $a) {
	$teamnr=mysql_result($result6,$i,"Teamnr");
	$naam=mysql_result($result6,$i,"Naam");
	$vktijd=mysql_result($result6,$i,"Vktijd");

	?>
	<tr>
	<?php IF (strstr($naam,$clubnaam)) { ?>
		<td class="left23" style="width:25px"><?php echo $teamnr; ?></td>
		<td class="left23" style="width:295px"><?php echo $naam; ?></td>
		<td class="center23" style="width:50px"><?php echo $vktijd; ?></td>
		</tr>
		<?php }
	ELSE { ?>
		<td class="left2" style="width:25px"><?php echo $teamnr; ?></td>
		<td class="left2" style="width:295px"><?php echo $naam; ?></td>
		<td class="center2" style="width:50px"><?php echo $vktijd; ?></td>
		</tr>
		<?php } ?>
	<?php
	$i++;
}
echo "</table></td>"; ?>


<td style="width:350px; vertical-align:top;">
<table style="width:350px">
<tr>
<th class="left" style="width:25px">nr</th>
<th class="left" style="width:300px">Team</th>
<th class="center" style="width:50px">Voorkeurstijd</th>
</tr>
<?php
$i=$a;
while ($i < $num6)  {
	$teamnr2=mysql_result($result6,$i,"Teamnr");
	$naam2=mysql_result($result6,$i,"Naam");
	$vktijd2=mysql_result($result6,$i,"Vktijd");
	?>
	<tr>
	<?php IF (strstr($naam2,$clubnaam)) { ?>
		<td class="left23" style="width:25px"><?php echo $teamnr2; ?></td>
		<td class="left23" style="width:300px"><?php echo $naam2; ?></td>
		<td class="center23" style="width:50px"><?php echo $vktijd2; ?></td>
		</tr>
		<?php }
	ELSE { ?>
		<td class="left2" style="width:25px"><?php echo $teamnr2; ?></td>
		<td class="left2" style="width:300px"><?php echo $naam2; ?></td>
		<td class="center2" style="width:50px"><?php echo $vktijd2; ?></td>
		</tr>
		<?php } ?>
	<?php
	$i++;
}
echo "</table></td>";
echo "</tr></table>";



?>

<h3>Speelronde</h3>
<table class="alle">

<tr><td style="width:350px; vertical-align:top;">
<table style="width:350px">
<tr>
<th class="left" style="width:400px">Team</th>
</tr>
<?php
$i=0;
while ($i < $a2) {
	$speelrondenr=mysql_result($result6a,$i,"Speelrondenr");
	$wedstrijd=mysql_result($result6a,$i,"Wedstrijd");

	?>
	<tr>
	<?php IF (strstr($wedstrijd,"speelronde")) $wedstrijd = "<div class=\"speelronde\">".$wedstrijd."</div>"; ?>
	<?php IF (strstr($wedstrijd,$clubnaam)) { ?>

		<td class="left23" style="width:300px"><?php echo $wedstrijd; ?></td>

		</tr>
		<?php }
	ELSE { ?>

		<td class="left2" style="width:300px"><?php echo $wedstrijd; ?></td>

		</tr>
		<?php } ?>
	<?php
	$i++;
}
echo "</table></td>"; ?>


<td style="width:350px; vertical-align:top;">
<table style="width:350px">
<tr>
<th class="left" style="width:300px">Team</th>
</tr>
<?php
$i=$a2;
while ($i < $num6a)  {
	$speelrondenr2=mysql_result($result6a,$i,"Speelrondenr");
	$wedstrijd2=mysql_result($result6a,$i,"Wedstrijd");

	?>
	<tr>
	<?php IF (strstr($wedstrijd2,"speelronde")) $wedstrijd2 = "<div class=\"speelronde\">".$wedstrijd2."</div>"; ?>
	<?php IF (strstr($wedstrijd2,$clubnaam)) { ?>

		<td class="left23" style="width:300px"><?php echo $wedstrijd2; ?></td>

		</tr>
		<?php }
	ELSE { ?>

		<td class="left2" style="width:300px"><?php echo $wedstrijd2; ?></td>

		</tr>
		<?php } ?>
	<?php
	$i++;
}
echo "</table></td>";
echo "</tr></table>";



?>


<table class="alle">
<tr>
<td class="small"><br />Bijgewerkt op: <?php
/* Set locale to Dutch */
if(getOS() == 'linux')
{
	setlocale(LC_ALL, array('nl_NL'));
}else{
	setlocale(LC_ALL, array('nld_nld'));
}
echo date('d/m/y : H:i', strtotime($Update_time));



?>
</td>
</tr>
<tr>
<td class="left"><br />Bron: <a href='http://www.voetbal.nl' target='_blank'>Voetbal.nl</a></td>
</tr>
</table>

</body>
</html>