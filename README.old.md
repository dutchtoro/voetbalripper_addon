Gebruik:
-----
1) Zoek je clubcode en teamcode  
   - ga naar https://www.voetbal.nl/profiel/teams => voeg een team toe, zoek je club op  => bekijk teams  
   - in de url staat dan je clubcode => https://www.voetbal.nl/club/CLUBCODE/teams  
   - bij de teams staan de teamcodes per team, bijv. https://www.voetbal.nl/team/T9999999990/overzicht, waarbij T9999999990 de teamcode is  

2) vullen database van juiste clubcode en teamcodes  
   - zorg dat je de juiste clubcode in tabel vrip_instellingen hebt staan (is meestal niet gewijzigd)  
   - zorg dat je de teamcodes goed invult in tabel vrip_teamlinks  
   - bij teams met een halve competitie (najaar en voorjaar) is er 1 wijziging in de database, vul hier bij wedstrijdtype Competitie_NJ (ipv Competitie)  

3) kopieer index2.php en functies2.php bij je originele installatie van voetbalripper 1.9.7

4) werkt met de volgende instellingen (getest):  
"Clubnaamcheck": "Aan"  
"CPophalen": "Aan"  
"CUophalen": "Aan"  
"VVinCP": "Uit"  
"Pdetails": "Uit"  
"Pdetailstonen": "Uit"  
"Klkamertonen": "Uit"  
"Veldtonen": "Uit"  
"Oefeninteam": "Uit"  
"Tindelinginteam": "Uit"  
"OphViaAcode": "Uit"  
"AutoAcode": "Uit"  
"Wacht": "Aan"  
"Opschonen": "Aan"  
"Meldingen": "Scherm"  
"Curl": "Aan"  
"Phpsafemode": "Uit"  
"Phpmaxtime": "90"  
"Beveiligindex": "Uit"  
"UserDebug": "Aan"  
  (geen ondersteuning voor safemode en proxy class)
  
5) Haal teamdata op per teamId (in dit voorbeeld teamId=1) met: index2.php?teamID=1
