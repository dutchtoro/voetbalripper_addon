<?php session_start();
// admin.php 1.9.7.1 (07-11-2012) Bugfixes en kleine cosmetische aanpassingen.
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 19-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel

error_reporting(E_ERROR | E_WARNING | E_PARSE);


include("config.php");
include("functies.php");
//mysql_connect($server,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
global $dbprefix;
$tbl_name=$dbprefix."teamlinks"; // Table name
$filter = "";


if (isset($_GET["orderadmin"])) $orderadmin = @$_GET["orderadmin"];
if (isset($_GET["typeadmin"])) $ordtypeadmin = @$_GET["typeadmin"];

if (isset($_POST["filter"])) $filter = @$_POST["filter"];
if (isset($_POST["filter_field"])) $filterfield = @$_POST["filter_field"];
$wholeonly = false;
if (isset($_POST["wholeonly"])) $wholeonly = @$_POST["wholeonly"];

if (!isset($orderadmin) && isset($_SESSION["orderadmin"])) $orderadmin = $_SESSION["orderadmin"];
if (!isset($ordtypeadmin) && isset($_SESSION["typeadmin"])) $ordtypeadmin = $_SESSION["typeadmin"];
if (!isset($filter) && isset($_SESSION["filter"])) $filter = $_SESSION["filter"];
if (!isset($filterfield) && isset($_SESSION["filter_field"])) $filterfield = $_SESSION["filter_field"];
if (isset($_POST['Submit']))
{	//echo "<p>submit pressed";


	foreach($_POST['TeamID'] as $id)
	{

		$_POST["Naam".$id] = htmlentities($_POST["Naam".$id], ENT_QUOTES);
		$sql1="UPDATE ".$tbl_name." SET Teamcode='".$_POST["Teamcode".$id]."', AltTeamcode='".$_POST["AltTeamcode".$id]."', Naam='".$_POST["Naam".$id]."', Klasse='".$_POST["Klasse".$id]."', Wedstrijdtype='".$_POST["Wedstrijdtype".$id]."', Wedstrijdduur='".$_POST["Wedstrijdduur".$id]."', Periode='".$_POST["Periode".$id]."', Ophalen='".$_POST["Ophalen".$id]."', GroupID='".$_POST["GroupID".$id]."' WHERE TeamID='".$id."'";
		//$result1=mysqli_query($sql1) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
		mysqli_query($con,$sql1) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));

	}
}

?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>Beheer Teams</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php echo "<link rel=\"stylesheet\" typeadmin=\"text/css\" href='$CSSadmin' />"; ?>
</head>
<body>
<table class="bdadmin" style="width:100%"><tr><td class="hr"><h2>Beheer Teams</h2></td></tr></table>

<?php
if (!login()) exit;
?>
<div style="float: right"><a href="admin.php?a=logout">[ Uitloggen ]</a></div>
<br />
<?php
$con = connect();
$showrecs = 30;
$pagerange = 10;

$a = @$_GET["a"];
if ($a == "import")
{
	echo "Importeren";
	$ch = curl_init();


	// Inloggen op voetbal.nl
	login_voetbalnl($gebruikersnaam, $wachtwoord);

	// Nu bekijken of we een melding krijgen dat er ingelogd moet worden als we een url aanroepen. Ja dan is het inloggen fout gegaan.
	$inlog = check_inlog("http://pupillen.voetbal.nl/clubs-competities/html/clubs/programma/".$club);
	if ($inlog == "Log in of registreer gratis!")
	{
		echo "Probleem met Voetbal.nl ripper.<br /><br />";
		echo "Het inloggen op de site is niet gelukt. <strong>Script is gestopt.</strong><br />";
		die;
	}else
	{
		//Geen probleem met inloggen dus doorgaan.
	}

	//teams ophalen
	//  $teams = get_teams("http://pupillen.voetbal.nl/clubs-competities/html/clubs/teams/".$club);
	$teams = get_teams("https://www.voetbal.nl/club/$myclub/teams");
	

	
	//teams opslaan
	//opslaan_teams($teams);
	opslaan_teams(get_teamsdetails($teams));


}

if ($a == "reorder")
{
	reorderdb($dbprefix."teamlinks","TeamID");
}

if ($a == "cleartabel")
{
	leegmakendbcp($dbprefix."teamlinks");
}


$recid = @$_GET["recid"];
$page = @$_GET["page"];
if (!isset($page)) $page = 1;

$sql = @$_POST["sql"];

switch ($sql) {
case "insert":
	sql_insert();
	break;
case "update":
	sql_update();
	break;
case "delete":
	sql_delete();
	break;
}

switch ($a) {
case "add":
	addrec();
	break;
case "view":
	viewrec($recid);
	break;
case "edit":
	editrec($recid);
	break;
case "del":
	deleterec($recid);
	break;
default:
	select();
	break;
}

if (isset($orderadmin)) $_SESSION["orderadmin"] = $orderadmin;
if (isset($ordtypeadmin)) $_SESSION["typeadmin"] = $ordtypeadmin;
if (isset($filter)) $_SESSION["filter"] = $filter;
if (isset($filterfield)) $_SESSION["filter_field"] = $filterfield;
if (isset($wholeonly)) $_SESSION["wholeonly"] = $wholeonly;

mysqli_close($con);
?>
<table class="bdadmin" style="width:100%"><tr><td class="hr">Gemaakt door Yarro/Johnvs</td></tr></table>
<div style="float: left"><a href="dashboard.php">[ DashBoard ]</a></div><br />
<div style="float: left"><a href="instellingen.php">[ Instellingen ]</a></div><br />
<div style="float: left"><a href="groupid.php">[ Group ID accounts ]</a></div><br />
<div style="float: left"><a href="oefenprogramma.php">[ Oefen Programma/Uitslagen ]</a></div><br />
<div style="float: left"><a href="extra.php">[ Extra informatie Club Programma ]</a></div><br />
<div style="float: left"><a href="uitslag.php">[ Uitslagen invoeren voor Club / Team ]</a></div><br />

</body>
</html>

<?php function select()
{
	global $a;
	global $showrecs;
	global $page;
	global $filter;
	global $filterfield;
	global $wholeonly;
	global $orderadmin;
	global $ordtypeadmin;


	if ($a == "reset") {
		$filter = "";
		$filterfield = "";
		$wholeonly = "";
		$orderadmin = "";
		$ordtypeadmin = "";
	}

	$checkstr = "";
	if ($wholeonly) $checkstr = " checked";
	if ($ordtypeadmin == "asc") { $ordtypeadminstr = "desc"; } else { $ordtypeadminstr = "asc"; }
	$res = sql_select();
	$count = sql_getrecordcount();
	if ($count % $showrecs != 0) {
		$pagecount = intval($count / $showrecs) + 1;
	}
	else {
		$pagecount = intval($count / $showrecs);
	}
	$startrec = $showrecs * ($page - 1);
	if ($startrec < $count) {mysqli_data_seek($res, $startrec);}
	$reccount = min($showrecs * $page, $count);
	?>
	<table class="bdadmin">
	<tr><td>Tabel: teamlinks</td></tr>
	<tr><td>Getoonde teams  <?php echo $startrec + 1 ?> - <?php echo $reccount ?>  &nbsp;van&nbsp;  <?php echo $count ?></td></tr>
	</table>
	<hr />
	<form action="admin.php" method="post">
	<table class="bdadmin">
	<tr>
	<td><b>Filter</b>&nbsp;</td>
	<td><input type="text" name="filter" value="<?php echo $filter ?>" /></td>
	<td><select name="filter_field">
	<option value="">Alle velden</option>
	<option value="<?php echo "TeamID" ?>"<?php if ($filterfield == "TeamID") { echo "selected"; } ?>><?php echo htmlspecialchars("TeamID") ?></option>
	<option value="<?php echo "Teamcode" ?>"<?php if ($filterfield == "Teamcode") { echo "selected"; } ?>><?php echo htmlspecialchars("Teamcode") ?></option>
	<option value="<?php echo "AltTeamcode" ?>"<?php if ($filterfield == "AltTeamcode") { echo "selected"; } ?>><?php echo htmlspecialchars("AltTeamcode") ?></option>
	<option value="<?php echo "Naam" ?>"<?php if ($filterfield == "Naam") { echo "selected"; } ?>><?php echo htmlspecialchars("Naam") ?></option>
	<option value="<?php echo "Klasse" ?>"<?php if ($filterfield == "Klasse") { echo "selected"; } ?>><?php echo htmlspecialchars("Klasse") ?></option>
	<option value="<?php echo "Wedstrijdtype" ?>"<?php if ($filterfield == "Wedstrijdtype") { echo "selected"; } ?>><?php echo htmlspecialchars("Wedstrijdtype") ?></option>
	<option value="<?php echo "Wedstrijdduur" ?>"<?php if ($filterfield == "Wedstrijdduur") { echo "selected"; } ?>><?php echo htmlspecialchars("Wedstrijdduur") ?></option>
	<option value="<?php echo "Periode" ?>"<?php if ($filterfield == "Periode") { echo "selected"; } ?>><?php echo htmlspecialchars("Periode") ?></option>
	<option value="<?php echo "Ophalen" ?>"<?php if ($filterfield == "Ophalen") { echo "selected"; } ?>><?php echo htmlspecialchars("Ophalen") ?></option>
	<option value="<?php echo "GroupID" ?>"<?php if ($filterfield == "GroupID") { echo "selected"; } ?>><?php echo htmlspecialchars("GroupID") ?></option>
	<option value="<?php echo "Datum/Tijd-Update" ?>"<?php if ($filterfield == "DatumTijd-Update") { echo "selected"; } ?>><?php echo htmlspecialchars("Datum/Tijd-Update") ?></option>
	</select></td>
	<td><input type="checkbox" name="wholeonly"<?php echo $checkstr ?> />Alleen hele woorden</td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	<td><input type="submit" name="action" value="Filter toepassen" /></td>
	<td><a href="admin.php?a=reset">Reset Filter</a></td>
	</tr>
	</table>
	</form>
	<hr />
	&nbsp;
	<a href="admin.php?a=import"> [Alle teams Importeren] </a>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

	<a href="admin.php?a=reorder"> [Opschonen/her-orden tabel (TeamID's kunnen wijzigen)] </a>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<a href="admin.php?a=cleartabel"> [Leegmaken teamlinks database (Alle teamID's worden verwijderd)] </a>


	<hr />
	<?php showpagenav($page, $pagecount); ?>
	<br />
	<form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
	<table class="tbl" style="width:100%">
	<tr>
	<td class="hr">&nbsp;</td>
	<td class="hr">&nbsp;</td>
	<td class="hr">&nbsp;</td>
	<td class="hr"><a class="hr" href="admin.php?orderadmin=<?php echo "TeamID" ?>&amp;typeadmin=<?php echo $ordtypeadminstr ?>"><?php echo htmlspecialchars("TeamID") ?></a></td>
	<td class="hr"><a class="hr" href="admin.php?orderadmin=<?php echo "Teamcode" ?>&amp;typeadmin=<?php echo $ordtypeadminstr ?>"><?php echo htmlspecialchars("Teamcode") ?></a></td>
	<td class="hr"><a class="hr" href="admin.php?orderadmin=<?php echo "AltTeamcode" ?>&amp;typeadmin=<?php echo $ordtypeadminstr ?>"><?php echo htmlspecialchars("AltTeamcode") ?></a></td>
	<td class="hr"><a class="hr" href="admin.php?orderadmin=<?php echo "Naam" ?>&amp;typeadmin=<?php echo $ordtypeadminstr ?>"><?php echo htmlspecialchars("Naam") ?></a></td>
	<td class="hr"><a class="hr" href="admin.php?orderadmin=<?php echo "Klasse" ?>&amp;typeadmin=<?php echo $ordtypeadminstr ?>"><?php echo htmlspecialchars("Klasse") ?></a></td>
	<td class="hr"><a class="hr" href="admin.php?orderadmin=<?php echo "Wedstrijdtype" ?>&amp;typeadmin=<?php echo $ordtypeadminstr ?>"><?php echo htmlspecialchars("Wedstrijdtype") ?></a></td>
	<td class="hr"><a class="hr" href="admin.php?orderadmin=<?php echo "Wedstrijdduur" ?>&amp;typeadmin=<?php echo $ordtypeadminstr ?>"><?php echo htmlspecialchars("Wedstrijdduur") ?></a></td>
	<td class="hr"><a class="hr" href="admin.php?orderadmin=<?php echo "Periode" ?>&amp;typeadmin=<?php echo $ordtypeadminstr ?>"><?php echo htmlspecialchars("Periode") ?></a></td>
	<td class="hr"><a class="hr" href="admin.php?orderadmin=<?php echo "Ophalen" ?>&amp;typeadmin=<?php echo $ordtypeadminstr ?>"><?php echo htmlspecialchars("Ophalen") ?></a></td>
	<td class="hr"><a class="hr" href="admin.php?orderadmin=<?php echo "GroupID" ?>&amp;typeadmin=<?php echo $ordtypeadminstr ?>"><?php echo htmlspecialchars("GroupID") ?></a></td>
	<td class="hr"><a class="hr" href="admin.php?orderadmin=<?php echo "DatumTijd-Update" ?>&amp;typeadmin=<?php echo $ordtypeadminstr ?>"><?php echo htmlspecialchars("DatumTijd-Update") ?></a></td>

	</tr>

	<?php
	for ($i = $startrec; $i < $reccount; $i++)
	{
		$row = mysqli_fetch_assoc($res);
		$style = "dr";
		$bstyle = "style=\"background-color: #FFFFFF; border: 1px solid #D0D0D0;\"";
		if ($i % 2 != 0) {
			$style = "sr";
			$bstyle = "style=\"background-color: #A6D2FF; border: 1px solid #D0D0D0;\"";

		}
		?>

		<tr>
		<td class="<?php echo $style ?>"><a href="admin.php?a=view&amp;recid=<?php echo $i ?>">Bekijk</a></td>
		<td class="<?php echo $style ?>"><a href="admin.php?a=edit&amp;recid=<?php echo $i ?>">Wijzig</a></td>
		<td class="<?php echo $style ?>"><a href="admin.php?a=del&amp;recid=<?php echo $i ?>">Wissen</a></td>
		<td class="<?php echo $style ?>"><input type="hidden" name="TeamID[]" value="<?php echo $row['TeamID']; ?>" /><?php echo $row['TeamID']; ?></td>
		<td class="<?php echo $style ?>"><input name="Teamcode<?php echo $row['TeamID']; ?>" typeadmin="text" class="<?php echo $style ?>" <?php echo $bstyle ?>  id="Teamcode<?php echo $row['Teamcode'] ?>" value="<?php echo $row['Teamcode'] ?>" /></td>
		<td class="<?php echo $style ?>"><input name="AltTeamcode<?php echo $row['TeamID']; ?>" typeadmin="text" class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="AltTeamcode<?php echo $row['Teamcode'] ?>" value="<?php echo $row['AltTeamcode']; ?>" /></td>
		<td class="<?php echo $style ?>"><input name="Naam<?php echo $row['TeamID']; ?>" typeadmin="text" class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="Naam<?php echo $row['Teamcode'] ?>" value="<?php echo $row['Naam']; ?>" /></td>
		<td class="<?php echo $style ?>"><input name="Klasse<?php echo $row['TeamID']; ?>" typeadmin="text" class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="Klasse<?php echo $row['Teamcode'] ?>" value="<?php echo $row['Klasse']; ?>" /></td>
		<td class="<?php echo $style ?>"><input name="Wedstrijdtype<?php echo $row['TeamID']; ?>" typeadmin="text" size="15" class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="Wedstrijdtype<?php echo $row['Teamcode'] ?>" value="<?php echo $row['Wedstrijdtype']; ?>" /></td>
		<td class="<?php echo $style ?>"><input name="Wedstrijdduur<?php echo $row['TeamID']; ?>" typeadmin="text" size="5" class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="Wedstrijdduur<?php echo $row['Teamcode'] ?>" value="<?php echo $row['Wedstrijdduur']; ?>" /></td>
		<td class="<?php echo $style ?>"><input name="Periode<?php echo $row['TeamID']; ?>" typeadmin="text" size="5" class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="Periode<?php echo $row['Teamcode'] ?>" value="<?php echo $row['Periode']; ?>" /></td>
		<td class="<?php echo $style ?>"><input name="Ophalen<?php echo $row['TeamID']; ?>" typeadmin="text" size="5" class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="Ophalen<?php echo $row['Teamcode'] ?>" value="<?php echo $row['Ophalen']; ?>" /></td>
		<td class="<?php echo $style ?>"><input name="GroupID<?php echo $row['TeamID']; ?>" typeadmin="text" size="5" class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="GroupID<?php echo $row['Teamcode'] ?>" value="<?php echo $row['GroupID']; ?>" /></td>
		<td class="<?php echo $style ?>"><?php echo htmlspecialchars(date("d/m/y : H:i:s",($row["DatumTijd-Update"]))) ?></td>

		</tr>


		<?php
	}

	mysqli_free_result($res);
	?>

	<tr>
	<td class="center" colspan="14"><input type="submit" name="Submit" value="Wijzigingen aanbrengen" /></td>
	</tr>


	</table>
	</form>
	<br />
	<?php showpagenav($page, $pagecount); ?>
	<?php } ?>

<?php function login()
{
	include("config.php");
	global $_POST;
	global $_SESSION;

	global $_GET;
	if (isset($_GET["a"]) && ($_GET["a"] == 'logout')) $_SESSION["logged_in_admin"] = false;
	if (!isset($_SESSION["logged_in_admin"])) $_SESSION["logged_in_admin"] = false;
	if (!$_SESSION["logged_in_admin"]) {
		$login = "";
		$password2 = "";

		if (isset($_POST["login"])) $login = @$_POST["login"];
		if (isset($_POST["password"])) $password2 = @$_POST["password"];

		if (($login != "") && ($password2 != "")) {
			if (($login == $Adminusername) && ($password2 == $Adminpassword)) {
				$_SESSION["logged_in_admin"] = true;
			}
			else {
				?>
				<p><b><font color="-1">De combinatie gebruikersnaam/wachtwoord is onjuist</font></b></p>
				<?php } } }if (isset($_SESSION["logged_in_admin"]) && (!$_SESSION["logged_in_admin"])) { ?>
		<form action="admin.php" method="post">
		<table class="bdadmin">
		<tr>
		<td>Gebruikersnaam</td>
		<td><input type="text" name="login" value="<?php echo $login ?>" /></td>
		</tr>
		<tr>
		<td>Wachtwoord</td>
		<td><input type="password" name="password" value="<?php echo $password2 ?>" /></td>
		</tr>
		<tr>
		<td><input type="submit" name="action" value="Inloggen" /></td>
		</tr>
		</table>
		</form>
		</body>
		</html>
		<?php
	}
	if (!isset($_SESSION["logged_in_admin"])) $_SESSION["logged_in_admin"] = false;
	return $_SESSION["logged_in_admin"];
} ?>

<?php function showrow($row, $recid)
{
	?>
	<table class="tbl" style="width:50%">
	<tr>
	<td class="hr"><?php echo htmlspecialchars("TeamID")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["TeamID"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Teamcode")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Teamcode"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("AltTeamcode")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["AltTeamcode"]) ?></td>
	</tr>

	<tr>
	<td class="hr"><?php echo htmlspecialchars("Naam")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Naam"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Klasse")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Klasse"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Wedstrijdtype")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Wedstrijdtype"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Wedstrijdduur")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Wedstrijdduur"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Periode")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Periode"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Ophalen")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Ophalen"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("GroupID")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["GroupID"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("DatumTijd-Update")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars(date("d/m/y : H:m:s",($row["DatumTijd-Update"]))) ?></td>
	</tr>
	</table>
	<?php } ?>

<?php function showroweditor($row, $iseditmode)
{
	global $con;
	?>
	<table class="tbl" style="width:50%">
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Teamcode")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="4" name="Teamcode"><?php echo str_replace('"', '&quot;', trim($row["Teamcode"])) ?></textarea></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("AltTeamcode")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="4" name="AltTeamcode"><?php echo str_replace('"', '&quot;', trim($row["AltTeamcode"])) ?></textarea></td>
	</tr>

	<tr>
	<td class="hr"><?php echo htmlspecialchars("Naam")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="4" name="Naam" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Naam"])) ?></textarea></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Klasse")."&nbsp;" ?></td>
	<td class="dr"><input type="text" name="Klasse" maxlength="400" value="<?php echo str_replace('"', '&quot;', trim($row["Klasse"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Wedstrijdtype")."&nbsp;" ?></td>
<?php if ($row["Wedstrijdtype"] == "Competitie") { ?>
<td class="dr"><select name="Wedstrijdtype">
<option value="Competitie" selected>Competitie</option><option value="Beker">Beker</option></select>
<?php } else { ?>
<?php } ?>
<?php if ($row["Wedstrijdtype"] == "Beker") { ?>
<td class="dr"><select name="Wedstrijdtype">
<option value="Competitie">Competitie</option><option value="Beker" selected>Beker</option></select>
<?php } else { ?>
<?php } ?>
<?php if ($row["Wedstrijdtype"] != "Competitie" and $row["Wedstrijdtype"] != "Beker") { ?>
<td class="dr"><select name="Wedstrijdtype">
<option value="Competitie" selected>Competitie</option><option value="Beker">beker</option></select>
<?php } else { ?>
<?php } ?>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Wedstrijdduur")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Wedstrijdduur" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Wedstrijdduur"])) ?>"> In minuten incl. rust</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Periode")."&nbsp;" ?></td>
	<?php if ($row["Periode"] == "ja") { ?>
		<td class="dr"><input type="checkbox" CHECKED name="Periode" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Periode"])) ?>"> Indien aangevinkt worden de periode standen voor het team opgehaald. </td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Periode" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Periode"])) ?>"> Indien aangevinkt worden de periode standen voor het team opgehaald. </td>
		<?php } ?>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Ophalen")."&nbsp;" ?></td>
	<?php if ($row["Ophalen"] == "ja") { ?>
		<td class="dr"><input type="checkbox" CHECKED name="Ophalen" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Ophalen"])) ?>"> Indien aangevinkt worden uitslagen, standen etc. voor het team opgehaald. </td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Ophalen" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Ophalen"])) ?>"> Indien aangevinkt worden uitslagen, standen etc. voor het team opgehaald. </td>
		<?php } ?>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("GroupID")."&nbsp;" ?></td>
	<td class="dr"><input type="text" name="GroupID" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["GroupID"])) ?>">  Alleen numerieke waarde. Indien ophalen per Group wordt gezet in config.php kan via index.php?groupID=* alle gegevens van de teams met de zelfde groupID worden opgehaald.</td>
</tr>


</table>
<?php } ?>

<?php function showpagenav($page, $pagecount)
{
?>
<table class="bdadmin">
<tr>
<td><a href="admin.php?a=add">[Team toevoegen]</a>&nbsp;</td>
<?php if ($page > 1) { ?>
<td><a href="admin.php?page=<?php echo $page - 1 ?>">&lt;&lt;&nbsp;Vorige</a>&nbsp;</td>
<?php } ?>
<?php
global $pagerange;

if ($pagecount > 1) {

if ($pagecount % $pagerange != 0) {
	$rangecount = intval($pagecount / $pagerange) + 1;
}
else {
	$rangecount = intval($pagecount / $pagerange);
}
for ($i = 1; $i < $rangecount + 1; $i++) {
	$startpage = (($i - 1) * $pagerange) + 1;
	$count = min($i * $pagerange, $pagecount);

	if ((($page >= $startpage) && ($page <= ($i * $pagerange)))) {
	for ($j = $startpage; $j < $count + 1; $j++) {
		if ($j == $page) {
?>
<td><b><?php echo $j ?></b></td>
<?php } else { ?>
<td><a href="admin.php?page=<?php echo $j ?>"><?php echo $j ?></a></td>
<?php } } } else { ?>
<td><a href="admin.php?page=<?php echo $startpage ?>"><?php echo $startpage ."..." .$count ?></a></td>
<?php } } } ?>
<?php if ($page < $pagecount) { ?>
<td>&nbsp;<a href="admin.php?page=<?php echo $page + 1 ?>">Volgende&nbsp;&gt;&gt;</a>&nbsp;</td>
<?php } ?>
</tr>
</table>
<?php } ?>

<?php function showrecnav($a, $recid, $count)
{
?>
<table class="bdadmin">
<tr>
<td><a href="admin.php">Index Pagina</a></td>
<?php if ($recid > 0) { ?>
<td><a href="admin.php?a=<?php echo $a ?>&amp;recid=<?php echo $recid - 1 ?>">Vorige team</a></td>
<?php } if ($recid < $count - 1) { ?>
<td><a href="admin.php?a=<?php echo $a ?>&amp;recid=<?php echo $recid + 1 ?>">Volgende team</a></td>
<?php } ?>
</tr>
</table>
<hr />
<?php } ?>

<?php function addrec()
{
?>
<table class="bdadmin">
<tr>
<td><a href="admin.php">Index Pagina</a></td>
</tr>
</table>
<hr />
<form enctypeadmin="multipart/form-data" action="admin.php" method="post">
<p><input type="hidden" name="sql" value="insert"></p>
<?php
$row = array(
"TeamID" => "",
"Teamcode" => "",
"AltTeamcode" => "",
"Naam" => "",
"Klasse" => "",
"Wedstrijdtype" => "",
"Wedstrijdduur" => "");
showroweditor($row, false);
?>
<p><input type="submit" name="action" value="Toevoegen"></p>
</form>
<?php } ?>

<?php function viewrec($recid)
{
$res = sql_select();
$count = sql_getrecordcount();
mysqli_data_seek($res, $recid);
$row = mysqli_fetch_assoc($res);
showrecnav("view", $recid, $count);
?>
<br />
<?php showrow($row, $recid) ?>
<br />
<hr />
<table class="bdadmin">
<tr>
<td><a href="admin.php?a=add">Team toevoegen</a></td>
<td><a href="admin.php?a=edit&amp;recid=<?php echo $recid ?>">Team wijzigen</a></td>
<td><a href="admin.php?a=del&amp;recid=<?php echo $recid ?>">Team wissen</a></td>
</tr>
</table>
<?php
mysqli_free_result($res);
} ?>

<?php function editrec($recid)
{
$res = sql_select();
$count = sql_getrecordcount();
mysqli_data_seek($res, $recid);
$row = mysqli_fetch_assoc($res);
showrecnav("edit", $recid, $count);
?>
<br />
<form enctypeadmin="multipart/form-data" action="admin.php" method="post">
<input type="hidden" name="sql" value="update">
<input type="hidden" name="xTeamID" value="<?php echo $row["TeamID"] ?>">
<?php showroweditor($row, true); ?>
<p><input type="submit" name="action" value="Wijzigen"></p>
</form>
<?php
mysqli_free_result($res);
} ?>

<?php function deleterec($recid)
{
$res = sql_select();
$count = sql_getrecordcount();
mysqli_data_seek($res, $recid);
$row = mysqli_fetch_assoc($res);
showrecnav("del", $recid, $count);
?>
<br />
<form action="admin.php" method="post">
<input type="hidden" name="sql" value="delete">
<input type="hidden" name="xTeamID" value="<?php echo $row["TeamID"] ?>">
<?php showrow($row, $recid) ?>
<p><input type="submit" name="action" value="Bevestigen"></p>
</form>
<?php
mysqli_free_result($res);
} ?>


<?php function connect()
{
include("config.php");
//$con = mysql_connect($server,$username,$password);
//mysql_select_db($database);
return $con;
}

function sqlvalue($val, $quote)
{
if ($quote)
	$tmp = sqlstr($val);
else
	$tmp = $val;
if ($tmp == "")
	$tmp = "NULL";
elseif ($quote)
	$tmp = "'".$tmp."'";
return $tmp;
}

function sqlstr($val)
{
return str_replace("'", "''", $val);
}

function sql_select()
{
global $con;
global $orderadmin;
global $ordtypeadmin;
global $filter;
global $filterfield;
global $wholeonly;
global $dbprefix;

$filterstr = sqlstr($filter);
if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
$sql = "SELECT `TeamID`, `Teamcode`, `AltTeamcode`, `Naam`, `Klasse`, `Wedstrijdtype`, `Wedstrijdduur`, `Periode`,`Ophalen`, `GroupID`, `DatumTijd-Update` FROM `".$dbprefix."teamlinks`";
if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
	$sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
} elseif (isset($filterstr) && $filterstr!='') {
	$sql .= " where (`TeamID` like '" .$filterstr ."') or (Teamcode like '" .$filterstr ."') or (AltTeamcode like '" .$filterstr ."') or (`Naam` like '" .$filterstr ."') or (`Klasse` like '" .$filterstr ."') or (`Wedstrijdtype` like '" .$filterstr ."') or (`Wedstrijdduur` like '" .$filterstr ."') or (`Periode` like '" .$filterstr ."') or (`Ophalen` like '" .$filterstr ."') or (`GroupID` like '" .$filterstr ."') or (`DatumTijd-Update` like '" .$filterstr ."')";
}
if (isset($orderadmin) && $orderadmin!='') $sql .= " order by `" .sqlstr($orderadmin) ."`";
if (isset($ordtypeadmin) && $ordtypeadmin!='') $sql .= " " .sqlstr($ordtypeadmin);
$res = mysqli_query($con,$sql) or die(mysqli_error($con));
return $res;
}

function sql_getrecordcount()
{
global $con;
global $orderadmin;
global $ordtypeadmin;
global $filter;
global $filterfield;
global $wholeonly;
global $dbprefix;

$filterstr = sqlstr($filter);
if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
$sql = "SELECT COUNT(*) FROM `".$dbprefix."teamlinks`";
if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
	$sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
} elseif (isset($filterstr) && $filterstr!='') {
	$sql .= " where (`TeamID` like '" .$filterstr ."') or (Teamcode like '" .$filterstr ."') or (AltTeamcode like '" .$filterstr ."') or (`Naam` like '" .$filterstr ."') or (`Klasse` like '" .$filterstr ."') or (`Wedstrijdtype` like '" .$filterstr ."') or (`Wedstrijdduur` like '" .$filterstr ."') or (`Periode` like '" .$filterstr ."') or (`Ophalen` like '" .$filterstr ."') or (`GroupID` like '" .$filterstr ."') or (`DatumTijd-Update` like '" .$filterstr ."')";
}
$res = mysqli_query($con,$sql) or die(mysqli_error($con));
$row = mysqli_fetch_assoc($res);
reset($row);
return current($row);
}

function sql_insert()
{
if(isset($_POST["Periode"])){
	$_POST["Periode"] = "ja";
}
else{
	$_POST["Periode"] = "nee";
}

if(isset($_POST["Ophalen"])){
	$_POST["Ophalen"] = "ja";
}
else{
	$_POST["Ophalen"] = "nee";
}

global $con;
global $_POST;
global $dbprefix;
$dt="1230768001";

$sql = "insert into `".$dbprefix."teamlinks` (`Teamcode`, `AltTeamcode`, `Naam`, `Klasse`, `Wedstrijdtype`, `Wedstrijdduur`, `Periode`, `Ophalen`, `GroupID`, `DatumTijd-Update`) values (" .sqlvalue(@$_POST["Teamcode"], true).", " .sqlvalue(@$_POST["AltTeamcode"], true).", " .sqlvalue(@$_POST["Naam"], true).", " .sqlvalue(@$_POST["Klasse"], true).", " .sqlvalue(@$_POST["Wedstrijdtype"], true).", "  .sqlvalue(@$_POST["Wedstrijdduur"], true).", "  .sqlvalue(@$_POST["Periode"], true).", " .sqlvalue(@$_POST["Ophalen"], true).", " .sqlvalue(@$_POST["GroupID"], true).", " .$dt.")";
mysqli_query($con,$sql) or die(mysqli_error($con));
}

function sql_update()
{
if(isset($_POST["Periode"])){
$_POST["Periode"] = "ja";
}
else{
$_POST["Periode"] = "nee";
}

if(isset($_POST["Ophalen"])){
$_POST["Ophalen"] = "ja";
}
else{
$_POST["Ophalen"] = "nee";
}

global $con;
global $_POST;
global $dbprefix;
$dt="1230768001";

$sql = "update `".$dbprefix."teamlinks` set Teamcode=" .sqlvalue(@$_POST["Teamcode"], true).", AltTeamcode=" .sqlvalue(@$_POST["AltTeamcode"], true).", `Naam`=" .sqlvalue(@$_POST["Naam"], true).", `Klasse`=" .sqlvalue(@$_POST["Klasse"], true).", `Wedstrijdtype`=" .sqlvalue(@$_POST["Wedstrijdtype"], true).",  `Wedstrijdduur`=" .sqlvalue(@$_POST["Wedstrijdduur"], true).",  `Periode`=" .sqlvalue(@$_POST["Periode"], true) .", `Ophalen`=" .sqlvalue(@$_POST["Ophalen"], true) .", `GroupID`=" .sqlvalue(@$_POST["GroupID"], true) .", `DatumTijd-Update`=" .$dt ." where " .primarykeycondition();
mysqli_query($con,$sql) or die(mysqli_error($con));
}

function sql_delete()
{
global $con;
global $dbprefix;

$sql = "delete from `".$dbprefix."teamlinks` where " .primarykeycondition();
mysqli_query($con,$sql) or die(mysqli_error($con));
}
function primarykeycondition()
{
global $_POST;
$pk = "";
$pk .= "(`TeamID`";
if (@$_POST["xTeamID"] == "") {
	$pk .= " IS NULL";
}else{
$pk .= " = " .sqlvalue(@$_POST["xTeamID"], false);
};
$pk .= ")";
return $pk;
}
?>