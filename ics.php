<?php
// ics.php
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl 
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl 
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van 
// Redroest, Yarro, patron2, FreddyHell, Killerbee


// Output buffer naar file
// aanmaken van iCalendar file in team.php



$datum = $_GET['datum']; 
$tijd = $_GET['tijd']; 
$wedstrijdnr = $_GET['wedstrijdnr']; 
$wedstrijdduur = $_GET['wedstrijdduur'];
$adres = $_GET['adres']; 
$postcode = $_GET['postcode']; 
$plaats = $_GET['plaats']; 
$telefoon = $_GET['telefoon']; 
$wedstrijd = $_GET['wedstrijd']; 
$accommodatie = $_GET['accommodatie'];

$adres = html_entity_decode($adres, ENT_QUOTES);
$plaats = html_entity_decode($plaats, ENT_QUOTES);
$wedstrijd = html_entity_decode($wedstrijd, ENT_QUOTES);
$accommodatie = html_entity_decode($accommodatie, ENT_QUOTES);


$datum = str_replace("-", "", $datum);
$tijd = $tijd . ":00";
$tijd = str_replace(":", "", $tijd);


$etijd =  strtotime($tijd . " +" . $wedstrijdduur . " minutes");
$etijd = date("His", $etijd);

$DTStamp = date('Ymd') . "T". date('His');
$DTStart = $datum . "T" . $tijd;
$DTEnd = $datum . "T" . $etijd;

$saveas=$datum."-".$wedstrijd.".ics"; 

ob_start(); 

// Verzamel gevens en print
print"BEGIN:VCALENDAR\n";
print"PRODID:-//Microsoft Corporation//Outlook 11.0 MIMEDIR//EN\n";
print"METHOD:PUBLISH\n";
print"BEGIN:VEVENT\n";
print"UID:$wedstrijd\n";
print"DTSTAMP:$DTStamp\n";
print"DTSTART:$DTStart\n";
print"DTEND:$DTEnd\n";
print"TRANSP:Transparant\n";
print"SUMMARY:$wedstrijd\n";
print"LOCATION:$accommodatie\n";
print"DESCRIPTION;ENCODING=QUOTED-PRINTABLE:$wedstrijd=0D=0A$accommodatie=0D=0AWedstrijdnr:$wedstrijdnr=0D=0A$adres=0D=0A$postcode $plaats=0D=0A$telefoon\n";
print"END:VEVENT\n";
print"END:VCALENDAR\n";

// Stop buffer; Alleen buffer printen verder niets
$buffer=ob_get_contents(); 
ob_end_clean(); 

header("Pragma: public"); 
header("Expires: 0"); 
header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
header("Cache-Control: private", false); 
header("Content-Transfer-Encoding: binary "); 
header('Content-Type: application/force-download'); 
header("Content-Type: application/download"); 
header('Content-Length: '.strlen($buffer)); 
header('Content-Disposition: attachment; filename="'.$saveas.'"'); 

echo $buffer; 

?> 