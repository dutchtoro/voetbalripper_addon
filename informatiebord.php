<?php
// informatiebord.php
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel


// programma overzicht van alle thuiswedstrijden voor 'vandaag' met overzicht van scheidsrechter, kleedkamer en veld, autmatische pagina verversing na 5 minuten, sortering optie.


include("config.php");

// waardes hard gezet.

$Klkamertonen ="Aan";
$Veldtonen = "Aan";
$ClubprogrammaScheids = "Aan";
$refreshs = "300"; // In seconden = 5 minuten
$refreshm = "5"; // In minuten = 5 minuten

include('functies.php');
mysql_connect($server,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");

If ($SortCP == "Datum-Tijd-Team") $outputsortTh = "Datum, Tijd, Thuis ASC"; // Sorteer op Datum, Tijd, Teamnaam
If ($SortCP == "Datum-Tijd-Team") $outputsortUi = "Datum, Tijd, Uit ASC"; // Sorteer op Datum, Tijd, Teamnaam

If ($SortCP == "Team-Datum-Tijd") $outputsortTh = "Thuis, Datum, Tijd ASC"; // Sorteer op Teamnaam, Datum, Tijd
If ($SortCP == "Team-Datum-Tijd") $outputsortUi = "uit, Datum, Tijd ASC"; // Sorteer op Teamnaam, Datum, Tijd

If ($SortCP == "Datum-Team-Tijd") $outputsortTh = "Datum, Thuis, Tijd ASC"; // Sorteer op Datum, Teamnaam, Tijd
If ($SortCP == "Datum-Team-Tijd") $outputsortUi = "Datum, Uit, Tijd ASC"; // Sorteer op Datum, Teamnaam, Tijd


$MinDagCP="0";
$PlusDagCP="0";
$MinTijd =  date( 'H:i', strtotime( '-2 hours' ) );

if ($CombCpAfg == "Uit")
// Alleen wedstrijden uit clubprogramma en oefenprogramma worden getoond
{

	$query1 = "SELECT DISTINCT Tijd, Thuis, Uit, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Datum, '' AS Vertrekverzameltijd, '' AS KlkThuis, '' AS KlkUit, '' AS Veld
FROM `".$dbprefix."clubprogramma` WHERE Thuis Like '$club1' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP'
and Tijd > '$MinTijd'
UNION
SELECT DISTINCT Tijd, Thuis, Uit, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Datum, Vertrekverzameltijd, KlkThuis, KlkUit, Veld
FROM `".$dbprefix."oefenprogramma` WHERE Thuis Like '$club1' AND Uitslag Like 'ng' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP'
and Tijd > '$MinTijd'
ORDER BY $outputsortTh";




	$result1=mysql_query($query1);
	$num1=mysql_numrows($result1);


	$query2 = "SELECT DISTINCT  Tijd,  Thuis,  Uit,  Type,  Accommodatie,  Wedstrijdnr,  Scheidsrechter, Status, Datum, '' AS Vertrekverzameltijd,'' AS KlkThuis, '' AS KlkUit, '' AS Veld
FROM `".$dbprefix."clubprogramma` WHERE Thuis NOT Like '$club1' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP'
UNION
SELECT DISTINCT Tijd, Thuis,  Uit,  Type,  Accommodatie,  Wedstrijdnr,  Scheidsrechter, Status, Datum, Vertrekverzameltijd, KlkThuis, KlkUit, Veld
FROM `".$dbprefix."oefenprogramma` WHERE Thuis NOT LIKE '$club1' AND Uitslag Like 'ng'
ORDER BY $outputsortUi";

	$result2=mysql_query($query2);
	$num2=mysql_numrows($result2);

}

else

// naast dat wedstrijden uit clubprogramma en oefenprogramma worden getoond worden ook de afgelastingen getoond
{
	$query1 = "SELECT DISTINCT Tijd, Thuis, Uit, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Datum, '' AS Vertrekverzameltijd, '' AS KlkThuis, '' AS KlkUit, '' AS Veld
FROM `".$dbprefix."clubprogramma` WHERE Thuis Like '$club1' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP'
and Tijd > '$MinTijd'
UNION
SELECT DISTINCT Tijd, Thuis, Uit, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Datum, Vertrekverzameltijd, KlkThuis, KlkUit, Veld
FROM `".$dbprefix."oefenprogramma` WHERE Thuis Like '$club1' AND Uitslag Like 'ng' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP'
and Tijd > '$MinTijd'
UNION
SELECT DISTINCT Tijd, Thuis, Uit, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Datum, '' AS Vertrekverzameltijd, '' AS KlkThuis, '' AS KlkUit, '' AS Veld
FROM `".$dbprefix."afgelasting` WHERE Thuis Like '$club1' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP'
and Tijd > '$MinTijd'
ORDER BY $outputsortTh";

	$result1=mysql_query($query1);
	$num1=mysql_numrows($result1);


	$query2 = "SELECT DISTINCT  Tijd,  Thuis,  Uit,  Type,  Accommodatie,  Wedstrijdnr,  Scheidsrechter, Status, Datum, '' AS Vertrekverzameltijd, '' AS KlkThuis, '' AS KlkUit, '' AS Veld
FROM `".$dbprefix."clubprogramma` WHERE Thuis NOT Like '$club1' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP'
UNION
SELECT DISTINCT Tijd, Thuis,  Uit,  Type,  Accommodatie,  Wedstrijdnr,  Scheidsrechter, Status, Datum, Vertrekverzameltijd, KlkThuis, KlkUit, Veld
FROM `".$dbprefix."oefenprogramma` WHERE Thuis NOT LIKE '$club1' AND Uitslag Like 'ng'
UNION
SELECT DISTINCT Tijd, Thuis, Uit, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Datum, '' AS Vertrekverzameltijd, '' AS KlkThuis, '' AS KlkUit, '' AS Veld
FROM `".$dbprefix."afgelasting` WHERE Thuis NOT Like '$club1'
ORDER BY $outputsortUi";


	$result2=mysql_query($query2);
	$num2=mysql_numrows($result2);
}


$query3 = "SELECT * FROM clubnaam WHERE ClubID='1'";
$result3=mysql_query($query3);

$query4 = "SHOW TABLE STATUS from ".$database." LIKE '".$dbprefix."clubprogramma'";
$result4=mysql_query($query4);

// In dit gedeelte halen we extra informatie op zoals zelf bepaalde scheidsrechter en Vertrek of verzameltijd
$query6 = "SELECT DISTINCT EWedstrijdnr, EScheidsrechter, VertrekVerzameltijd, KlkThuis, KlkUit, Veld, EAfgelast FROM ".$dbprefix."extraprogramma";
$result6=mysql_query($query6) or die(mysql_error());
$num6=mysql_numrows($result6);


mysql_close();


?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="refresh" content="<?php echo $refreshs ?>" />

<title>Programma <?php echo $clubnaam; ?></title>

<?php echo "<link rel=\"stylesheet\" type=\"text/css\" href='$CSSweergave' />"; ?>


</head>

<body>

<script type="text/javascript">
/* <![CDATA[ */

var timer = {
	minutes :0,
	seconds : 0,
	elm :null,
	samay : null,
	sep : ':',
	init : function(m,s,elm)
	{
		m = parseInt(m,10);
		s = parseInt(s,10);
		if(m<0 || s<0 || isNaN(m) || isNaN(s)) { alert('Invalid Values'); return; }
		this.minutes = m;
		this.seconds = s;
		this.elm = document.getElementById(elm);
		timer.start();
	},
	start : function()
	{
		this.samay = setInterval((this.doCountDown),1000);
	},
	doCountDown : function()
	{
		if(timer.seconds == 0)
		{
			if(timer.minutes == 0)
			{
				clearInterval(timer.samay);
				timerComplete();
				return;
			}
			else
			{
				timer.seconds=60;
				timer.minutes--;
			}
		}
		timer.seconds--;
		timer.updateTimer(timer.minutes,timer.seconds);
	},
	updateTimer :  function(min,secs)
	{
		min = (min < 10 ? '0'+min : min);
		secs = (secs < 10 ? '0'+secs : secs);
		(this.elm).innerHTML = min+(this.sep)+secs;
	}
}
function timerComplete()
{
	alert('time out!!!');
}
/* ]]> */



</script>

<script type="text/javascript">
window.onload = init;
function init()
{
	timer.init(<?php echo $refreshm; ?>,00,'container');
}
</script>


<div style="text-align:center"><br/>
<h2>Wedstrijden <?php echo $clubnaam." voor ".date('d - m - Y'); ?></h2> </div>
<div style="text-align:center">
<table class="clubprogramma">
<tr>
<th class="left" style="width:50px">Datum</th>
<th class="left" style="width:50px">Tijd</th>
<th class="left" style="width:170px">Thuis</th>
<?php if ($Klkamertonen == "Aan") { ?> <th class="left" style="width:75px">Kl.kamer</th> <?php } ?>
<th class="left" style="width:170px">Uit</th>
<?php if ($Klkamertonen == "Aan") { ?> <th class="left" style="width:75px">Kl.kamer</th> <?php } ?>
<?php if ($Veldtonen == "Aan") { ?> <th class="left" style="width:50px">Veld</th> <?php } ?>
<th class="left" style="width:50px">Type</th>
<th class="left" style="width:50px">Wed<br />Nr</th>
<?php if ($ClubprogrammaScheids == "Aan") { ?> <th class="left" style="width:150px">Scheidsrechter</th> <?php } ?>
</tr>

<?php
$rowclass = 0;
$i=0;

while ($i < $num1) {
	$KlkThuis="";
	$KlkUit="";
	$Veld="";
	$datum=makeNiceDate(mysql_result($result1,$i,"Datum"));
	$tijd=mysql_result($result1,$i,"Tijd");
	$thuis=mysql_result($result1,$i,"Thuis");
	$uit=mysql_result($result1,$i,"Uit");
	$type=mysql_result($result1,$i,"Type");
	$accommodatie=mysql_result($result1,$i,"Accommodatie");
	$wedstrijdnr=mysql_result($result1,$i,"Wedstrijdnr");
	$scheidsrechter=mysql_result($result1,$i,"Scheidsrechter");
	$status=mysql_result($result1,$i,"Status");
	$Oefvertrekverzameltijd=mysql_result($result1,$i,"Vertrekverzameltijd");
	$OefKlkThuis=mysql_result($result1,$i,"KlkThuis");
	$OefKlkUit=mysql_result($result1,$i,"KlkUit");
	$OefVeld=mysql_result($result1,$i,"Veld");


	$i2=0;
	$escheidsrechter="";
	$vertrekverzameltijd="";
	$KlkThuis="";
	$KlkUit="";
	$Veld="";

	while ($i2 < $num6) {
		$ewedstrijdnr=mysql_result($result6,$i2,"EWedstrijdnr");
		$escheidsrechter1=mysql_result($result6,$i2,"EScheidsrechter");
		$vertrekverzameltijd1=mysql_result($result6,$i2,"VertrekVerzameltijd");
		$eKlkThuis=mysql_result($result6,$i2,"KlkThuis");
		$eKlkUit=mysql_result($result6,$i2,"KlkUit");
		$eVeld=mysql_result($result6,$i2,"Veld");
		$EAfgelast=mysql_result($result6,$i2,"EAfgelast");

		if ($ewedstrijdnr == $wedstrijdnr)
		{

			$KlkThuis=$eKlkThuis;
			$KlkUit=$eKlkUit;
			$Veld=$eVeld;


			$escheidsrechter=$escheidsrechter1;
			if ($escheidsrechter !== "")
			{
				$scheidsrechter = $escheidsrechter;
			}

			$vertrekverzameltijd=$vertrekverzameltijd1;
			if ($status !== "afgelast") $status=$EAfgelast;
		}
		$i2++;
	}


	?>

	<tr>
	<td class="row<?php echo $rowclass ?>"><?php echo $datum; ?></td>
	<td class="row<?php echo $rowclass ?>"><?php echo $tijd; ?></td>
	<td class="row<?php echo $rowclass ?>"><?php echo $thuis; ?></td>
	<?php if ($Klkamertonen == "Aan") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $KlkThuis; echo $OefKlkThuis; ?></td> <?php } ?>
	<td class="row<?php echo $rowclass ?>"><?php echo $uit; ?></td>
	<?php if ($Klkamertonen == "Aan") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $KlkUit; echo $OefKlkUit; ?></td> <?php } ?>
	<?php if ($Veldtonen == "Aan") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $Veld; echo $OefVeld; ?></td> <?php } ?>
	<td class="row<?php echo $rowclass ?>"><?php echo $type; ?></td>
	<?php if ($status == "afgelast") { ?> <td class="row<?php echo $rowclass ?>"><?php echo "<div class=\"afgelastred\">".$status."</div>"; ?></td> <?php } ?>
	<?php if ($status !== "afgelast") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $wedstrijdnr; ?></td> <?php } ?>
	<?php if ($ClubprogrammaScheids == "Aan") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $scheidsrechter; ?></td> <?php } ?>
	</tr>

	<?php
	$i++;
	$rowclass = 1 - $rowclass;

}
IF ($num1==0) { ?>
	<tr>
	<td class="center" colspan="10"><br /><b><?php echo 'Er is geen actueel programma bekend'; ?></b><br /></td>
	</tr>
	<?php }

echo "</table>";
?>

<table class="clubprogramma">
<tr>
<td class="small"><br />Pagina wordt ververst in: <p id='container'></p>
</td>
</tr>


</table>

</div>
</body>
</html>