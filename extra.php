<?php session_start();
// extra.php
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include("config.php");
include("functies.php");
//mysql_connect($server,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
global $dbprefix;
$tbl_name=$dbprefix."extraprogramma"; // Table name
//    $efilter = "";
//    $efilterfield = "";
//    $wholeonly = "";


if (isset($_GET["orderext"])) $orderext = @$_GET["orderext"];
if (isset($_GET["typeext"])) $ordtypeext = @$_GET["typeext"];

if (isset($_POST["efilter"])) $efilter = @$_POST["efilter"];
if (isset($_POST["efilter_field"])) $efilterfield = @$_POST["efilter_field"];
$wholeonly = false;
if (isset($_POST["wholeonly"])) $wholeonly = @$_POST["wholeonly"];

if (!isset($orderext) && isset($_SESSION["orderext"])) $orderext = $_SESSION["orderext"];
if (!isset($ordtypeext) && isset($_SESSION["typeext"])) $ordtypeext = $_SESSION["typeext"];
if (!isset($efilter) && isset($_SESSION["efilter"])) $efilter = $_SESSION["efilter"];
if (!isset($efilterfield) && isset($_SESSION["efilter_field"])) $efilterfield = $_SESSION["efilter_field"];
if (isset($_POST['Submit']))
{	//echo "<p>submit pressed";


	foreach($_POST['ClubProgrammaID'] as $id)
	{
		/////


		$sql = "SELECT * FROM ".$dbprefix."extraprogramma WHERE EWedstrijdnr='".$_POST["Wedstrijdnr".$id]."'";
		if(isset($_POST["EAfgelast".$id])){
			$_POST["EAfgelast".$id] = "afgelast";
		}
		else{
			$_POST["EAfgelast".$id] = "";
		}


		if ( !( $result = mysqli_query($con,$sql) ) )
		var_dump($result);
		{
		}
		$search = mysqli_num_rows( $result );
		//echo $search;
		if ( 0 < $search )
		{

			$sql1 = "update `".$dbprefix."extraprogramma` set EDatum='".$_POST["Datum".$id]."', EWedstrijdnr='".$_POST["Wedstrijdnr".$id]."', EScheidsrechter='".$_POST["EScheidsrechter".$id]."', Vertrekverzameltijd='".$_POST["Vertrekverzameltijd".$id]."', KlkThuis='".$_POST["KlkThuis".$id]."', KlkUit='".$_POST["KlkUit".$id]."', Veld='".$_POST["Veld".$id]."', EAfgelast='".$_POST["EAfgelast".$id]."' WHERE EWedstrijdnr='".$_POST["Wedstrijdnr".$id]."'";
			//echo $sql1;
		}
		else
		{
			$sql1 = "insert into `".$dbprefix."extraprogramma` (EDatum, EWedstrijdnr, EScheidsrechter, Vertrekverzameltijd, KlkThuis, KlkUit, Veld, EAfgelast) values ('".$_POST["Datum".$id]."','".$_POST["Wedstrijdnr".$id]."','".$_POST["EScheidsrechter".$id]."','".$_POST["Vertrekverzameltijd".$id]."','".$_POST["KlkThuis".$id]."','".$_POST["KlkUit".$id]."','".$_POST["Veld".$id]."','".$_POST["EAfgelast".$id]."')";




		}

		//$result1=mysqli_query($sql1) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
		//  mysqli_query($sql1, $con) or die(mysqli_error($con));

		///



		//         	$sql1= $sqlaction."EDatum='".$_POST["Datum".$id]."', EWedstrijdnr='".$_POST["Wedstrijdnr".$id]."', EScheidsrechter='".$_POST["EScheidsrechter".$id]."', Vertrekverzameltijd='".$_POST["Vertrekverzameltijd".$id]."' WHERE ExtraProgrammaID='".$id."'";

		$result1=mysqli_query($con,$sql1) or die("A MySQL error has occurred. <br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
		//		//mysqli_query($sql1) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));

	}
}
?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Extra wedstrijd informatie toevoegen</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php echo "<link rel=\"stylesheet\" type=\"text/css\" href='$CSSadmin'>"; ?>

</head>
<body>
<table class="bdadmin sorttable" style="width:100%"><tr><td class="hr"><h2>Extra informatie toevoegen aan clubprogramma</h2></td></tr></table>
<?php
if (!login()) exit;
?>
<div style="float: right"><a href="extra.php?a=logout">[ Uitloggen ]</a></div>
<br>
<?php
$con = connect();
$showrecs = 30;
$pagerange = 10;

$a = @$_GET["a"];
$recid = @$_GET["recid"];
$page = @$_GET["page"];
if (!isset($page)) $page = 1;

if ($a == "opschonen")
{
	global $con;

	$today = date("Y-m-d");
	$sql = "delete from `".$dbprefix."extraprogramma` where Edatum<'".$today."'";
	mysqli_query($con,$sql) or die(mysqli_error($con));

}

$sql = @$_POST["sql"];

switch ($sql) {
case "insert":
	sql_insert();
	break;
case "update":
	sql_update();
	break;
case "delete":
	sql_delete();
	break;
}

switch ($a) {
case "add":
	addrec();
	break;
case "view":
	viewrec($recid);
	break;
case "edit":
	editrec($recid);
	break;
case "del":
	deleterec($recid);
	break;
default:
	select();
	break;
}

if (isset($orderext)) $_SESSION["orderext"] = $orderext;
if (isset($ordtypeext)) $_SESSION["typeext"] = $ordtypeext;
if (isset($efilter)) $_SESSION["efilter"] = $efilter;
if (isset($efilterfield)) $_SESSION["efilter_field"] = $efilterfield;
if (isset($wholeonly)) $_SESSION["wholeonly"] = $wholeonly;

mysqli_close($con);
?>
<table class="bdadmin sorttable" style="width:100%"><tr><td class="hr">Opties - Gemaakt door Yarro/Johnvs</td></tr></table>
<div style="float: left"><a href="dashboard.php">[ DashBoard ]</a></div><br />
<div style="float: left"><a href="instellingen.php">[ Instellingen ]</a></div><br>
<div style="float: left"><a href="admin.php">[ Beheer Teams ]</a></div><br>
<div style="float: left"><a href="groupid.php">[ Group ID accounts ]</a></div><br>
<div style="float: left"><a href="oefenprogramma.php">[ Oefen Programma/Uitslagen ]</a></div><br>
<div style="float: left"><a href="uitslag.php">[ Uitslagen invoeren voor Club / Team ]</a></div><br>
<div style="float: left"><a href="kantine.php">[ Kantinebezetting beheren ]</a></div><br>

</body>
</html>

<?php function select()
{
	global $a;
	global $showrecs;
	global $page;
	global $efilter;
	global $efilterfield;
	global $wholeonly;
	global $orderext;
	global $ordtypeext;
	global $dbprefix;


	if ($a == "reset") {
		$efilter = "";
		$efilterfield = "";
		$wholeonly = "";
		$orderext = "";
		$ordtypeext = "";
	}

	$checkstr = "";
	if ($wholeonly) $checkstr = " checked";
	if ($ordtypeext == "asc") { $ordtypeextstr = "desc"; } else { $ordtypeextstr = "asc"; }
	$res = sql_select();
	$count = sql_getrecordcount();
	if ($count % $showrecs != 0) {
		$pagecount = intval($count / $showrecs) + 1;
	}
	else {
		$pagecount = intval($count / $showrecs);
	}
	$startrec = $showrecs * ($page - 1);
	if ($startrec < $count) {mysqli_data_seek($res, $startrec);}
	$reccount = min($showrecs * $page, $count);
	?>
	<table class="bdadmin sorttable">
	<tr><td>Getoonde wedstrijden <?php echo $startrec + 1 ?> - <?php echo $reccount ?>  van  <?php echo $count ?></td></tr>
	</table>
	<hr />
	<form action="extra.php" method="post">
	<table class="bdadmin  sorttable">
	<tr>
	<td><select name="efilter_field">
	<option value="">Clubprogramma-Extraprogramma</option>
	<option value="<?php echo "Club Thuis Programma-Extraprogramma" ?>"<?php if ($efilterfield == "Club Thuis Programma-Extraprogramma") { echo "selected"; } ?>><?php echo htmlspecialchars("Club Thuis Programma-Extraprogramma") ?></option>
	<option value="<?php echo "Club Uit Programma-Extraprogramma" ?>"<?php if ($efilterfield == "Club Uit Programma-Extraprogramm") { echo "selected"; } ?>><?php echo htmlspecialchars("Club Uit Programma-Extraprogramma") ?></option>
	<option value="<?php echo "Extraprogramma" ?>"<?php if ($efilterfield == "Extraprogramma") { echo "selected"; } ?>><?php echo htmlspecialchars("Extraprogramma") ?></option>
	</select></td>

	<tr><td><input type="submit" name="action" value="Filter toepassen"> <a href="extra.php?a=reset">&nbsp;Reset Filter</a></td>
	</tr>
	</table>
	</form>
	<?php
	if ($efilterfield == "Extraprogramma") { ?>
		<hr />
		<td>&nbsp;</td>
		<td><a href="extra.php?a=opschonen"> [Opschonen extra tabel] </a></td>
		<?php } ?>
	<hr />
	<?php showpagenav($page, $pagecount); ?>
	<br>
	<form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF'];  ?>">
	<table class="bdadmin  sorttable">
	<tr>
	<td class="hr">&nbsp;</td>
	<td class="hr">&nbsp;</td>
	<td class="hr">&nbsp;</td>
	<td class="hr"><a class="hr"><?php echo htmlspecialchars(" ") ?></a></td>
	<td class="hr"><a class="hr"><?php echo htmlspecialchars("Datum") ?></a></td>
	<td class="hr"><a class="hr"><?php echo htmlspecialchars("Tijd") ?></a></td>
	<td class="hr"><a class="hr"><?php echo htmlspecialchars("Thuis") ?></a></td>
	<td class="hr"><a class="hr"><?php echo htmlspecialchars("Uit") ?></a></td>
	<td class="hr"><a class="hr"><?php echo htmlspecialchars("Wedstrijdnr") ?></a></td>
	<td class="hr"><a class="hr"><?php echo htmlspecialchars("Scheidsrechter") ?></a></td>
	<td class="hr"><a class="hr"><?php echo htmlspecialchars("Eigen Scheidsrechter") ?></a></td>
	<td class="hr"><a class="hr"><?php echo htmlspecialchars("Vertrekverzameltijd") ?></a></td>
	<td class="hr"><a class="hr"><?php echo htmlspecialchars("Klk Thuis") ?></a></td>
	<td class="hr"><a class="hr"><?php echo htmlspecialchars("Klk Uit") ?></a></td>
	<td class="hr"><a class="hr"><?php echo htmlspecialchars("Veld") ?></a></td>
	<td class="hr"><a class="hr"><?php echo htmlspecialchars("Zelf Afgelast") ?></a></td>

	</tr>




	<?php

	for ($i = $startrec; $i < $reccount; $i++)
	{
		$row = mysqli_fetch_assoc($res);
		$style = "dr";
		$bstyle = "style=\"background-color: #FFFFFF; border: 1px solid #D0D0D0;\"";
		$cstyle = "style=\"background-color: #FFFFFF; border: none;\"";
		if ($i % 2 != 0) {
			$style = "sr";
			$bstyle = "style=\"background-color: #A6D2FF; border: 1px solid #D0D0D0;\"";
			$cstyle = "style=\"background-color: #A6D2FF; border: none;\"";

		}
		?>
		<?php
		if ($efilterfield == "Extraprogramma")
		{

			?>

			<tr>
			<td class="<?php echo $style ?>"><a href="extra.php?a=view&amp;recid=<?php echo $i ?>">Bekijk</a></td>
			<td class="<?php echo $style ?>"><a href="extra.php?a=edit&amp;recid=<?php echo $i ?>">Wijzig</a></td>
			<td class="<?php echo $style ?>"><a href="extra.php?a=del&amp;recid=<?php echo $i ?>">Wissen</a></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row[" "]) ?></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Edatum"]) ?></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Tijd"]) ?></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Thuis"]) ?></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Uit"]) ?></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["EWedstrijdnr"]) ?></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row[" "]) ?></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["EScheidsrechter"]) ?></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Vertrekverzameltijd"]) ?></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["KlkThuis"]) ?></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["KlkUit"]) ?></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Veld"]) ?></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["EAfgelast"]) ?></td>
			</tr>


			<?php
		}
		else
		{
			?>
			<?php

			?>

			<tr>
			<td class="<?php echo $style ?>"><a href="extra.php?a=view&amp;recid=<?php echo $i ?>">Bekijk</a></td>
			<td class="<?php echo $style ?>"><a href="extra.php?a=edit&amp;recid=<?php echo $i ?>">Wijzig</a></td>
			<td class="<?php echo $style ?>"><a href="extra.php?a=del&amp;recid=<?php echo $i ?>">Wissen</a></td>
			<td class="<?php echo $style ?>"><input type="hidden" name="ClubProgrammaID[]" value="<?php echo $row['ClubProgrammaID']; ?>" /></td>
			<td class="<?php echo $style ?>"><input name="Datum<?php echo $row['ClubProgrammaID']; ?>" type="text" readonly  class="<?php echo $style; ?>" <?php echo $cstyle; ?>  id="Datum<?php echo $row['Wedstrijdnr'] ?>" value="<?php echo $row['Datum']; ?>"></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Tijd"]) ?></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Thuis"]) ?></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Uit"]) ?></td>
			<td class="<?php echo $style ?>"><input name="Wedstrijdnr<?php echo $row['ClubProgrammaID']; ?>" type="text" class="<?php echo $style; ?>" <?php echo $cstyle; ?>  id="Wedstrijdnr<?php echo $row['Wedstrijdnr'] ?>" value="<?php echo $row['Wedstrijdnr']; ?>"></td>
			<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Scheidsrechter"]) ?></td>
			<td class="<?php echo $style ?>"><input name="EScheidsrechter<?php echo $row['ClubProgrammaID']; ?>" type="text" class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="EScheidsrechter<?php echo $row['Wedstrijdnr'] ?>" value="<?php echo $row['EScheidsrechter']; ?>"></td>
			<td class="<?php echo $style ?>"><input name="Vertrekverzameltijd<?php echo $row['ClubProgrammaID']; ?>" type="text" class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="Vertrekverzameltijd<?php echo $row['Wedstrijdnr'] ?>" value="<?php echo $row['Vertrekverzameltijd']; ?>"></td>
			<td class="<?php echo $style ?>"><input name="KlkThuis<?php echo $row['ClubProgrammaID']; ?>" type="text" class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="KlkThuis<?php echo $row['Wedstrijdnr'] ?>" value="<?php echo $row['KlkThuis']; ?>"></td>
			<td class="<?php echo $style ?>"><input name="KlkUit<?php echo $row['ClubProgrammaID']; ?>" type="text" class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="KlkUit<?php echo $row['Wedstrijdnr'] ?>" value="<?php echo $row['KlkUit']; ?>"></td>
			<td class="<?php echo $style ?>"><input name="Veld<?php echo $row['ClubProgrammaID']; ?>" type="text" class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="Veld<?php echo $row['Wedstrijdnr'] ?>" value="<?php echo $row['Veld']; ?>"></td>

			<td class="<?php echo $style ?>">
			<?php if ($row["EAfgelast"] == "afgelast") { ?>
				<input name="EAfgelast<?php echo $row['ClubProgrammaID']; ?>" type="checkbox" CHECKED class="<?php echo $style; ?>" <?php echo $bstyle; ?> id=EAfgelast<?php echo $row['Wedstrijdnr'] ?> value="<?php echo $row['EAfgelast']; ?>"></td>
				<?php } else { ?>
				<input name="EAfgelast<?php echo $row['ClubProgrammaID']; ?>" type="checkbox" class="<?php echo $style; ?>" <?php echo $bstyle; ?> id=EAfgelast<?php echo $row['Wedstrijdnr'] ?>></td>
				<?php } ?>
			</tr>

			<?php
		}
	}
	mysqli_free_result($res);
	?>
	<tr>
	<?php
	if ($efilterfield !== "Extraprogramma")
	{ ?>
		<td class="center" colspan="14"><input type="submit" name="Submit" value="Wijzigingen aanbrengen" /></td>
		<?php } ?>
	</tr>
	</table>
	</form>
	<br>
	<?php showpagenav($page, $pagecount); ?>
	<?php }  ?>

<?php function login()
{
	include("config.php");
	global $_POST;
	global $_SESSION;

	global $_GET;
	if (isset($_GET["a"]) && ($_GET["a"] == 'logout')) $_SESSION["logged_in_extra"] = false;
	if (!isset($_SESSION["logged_in_extra"])) $_SESSION["logged_in_extra"] = false;
	if (!$_SESSION["logged_in_extra"]) {
		$login = "";
		$password2 = "";

		if (isset($_POST["login"])) $login = @$_POST["login"];
		if (isset($_POST["password"])) $password2 = @$_POST["password"];

		if (($login != "") && ($password2 != "")) {
			if (($login == $Extrausername) && ($password2 == $Extrapassword)) {
				$_SESSION["logged_in_extra"] = true;
			}
			else {
				?>
				<p><b><font color="-1">De combinatie gebruikersnaam/wachtwoord is onjuist</font></b></p>
				<?php } } }if (isset($_SESSION["logged_in_extra"]) && (!$_SESSION["logged_in_extra"])) { ?>
		<form action="extra.php" method="post">
		<table class="bdadmin sorttable">
		<tr>
		<td>Gebruikersnaam</td>
		<td><input type="text" name="login" value="<?php echo $login ?>"></td>
		</tr>
		<tr>
		<td>Wachtwoord</td>
		<td><input type="password" name="password" value="<?php echo $password2 ?>"></td>
		</tr>
		<tr>
		<td><input type="submit" name="action" value="Inloggen"></td>
		</tr>
		</table>
		</form>
		<?php
	}
	if (!isset($_SESSION["logged_in_extra"])) $_SESSION["logged_in_extra"] = false;
	return $_SESSION["logged_in_extra"];
} ?>

<?php function showrow($row, $recid)
{
	echo $hetfilter;
	if (isset($efilterfield) && $efilterfield!='') echo "xxxxx";
	?>
	<table class="tbl" border="0" cellspacing="1" cellpadding="3"width="50%">
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Datum")."&nbsp;" ?></td>
	<td class="dr"><?php if ($row["EDatum"] == '') echo htmlspecialchars($row["Datum"]) ?><?php if ($row["EDatum"] != '') echo htmlspecialchars($row["EDatum"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Tijd")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Tijd"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Thuis")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Thuis"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Uit")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Uit"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Wedstrijdnr")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Wedstrijdnr"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Scheidsrechter")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Scheidsrechter"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Eigen Scheidsrechter")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["EScheidsrechter"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Vertrekverzameltijd")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Vertrekverzameltijd"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Kleedkamer Thuis")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["KlkThuis"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Kleedkamer Uit")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["KlkUit"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Veld")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Veld"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Zelf Afgelast")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["EAfgelast"]) ?></td>
	</tr>
	</table>
	<?php } ?>

<?php function showroweditor($row, $iseditmode)
{
	global $con;
	?>
	<table class="tbl" border="0" cellspacing="1" cellpadding="3"width="50%">
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Wedstrijdnr")."&nbsp;" ?></td>
	<td class="dr"><input type="text" name="Wedstrijdnr" value="<?php echo str_replace('"', '&quot;', trim($row["Wedstrijdnr"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Eigen Scheidsrechter")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="EScheidsrechter" maxlength="300" value="<?php echo str_replace('"', '&quot;', trim($row["EScheidsrechter"])) ?>"></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Vertrekverzameltijd")."&nbsp;" ?></td>
	<td class="dr"><input type="text" name="Vertrekverzameltijd" maxlength="5" value="<?php echo str_replace('"', '&quot;', trim($row["Vertrekverzameltijd"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Zelf Afgelast")."&nbsp;" ?></td>
<?php if ($row["EAfgelast"] == "afgelast") { ?>
<td class="dr"><input type="checkbox" CHECKED name="EAfgelast" maxlength="10" value="<?php echo str_replace('"', '&quot;', trim($row["EAfgelast"])) ?>"> Indien aangevinkt is wedstrijd afgelast door Club.</td>
	<?php } else { ?>
	<td class="dr"><input type="checkbox" name="EAfgelast" maxlength="10" value="<?php echo str_replace('"', '&quot;', trim($row["EAfgelast"])) ?>"> Indien aangevinkt is wedstrijd afgelast door Club.</td>
<?php } ?>

</tr>

</table>
<?php } ?>

<?php function showroweditor2($row, $iseditmode)
{
global $con;
?>
<table class="tbl" border="0" cellspacing="1" cellpadding="3"width="50%">
<tr>
<td class="hr"><?php echo htmlspecialchars("Datum")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Datum" value="<?php echo str_replace('"', '&quot;', trim($row["Datum"])) ?>" readonly style="border: none"></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Tijd")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Tijd"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Thuis")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Thuis"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Uit")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Uit"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Wedstrijdnr")."&nbsp;" ?></td>
	<td class="dr"><input type="text" name="Wedstrijdnr" maxlength="300" value="<?php echo str_replace('"', '&quot;', trim($row["Wedstrijdnr"])) ?>" readonly style="border: none"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Scheidsrechter")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Scheidsrechter"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Eigen Scheidsrechter")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="EScheidsrechter" maxlength="300" value="<?php echo str_replace('"', '&quot;', trim($row["EScheidsrechter"])) ?>"></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Vertrekverzameltijd")."&nbsp;" ?></td>
	<td class="dr"><input type="text" name="Vertrekverzameltijd" maxlength="5" value="<?php echo str_replace('"', '&quot;', trim($row["Vertrekverzameltijd"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Kleedkamer Thuis")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="KlkThuis" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["KlkThuis"])) ?>"></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Kleedkamer Uit")."&nbsp;" ?></td>
	<td class="dr"><input type="text" name="KlkUit" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["KlkUit"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Veld")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Veld" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Veld"])) ?>"></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Zelf Afgelast")."&nbsp;" ?></td>
	<?php if ($row["EAfgelast"] == "afgelast") { ?>
		<td class="dr"><input type="checkbox" CHECKED name="EAfgelast" maxlength="10" value="<?php echo str_replace('"', '&quot;', trim($row["EAfgelast"])) ?>"> Indien aangevinkt is wedstrijd afgelast door Club.</td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="EAfgelast" maxlength="10" value="<?php echo str_replace('"', '&quot;', trim($row["EAfgelast"])) ?>"> Indien aangevinkt is wedstrijd afgelast door Club.</td>
		<?php } ?>
	</tr>
	</table>
	<?php } ?>

<?php function showpagenav($page, $pagecount)
{
	?>
	<table class="bdadmin  sorttable">
	<tr>
	<?php if ($page > 1) { ?>
		<td><a href="extra.php?page=<?php echo $page - 1 ?>">&lt;&lt;&nbsp;Vorige</a>&nbsp;</td>
		<?php } ?>
	<?php
	global $pagerange;

	if ($pagecount > 1) {

		if ($pagecount % $pagerange != 0) {
			$rangecount = intval($pagecount / $pagerange) + 1;
		}
		else {
			$rangecount = intval($pagecount / $pagerange);
		}
		for ($i = 1; $i < $rangecount + 1; $i++) {
			$startpage = (($i - 1) * $pagerange) + 1;
			$count = min($i * $pagerange, $pagecount);

			if ((($page >= $startpage) && ($page <= ($i * $pagerange)))) {
				for ($j = $startpage; $j < $count + 1; $j++) {
					if ($j == $page) {
						?>
						<td><b><?php echo $j ?></b></td>
						<?php } else { ?>
						<td><a href="extra.php?page=<?php echo $j ?>"><?php echo $j ?></a></td>
						<?php } } } else { ?>
				<td><a href="extra.php?page=<?php echo $startpage ?>"><?php echo $startpage ."..." .$count ?></a></td>
				<?php } } } ?>
	<?php if ($page < $pagecount) { ?>
		<td>&nbsp;<a href="extra.php?page=<?php echo $page + 1 ?>">Volgende&nbsp;&gt;&gt;</a>&nbsp;</td>
		<?php } ?>
	</tr>
	</table>
	<?php } ?>

<?php function showrecnav($a, $recid, $count)
{
	?>
	<table class="bdadmin  sorttable">
	<tr>
	<td><a href="extra.php">Index Pagina</a></td>
	<?php if ($recid > 0) { ?>
		<td><a href="extra.php?a=<?php echo $a ?>&recid=<?php echo $recid - 1 ?>">Vorige wedstrijd</a></td>
		<?php } if ($recid < $count - 1) { ?>
		<td><a href="extra.php?a=<?php echo $a ?>&recid=<?php echo $recid + 1 ?>">Volgende wedstrijd</a></td>
		<?php } ?>
	</tr>
	</table>
	<hr />
	<?php } ?>

<?php function addrec()
{
	?>
	<table class="bdadmin  sorttable">
	<tr>
	<td><a href="extra.php">Index Pagina</a></td>
	</tr>
	</table>
	<hr />
	<form enctype="multipart/form-data" action="extra.php" method="post">
	<p><input type="hidden" name="sql" value="insert"></p>
	<?php
	$row = array(
	"ExtraProgrammaID" => "",
	"Wedstrijdnr" => "",
	"EScheidsrechter" => "",
	"Vertrekverzameltijd" => "",
	"KlkThuis" => "",
	"KlkUit" => "",
	"Veld" => "",
	"EAfgelast" => "");
	showroweditor($row, false);
	?>
	<p><input type="submit" name="action" value="Toevoegen"></p>
	</form>
	<?php } ?>

<?php function viewrec($recid)
{
	$res = sql_select();
	$count = sql_getrecordcount();
	mysqli_data_seek($res, $recid);
	$row = mysqli_fetch_assoc($res);
	showrecnav("view", $recid, $count);
	?>
	<br>
	<?php showrow($row, $recid) ?>
	<br>
	<hr />
	<table class="bdadmin sorttable">
	<tr>
	<td><a href="extra.php?a=edit&recid=<?php echo $recid ?>">Wedstrijd wijzigen</a></td>
	<td><a href="extra.php?a=del&recid=<?php echo $recid ?>">Wedstrijd wissen</a></td>
	</tr>
	</table>
	<?php
	mysqli_free_result($res);
} ?>

<?php function editrec($recid)
{
	$res = sql_select();
	$count = sql_getrecordcount();
	mysqli_data_seek($res, $recid);
	$row = mysqli_fetch_assoc($res);
	showrecnav("edit", $recid, $count);
	?>
	<br>
	<form enctype="multipart/form-data" action="extra.php" method="post">
	<input type="hidden" name="sql" value="update">
	<input type="hidden" name="xExtraProgrammaID" value="<?php echo $row["ExtraProgrammaID"] ?>">
	<?php showroweditor2($row, true); ?>
	<p><input type="submit" name="action" value="Toevoegen"></p>
	</form>
	<?php
	mysqli_free_result($res);
} ?>

<?php function deleterec($recid)
{
	$res = sql_select();
	$count = sql_getrecordcount();
	mysqli_data_seek($res, $recid);
	$row = mysqli_fetch_assoc($res);
	showrecnav("del", $recid, $count);
	?>
	<br>
	<form action="extra.php" method="post">
	<input type="hidden" name="sql" value="delete">
	<input type="hidden" name="xExtraProgrammaID" value="<?php echo $row["ExtraProgrammaID"] ?>">
	<?php showrow($row, $recid) ?>
	<p><input type="submit" name="action" value="Bevestigen"></p>
	</form>
	<?php
	mysqli_free_result($res);
} ?>

<?php function connect()
{
	include("config.php");
	//$con = mysql_connect($server,$username,$password);
	//mysql_select_db($database);
	return $con;
}

function sqlvalue($val, $quote)
{
	if ($quote)
	$tmp = sqlstr($val);
	else
	$tmp = $val;
	if ($tmp == "")
	$tmp = "NULL";
	elseif ($quote)
	$tmp = "'".$tmp."'";
	return $tmp;
}

function sqlstr($val)
{
	return str_replace("'", "''", $val);
}

function sql_select()
{
	global $con;
	global $orderext;
	global $ordtypeext;
	global $efilter;
	global $efilterfield;
	global $wholeonly;
	global $dbprefix;
	global $club1;
	$efilterstr = sqlstr($efilter);


	if (!$wholeonly && isset($wholeonly) && $efilterstr!='') $efilterstr = "%" .$efilterstr ."%";
	// -----
	if ($efilterfield=='')
	{
		$sql = "SELECT *  from ".$dbprefix."clubprogramma left outer join ".$dbprefix."extraprogramma on ".$dbprefix."clubprogramma.Wedstrijdnr = ".$dbprefix."extraprogramma.EWedstrijdnr ORDER BY Datum, Tijd ASC";



	}
	if (isset($efilterfield) && $efilterfield=='Club Thuis Programma-Extraprogramma')
	{
		$sql = "SELECT *  from ".$dbprefix."clubprogramma  left outer join ".$dbprefix."extraprogramma on ".$dbprefix."clubprogramma.Wedstrijdnr = ".$dbprefix."extraprogramma.EWedstrijdnr WHERE Thuis Like '$club1' ORDER BY Datum, Tijd ASC";
	}
	if (isset($efilterfield) && $efilterfield=='Club Uit Programma-Extraprogramma')
	{
		$sql = "SELECT *  from ".$dbprefix."clubprogramma left outer join ".$dbprefix."extraprogramma on ".$dbprefix."clubprogramma.Wedstrijdnr = ".$dbprefix."extraprogramma.EWedstrijdnr WHERE Uit Like '$club1' ORDER BY Datum, Tijd ASC";
	}
	// -----
	//   $sql = "SELECT *  from ".$dbprefix."clubprogramma left outer join ".$dbprefix."extraprogramma on ".$dbprefix."clubprogramma.Wedstrijdnr = ".$dbprefix."extraprogramma.EWedstrijdnr ORDER BY Datum, Tijd ASC";
	if (isset($efilterfield) && $efilterfield=='Extraprogramma') {

		$sql = "SELECT *  from ".$dbprefix."extraprogramma ORDER BY Edatum ASC";
	} elseif (isset($efilterstr) && $efilterstr!='') {
		$sql .= " where (`Wedstrijdnr` like '" .$efilterstr ."') or (`Scheidsrechter` like '" .$efilterstr ."') or `Vertrekverzameltijd` like '" .$efilterstr ."')";
	}
	if (isset($orderext) && $orderext!='') $sql .= " order by `" .sqlstr($orderext) ."`";
	if (isset($ordtypeext) && $ordtypeext!='') $sql .= " " .sqlstr($ordtypeext);


	$res = mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
	// mysqli_query($sql,$con) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));;
	// or die(mysqli_error($con));

	return $res;
}

function sql_getrecordcount()
{
	global $con;
	global $orderext;
	global $ordtypeext;
	global $efilter;
	global $efilterfield;
	global $wholeonly;
	global $dbprefix;
	global $club1;

	$efilterstr = sqlstr($efilter);
	if (!$wholeonly && isset($wholeonly) && $efilterstr!='') $efilterstr = "%" .$efilterstr ."%";

	// ------

	if ($efilterfield=='')
	{
		$sql = "SELECT COUNT(*) FROM `".$dbprefix."clubprogramma`";
	}
	if (isset($efilterfield) && $efilterfield=='Club Thuis Programma-Extraprogramma')
	{
		$sql = "SELECT COUNT(*) FROM `".$dbprefix."clubprogramma` WHERE Thuis like '$club1'";
	}
	if (isset($efilterfield) && $efilterfield=='Club Uit Programma-Extraprogramma')
	{
		$sql = "SELECT COUNT(*) FROM `".$dbprefix."clubprogramma` WHERE Uit like '$club1'";
	}

	if (isset($efilterfield) && $efilterfield=='Extraprogramma') {
		$sql = "SELECT COUNT(*) from ".$dbprefix."extraprogramma";
	} elseif (isset($efilterstr) && $efilterstr!='') {
		$sql .= " where (`Wedstrijdnr` like '" .$efilterstr ."') or (`Scheidsrechter` like '" .$efilterstr ."') or `Vertrekverzameltijd` like '" .$efilterstr ."')";
	}

	$res = mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));;
	$row = mysqli_fetch_assoc($res);
	reset($row);
	return current($row);
}

function sql_insert()
{
	if(isset($_POST["EAfgelast"])){
		$_POST["EAfgelast"] = "afgelast";
	}
	else{
		$_POST["EAfgelast"] = "";
	}

	global $con;
	global $_POST;
	global $dbprefix;

	$sql = "insert into `".$dbprefix."extraprogramma` (`EWedstrijdnr`, `EScheidsrechter`, `Vertrekverzameltijd`, `KlkThuis`, `KlkUit`, `Veld`, `EAfgelast`) values (" .sqlvalue(@$_POST["Wedstrijdnr"], true).", " .sqlvalue(@$_POST["EScheidsrechter"], true).", " .sqlvalue(@$_POST["Vertrekverzameltijd"], true).", " .sqlvalue(@$_POST["KlkThuis"], true).", " .sqlvalue(@$_POST["KlkUit"], true).", " .sqlvalue(@$_POST["Veld"], true).", " .sqlvalue(@$_POST["EAfgelast"], true).")";
	mysqli_query($sql, $con) or die(mysqli_error($con));
}

function sql_update()
{
	if(isset($_POST["EAfgelast"])){
		$_POST["EAfgelast"] = "afgelast";
	}
	else{
		$_POST["EAfgelast"] = "";
	}
	global $con;
	global $_POST;
	global $dbprefix;

	$sql = "SELECT * FROM ".$dbprefix."extraprogramma WHERE EWedstrijdnr='$_POST[Wedstrijdnr]'";
	if ( !( $result = mysqli_query($con,$sql ) ) )
	{
	}
	$search = mysqli_num_rows( $result );
	if ( 0 < $search )
	{
		$sql = "update `".$dbprefix."extraprogramma` set `EDatum`=" .sqlvalue(@$_POST["Datum"], true).", `EWedstrijdnr`=" .sqlvalue(@$_POST["Wedstrijdnr"], true).", `EScheidsrechter`=" .sqlvalue(@$_POST["EScheidsrechter"], true).", `Vertrekverzameltijd`=" .sqlvalue(@$_POST["Vertrekverzameltijd"], true) .", `KlkThuis`=" .sqlvalue(@$_POST["KlkThuis"], true) .", `KlkUit`=" .sqlvalue(@$_POST["KlkUit"], true) .", `Veld`=" .sqlvalue(@$_POST["Veld"], true) .", `EAfgelast`=" .sqlvalue(@$_POST["EAfgelast"], true) ." where " .primarykeycondition();
	}
	else
	{
		$sql = "insert into `".$dbprefix."extraprogramma` (`EDatum`,`EWedstrijdnr`, `EScheidsrechter`, `Vertrekverzameltijd`, `KlkThuis`, `KlkUit`, `Veld`, `EAfgelast`) values (" .sqlvalue(@$_POST["Datum"], true).", " .sqlvalue(@$_POST["Wedstrijdnr"], true).", " .sqlvalue(@$_POST["EScheidsrechter"], true).", " .sqlvalue(@$_POST["Vertrekverzameltijd"], true).", " .sqlvalue(@$_POST["KlkThuis"], true).", " .sqlvalue(@$_POST["KlkUit"], true).", " .sqlvalue(@$_POST["Veld"], true).", " .sqlvalue(@$_POST["EAfgelast"], true).")";
	}
	mysqli_query($con,$sql) or die(mysqli_error($con));
}

function sql_delete()
{
	global $con;
	global $dbprefix;

	$sql = "delete from `".$dbprefix."extraprogramma` where " .primarykeycondition();
	mysqli_query($con,$sql) or die(mysqli_error($con));
}
function primarykeycondition()
{
	global $_POST;
	$pk = "";
	$pk .= "(`ExtraProgrammaID`";
	if (@$_POST["xExtraProgrammaID"] == "") {
		$pk .= " IS NULL";
	}else{
		$pk .= " = " .sqlvalue(@$_POST["xExtraProgrammaID"], false);
	};
	$pk .= ")";
	return $pk;
}
?>
