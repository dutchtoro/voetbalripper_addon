<?php
// functies.php 1.9.7.8 (27-08-2013) Aanpassing ivm wijziging inloggen voetbal.nl. cURL waarbij open_dir en/of savedir aan staat
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel

// Als Curl niet aan staat wordt de socket class gebruikt
If ($Curl == "Uit") {
	include("class.socket.php");
	$usesocket = 'true';
}
//Debug print function
function dp($input)
{
	if(is_array($input) || is_object($input))
	{
		echo 'Begin array: '.'<br />';
		echo '<pre>';
		print_r($input);
		echo '<br />'.'</pre>';
		echo '<br />';
		echo 'End array<br />';
	}else{
		echo 'Begin string: '.'<br />';
		echo '<pre>';
		echo '|||'.htmlentities($input).'|||';
		echo '<br />'.'</pre>'.'END string'.'<br />';
	}
}
function removeDuplicateMatches($matches)
{
	$processed_matches = array();
	$result = array();
	foreach($matches as $match)
	{
		if(!in_array($match["Thuis"].'_'.$match["Uit"],$processed_matches))
		{
			$result[] = $match;
			$processed_matches[] = $match["Thuis"].'_'.$match["Uit"];
		}
	}
	return $result;
}
function convertStringToDate($string)
{
	$string = str_ireplace('okt','oct',$string);
	$string = str_ireplace('maa','mar',$string);
	$string = str_ireplace('mrt','mar',$string);
	$string = str_ireplace('mei','may',$string);

	$datum  = date('Y-m-d',strtotime($string));
	$jul = date('Y-m-d',strtotime('01 jul'));

	//Is het vandaag voor of na 1 juli
	if (datediff(Date("Y-m-d"),$jul) < 0)
	{
		// Vandaag is na 1 juli: Ligt de gevraagde datum ervoor, dan 1 jaar erbij.
		If (dateDiff($jul,$datum) < 0)
		{
			//Tel er een jaar bij op
			$datum = date('Y-m-d',strtotime('+1 year',strtotime($datum)));
		}
	} else {
		// Vandaag is voor 1 juli: Ligt de gevraagde datum erna, dan 1 jaar eraf.
		If (dateDiff($jul,$datum) >= 0)
		{
			//Tel er een jaar bij op
			$datum = date('Y-m-d',strtotime('-1 year',strtotime($datum)));
		}
	}
	return $datum;
}



//Algemene functie om de resultaten van een url op te halen en deze als resultaat terug te geven.

function fetch_url($url)
{
	global $ch, $site, $cookies, $usesocket, $useproxy, $proxyhost, $proxyport;

	If ($usesocket == 'true') {
		//Haal de url op middels de socket class van Lucas
		$get = new HttpGetRequest();
		if ($useproxy == 'true') {
			$get -> set_ip($site, $proxyport);
		} else {
			$get -> set_ip($site, 80);
		}
		$get -> set_file($url);

		foreach($cookies as $name => $value) {
			$get -> set_cookie($name, $value);
		}
		if ($get -> request()) {
			$html = $get -> get_contents();
		}
	} else {
		//Haal de url op middels CURL

		curl_setopt($ch,CURLOPT_HTTPHEADER,array('Expect:'));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_COOKIE,  $cookies);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		if ($useproxy == 'true') {
			curl_setopt($ch, CURLOPT_PROXY, $proxyhost);
			curl_setopt($ch, CURLOPT_PROXYPORT, $proxyport);
		}
		$html = curl_exec($ch);
	}
	return $html;
}
// -------------------------------------


//Inloggen -----------------------------
function login_voetbalnl($Gebruikersnaam, $Wachtwoord)
{
	global $ch, $cookies, $usesocket, $site, $Phpsafemode, $useproxy, $proxyhost, $proxyport;

	If ($usesocket == 'true') {
		//Inloggen op voetbal.nl middels de socket class van Lucas
		$url  = "http://www.voetbal.nl/inloggen"; //"http://pupillen.voetbal.nl/home?destination=node%2F23942";
		if ($useproxy == 'true') {
			$site  = $proxyhost;
			$port  = $proxyport;
		} else {
			$site = "www.voetbal.nl";
			$port = 80;
		}
		$post = new HttpPostRequest();
		$post -> set_ip($site, $port);
		$post -> set_file($url);
		$post -> include_headers();
		$post -> use_redirect(false);
		$post -> add_var("email", $Gebruikersnaam); //add_var("name", $Gebruikersnaam);    // Je email
		$post -> add_var("password", $Wachtwoord);//add_var("pass", $Wachtwoord);        // je wachtwoord
		$post -> add_var("op", "Inloggen");
		$post -> add_var("form_id", "voetbal_login_login_form"); //"login_block_v3_login_form");
		$conected = $post -> request();
		$cookies = $post -> get_cookies();
		$result = $conected;
			} 
		else 
		{
		$site = 'http://www.voetbal.nl';
		$url = $site."/clubs_comp/mijn-teams/competitie/"; // simple scherm om in te loggen	 
		$ch = curl_init();

		$httpagent = $_SERVER["HTTP_USER_AGENT"]; 
		curl_setopt($ch, CURLOPT_USERAGENT, $httpagent);		
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('Expect:'));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 1);
	//	if ($Phpsafemode == "Uit") curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);

		if ($useproxy == 'true') {
			curl_setopt($ch, CURLOPT_PROXY, $proxyhost);
			curl_setopt($ch, CURLOPT_PROXYPORT, $proxyport);
		}


		$logindata = file_get_contents("http://voetbal.nl/login/menu/data"); //hierin staat de form_build_id die bij iedere sessie anders is
	        $formbuilddata = explode('name=\"form_build_id\" id=\"',$logindata,2);

		
		// name is je gebruikersnaam
		// password is je wachtwoord
		$postdata=array(
			"name" => $Gebruikersnaam,    
			"pass" => $Wachtwoord,   
			"op" => "Inloggen",
                        "form_build_id" => substr($formbuilddata[1],0,37),
			"form_id" => "login_block_v3_login_form"
		);

		curl_setopt ($ch, CURLOPT_POSTFIELDS, $postdata);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//error_reporting(E_ALL);
		if ($Phpsafemode == "Uit") {
			$result = curl_exec($ch);
			}
		else
		{
			$result = curl_exec_follow($ch);
		}
		

		preg_match_all('|Set-Cookie: (.*);|U', $result, $results);
		$cookies = implode(';', $results[1]);
	}
	//echo "cookies zijn: $cookies <br />";
	//echo "result is: $result <br />";


	return $result;
}
// -------------------------------------

// cURL follow location functie wanneer safe_mode aan staat en/of open_savedir gezet is ----------------------

function curl_exec_follow(/*resource*/ $ch, /*int*/ &$maxredirect = null)
{ 
    $mr = $maxredirect === null ? 5 : intval($maxredirect); 
 

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false); 
        if ($mr > 0) { 
            $newurl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL); 
            $rch = curl_copy_handle($ch);

            curl_setopt($rch, CURLOPT_HEADER, true); 
            curl_setopt($rch, CURLOPT_NOBODY, true); 
            curl_setopt($rch, CURLOPT_FORBID_REUSE, false); 
            curl_setopt($rch, CURLOPT_RETURNTRANSFER, true); 
            do { 
                curl_setopt($rch, CURLOPT_URL, $newurl); 
                $header = curl_exec($rch); 
                if (curl_errno($rch)) { 
                    $code = 0; 
                } else { 
                    $code = curl_getinfo($rch, CURLINFO_HTTP_CODE); 
                    if ($code == 301 || $code == 302) { 
                        preg_match('/Location:(.*?)\n/', $header, $matches); 
                        $newurl = trim(array_pop($matches)); 
                    } else { 
                        $code = 0; 
                    } 
                } 
            } while ($code && --$mr); 
            //curl_close($rch); 
            if (!$mr) { 
                if ($maxredirect === null) { 
                    trigger_error('Too many redirects. When following redirects, libcurl hit the maximum amount.', E_USER_WARNING);
                 } else { 
                    $maxredirect = 0; 
                } 
                return false; 
            } 
            curl_setopt($ch, CURLOPT_URL, $newurl); 
        } 
    return curl_exec($ch); 
} 

// -------------------------------------


//Inlog check ----------------------
function check_inlog($url)
{
	$html = fetch_url($url);

	//Eerste kijken of er wel informatie is als er alleen een . staat is het account geblokkeerd. De "lege regel punt" opde volgende regel is belangrijk.
	$zoekstring = "\n.";
	if(strstr($html,$zoekstring)) return $zoekstring;

	// Kijken of er niet staat - Log in of registreer gratis! - anders is inloggen mislukt
	$zoekstring = "Log in of registreer gratis!";
	if(strstr($html,$zoekstring)) return $zoekstring;

	//$regexp omschrijft nu hoe de pagina eruit moet zien
	$regexp ='/<div><h1>(.*?)<.h1><.div>\s+/';

	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Inlog'] = $value[1];
		$i++;
	}
	if(isset($value))
	{
		//Waarde terug geven
		return $value[1];
	}else{
		return NULL;
	}
}
// -------------------------------------


//Database leegmaken -------------------
function leegmakendbcp($table)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die( "Unable to select database to clear table");

	mysqli_query($con,"TRUNCATE TABLE $table")
	or die(mysqli_error($con));
}
// -------------------------------------


//Database leegmaken -------------------
function leegmakendb($table,$teamid)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die( "Unable to select database to clear table for teamid");

	mysqli_query($con,"DELETE FROM $table WHERE TeamID=$teamid")
	or die(mysqli_error($con));
	mysqli_query($con,"ALTER TABLE $table AUTO_INCREMENT =1")
	or die(mysqli_error($con));
}
// -------------------------------------

//Haal de teamgegevens op en sla deze op.
function get_teamdata($baseurl)
{
	$html = fetch_url($baseurl);
	return $html;
}
// -------------------------------------

// Stand ophalen -----------------------
function get_stand($html, $what)
{
	//Omdat de normale en de periode standen achter elkaar staan in $html kopieren we alleen het juiste deel in $htmlstripped

	//Stand periode 1 ophalen
	if ($what == "p1")
	{
		$htmlstripped = get_string_between($html,"id=\"RankingsContent2","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
	}

	//Stand periode 2 ophalen
	if ($what == "p2")
	{
		$htmlstripped = get_string_between($html,"id=\"RankingsContent3","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
		if(empty($htmlstripped))
		{
			// $htmlstripped = get_string_between($html,"id=\"RankingsContent3","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
		}
	}

	//Stand periode 3 ophalen, als deze er is. Anders nogmaals periode 2 ophalen, omdat er maar 2 zijn.
	if ($what == "p3")
	{
		$htmlstripped = get_string_between($html,"id=\"RankingsContent4","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
		if(empty($htmlstripped))
		{
			// $htmlstripped = get_string_between($html,"id=\"RankingsContent3","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
		}
	}

	//Normale stand ophalen

	if ($what == "normaal")
	{
		//Controleer of het team uberhaupt periode standen kent.
		if (strpos($html,"id=\"RankingsContent2") <> 0)
		{
			//Team kent periode standen
			$htmlstripped = get_string_between($html,"id=\"RankingsContent1","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
		} else {
			//Team kent geen periode standen
			$htmlstripped = get_string_between($html,"id=\"RankingsContent1","");
		}
	}

	$regexp = '/<div class="nr">(.*?)<.div>\s+<div class="team.*?">(.*?)<.div>\s+<div class="g"><strong>(.*?)<.strong><.div>\s+<div class="w">(.*?)<.div>\s+<div class="gl">(.*?)<.div>\s+<div class="v">(.*?)<.div>\s+<div class="p"><strong>(.*?)<.strong><.div>\s+<div class="dvp">(.*?)<.div>\s+<div class="dpt">(.*?)<.div>\s+<div class="pm">(.*?)<.div>\s+<.div>\s+/';

	//Waardes ophalen en opslaan in $matches
	preg_match_all($regexp, $htmlstripped, $matches, PREG_SET_ORDER);

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		if(!empty($value[1]) && !empty($value[2]))
		{
			$match[$i]['#'] = $value[1];
			$match[$i]['Elftal'] = $value[2];
			$match[$i]['G'] = $value[3];
			$match[$i]['W'] = $value[4];
			$match[$i]['GW'] = $value[5];
			$match[$i]['V'] = $value[6];
			$match[$i]['P'] = $value[7];
			$match[$i]['DPV'] = $value[8];
			$match[$i]['DPT'] = $value[9];
			$match[$i]['PM'] = $value[10];
			$i++;
		}
	}
	//Elke match teruggeven
	return $match;
}
// -------------------------------------


//Uitslag ophalen ----------------------
function get_uitslag($html)
{
	//Tegenwoordig bevat de pagina meerdere tabellen met uitslagen. Neem de eerste. Aangenomen dat dit de laatste uitslagen zijn
	$htmlstripped = get_string_between($html,"id=\"showResults","id=\"showRankings");


	// eerste kijken of er wel informatie is
	$zoekstring = "Er zijn geen uitslagen";

	// Indien er geen gegevens zijn gevonden stoppen
	if(strstr($htmlstripped,$zoekstring)) return $zoekstring;


	//Er zijn wel gegevens gevonden dus gaan we door
	//$regexp omschrijft nu hoe de tabel eruit moet zien
	//Houd ook rekening met "higlight" voor als het een "mijn elftal" elftal is.
	$regexp ='/<div.class="datum-tijd">(.*?)<.div>\s+<div.class="wedstrijd"><div.class=".*?">(.*?)<.div><div.*?>-<.div><div.class=".*?">(.*?)<.div><.div>\s+<div.class="uitslag">(.*?)<.div>/';
	preg_match_all($regexp, $htmlstripped, $matches, PREG_SET_ORDER);

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Datum']   = $value[1];
		$match[$i]["Thuis"]   = trim($value[2]);
		$match[$i]["Uit"]     = trim($value[3]);
		//    $match[$i]['Uitslag'] = $value[4]." - ".$value[5];
		$match[$i]['Uitslag'] = $value[4];
		$i++;
	}
	//Elke match teruggeven
	return $match;
}
// -------------------------------------


//Programma ophalen -------------------- Programma met details is de functie hieronder
function get_programma($html, $teamcode)
{

	include("config.php");

	//Tegenwoordig bevat de pagina meerdere tabellen met programma's. Neem de laatste
	//  $zoekterm =  "id=\"FixturesContent";
	$zoekterm =  "id=\"FixturesContent".$teamcode."-3";
	$htmlstripped = get_string_between($html,$zoekterm,"");

	//Eerste kijken of er wel informatie is
	$zoekstring = "Er zijn geen programmagegevens";

	//Indien er geen gegevens zijn gevonden stoppen
	if(strstr($htmlstripped,$zoekstring)) return $zoekstring;

	if ($Clubnaamcheck == 'Aan')
	// Check op clubnaam of deze wel of niet voorkomt. Hogere betrouwbaarheid bij wegschrijven gegevens
	{
		//Nu kijken of er wel informatie in zit door te checken op de clubnaam
		$zoekstring = $clubverkort;
		$emptystring = "leegprogramma";

		//Indien clubnaam niet is gevonden stoppen

		$pos = strpos($html, $zoekstring);

		if ($pos === false) {
			if ($UserDebug == 'Aan') echo "Team Programma: Clubnaam  '$zoekstring' is niet gevonden in team programma.<br />";
			return $emptystring;
		} else {
			if ($UserDebug == 'Aan') echo "team programma: Clubnaam '$zoekstring' is gevonden in team programma.<br />";
		}
	}
	else
	{
		// Geen controle op clubnaam. Hierdoor kunnen de gegevens van meerdere club teams worden opgehaals maar mogelijk problemen met wegschrijven van gegevens
	}


	// Er zijn wel gegevens gevonden dus gaan we door
	//$regexp omschrijft nu hoe de tabel eruit moet zien
	$regexp = '/<div.class="datum-tijd">(.*?),.(.*?)<.div>\s+<div.class="status(\s(afgelast))*">\s+<div.class="wedstrijd"><div.class=".*?">(.*?)<.div><div..*?>-<.div><div.class=".*?">(.*?)<.div><.div>\s+<div.class="type">(.*?)<.div>\s+<div.class="accomodatie">(.*?)<.div>\s+<.div>/';
	preg_match_all($regexp, $htmlstripped, $matches, PREG_SET_ORDER);

	//dp($matches);
	//die;

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Datum']          = $value[1];
		$match[$i]['Tijd']           = $value[2];
		$match[$i]['Status']         = $value[4];
		$match[$i]['Wedstrijd']      = $value[5]." - ".$value[6];
		$match[$i]['Type']           = $value[7];
		//Bij een afgelasting staat er een plaatje in plaats van de accommodatie.
		if(stristr($value[8], 'AFGELAST') === FALSE)
		{
			$match[$i]['Accommodatie'] = $value[8];
		}else{
			$match[$i]['Accommodatie'] = '';
		}
		//$match[$i]['Wedstrijdnr']    = $value[8];
		//$match[$i]['Scheidsrechter'] = $value[9];

		$i++;
	}
	//Elke match teruggeven
	return $match;
}
// Einde functie programma ophalen zonder details -------------------------------------


//Programma met details ophalen ---- Programma zonder details hierboven
function get_programmadetailed($html, $teamcode)
{

	include("config.php");

	//Tegenwoordig bevat de pagina meerdere tabellen met programma's. Neem de laatste
	//$zoekterm =  "id=\"FixturesContent";
	$zoekterm =  "id=\"FixturesContent".$teamcode."-3";
	$htmlstripped = get_string_between($html,$zoekterm,"");

	//Eerste kijken of er wel informatie is
	$zoekstring = "Er zijn geen programmagegevens";

	//Indien er geen gegevens zijn gevonden stoppen
	if(strstr($htmlstripped,$zoekstring)) return $zoekstring;

	if ($Clubnaamcheck == 'Aan')
	// Check op clubnaam of deze wel of niet voorkomt. Hogere betrouwbaarheid bij wegschrijven gegevens
	{
		//Nu kijken of er wel informatie in zit door te checken op de clubnaam
		$zoekstring = $clubverkort;
		$emptystring = "leegprogramma";

		//Indien clubnaam niet is gevonden stoppen

		$pos = strpos($html, $zoekstring);

		if ($pos === false) {
			if ($UserDebug == 'Aan') echo "Team Programma: Clubnaam  '$zoekstring' is niet gevonden in team programma.<br />";
			return $emptystring;
		} else {
			if ($UserDebug == 'Aan') echo "team programma: Clubnaam '$zoekstring' is gevonden in team programma.<br />";
		}
	}
	else
	{
		// Geen controle op clubnaam. Hierdoor kunnen de gegevens van meerdere club teams worden opgehaals maar mogelijk problemen met wegschrijven van gegevens
	}


	// Er zijn wel gegevens gevonden dus gaan we door
	//$regexp omschrijft nu hoe de tabel eruit moet zien
	$regexp = '/<div.class="datum-tijd">(.*?),.(.*?)<.div>\s+<div.class="status(\s(afgelast))*">\s+<div.class="wedstrijd"><div.class=".*?">(.*?)<.div><div..*?>-<.div><div.class=".*?">(.*?)<.div><.div>\s+<div.class="type">(.*?)<.div>\s+<div.class="accomodatie">(.*?)<.div>\s+<.div>\s+.*?\s+<div.class="details"><a.class="fancy-box".href="(.*?)\?iframe/';
	preg_match_all($regexp, $htmlstripped, $matches, PREG_SET_ORDER);

	//dp($matches);
	//die;

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Datum']          = $value[1];
		$match[$i]['Tijd']           = $value[2];
		$match[$i]['Status']         = $value[4];
		$match[$i]['Wedstrijd']      = $value[5]." - ".$value[6];
		$match[$i]['Type']           = $value[7];
		//Bij een afgelasting staat er een plaatje in plaats van de accommodatie.
		if(stristr($value[8], 'AFGELAST') === FALSE)
		{
			$match[$i]['Accommodatie'] = $value[8];
		}else{
			$match[$i]['Accommodatie'] = '';
		}

		// Alle wedstrijd details staan nu in een aparte link. Deze wordt nu opgevraagd en alle details opgeslagen
		$htmlwdetails = fetch_url("http://pupillen.voetbal.nl".$value[9]);

		$regexp = '/<div.class="naw.*><label>Wedstrijdnummer:<.label>.(.*?)<.div>\s+(<div.class="naw.*><label>Periode:<.label>.(.*?)<.div>\s+)?<div.class="naw.*><label>Scheidsrechter:<.label>.(.*?)<.div>\s+<div.class="naw.*><label>Sportpark:<.label>.(.*?)<.div>\s+<div.class="naw.*><label>Adres:<.label>.(.*?)<.div>\s+<div.class="naw.*><label>Postcode:<.label>.(.*?)<.div>\s+<div.class="naw.*><label>Plaats:<.label>.(.*?)<.div>\s+<div.class="naw.*><label>Telefoon:<.label>.(.*?)<.div>\s+/';
		preg_match_all($regexp, $htmlwdetails, $dmatches, PREG_SET_ORDER);

		$dmatch =array();
		$i1=0;
		foreach($dmatches AS $dvalue)
		{
			$match[$i]['Wedstrijdnr']    = $dvalue[1];
			$match[$i]['Periodestring']  = $dvalue[2];
			$match[$i]['Periode']        = $dvalue[3];
			$match[$i]['Scheidsrechter'] = $dvalue[4];
			$match[$i]['Sportpark']	 = $dvalue[5];
			$match[$i]['Adres'] 	 = $dvalue[6];
			$match[$i]['Postcode'] 	 = $dvalue[7];
			$match[$i]['Plaats'] 	 = $dvalue[8];
			$match[$i]['Telefoon'] 	 = $dvalue[9];
			$i1++;
		}

		$i++;
	}

	//Elke match teruggeven
	return $match;
}
// Einde functie programma ophalen met details -------------------------------------


//Alle Uitslagen ophalen ---------------
function get_alle($html)
{

	//Tegenwoordig bevat de pagina meerdere tabellen met uitslagen. Neem de alles na de eerste
	//$htmlstripped = get_string_between($html,"id=\"ResultsContent3","");
	$htmlstripped = get_string_between($html,'<div name="ResultsContent" id="ResultsContent2" style="display: none;">',"");


	// Er zijn wel gegevens gevonden dus gaan we door //$regexp omschrijft nu hoe de tabel eruit moet zien
	$regexp = '/<div.class="datum-tijd">(.*?)<.div>\s+<div.class="wedstrijd"><div.class=".*?">(.*?)<.div><div.*?>-<.div><div.class=".*?">(.*?)<.div><.div>\s+<div.class="uitslag">(.*?)-(.*?)<.div>/';
	preg_match_all($regexp, $htmlstripped, $matches, PREG_SET_ORDER);

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Datum']   = $value[1];
		$match[$i]["Thuis"]   = trim($value[2]);
		$match[$i]["Uit"]     = trim($value[3]);
		$match[$i]['Uitslag'] = $value[4]." - ".$value[5];
		$i++;
	}
	//Elke match teruggeven
	$match = removeDuplicateMatches($match);
	return $match;
}
// -------------------------------------


//Alle Programma's ophalen voor Club ---
function get_clubprog($cpurl)
{

	include("config.php");

	global $debug,$debug_programma_html;

	$html = fetch_url($cpurl);

	// eerste kijken of er wel informatie is
 	$htmlexplode = explode('<div class="fixtures">',$html);
	$thuischeck = $htmlexplode[2];
	$uitcheck = $htmlexplode[3];
	$zoekstring = 'Er zijn geen programmagegevens';

	// Indien er geen gegevens zijn gevonden stoppen
	if((strstr($thuischeck,$zoekstring)) and (strstr($uitcheck,$zoekstring))) return $zoekstring;


	//Nu kijken of er wel informatie in zit door te checken op de clubnaam
	$zoekstring = $clubverkort;
	$emptystring = "leegprogramma";

	//Indien clubnaam niet is gevonden stoppen

	$pos = strpos($html, $zoekstring);

	if ($pos === false) {
		if ($UserDebug == 'Aan') echo "Clubnaam  '$zoekstring' is niet gevonden in Club programma.<br />";
		return array($emptystring,$emptystring);
	} else {
		if ($UserDebug == 'Aan') echo "Clubnaam '$zoekstring' is gevonden in Club programma.<br />";
	}



	//Er zijn wel gegevens gevonden dus gaan we door
	//$regexp omschrijft nu hoe de tabel eruit moet zien
	$regexp = '/<div.class="datum-tijd" style=".*?">(.*?),.(.*?)<.div>\s+<div style=".*?">.*?<.div>\s+<div.class="status(\s(afgelast))*">\s+<div.class="wedstrijd".style=".*?">\s+<div class="".style=".*?">(.*?)<.div>\s+<div style=".*?">.+?<.div>\s+<div class="".*?style=".*?">(.*?)<.div>\s+<.div>\s+<div class="type">(.*?)<.div>\s+<div class="wedstrijdnr".*?style=".*?">(.*?)<.div>\s+<.div>\s+<div class="details">.*?<.div>\s+<div.class="scheidsrechter".*?style=".*?">(.*?)<.div>\s+/';
	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);

	if(count($matches) < 1)
	{
		if($html != '')
		{
			echo 'Fout bij het ophalen van het programma, search string niet gevonden.';
		}else{
			echo 'Fout bij het ophalen van het programma, lijkt fout te gaan met het ophalen van de html:<br /><br />'.$html;
		}
	}
	$match = array();
	$afgmatch = array();
	$i = 0;
	$j = 0;
	foreach($matches AS $value)
	{
		if($value[4] == 'afgelast')
		{
			$afgmatch[$j]['Datum']  = $value[1];
			$afgmatch[$j]['Tijd']   = $value[2];
			$afgmatch[$j]['Status'] = $value[4];
			$afgmatch[$j]["Thuis"]  = trim($value[5]);
			$afgmatch[$j]["Uit"]    = trim($value[6]);
			$afgmatch[$j]['Type']   = $value[7];
			//Accomodatie bestaat niet meer dus vul hem maar met de thuis spelende club
			$afgmatch[$j]['Accommodatie'] = trim($value[5]);
			if(stristr($value[8], 'AFGELAST') === FALSE)
			{
				$afgmatch[$j]['Wedstrijdnr'] = $value[8];
			}else{
				$afgmatch[$j]['Wedstrijdnr'] = '';
			}

			$afgmatch[$j]['Scheidsrechter'] = $value[9];
			$j++;
		}else{
			$match[$i]['Datum'] = $value[1];
			$match[$i]['Tijd']  = $value[2];
			$match[$i]["Thuis"] = trim($value[5]);
			$match[$i]["Uit"]   = trim($value[6]);
			$match[$i]['Type']  = $value[7];
			//Accomodatie bestaat niet meer dus vul hem maar met de thuis spelende club
			$match[$i]['Accommodatie'] = trim($value[5]);
			$match[$i]['Wedstrijdnr']  = $value[8];
			$match[$i]['Scheidsrechter'] = $value[9];
			$i++;
		}
	}
	//Elke match teruggeven
	return array($match,$afgmatch);
	var_dump ($match);
}
// -------------------------------------


//Afgelastingen ophalen voor Club --- Deze functie wordt niet meer gebruikt.... Afgelasting zitten in functie hierboven
function get_clubaf($caurl)
{

	include("config.php");

	$html = fetch_url($caurl);

	// eerste kijken of er wel informatie is
	$zoekstring = "Er zijn geen programmagegevens";
	$zoekstring2 = "Er zijn geen programmagegevens";

	// Indien er geen gegevens zijn gevonden stoppen
	if((strstr($html,$zoekstring)) and (strstr($html,$zoekstring2))) return $zoekstring;

	//Nu kijken of er wel informatie in zit door te checken op de clubnaam
	$zoekstring = $clubverkort;
	$emptystring = "";

	//Indien clubnaam niet is gevonden stoppen
	if(strstr($html,$zoekstring)) return $emptystring;


	// Er zijn wel gegevens gevonden dus gaan we door
	// $regexp omschrijft nu hoe de tabel eruit moet zien
	$regexp = '#<div.class="calendar-month">(.*?)<.div>\s+
		<div.class="calendar-day">(.*?)<.div>\s+
		<.div>\s+
		<.td>\s+
		<td.class="time".style="padding:0px;.margin:0px;font-weight:normal;">(.*?)<.td>\s+
		<td.class="match".style="padding:0px;.margin:0px;font-weight:normal;">(.*?)&nbsp;&nbsp;-&nbsp;&nbsp;(.*?)<.td>\s+
		<td.class="where".style="padding:0px;.margin:0px;font-weight:normal;">(.*?)<.td>\s+
		<td.class="where".style="padding:0px;.margin:0px;font-weight:normal;">(.*?)<.td>\s+
		<td.class="where".style="padding:0px;.margin:0px;font-weight:normal;">(.*?)<.td>\s+
		<td.class="where".style="padding:0px;.margin:0px;font-weight:normal;">(.*?)<.td>\s+
		<td.class="where".style="padding:0px;.margin:0px;font-weight:normal;">(.*?)<.td>\s+#simx';

	//Waardes ophalen en opslaan in $matches

	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);
	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Datum']          = $value[2]." ".$value[1];
		$match[$i]['Tijd']           = $value[3];
		$match[$i]["Thuis"]          = trim($value[4]);
		$match[$i]["Uit"]            = trim($value[5]);
		$match[$i]['Type']           = $value[6];
		$match[$i]['Accommodatie']   = $value[7];
		$match[$i]['Wedstrijdnr']    = $value[8];
		$match[$i]['Scheidsrechter'] = $value[9];
		$match[$i]['Status']         = $value[10];
		$i++;
	}
	//Elke match teruggeven
	return $match;
}
// -------------------------------------


//Alle teams ophalen van clubpagina ---------------
function get_teams($teamsurl)
{

	$html = fetch_url($teamsurl);


	//Tegenwoordig bevat de pagina meerdere tabellen met uitslagen. Neem de alles na de eerste
	//$htmlstripped = get_string_between($html,"id=\"ResultsContent3","");
	//  $htmlstripped = get_string_between($html,'<div name="ResultsContent" id="ResultsContent2" style="display: none;">',"");

	// Er zijn wel gegevens gevonden dus gaan we door //$regexp omschrijft nu hoe de tabel eruit moet zien
	$regexp ='/clubs-competities.mijn.poules.uitslagen.(.*?)".title="Selecteer poule">(.*?).nbsp.(.*?)<.a>/';


	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Naam']		= $value[2];
		$match[$i]['Klasse']   	= $value[3];
		$match[$i]['Teamcode']	= $value[1];
		$match[$i]['Wedstrijdduur'] = "0";
		$match[$i]['Periode'] = "nee";
		$match[$i]['Ophalen'] = "ja";
		$match[$i]['GroupID'] = "1";
		$match[$i]['DatumTijd-Update'] = "1230768001";
		$i++;
	}
	//Elke match teruggeven
	//  $match = removeDuplicateMatches($match);
	return $match;
}
// -------------------------------------


//Teamindeling Ophalen ---------------
function get_teamindeling($html)
{


	//Nu kijken of er wel informatie in zit door te checken op de clubnaam
	$zoekstring = $clubverkort;
	$emptystring = "";

	//Indien clubnaam niet is gevonden stoppen
	if(strstr($html,$zoekstring)) return $emptystring;

	// Er zijn wel gegevens gevonden dus gaan we door //$regexp omschrijft nu hoe de tabel eruit moet zien

	$regexp ='/<div.*class="indeling-row.*">\s+<div.class="nr">(.*?)<.div>\s+<div class="elftal">(.*?)<.div>\s+<div class="voorkeurstijd">(.*?)<.div>\s+/';

	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Teamnr']		= $value[1];
		$match[$i]['Naam']   	= $value[2];
		$match[$i]['Vktijd']	= $value[3];
		//echo $value[1];
		//echo $value[2];
		//echo $value[3];
		$i++;
	}
	//Elke match teruggeven
	//  $match = removeDuplicateMatches($match);
	return $match;
}
// -------------------------------------


//Speelronde Ophalen ---------------
function get_speelronde($html)
{

	//Tegenwoordig bevat de pagina meerdere tabellen met uitslagen. Neem de eerste. Aangenomen dat dit de laatste uitslagen zijn

	$htmlstripped = get_string_between($html,"<div class=\"speelschema-topheader\">speelschema</div>","");

	$html=$htmlstripped;


	//Nu kijken of er wel informatie in zit door te checken op de clubnaam
	$zoekstring = $clubverkort;
	$emptystring = "";

	//Indien clubnaam niet is gevonden stoppen
	if(strstr($html,$zoekstring)) return $emptystring;

	// Er zijn wel gegevens gevonden dus gaan we door //$regexp omschrijft nu hoe de tabel eruit moet zien

	$regexp ='/<div.*class="nr">(.*?)<.div>\s+/';

	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Speelronde']	= "00";
		$match[$i]['Wedstrijd']	= $value[1];
		$i++;
	}
	//Elke match teruggeven
	//  $match = removeDuplicateMatches($match);
	return $match;
}
// -------------------------------------


//Standen opslaan in database ----------
function opslaan_stand($standen,$link,$tabel)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die( "Unable to select database to store Standen");
	$query = "
	INSERT INTO $tabel
	(
		TeamID,
		Elftal,
		Plaats,
		G,
		W,
		GW,
		V,
		P,
		DPV,
		DPT,
		PM
	)
	VALUES
";
	$t = 0;
	$CountData = count($standen);
	foreach($standen AS $value)
	{
		$value['Elftal'] = htmlentities($value['Elftal'], ENT_QUOTES);

		if ($value['PM'] == '0'){
			$PM = '-';
		}else{
			$PM = $value['PM'];
		}

		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$link["TeamID"]."', '".$value['Elftal']."', '".$value['#']."', '".$value['G']."', '".$value['W']."', '".$value['GW']."', '".$value['V']."', '".$value['P']."', '".$value['DPV']."', '".$value['DPT']."', '".$PM."'),";
		}
		else
		{
			$query.="('".$link["TeamID"]."', '".$value['Elftal']."', '".$value['#']."', '".$value['G']."', '".$value['W']."', '".$value['GW']."', '".$value['V']."', '".$value['P']."', '".$value['DPV']."', '".$value['DPT']."', '".$PM."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$query) or die(mysqli_error($con));
	}


	// Verwijderen van lege entries
	$sql = "DELETE FROM ".$tabel." WHERE Elftal = ''";
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$sql) or die(mysqli_error($con));
	}
}
// -------------------------------------

//Alle Uitslagen ophalen voor Club ---
function get_clubuitslag($cuurl)
{

	include("config.php");

	$html = fetch_url($cuurl);

	// eerste kijken of er wel informatie is
	$zoekstring = "Er zijn geen thuis uitslagen";
	$zoekstring2 = "Er zijn geen uit uitslagen";

	// Indien er geen gegevens zijn gevonden stoppen
	if((strstr($html,$zoekstring)) and (strstr($html,$zoekstring2))) return $zoekstring;

	// Er zijn wel gegevens gevonden dus gaan we door //$regexp omschrijft nu hoe de tabel eruit moet zien
	$regexp ='/<div.class="datum-tijd".style="margin-left:15px;">(.*?)<.div>\s+<div.class="wedstrijd"><div.class=".*?">(.*?)<.div><div.*?>-<.div><div.class=".*?">(.*?)<.div><.div>\s+<div.class="uitslag">(.*?)<.div>/';
	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);


	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Datum']   = $value[1];
		$match[$i]["Thuis"]   = trim($value[2]);
		$match[$i]["Uit"]     = trim($value[3]);
		$match[$i]['Uitslag'] = $value[4];
		$i++;
	}
	//Elke match teruggeven
	$match = removeDuplicateMatches($match);
	return $match;
}
// -------------------------------------

//ophalen van afgelast/lege/dubbele team uitslag
function ontdubbeluitslagen($afgelastteamuitslag,$teamid,$table)
{

	include("config.php");
	
	$query1 = "SELECT DISTINCT TeamID, Datum, Thuis, Uit, Uitslag
FROM $table WHERE TeamID=$teamid AND Uitslag LIKE '%afgelast%' ";

	$auitslag=mysqli_query($con,$query1);
	$auitslagnum=mysqli_num_rows($auitslag);

	return array ($auitslag,$auitslagnum);
}
// -------------------------------------

//Opslaan afgelast/lege/dubbele team uitslag

function opslaan_afuitslagen($auitslag,$auitslagnum,$table)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die( "Unable to select database to store Uitslagen");

	$i=0;
	while ($row = mysqli_fetch_assoc($auitslagnum)) {

		$TeamID = $row["TeamID"];
		$Datum = $row["Datum"];
		$Thuis = $row["Thuis"];
		$Uit = $row["Uit"];
		$Uitslag = $row["Uitslag"];



		$sql = "SELECT * FROM ".$table." WHERE teamID='$TeamID' AND datum='$Datum' AND thuis='$Thuis' AND uit='$Uit' AND TRIM(uitslag) = \"-\"";
		$result=mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
		$num2=mysqli_num_rows($result);

		if($num2 >'0')
		{
			// De uitslag is nog steeds een '-' dus we verwijderen die entry en plaatsen de afgelaste uitslag weer in de tabel.

			$sql = "DELETE FROM ".$table." WHERE teamID='$TeamID' AND datum='$Datum' AND thuis='$Thuis' AND uit='$Uit' AND TRIM(uitslag) = \"-\"";
			mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));

			$sql= "INSERT INTO ".$table." (TeamID, Datum, Thuis, Uit, Uitslag) VALUES ('TeamID', '$Datum', '$Thuis', '$Uit', '$Uitslag')";
			mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));

		}
		else
		{
			// De uitslag lijkt dubbel in de tabel te zitten
		}

		$i++;
	}

}
// -------------------------------------



//Ophalen Handmatig ingevoerde team uitslagen --------
function teamhanduitslagen($saveteamuitslag,$teamid,$table)
{

	include("config.php");
	
	$query1 = "SELECT DISTINCT TeamID, Datum, Thuis, Uit, Uitslag
FROM $table WHERE TeamID=$teamid AND Uitslag LIKE '%*' ";

	$huitslag=mysqli_query($con,$query1);
	$huitslagnum=mysqli_num_rows($huitslag);

	return array ($huitslag,$huitslagnum);
}
// -------------------------------------



//Opslaan Handmatig ingevoerde team uitslagen --------
function opslaan_thuitslagen($huitslag,$huitslagnum,$table)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die( "Unable to select database to store Uitslagen");

	$i=0;
while ($row = mysqli_fetch_assoc($huitslag)) {

		$TeamID = $row["TeamID"];
		$Datum = $row["Datum"];
		$Thuis = $row["Thuis"];
		$Uit = $row["Uit"];
		$Uitslag = $row["Uitslag"];

		$sql = "SELECT * FROM ".$table." WHERE teamID='$TeamID' AND datum='$Datum' AND thuis='$Thuis' AND uit='$Uit' AND TRIM(uitslag) = \"-\"";

		$result=mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
		$num2=mysqli_num_rows($result);



		if($num2 >'0')
		{
			// De uitslag is nog steeds een '-' dus we verwijderen die entry en plaatsen de handmatige uitslag weer in de tabel.

			$sql = "DELETE FROM ".$table." WHERE teamID='$TeamID' AND datum='$Datum' AND thuis='$Thuis' AND uit='$Uit' AND TRIM(uitslag) = \"-\"";
			mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));

			//$sql= 'INSERT INTO `'.$table.'` (TeamID, Datum, Thuis, Uit, Uitslag) VALUES ("$TeamID", "$Datum", "$Thuis", "$Uit", "$Uitslag")';
			$sql= "INSERT INTO ".$table." (TeamID, Datum, Thuis, Uit, Uitslag) VALUES ('TeamID', '$Datum', '$Thuis', '$Uit', '$Uitslag')";
			mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));}
		else
		{
			// De uitslag lijkt te zijn verwerkt dus we plaatsen de handmatige uitslag NIET in de tabel.
		}

		$i++;
	}

}
// -------------------------------------



//Ophalen Handmatig ingevoerde club uitslagen --------
function clubhanduitslagen($saveteamuitslag,$teamid,$table)
{

	include("config.php");
	
	$query1 = "SELECT DISTINCT Datum, Thuis, Uit, Uitslag
FROM $table WHERE Uitslag LIKE '%*' ";

	$huitslag=mysqli_query($con,$query1);
	$huitslagnum=mysqli_num_rows($huitslag);

	return array ($huitslag,$huitslagnum);
}
// -------------------------------------

//Opslaan Handmatig ingevoerde club uitslagen --------
function opslaan_chuitslagen($huitslag,$huitslagnum,$table)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die( "Unable to select database to store Uitslagen");

	$i=0;
while ($row = mysqli_fetch_assoc($huitslag)) {

		$Datum = $row["Datum"];
		$Thuis = $row["Thuis"];
		$Uit = $row["Uit"];
		$Uitslag = $row["Uitslag"];
		// Ook  deze aangepast
		$sql = 'SELECT * FROM `'.$table.'` WHERE Datum= "$Datum" AND Thuis="$Thuis" AND Uit="$Uit" AND TRIM(Uitslag) = "-"';
		$result=mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
		$num2=mysqli_num_rows($result);

		if($num2 >'0')
		{
			// De uitslag is nog steeds een '-' dus we verwijderen die entry en plaatsen de handmatige uitslag weer in de tabel.

			$sql = 'DELETE FROM `'.$table.'` WHERE Datum="$Datum" AND Thuis="$Thuis" AND Uit="$Uit" AND TRIM(Uitslag) = "-"';
			mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));

			$sql= 'INSERT INTO `'.$table.'` (Datum, Thuis, Uit, Uitslag) VALUES ("$Datum", "$Thuis", "$Uit", "$Uitslag")';
			mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));}
		else
		{
			// De uitslag lijkt te zijn verwerkt dus we plaatsen de handmatige uitslag NIET in de tabel.
		}

		$i++;
	}


}
// -------------------------------------


//Uitslagen opslaan in database --------
function opslaan_uitslagen($uitslagen,$link)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die( "Unable to select database to store Uitslagen");


	$query = "
	INSERT INTO ".$dbprefix."uitslag
	(
	TeamID,
	Datum,
	Thuis,
	Uit,
	Uitslag
	)
	VALUES
";
	$t = 0;
	$CountData = count($uitslagen);
	foreach($uitslagen AS $value)
	{
		$value["Thuis"] = htmlentities($value["Thuis"], ENT_QUOTES);
		$value["Uit"] = htmlentities($value["Uit"], ENT_QUOTES);
		$value['Uitslag'] = str_replace('&nbsp;&nbsp;', ' ', $value['Uitslag']);
		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$link["TeamID"]."', '".convertStringToDate($value["Datum"])."', '".$value["Thuis"]."', '".$value["Uit"]."', '".$value['Uitslag']."'),";
		}
		else
		{
			$query.="('".$link["TeamID"]."', '".convertStringToDate($value["Datum"])."', '".$value["Thuis"]."', '".$value["Uit"]."', '".$value['Uitslag']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$query) or die(mysqli_error($con));
	}

	// Verwijderen van lege entries
	$sql = "DELETE FROM `".$dbprefix."uitslag` WHERE Datum = '0000-00-00'";
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$sql) or die(mysqli_error($con));
	}
}
// -------------------------------------


//Programma zonder details opslaan in database -------- Programma opslaan met details zie functie hieronder
function opslaan_programma($programma,$link)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die( "Unable to select database to store programma");

	$query = "
	INSERT INTO ".$dbprefix."programma
	(
	TeamID,
	Datum,
	Tijd,
	Wedstrijd,
	Type,
	Accommodatie,
	Wedstrijdnr,
	Scheidsrechter,
		Status
	)
	VALUES
";
	$t = 0;
	$CountData = count($programma);
	foreach($programma AS $value)
	{
		if(!isset($value['Wedstrijd']))
		{
			$wedstrijd = '';
		}else{
			$value['Wedstrijd'] = htmlentities($value['Wedstrijd'], ENT_QUOTES);
		}
		$value['Accommodatie'] = htmlentities($value['Accommodatie'],ENT_QUOTES);
		if(!isset($value['Wedstrijdnr']))
		{
			$value['Wedstrijdnr'] = '';
		}
		if(!isset($value['Scheidsrechter']))
		{
			$value['Scheidsrechter'] = '';
		}else{
			$value['Scheidsrechter'] = htmlentities($value['Scheidsrechter'],ENT_QUOTES);
		}

		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$link["TeamID"]."', '".convertStringToDate($value['Datum'])."', '".$value['Tijd']."', '".$value['Wedstrijd']."', '".$value['Type']."', '".$value['Accommodatie']."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."', '".$value['Status']."'),";
		}
		else
		{
			$query.="('".$link["TeamID"]."', '".convertStringToDate($value['Datum'])."', '".$value['Tijd']."', '".$value['Wedstrijd']."', '".$value['Type']."', '".$value['Accommodatie']."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."', '".$value['Status']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$query) or die(mysqli_error($con));
	}

	// Verwijderen van lege entries
	$sql = "DELETE FROM `".$dbprefix."programma` WHERE Datum = '0000-00-00'";
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$sql) or die(mysqli_error($con));
	}

	// Wijzigen van bepaalde tekens in Accomodatie. Dit wordt veroorzaakt in de html code van voetbal.nl
	// Indien nodig de // van de volgende 3 regels verwijderen en de codes veranderen. de eerste is wat vervangen moet worden door de tweede.
	// $sql = "UPDATE ".$dbprefix."programma SET Accommodatie = REPLACE(Accommodatie, '&Atilde;&cent;','&acirc;')";
	//  mysqli_query($sql)
	//  or die(mysqli_error());



}
// Einde functie programma zonder details opslaan -------------------------------------


//Programma met details opslaan in database -------- Programma opslaan zonder details zie functie hierboven
function opslaan_programmadetailed($programma,$link)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die( "Unable to select database to Store ");

	$query = "
	INSERT INTO ".$dbprefix."programma
	(
	TeamID,
	Datum,
	Tijd,
	Wedstrijd,
	Type,
	Accommodatie,
	Wedstrijdnr,
	Scheidsrechter,
	Status,
	Sportpark,
	Adres,
	Postcode,
	Plaats,
	Telefoon
	)
	VALUES
";
	$t = 0;
	$CountData = count($programma);
	foreach($programma AS $value)
	{
		if(!isset($value['Wedstrijd']))
		{
			$wedstrijd = 'Leeg';
		}else{
			$value['Wedstrijd'] = htmlentities($value['Wedstrijd'], ENT_QUOTES);
		}
		$value['Accommodatie'] = htmlentities($value['Accommodatie'],ENT_QUOTES);
		$value['Sportpark'] = htmlentities($value['Sportpark'],ENT_QUOTES);
		$value['Adres'] = htmlentities($value['Adres'],ENT_QUOTES);
		$value['Plaats'] = htmlentities($value['Plaats'],ENT_QUOTES);
		if(!isset($value['Wedstrijdnr']))
		{
			$value['Wedstrijdnr'] = '';
		}
		if(!isset($value['Scheidsrechter']))
		{
			$value['Scheidsrechter'] = '';
		}else{
			$value['Scheidsrechter'] = htmlentities($value['Scheidsrechter'],ENT_QUOTES);
		}

		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$link["TeamID"]."', '".convertStringToDate($value['Datum'])."', '".$value['Tijd']."', '".$value['Wedstrijd']."', '".$value['Type']."', '".$value['Accommodatie']."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."', '".$value['Status']."', '".$value['Sportpark']."', '".$value['Adres']."', '".$value['Postcode']."', '".$value['Plaats']."', '".$value['Telefoon']."'),";
		}
		else
		{
			$query.="('".$link["TeamID"]."', '".convertStringToDate($value['Datum'])."', '".$value['Tijd']."', '".$value['Wedstrijd']."', '".$value['Type']."', '".$value['Accommodatie']."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."', '".$value['Status']."', '".$value['Sportpark']."', '".$value['Adres']."', '".$value['Postcode']."', '".$value['Plaats']."', '".$value['Telefoon']."')";
			//        $query.="('".$link["TeamID"]."', '".convertStringToDate($value['Datum'])."', '".$value['Tijd']."', '".$value['Wedstrijd']."', '".$value['Type']."', '".$value['Accommodatie']."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."', '".$value['Status']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$query) or die(mysqli_error($con));
	}

	// Verwijderen van lege entries
	$sql = "DELETE FROM `".$dbprefix."programma` WHERE Datum = '0000-00-00'";
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$sql) or die(mysqli_error($con));
	}
	// Wijzigen van bepaalde tekens in Accomodatie. Dit wordt veroorzaakt in de html code van voetbal.nl
	// Indien nodig de // van de volgende 3 regels verwijderen en de codes veranderen. de eerste is wat vervangen moet worden door de tweede.
	// $sql = "UPDATE ".$dbprefix."programma SET Accommodatie = REPLACE(Accommodatie, '&Atilde;&cent;','&acirc;')";
	//  mysqli_query($sql)
	//  or die(mysqli_error());



}
// Einde functie programma met details opslaan -------------------------------------



//Alle uitslagen in database -----------
function opslaan_alle($alle,$link)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die( "Unable to select database to store alle uitslagen");

	//Alle Uitslagen opslaan in database
	//Veld Datum aangehouden aangezien dit wellicht in de toekomst gebruikt wordt.
	//Ntb veld is nader te bepalen. Dit zo gehouden omdat de structuur dan het zelfde is als Uitslag.

	$query = "
	INSERT INTO ".$dbprefix."alle
	(
	TeamID,
	Datum,
	Wedstrijd,
	Ntb,
	Uitslag
	)
	VALUES
";
	$t = 0;
	$CountData = count($alle);
	foreach($alle AS $value)
	{
		if(!isset($value['Wedstrijd']) && isset($value["Thuis"]) && isset($value["Uit"]))
		{
			$Wedstrijd = htmlentities($value["Thuis"] . ' - '.$value["Uit"], ENT_QUOTES);
		}else{
			$Wedstrijd = htmlentities($value['Wedstrijd'], ENT_QUOTES);
		}
		$t++;
		#last row has , (comma)
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$link["TeamID"]."', '".convertStringToDate($value["Datum"])."', '".$Wedstrijd."', '', '".$value["Uitslag"]."'),";
		}else{
			$query.="('".$link["TeamID"]."', '".convertStringToDate($value["Datum"])."', '".$Wedstrijd."', '', '".$value["Uitslag"]."')";
		}
	}

	if ($UserDebug == 'Aan') {
		mysqli_query($con,$query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$query) or die(mysqli_error($con));
	}

	// Verwijderen van lege entries
	$sql = "DELETE FROM `".$dbprefix."alle` WHERE Uitslag = ' * '";
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$sql) or die(mysqli_error($con));
	}
}
// -------------------------------------


//Club Programma opslaan in database ---
function opslaan_clubprog($clubprog)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die( "Unable to select database to store clubprogramma");

	$query = "
	INSERT INTO ".$dbprefix."clubprogramma
	(
	Datum,
	Tijd,
	Thuis,
	Uit,
	Type,
	Accommodatie,
	Wedstrijdnr,
	Scheidsrechter
	)
	VALUES
";
	$t = 0;
	$CountData = count($clubprog);
	foreach($clubprog AS $value)
	{
		$value["Thuis"] = htmlentities($value["Thuis"], ENT_QUOTES);
		$value["Uit"] = htmlentities($value["Uit"], ENT_QUOTES);
		$value['Scheidsrechter'] = htmlentities($value['Scheidsrechter'], ENT_QUOTES);

		// $value['Accommodatie'] = htmlentities($value['Accommodatie'], ENT_QUOTES);
		// Omdat de Accomodatie niet meer direct beschikbaar bij het clubprogramma is vul deze maar met Thuis of Uit.

		If (stristr(strtolower($value["Thuis"]),str_replace('%','',strtolower($club1)))) { $Accomodatie = "Thuis"; } else { $Accomodatie = "Uit"; }

		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".convertStringToDate($value["Datum"])."', '".$value['Tijd']."', '".$value["Thuis"]."', '".$value["Uit"]."', '".$value['Type']."', '".$Accomodatie."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."'),";
		}
		else
		{
			$query.="('".convertStringToDate($value["Datum"])."', '".$value['Tijd']."', '".$value["Thuis"]."', '".$value["Uit"]."', '".$value['Type']."', '".$Accomodatie."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$query) or die(mysqli_error($con));
	}

	// verwijderen van bepaalde wedstrijden volgens instellingen.php
	if ($DelwdstrdCPCU == 'Aan') {
		$DelwdstrdCPCUtxt = "%".$DelwdstrdCPCUtxt."%";
		$sql = "DELETE FROM `".$dbprefix."clubprogramma` WHERE Thuis Like '$DelwdstrdCPCUtxt' OR Uit Like '$DelwdstrdCPCUtxt'";
		if ($UserDebug == 'Aan') {
			mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
		}
		else
		{
			mysqli_query($con,$sql) or die(mysqli_error($con));
		}
	}

	// Verwijderen van lege entries
	//  $sql = "DELETE FROM `".$dbprefix."clubprogramma` WHERE Datum = ''";
	//  mysqli_query($sql)
	//  or die(mysqli_error());

	// Wijzigen van bepaalde tekens in Accomodatie. Dit wordt veroorzaakt in de html code van voetbal.nl
	// Indien nodig de // van de volgende 3 regels verwijderen en de codes veranderen. de eerste is wat vervangen moet worden door de tweede.
	// $sql = "UPDATE ".$dbprefix."clubprogramma SET Accommodatie = REPLACE(Accommodatie, '&Atilde;&cent;','&acirc;')";
	//  mysqli_query($sql)
	//  or die(mysqli_error());
	// Queries die Maa en Okt vervangen door Mar en Oct
	//$sql = "update ".$dbprefix."clubprogramma set Datum = replace(Datum,'Maa','Mar')";
	//mysqli_query($sql)
	//or die(mysqli_error());
	//$sql = "update ".$dbprefix."clubprogramma set Datum = replace(Datum,'Okt','Oct')";
	//mysqli_query($sql)
	//or die(mysqli_error());

}
// -------------------------------------


//Afgelastingen opslaan in database ---
function opslaan_clubaf($clubaf)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die( "Unable to select database to store afgelastingen");

	$query = "
	INSERT INTO ".$dbprefix."afgelasting
	(
	Datum,
	Tijd,
	Thuis,
	Uit,
	Type,
	Accommodatie,
	Wedstrijdnr,
	Scheidsrechter,
	Status
	)
	VALUES
";
	$t = 0;
	$CountData = count($clubaf);
	foreach($clubaf AS $value)
	{
		$value["Thuis"] = htmlentities($value["Thuis"], ENT_QUOTES);
		$value["Uit"] = htmlentities($value["Uit"], ENT_QUOTES);
		$value['Scheidsrechter'] = htmlentities($value['Scheidsrechter'], ENT_QUOTES);
		$value['Accommodatie'] = htmlentities($value['Accommodatie'], ENT_QUOTES);
		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".convertStringToDate($value["Datum"])."', '".$value['Tijd']."', '".$value["Thuis"]."', '".$value["Uit"]."', '".$value['Type']."', '".$value['Accommodatie']."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."', '".$value['Status']."'),";
		}
		else
		{
			$query.="('".convertStringToDate($value["Datum"])."', '".$value['Tijd']."', '".$value["Thuis"]."', '".$value["Uit"]."', '".$value['Type']."', '".$value['Accommodatie']."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."', '".$value['Status']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$query) or die(mysqli_error($con));
	}

	// Verwijderen van lege entries
	$sql = "DELETE FROM `".$dbprefix."clubprogramma` WHERE Datum = '0000-00-00'";
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$sql) or die(mysqli_error($con));
	}

	// Wijzigen van bepaalde tekens in Accomodatie. Dit wordt veroorzaakt in de html code van voetbal.nl
	// Indien nodig de // van de volgende 3 regels verwijderen en de codes veranderen. de eerste is wat vervangen moet worden door de tweede.
	// $sql = "UPDATE ".$dbprefix."afgelasting SET Accommodatie = REPLACE(Accommodatie, '&Atilde;&cent;','&acirc;')";
	//  mysqli_query($sql)
	//  or die(mysqli_error());
	// Queries die Maa en Okt vervangen door Mar en Oct
	//$sql = "update ".$dbprefix."afgelasting set Datum = replace(Datum,'Maa','Mar')";
	//mysqli_query($sql)
	//or die(mysqli_error());
	//$sql = "update ".$dbprefix."afgelasting set Datum = replace(Datum,'Okt','Oct')";
	//mysqli_query($sql)
	//or die(mysqli_error());
}
// -------------------------------------

//Alle teams opslaan in database --------
function opslaan_teams($teams)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die("Er ging iets mis met de connectie.");
	//@mysqli_select_db($database) or die( "Unable to select database to store Uitslagen");


	$query = "
	INSERT INTO ".$dbprefix."teamlinks
	(
	`Teamcode`,
	`Naam`,
	`Klasse`,
	`Wedstrijdduur`,
	`Periode`,
	`Ophalen`,
	`GroupID`,
	`DatumTijd-Update`
	)
	VALUES
";
	$t = 0;
	$CountData = count($teams);


	foreach($teams AS $value)
	{

		$value["Naam"] = htmlentities($value["Naam"], ENT_QUOTES);
		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$value["Teamcode"]."', '".$value["Naam"]."', '".$value['Klasse']."', '".$value['Wedstrijdduur']."', '".$value['Periode']."', '".$value['Ophalen']."', '".$value['GroupID']."', '".$value['DatumTijd-Update']."'),";
		}
		else
		{
			$query.="('".$value["Teamcode"]."', '".$value["Naam"]."', '".$value['Klasse']."', '".$value['Wedstrijdduur']."', '".$value['Periode']."', '".$value['Ophalen']."', '".$value['GroupID']."', '".$value['DatumTijd-Update']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$query) or die(mysqli_error($con));
	}


}
// -------------------------------------

//ClubUitslagen opslaan in database --------
function opslaan_clubuitslagen($clubuitslagen)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die( "Unable to select database to store Uitslagen");


	$query = "
	INSERT INTO ".$dbprefix."clubuitslagen
	(
	Datum,
	Thuis,
	Uit,
	Uitslag
	)
	VALUES
";
	$t = 0;
	$CountData = count($clubuitslagen);
	foreach($clubuitslagen AS $value)
	{
		$value["Thuis"] = htmlentities($value["Thuis"], ENT_QUOTES);
		$value["Uit"] = htmlentities($value["Uit"], ENT_QUOTES);
		$value['Uitslag'] = str_replace('&nbsp;&nbsp;', ' ', $value['Uitslag']);
		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".convertStringToDate($value["Datum"])."', '".$value["Thuis"]."', '".$value["Uit"]."', '".$value['Uitslag']."'),";
		}
		else
		{
			$query.="('".convertStringToDate($value["Datum"])."', '".$value["Thuis"]."', '".$value["Uit"]."', '".$value['Uitslag']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$query) or die(mysqli_error($con));
	}

	// verwijderen van bepaalde wedstrijden volgens instellingen.php

	if ($DelwdstrdCPCU == 'Aan') {
		$DelwdstrdCPCUtxt = "%".$DelwdstrdCPCUtxt."%";
		$sql = "DELETE FROM `".$dbprefix."clubuitslagen` WHERE Thuis Like '$DelwdstrdCPCUtxt' OR Uit Like '$DelwdstrdCPCUtxt'";
		if ($UserDebug == 'Aan') {
			mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
		}
		else
		{
			mysqli_query($con,$sql) or die(mysqli_error($con));
		}
	}


	// Verwijderen van lege entries
	//  $sql = "DELETE FROM `".$dbprefix."uitslag` WHERE Datum = '0000-00-00'";
	//  mysqli_query($con,$sql)
	//  or die(mysqli_error());
}


// -------------------------------------

//Teamindeling opslaan in database --------
function opslaan_teamindeling($teamindeling,$link)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die( "Unable to select database to store Uitslagen");


	$query = "
	INSERT INTO ".$dbprefix."teamindeling
	(
	TeamID,
	Teamnr,
	Naam,
	Vktijd
	)
	VALUES
";
	$t = 0;
	$CountData = count($teamindeling);
	foreach($teamindeling AS $value)
	{
		$value["Naam"] = htmlentities($value["Naam"], ENT_QUOTES);
		$value["Vktijd"] = htmlentities($value["Vktijd"], ENT_QUOTES);
		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$link["TeamID"]."', '".$value['Teamnr']."', '".$value['Naam']."', '".$value['Vktijd']."'),";
		}
		else
		{
			$query.="('".$link["TeamID"]."', '".$value['Teamnr']."', '".$value['Naam']."', '".$value['Vktijd']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$query) or die(mysqli_error($con));
	}


}
// -------------------------------------

//Speelronde opslaan in database --------
function opslaan_speelronde($speelronde,$link)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die( "Unable to select database to store Uitslagen");


	$query = "
	INSERT INTO ".$dbprefix."speelronde
	(
	TeamID,
	Speelrondenr,
	Wedstrijd
	)
	VALUES
";
	$t = 0;
	$CountData = count($speelronde);
	foreach($speelronde AS $value)
	{
		$value["Wedstrijd"] = htmlentities($value["Wedstrijd"], ENT_QUOTES);
		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$link["TeamID"]."', '".$value['Speelrondenr']."', '".$value['Wedstrijd']."'),";
		}
		else
		{
			$query.="('".$link["TeamID"]."', '".$value['Speelrondenr']."', '".$value['Wedstrijd']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$query) or die(mysqli_error($con));
	}


}
// -------------------------------------


//Reorder Database ---------------------
//Opschonen van bestanden
function reorderdb($table,$tableID)
{
	include("config.php");
	$con=mysqli_connect($server,$username,$password,$database) or die( "Unable to select database to reorder database");

	mysqli_query($con,"ALTER TABLE $table DROP $tableID")
	or die(mysqli_error($con));
	mysqli_query($con,"OPTIMIZE TABLE $table")
	or die(mysqli_error($con));
	mysqli_query($con,"ALTER TABLE $table ADD $tableID INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST")
	or die(mysqli_error($con));
}

// -------------------------------------
//Functie om een substring op te halen
function get_string_between($string, $start, $end)
{
	$string = " ".$string;
	$ini = strpos($string,$start);
	//Substring is niet gevonden
	if ($ini == 0) return "";
	//Bepaal de nieuwe startpositie voor het zoeken naar deel twee
	$ini += strlen($start);
	//Indien er geen $end is opgegeven geef de complete string vanaf $start terug
	if ($end == "") return substr($string,$ini);
	//Zoek deel twee en bepaal de lengte van het terug te geven deel.
	$len = strpos($string,$end,$ini) - $ini;
	//Geef de string tussen $start en $end terug.
	return substr($string,$ini,$len);
}

//Functie om twee datas van elkaar af te trekken en het verschil in dagen terug te geven
function dateDiff($startDate, $endDate)
{
	// Parse dates for conversion
	$startArry = date_parse($startDate);
	$endArry = date_parse($endDate);

	// Convert dates to Julian Days
	$start_date = gregoriantojd($startArry["month"],$startArry["day"], $startArry["year"]);
	$end_date = gregoriantojd($endArry["month"], $endArry["day"], $endArry["year"]);

	// Return difference
	return round(($end_date - $start_date), 0);
}
function makeNiceDate($date)
{
	if(!empty($date))
	{
		$epoch = mktime(0,0,0,substr($date,5,2),substr($date,8,2),substr($date,0,4));
		/* Set locale to Dutch */
		if(getOS() == 'linux')
		{
			setlocale(LC_ALL, array('nl_NL'));
		}else{
			setlocale(LC_ALL, array('nld_nld'));
		}
		$date=strftime('%d %b',$epoch);
	}
	return $date;
}
function makePublishDate($date)
{
	if(!empty($date))
	{
		$epoch = mktime(substr($date,11,2),substr($date,14,2),0,substr($date,5,2),substr($date,8,2),substr($date,0,4));
		/* Set locale to Dutch */
		if(getOS() == 'linux')
		{
			setlocale(LC_ALL, array('en_US'));
		}else{
			setlocale(LC_ALL, array('en_US'));
		}
		$date=strftime('%a, %d %Y %T %z',$epoch); //Tue, 07 Oct 2014 20:26:58 +0200
	}
	return $date;
}
function ConvertDate($date)
{
	if(!empty($date))
	{
		/* Set locale to Dutch */
		if(getOS() == 'linux')
		{
			setlocale(LC_ALL, array('nl_NL'));
		}else{
			setlocale(LC_ALL, array('nld_nld'));
		}
//		$date=date('Y-m-d',$date);
$dagvanweek = date('l', $date);
$arraydag = array(
'Zondag',
'Maandag',
'Dinsdag',
'Woensdag',
'Donderdag',
'Vrijdag',
'Zaterdag'
);
$dagvanweek = $arraydag[date('w', $date)];
$arraymaand = array(
'januari',
'februari',
'maart',
'april',
'mei',
'juni',
'juli',
'augustus',
'september',
'oktober',
'november',
'december'
);
$date = $arraydag [date('w', $date)] . date(' d ', $date) . $arraymaand [date('n', $date) - 1];// . date(' Y', $date); 
	}
	return $date;
}
function getOS()
{
	if(isset($_SERVER['SERVER_SOFTWARE']))
	{
		if(strpos($_SERVER['SERVER_SOFTWARE'],'Win32') === false)
		{
			return 'linux';
		}else{
			return 'windows';
		}
	}else{
		return 'linux';
	}
}
?>