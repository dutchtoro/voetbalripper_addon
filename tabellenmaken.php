<?php
// tabellenmaken.php
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel

error_reporting(E_ERROR);

include_once("config.php");
$sql;

// Tabel teamlinks maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}teamlinks` (
`TeamID` int(11) NOT NULL AUTO_INCREMENT,
`Teamcode` text NOT NULL,
`AltTeamcode` text,
`Naam` varchar(400),
`Klasse` varchar(50),
`Wedstrijdtype` VARCHAR(15) DEFAULT 'Competitie',
`Wedstrijdduur` varchar(3) default '0',
`Periode` VARCHAR(3) NOT NULL,
`Ophalen` VARCHAR(3) NOT NULL,
`GroupID` VARCHAR(3),
`DatumTijd-Update` varchar(10) NOT NULL,
PRIMARY KEY (`TeamID`)
) ENGINE = MYISAM;";


// Tabel programma maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}programma` (
`ProgrammaID` int(11) NOT NULL AUTO_INCREMENT,
`TeamID` int(11) NOT NULL,
`Datum` varchar(11) NOT NULL,
`Tijd` varchar(5) NOT NULL,
`Wedstrijd` varchar(300) NOT NULL,
`Type` varchar(2) NOT NULL,
`Accommodatie` varchar(300) NOT NULL,
`Wedstrijdnr` varchar(6) NOT NULL,
`Scheidsrechter` varchar(30) NOT NULL,
`Status` varchar(30) NOT NULL,
`Sportpark` varchar(300),
`Adres` varchar(100),
`Postcode` varchar(10),
`Plaats` varchar(30),
`Telefoon` varchar(15),
PRIMARY KEY (`ProgrammaID`)
) ENGINE = MYISAM;";

// Tabel clubprogramma maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}clubprogramma` (
`ClubProgrammaID` int(11) NOT NULL AUTO_INCREMENT,
`Datum` varchar(11) NOT NULL,
`Tijd` varchar(5) NOT NULL,
`Thuis` varchar(300) NOT NULL,
`Uit` varchar(300) NOT NULL,
`Type` varchar(2) NOT NULL,
`Accommodatie` varchar(300) NOT NULL,
`Wedstrijdnr` varchar(6) NOT NULL,
`Scheidsrechter` varchar(30) NOT NULL,
`Status` varchar(30) NOT NULL,
PRIMARY KEY (`ClubProgrammaID`)
) ENGINE = MYISAM;";

// Tabel club uitslagen maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}clubuitslagen` (
`ClubUitslagID` int(11) NOT NULL AUTO_INCREMENT,
`Datum` varchar(10) NOT NULL,
`Thuis` varchar(100) NOT NULL,
`Uit` varchar(100) NOT NULL,
`Uitslag` varchar(10) NOT NULL,
PRIMARY KEY (`ClubUitslagID`)
) ENGINE = MYISAM;";


// Tabel Afgelasting maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}afgelasting` (
`AfgelastingID` int(11) NOT NULL AUTO_INCREMENT,
`Datum` varchar(11) NOT NULL,
`Tijd` varchar(5) NOT NULL,
`Thuis` varchar(300) NOT NULL,
`Uit` varchar(300) NOT NULL,
`Type` varchar(2) NOT NULL,
`Accommodatie` varchar(300) NOT NULL,
`Wedstrijdnr` varchar(6) NOT NULL,
`Scheidsrechter` varchar(30) NOT NULL,
`Status` varchar(30) NOT NULL,
PRIMARY KEY (`AfgelastingID`)
) ENGINE = MYISAM;";


// Tabel stand maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}stand` (
`StandID` int(11) NOT NULL AUTO_INCREMENT,
`TeamID` int(11) NOT NULL,
`Elftal` varchar(100) NOT NULL,
`Plaats` int(11) NOT NULL,
`G` int(11) NOT NULL,
`W` int(11) NOT NULL,
`GW` int(11) NOT NULL,
`V` int(11) NOT NULL,
`P` int(11) NOT NULL,
`DPV` int(11) NOT NULL,
`DPT` int(11) NOT NULL,
`PM` varchar(3) NOT NULL,
PRIMARY KEY (`StandID`)
) ENGINE = MYISAM;";

// Tabel Periode stand 1 maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}standp1` (
`StandID` int(11) NOT NULL AUTO_INCREMENT,
`TeamID` int(11) NOT NULL,
`Elftal` varchar(100) NOT NULL,
`Plaats` int(11) NOT NULL,
`G` int(11) NOT NULL,
`W` int(11) NOT NULL,
`GW` int(11) NOT NULL,
`V` int(11) NOT NULL,
`P` int(11) NOT NULL,
`DPV` int(11) NOT NULL,
`DPT` int(11) NOT NULL,
`PM` varchar(3) NOT NULL,
PRIMARY KEY (`StandID`)
) ENGINE = MYISAM;";

// Tabel Periode stand 2 maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}standp2` (
`StandID` int(11) NOT NULL AUTO_INCREMENT,
`TeamID` int(11) NOT NULL,
`Elftal` varchar(100) NOT NULL,
`Plaats` int(11) NOT NULL,
`G` int(11) NOT NULL,
`W` int(11) NOT NULL,
`GW` int(11) NOT NULL,
`V` int(11) NOT NULL,
`P` int(11) NOT NULL,
`DPV` int(11) NOT NULL,
`DPT` int(11) NOT NULL,
`PM` varchar(3) NOT NULL,
PRIMARY KEY (`StandID`)
) ENGINE = MYISAM;";

// Tabel periode stand 3 maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}standp3` (
`StandID` int(11) NOT NULL AUTO_INCREMENT,
`TeamID` int(11) NOT NULL,
`Elftal` varchar(100) NOT NULL,
`Plaats` int(11) NOT NULL,
`G` int(11) NOT NULL,
`W` int(11) NOT NULL,
`GW` int(11) NOT NULL,
`V` int(11) NOT NULL,
`P` int(11) NOT NULL,
`DPV` int(11) NOT NULL,
`DPT` int(11) NOT NULL,
`PM` varchar(3) NOT NULL,
PRIMARY KEY (`StandID`)
) ENGINE = MYISAM;";

// Tabel uitslag maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}uitslag` (
`UitslagID` int(11) NOT NULL AUTO_INCREMENT,
`TeamID` int(11) NOT NULL,
`Datum` varchar(10) NOT NULL,
`Thuis` varchar(100) NOT NULL,
`Uit` varchar(100) NOT NULL,
`Uitslag` varchar(10) NOT NULL,
PRIMARY KEY (`UitslagID`)
) ENGINE = MYISAM;";


// Tabel alle maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}alle` (
`AlleID` int(11) NOT NULL AUTO_INCREMENT,
`TeamID` int(11) NOT NULL,
`Datum` varchar(10) NOT NULL,
`Wedstrijd` varchar(100) NOT NULL,
`Ntb` varchar(100) NOT NULL,
`Uitslag` varchar(10) NOT NULL,
PRIMARY KEY (`AlleID`)
) ENGINE = MYISAM;";

// Tabel oefenprogramma maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}oefenprogramma` (
`ClubProgrammaID` int(11) NOT NULL AUTO_INCREMENT,
`Datum` varchar(11) NOT NULL,
`Tijd` varchar(5) NOT NULL,
`Thuis` varchar(300) NOT NULL,
`Uit` varchar(300) NOT NULL,
`Type` varchar(2) NOT NULL default 'oe',
`Accommodatie` varchar(300) NULL,
`Wedstrijdnr` varchar(20) NOT NULL default '<b>Oefen</b>',
`Scheidsrechter` varchar(30) NULL,
`Vertrekverzameltijd` varchar(5),
`KlkThuis` VARCHAR(3) NULL,
`KlkUit` VARCHAR(3) NULL,
`Veld` VARCHAR(3) NULL,
`Status` varchar(30) NOT NULL,
`Uitslag` varchar(30) NOT NULL default 'ng',
`Adres` varchar(100),
`Postcode` varchar(10),
`Plaats` varchar(30),
`Telefoon` varchar(15),
`Datumstamp` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`ClubProgrammaID`)
) ENGINE = MYISAM;";

// Tabel extraprogramma maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}extraprogramma` (
`ExtraProgrammaID` int(11) NOT NULL AUTO_INCREMENT,
`Edatum` varchar(11) NOT NULL,
`EWedstrijdnr` varchar(6) NOT NULL,
`EScheidsrechter` varchar(30) NULL,
`Vertrekverzameltijd` VARCHAR(5) NULL,
`KlkThuis` VARCHAR(3) NULL,
`KlkUit` VARCHAR(3) NULL,
`Veld` VARCHAR(3) NULL,
`EAfgelast` varchar(30) NULL,
PRIMARY KEY (`ExtraProgrammaID`)
) ENGINE = MYISAM;";

// Tabel instellingen maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}instellingen` (
`InstellingenID` int(11) NOT NULL AUTO_INCREMENT,
`Clubnaam` varchar(300) NOT NULL,
`Club1` varchar(100) NOT NULL,
`Club2` varchar(100) NOT NULL,
`Clubcode` varchar(30) NOT NULL,
`AltClubcode` varchar(30) NOT NULL,
`Clubprogramma` varchar(15) NOT NULL default 'periode',
`SortCP` varchar(20) NOT NULL default 'Datum-Tijd-Team',
`FilterCP` varchar(3) NOT NULL default 'Uit',
`MinDagCP` varchar(4) NOT NULL default '1',
`PlusDagCP` varchar(4) NOT NULL default '21',
`MinDagUitslagen` varchar(4) NOT NULL default '6',
`PlusDagUitslagen` varchar(4) NOT NULL default '6',
`CombCpAfg` varchar(3) NOT NULL default 'Aan',
`Clubnaamcheck` varchar(3) NOT NULL default 'Aan',
`CPophalen` varchar(3) NOT NULL default 'Aan',
`CUophalen` varchar(3) NOT NULL default 'Aan',
`DelwdstrdCPCUtxt` varchar(20) NOT NULL default '(zaal)',
`DelwdstrdCPCU` varchar(3) NOT NULL default 'Uit',
`ScheidsinCP` varchar(3) NOT NULL default 'Uit',
`VVinCP` varchar(3) NOT NULL default 'Uit',
`Pdetails` varchar(3) NOT NULL default 'Uit',
`Pdetailstonen` varchar(3) NOT NULL default 'Uit',
`Klkamertonen` varchar(3) NOT NULL default 'Uit',
`Veldtonen` varchar(3) NOT NULL default 'Uit',
`Oefeninteam` varchar(3) NOT NULL default 'Uit',
`Tindelinginteam` varchar(3) NOT NULL default 'Uit',
`CSSweergave` VARCHAR(100) NOT NULL DEFAULT 'opmaak.css',
`CSSadmin` VARCHAR(100) NOT NULL DEFAULT 'opmaak.css',
`OphViaAcode` varchar(3) NOT NULL default 'Uit',
`AutoAcode` varchar(3) NOT NULL default 'Uit',
`Wacht` varchar(3) NOT NULL default 'Aan',
`Opschonen` varchar(3) NOT NULL default 'Aan',
`Meldingen` varchar(10) NOT NULL default 'Scherm',
`Curl` varchar(3) NOT NULL default 'Aan',
`Phpsafemode` varchar(3) NOT NULL default 'Uit',
`Phpmaxtime` varchar(4) NOT NULL default '90',
`Gebruikersnaam` varchar(100) NOT NULL,
`Wachtwoord` varchar(100) NOT NULL,
`Emailvoormeldingen` varchar(100) NOT NULL,
`Beveiligindex` varchar(3) NOT NULL default 'Uit',
`UserDebug` varchar(3) NOT NULL default 'Uit',
`Igebruikersnaam` varchar(100) NOT NULL default 'aaaaaa',
`Iwachtwoord` varchar(100) NOT NULL default 'aaaaaa',
`Agebruikersnaam` varchar(100) NOT NULL default 'aaaaaa',
`Awachtwoord` varchar(100) NOT NULL default 'aaaaaa',
`Ogebruikersnaam` varchar(100) NOT NULL default 'aaaaaa',
`Owachtwoord` varchar(100) NOT NULL default 'aaaaaa',
`Egebruikersnaam` varchar(100) NOT NULL default 'aaaaaa',
`Ewachtwoord` varchar(100) NOT NULL default 'aaaaaa',
`Ggebruikersnaam` varchar(100) NOT NULL default 'aaaaaa',
`Gwachtwoord` varchar(100) NOT NULL default 'aaaaaa',
`Ugebruikersnaam` varchar(100) NOT NULL default 'aaaaaa',
`Uwachtwoord` varchar(100) NOT NULL default 'aaaaaa',
`Instgebruikersnaam` varchar(100) NOT NULL default 'voetbal',
`Instwachtwoord` varchar(100) NOT NULL default 'voetbal',
`Dbgebruikersnaam` varchar(100) NOT NULL default 'aaaaaa',
`Dbwachtwoord` varchar(100) NOT NULL default 'aaaaaa',
PRIMARY KEY (`InstellingenID`)
) ENGINE = MYISAM;";
$sql .= "INSERT INTO `${dbprefix}instellingen` (`InstellingenID`) VALUES ('1');";

// Tabel groupidaccount maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}groupidaccount` (
`GroupaccountID` int(11) NOT NULL AUTO_INCREMENT,
`GroupID` int(3) NOT NULL,
`Gebruikersnaam` varchar(100) NOT NULL,
`Wachtwoord` varchar(100) NOT NULL,
PRIMARY KEY (`GroupaccountID`)
) ENGINE = MYISAM;";

// Tabel Teamindeling maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}teamindeling` (
`TeamindelingID` int(11) NOT NULL AUTO_INCREMENT,
`TeamID` int(11) NOT NULL,
`Teamnr` int(11) NOT NULL,
`Naam` varchar(400) NOT NULL,
`Vktijd` varchar(5) NOT NULL,
PRIMARY KEY (`TeamindelingID`)
) ENGINE = MYISAM;";

// Tabel Speelronde maken
$sql .= "CREATE TABLE IF NOT EXISTS `${dbprefix}speelronde` (
`SpeelrondeID` int(11) NOT NULL AUTO_INCREMENT,
`TeamID` int(11) NOT NULL,
`Speelrondenr` int(11) NOT NULL,
`Wedstrijd` varchar(400) NOT NULL,
PRIMARY KEY (`SpeelrondeID`)
) ENGINE = MYISAM;";
mysqli_multi_query($con, $sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error());




// Verbinding met database verbreken
mysqli_reset();

echo "<br />Tabellen aangemaakt";

?>
