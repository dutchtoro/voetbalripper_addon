<?php session_start();
// groupid.php 1.9.7.1 (07-11-2012) Bugfixes en kleine cosmetische aanpassingen.
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 19-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel


include("config.php");
include("functies.php");
//mysql_connect($server,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");
global $dbprefix;
$tbl_name=$dbprefix."groupidaccount"; // Table name
$filter = "";

  if (isset($_GET["ordergroup"])) $ordergroup = @$_GET["ordergroup"];
  if (isset($_GET["typegroup"])) $ordtypegroup = @$_GET["typegroup"];

  if (isset($_POST["filter"])) $filter = @$_POST["filter"];
  if (isset($_POST["filter_field"])) $filterfield = @$_POST["filter_field"];
  $wholeonly = false;
  if (isset($_POST["wholeonly"])) $wholeonly = @$_POST["wholeonly"];

  if (!isset($ordergroup) && isset($_SESSION["ordergroup"])) $ordergroup = $_SESSION["ordergroup"];
  if (!isset($ordtypegroup) && isset($_SESSION["typegroup"])) $ordtypegroup = $_SESSION["typegroup"];
  if (!isset($filter) && isset($_SESSION["filter"])) $filter = $_SESSION["filter"];
  if (!isset($filterfield) && isset($_SESSION["filter_field"])) $filterfield = $_SESSION["filter_field"];
if (isset($_POST['Submit']))
{	//echo "<p>submit pressed";


	foreach($_POST['GroupaccountID'] as $id)
	{
		$sql1="UPDATE ".$tbl_name." SET GroupID='".$_POST["GroupID".$id]."', Gebruikersnaam='".$_POST["Gebruikersnaam".$id]."', Wachtwoord='".$_POST["Wachtwoord".$id]."' WHERE GroupaccountID='".$id."'";
		//$result1=mysqli_query($con,$sql1) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
		mysqli_query($con,$sql1) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));

	}
}

?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>Beheer Group ID Accounts</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php echo "<link rel=\"stylesheet\" type=\"text/css\" href='$CSSadmin'>"; ?>
</head>
<body>
<table class="bdadmin" style="width:100%"><tr><td class="hr"><h2>Beheer Group ID Accounts</h2></td></tr></table>

<?php
  if (!login()) exit;
?>
<div style="float: right"><a href="groupid.php?a=logout">[ Uitloggen ]</a></div>
<br>
<?php
  $con = connect();
  $showrecs = 30;
  $pagerange = 10;

  $a = @$_GET["a"];

  $recid = @$_GET["recid"];
  $page = @$_GET["page"];
  if (!isset($page)) $page = 1;

  $sql = @$_POST["sql"];

  switch ($sql) {
    case "insert":
      sql_insert();
      break;
    case "update":
      sql_update();
      break;
    case "delete":
      sql_delete();
      break;
  }

  switch ($a) {
    case "add":
      addrec();
      break;
    case "view":
      viewrec($recid);
      break;
    case "edit":
      editrec($recid);
      break;
    case "del":
      deleterec($recid);
      break;
    default:
      select();
      break;
  }

  if (isset($ordergroup)) $_SESSION["ordergroup"] = $ordergroup;
  if (isset($ordtypegroup)) $_SESSION["typegroup"] = $ordtypegroup;
  if (isset($filter)) $_SESSION["filter"] = $filter;
  if (isset($filterfield)) $_SESSION["filter_field"] = $filterfield;
  if (isset($wholeonly)) $_SESSION["wholeonly"] = $wholeonly;

  mysqli_close($con);
?>
<table class="bdadmin" style="width:100%"><tr><td class="hr">Gemaakt door Yarro/Johnvs</td></tr></table>
<div style="float: left"><a href="dashboard.php">[ DashBoard ]</a></div><br />
<div style="float: left"><a href="instellingen.php">[ Instellingen ]</a></div><br>
<div style="float: left"><a href="admin.php">[ Beheer Teams ]</a></div><br>
<div style="float: left"><a href="oefenprogramma.php">[ Oefen Programma/Uitslagen ]</a></div><br>
<div style="float: left"><a href="extra.php">[ Extra informatie Club Programma ]</a></div><br>
<div style="float: left"><a href="uitslag.php">[ Uitslagen invoeren voor Club / Team ]</a></div><br>

</body>
</html>

<?php function select()
  {
  global $a;
  global $showrecs;
  global $page;
  global $filter;
  global $filterfield;
  global $wholeonly;
  global $ordergroup;
  global $ordtypegroup;
  global $dbprefix;


  if ($a == "reset") {
    $filter = "";
    $filterfield = "";
    $wholeonly = "";
    $ordergroup = "";
    $ordtypegroup = "";
  }

  $checkstr = "";
  if ($wholeonly) $checkstr = " checked";
  if ($ordtypegroup == "asc") { $ordtypegroupstr = "desc"; } else { $ordtypegroupstr = "asc"; }
  $res = sql_select();
  $count = sql_getrecordcount();
  if ($count % $showrecs != 0) {
    $pagecount = intval($count / $showrecs) + 1;
  }
  else {
    $pagecount = intval($count / $showrecs);
  }
  $startrec = $showrecs * ($page - 1);
  if ($startrec < $count) {mysqli_data_seek($res, $startrec);}
  $reccount = min($showrecs * $page, $count);
?>
<table class="bdadmin">
<tr><td>Tabel: groupidaccounts</td></tr>
<tr><td>Getoonde accounts  <?php echo $startrec + 1 ?> - <?php echo $reccount ?>  &nbsp;van&nbsp;  <?php echo $count ?></td></tr>
</table>
<hr />
<form action="groupid.php" method="post">
<table class="bdadmin">

</table>
</form>

<hr />
<?php showpagenav($page, $pagecount); ?>
<br>
<form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
<table class="tbl" style="width:100%">
<tr>
<td class="hr">&nbsp;</td>
<td class="hr">&nbsp;</td>
<td class="hr">&nbsp;</td>
<td class="hr"><a class="hr" href="groupid.php?ordergroup=<?php echo "" ?>&amp;typegroup=<?php echo $ordtypegroupstr ?>"><?php echo htmlspecialchars(" ") ?></a></td>
<td class="hr"><a class="hr" href="groupid.php?ordergroup=<?php echo "GroupID" ?>&amp;typegroup=<?php echo $ordtypegroupstr ?>"><?php echo htmlspecialchars("GroupID") ?></a></td>
<td class="hr"><a class="hr" href="groupid.php?ordergroup=<?php echo "Gebruikersnaam" ?>&amp;typegroup=<?php echo $ordtypegroupstr ?>"><?php echo htmlspecialchars("Gebruikersnaam") ?></a></td>
<td class="hr"><a class="hr" href="groupid.php?ordergroup=<?php echo "Wachtwoord" ?>&amp;typegroup=<?php echo $ordtypegroupstr ?>"><?php echo htmlspecialchars("Wachtwoord") ?></a></td>
</tr>
<?php
  for ($i = $startrec; $i < $reccount; $i++)
  {
    $row = mysqli_fetch_assoc($res);
    $style = "dr";
    $bstyle = "style=\"background-color: #FFFFFF; border: 1px solid #D0D0D0;\"";
    if ($i % 2 != 0) {
      $style = "sr";
      $bstyle = "style=\"background-color: #A6D2FF; border: 1px solid #D0D0D0;\"";

    }
?>

<tr>
<td class="<?php echo $style ?>"><a href="groupid.php?a=view&amp;recid=<?php echo $i ?>">Bekijk</a></td>
<td class="<?php echo $style ?>"><a href="groupid.php?a=edit&amp;recid=<?php echo $i ?>">Wijzig</a></td>
<td class="<?php echo $style ?>"><a href="groupid.php?a=del&amp;recid=<?php echo $i ?>">Wissen</a></td>
<td class="<?php echo $style ?>"><input type="hidden" name="GroupaccountID[]" value="<?php echo $row['GroupaccountID']; ?>" /></td>
<td class="<?php echo $style ?>"><input name="GroupID<?php echo $row['GroupaccountID']; ?>" type="text" class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="GroupID<?php echo $row['GroupaccountID'] ?>" value="<?php echo $row['GroupID']; ?>"></td>
<td class="<?php echo $style ?>"><input name="Gebruikersnaam<?php echo $row['GroupaccountID']; ?>" type="text" class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="Gebruikersnaam<?php echo $row['GroupaccountID'] ?>" value="<?php echo $row['Gebruikersnaam']; ?>"></td>
<td class="<?php echo $style ?>"><input name="Wachtwoord<?php echo $row['GroupaccountID']; ?>" type="text" class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="Wachtwoord<?php echo $row['GroupaccountID'] ?>" value="<?php echo $row['Wachtwoord']; ?>"></td>
</tr>
<?php
  }

  mysqli_free_result($res);
?>
<tr>
<td class="center" colspan="7"><input type="submit" name="Submit" value="Wijzigingen aanbrengen" /></td>
</tr>

</table>
</form>
<br>
<?php showpagenav($page, $pagecount); ?>
<?php } ?>

<?php function login()
{
    include("config.php");
  global $_POST;
  global $_SESSION;

  global $_GET; 
  if (isset($_GET["a"]) && ($_GET["a"] == 'logout')) $_SESSION["logged_in_group"] = false;
  if (!isset($_SESSION["logged_in_group"])) $_SESSION["logged_in_group"] = false;
  if (!$_SESSION["logged_in_group"]) {
    $login = "";
    $password2 = "";

    if (isset($_POST["login"])) $login = @$_POST["login"];
    if (isset($_POST["password"])) $password2 = @$_POST["password"];

    if (($login != "") && ($password2 != "")) {
      if (($login == $Groupidusername) && ($password2 == $Groupidpassword)) {
        $_SESSION["logged_in_group"] = true;
    }
    else {
?>
<p><b><font color="-1">De combinatie gebruikersnaam/wachtwoord is onjuist</font></b></p>
<?php } } }if (isset($_SESSION["logged_in_group"]) && (!$_SESSION["logged_in_group"])) { ?>
<form action="groupid.php" method="post">
<table class="bdadmin">
<tr>
<td>Gebruikersnaam</td>
<td><input type="text" name="login" value="<?php echo $login ?>"></td>
</tr>
<tr>
<td>Wachtwoord</td>
<td><input type="password" name="password" value="<?php echo $password2 ?>"></td>
</tr>
<tr>
<td><input type="submit" name="action" value="Inloggen"></td>
</tr>
</table>
</form>
<?php
  }
  if (!isset($_SESSION["logged_in_group"])) $_SESSION["logged_in_group"] = false;
  return $_SESSION["logged_in_group"];
} ?>

<?php function showrow($row, $recid)
  {
?>
<table class="tbl" style="width:50%">
<tr>
<td class="hr"><?php echo htmlspecialchars("GroupID")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["GroupID"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Gebruikersnaam")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Gebruikersnaam"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Wachtwoord")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Wachtwoord"]) ?></td>
</tr>
</table>
<?php } ?>

<?php function showroweditor($row, $iseditmode)
  {
  global $con;
?>
<table class="tbl" style="width:50%">
<tr>
<td class="hr"><?php echo htmlspecialchars("GroupID")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="1" name="GroupID"><?php echo str_replace('"', '&quot;', trim($row["GroupID"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Gebruikersnaam")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="1" name="Gebruikersnaam" maxlength="30"><?php echo str_replace('"', '&quot;', trim($row["Gebruikersnaam"])) ?></textarea></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Wachtwoord")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="1" name="Wachtwoord" maxlength="30"><?php echo str_replace('"', '&quot;', trim($row["Wachtwoord"])) ?></textarea></td>
</tr>

</table>
<?php } ?>

<?php function showpagenav($page, $pagecount)
{
?>
<table class="bdadmin">
<tr>
<td><a href="groupid.php?a=add">[Group ID Account toevoegen]</a>&nbsp;</td>
<?php if ($page > 1) { ?>
<td><a href="groupid.php?page=<?php echo $page - 1 ?>">&lt;&lt;&nbsp;Vorige</a>&nbsp;</td>
<?php } ?>
<?php
  global $pagerange;

  if ($pagecount > 1) {

  if ($pagecount % $pagerange != 0) {
    $rangecount = intval($pagecount / $pagerange) + 1;
  }
  else {
    $rangecount = intval($pagecount / $pagerange);
  }
  for ($i = 1; $i < $rangecount + 1; $i++) {
    $startpage = (($i - 1) * $pagerange) + 1;
    $count = min($i * $pagerange, $pagecount);

    if ((($page >= $startpage) && ($page <= ($i * $pagerange)))) {
      for ($j = $startpage; $j < $count + 1; $j++) {
        if ($j == $page) {
?>
<td><b><?php echo $j ?></b></td>
<?php } else { ?>
<td><a href="groupid.php?page=<?php echo $j ?>"><?php echo $j ?></a></td>
<?php } } } else { ?>
<td><a href="groupid.php?page=<?php echo $startpage ?>"><?php echo $startpage ."..." .$count ?></a></td>
<?php } } } ?>
<?php if ($page < $pagecount) { ?>
<td>&nbsp;<a href="groupid.php?page=<?php echo $page + 1 ?>">Volgende&nbsp;&gt;&gt;</a>&nbsp;</td>
<?php } ?>
</tr>
</table>
<?php } ?>

<?php function showrecnav($a, $recid, $count)
{
?>
<table class="bdadmin">
<tr>
<td><a href="groupid.php">Index Pagina</a></td>
<?php if ($recid > 0) { ?>
<td><a href="groupid.php?a=<?php echo $a ?>&recid=<?php echo $recid - 1 ?>">Vorige groupid</a></td>
<?php } if ($recid < $count - 1) { ?>
<td><a href="groupid.php?a=<?php echo $a ?>&recid=<?php echo $recid + 1 ?>">Volgende groupid</a></td>
<?php } ?>
</tr>
</table>
<hr />
<?php } ?>

<?php function addrec()
{
?>
<table class="bdadmin">
<tr>
<td><a href="groupid.php">Index Pagina</a></td>
</tr>
</table>
<hr />
<form enctype="multipart/form-data" action="groupid.php" method="post">
<p><input type="hidden" name="sql" value="insert"></p>
<?php
$row = array(
  "GroupaccountID" => "",
  "GroupID" => "",
  "Gebruikersnaam" => "",
  "Wachtwoord" => "");
showroweditor($row, false);
?>
<p><input type="submit" name="action" value="Toevoegen"></p>
</form>
<?php } ?>

<?php function viewrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  mysqli_data_seek($res, $recid);
  $row = mysqli_fetch_assoc($res);
  showrecnav("view", $recid, $count);
?>
<br>
<?php showrow($row, $recid) ?>
<br>
<hr />
<table class="bdadmin">
<tr>
<td><a href="groupid.php?a=add">Group ID Account toevoegen</a></td>
<td><a href="groupid.php?a=edit&recid=<?php echo $recid ?>">groupid wijzigen</a></td>
<td><a href="groupid.php?a=del&recid=<?php echo $recid ?>">groupid wissen</a></td>
</tr>
</table>
<?php
  mysqli_free_result($res);
} ?>

<?php function editrec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  mysqli_data_seek($res, $recid);
  $row = mysqli_fetch_assoc($res);
  showrecnav("edit", $recid, $count);
?>
<br>
<form enctype="multipart/form-data" action="groupid.php" method="post">
<input type="hidden" name="sql" value="update">
<input type="hidden" name="xGroupaccountID" value="<?php echo $row["GroupaccountID"] ?>">
<?php showroweditor($row, true); ?>
<p><input type="submit" name="action" value="Wijzigen"></p>
</form>
<?php
  mysqli_free_result($res);
} ?>

<?php function deleterec($recid)
{
  $res = sql_select();
  $count = sql_getrecordcount();
  mysqli_data_seek($res, $recid);
  $row = mysqli_fetch_assoc($res);
  showrecnav("del", $recid, $count);
?>
<br>
<form action="groupid.php" method="post">
<input type="hidden" name="sql" value="delete">
<input type="hidden" name="xGroupaccountID" value="<?php echo $row["GroupaccountID"] ?>">
<?php showrow($row, $recid) ?>
<p><input type="submit" name="action" value="Bevestigen"></p>
</form>
<?php
  mysqli_free_result($res);
} ?>


<?php function connect()
{
  include("config.php");
  //$con = mysql_connect($server,$username,$password);
  //mysql_select_db($database);
    return $con;
}

function sqlvalue($val, $quote)
{
  if ($quote)
    $tmp = sqlstr($val);
  else
    $tmp = $val;
  if ($tmp == "")
    $tmp = "NULL";
  elseif ($quote)
    $tmp = "'".$tmp."'";
  return $tmp;
}

function sqlstr($val)
{
  return str_replace("'", "''", $val);
}

function sql_select()
{
  global $con;
  global $ordergroup;
  global $ordtypegroup;
  global $filter;
  global $filterfield;
  global $wholeonly;
  global $dbprefix;

  $filterstr = sqlstr($filter);
  if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
  $sql = "SELECT * FROM `".$dbprefix."groupidaccount`";

  if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
    $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
  } elseif (isset($filterstr) && $filterstr!='') {
    $sql .= " where (`GroupID` like '" .$filterstr ."') or (Teamcode like '" .$filterstr ."') or (`Naam` like '" .$filterstr ."') or (`Klasse` like '" .$filterstr ."') or (`Wedstrijdduur` like '" .$filterstr ."') or (`Periode` like '" .$filterstr ."') or (`Ophalen` like '" .$filterstr ."') or (`GroupID` like '" .$filterstr ."') or (`DatumTijd-Update` like '" .$filterstr ."')";
  }
  if (isset($ordergroup) && $ordergroup!='') $sql .= " order by `" .sqlstr($ordergroup) ."`";
  if (isset($ordtypegroup) && $ordtypegroup!='') $sql .= " " .sqlstr($ordtypegroup);
  $res = mysqli_query($con,$sql) or die(mysqli_error($con));
  return $res;
}

function sql_getrecordcount()
{
  global $con;
  global $ordergroup;
  global $ordtypegroup;
  global $filter;
  global $filterfield;
  global $wholeonly;
  global $dbprefix;

  $filterstr = sqlstr($filter);
  if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
  $sql = "SELECT COUNT(*) FROM `".$dbprefix."groupidaccount`";
  if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
    $sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
  } elseif (isset($filterstr) && $filterstr!='') {
    $sql .= " where (`GroupaccountID` like '" .$filterstr ."') or (Teamcode like '" .$filterstr ."') or (`Naam` like '" .$filterstr ."') or (`Klasse` like '" .$filterstr ."') or (`Wedstrijdduur` like '" .$filterstr ."') or (`Periode` like '" .$filterstr ."') or (`Ophalen` like '" .$filterstr ."') or (`GroupID` like '" .$filterstr ."') or (`DatumTijd-Update` like '" .$filterstr ."')";
  }
  $res = mysqli_query($con,$sql) or die(mysqli_error($con));
  $row = mysqli_fetch_assoc($res);
  reset($row);
  return current($row);
}

function sql_insert()
{

  global $con;
  global $_POST;
  global $dbprefix;

  $sql = "insert into `".$dbprefix."groupidaccount` (GroupID, Gebruikersnaam, Wachtwoord) values (" .sqlvalue(@$_POST["GroupID"], true).", " .sqlvalue(@$_POST["Gebruikersnaam"], true).", " .sqlvalue(@$_POST["Wachtwoord"], true).")";
//  mysqli_query($con,$sql) or die(mysqli_error($con));
  mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));

}

function sql_update()
{

  global $con;
  global $_POST;
  global $dbprefix;

  $sql = "update `".$dbprefix."groupidaccount` set GroupID=" .sqlvalue(@$_POST["GroupID"], true).", `Gebruikersnaam`=" .sqlvalue(@$_POST["Gebruikersnaam"], true).", `Wachtwoord`=" .sqlvalue(@$_POST["Wachtwoord"], true)." where " .primarykeycondition();
  mysqli_query($con,$sql) or die(mysqli_error($con));
}

function sql_delete()
{
  global $con;
  global $dbprefix;

  $sql = "delete from `".$dbprefix."groupidaccount` where " .primarykeycondition();
  mysqli_query($con,$sql) or die(mysqli_error($con));
}
function primarykeycondition()
{
  global $_POST;
  $pk = "";
  $pk .= "(`GroupaccountID`";
  if (@$_POST["xGroupaccountID"] == "") {
    $pk .= " IS NULL";
  }else{
  $pk .= " = " .sqlvalue(@$_POST["xGroupaccountID"], false);
  };
  $pk .= ")";
  return $pk;
}
 ?>
