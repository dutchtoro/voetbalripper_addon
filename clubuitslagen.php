<?php 
// clubuitslagen.php
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl 
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl 
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van 
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel

// clubuitslagen volledige lijst, uit en thuis gesplitst, sortering optie.


include("config.php"); 
include('functies.php');
mysql_connect($server,$username,$password); 
@mysql_select_db($database) or die( "Unable to select database"); 

$playdate = $_GET['datum']; //alleen numerieke waarde toegstaan - voorkomt mogelijkheid tot sql injection
$playdate = $jumi[0];
If ($playdate == "") $playdate = "%";

$query1 = "SELECT DISTINCT Uit, Thuis, Uitslag, Datum, DATE_FORMAT(Datum, '%Y-%m-%d') AS date_for_sort  
 FROM `".$dbprefix."clubuitslagen` WHERE Thuis LIKE '$club1' and Datum >= DATE_SUB(CURRENT_DATE, INTERVAL '$MinDagUitslagen' DAY) 
 UNION 
 SELECT DISTINCT Uit, Thuis, Uitslag, Datum, DATE_FORMAT(Datum, '%Y-%m-%d') AS date_for_sort  
 FROM `".$dbprefix."oefenprogramma` WHERE Thuis LIKE '$club1' and Datum >= DATE_SUB(CURRENT_DATE, INTERVAL '$MinDagUitslagen' DAY) AND Uitslag NOT LIKE 'ng' ORDER BY date_for_sort ASC, Thuis, Uit";  
$result1=mysql_query($query1); 
$num1=mysql_numrows($result1);

$query2 = "SELECT DISTINCT Uit, Thuis, Uitslag, Datum, DATE_FORMAT(Datum, '%Y-%m-%d' ) AS date_for_sort  
 FROM `".$dbprefix."clubuitslagen` WHERE Uit LIKE '$club1' and Datum >= DATE_SUB(CURRENT_DATE, INTERVAL '$MinDagUitslagen' DAY)  
 UNION 
 SELECT DISTINCT Uit, Thuis, Uitslag, Datum, DATE_FORMAT(Datum, '%Y-%m-%d' ) AS date_for_sort  
 FROM `".$dbprefix."oefenprogramma` WHERE Uit LIKE '$club1' and Datum >= DATE_SUB(CURRENT_DATE, INTERVAL '$MinDagUitslagen' DAY) AND Uitslag NOT LIKE 'ng' ORDER BY date_for_sort ASC, Thuis, Uit";
$result2=mysql_query($query2); 
$num2=mysql_numrows($result2);

$query3 = "SELECT * FROM clubnaam WHERE ClubID='1'"; 
$result3=mysql_query($query3); 

$query4 = "SHOW TABLE STATUS from ".$database." LIKE '".$dbprefix."clubuitslagen'"; 
$result4=mysql_query($query4);


//Aantal uitslagen tellen met *
$result5  = mysql_query("SELECT `Uitslag` FROM `".$dbprefix."clubuitslagen` where `Uitslag` like '%*'");
$numrows5 = mysql_num_rows($result5);


mysql_close(); 


?> 
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">  

    <head>  

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />  

        <title>Uitslagen <?php echo $clubnaam; ?></title>   

<?php echo "<link rel=\"stylesheet\" type=\"text/css\" href='$CSSweergave' />"; ?>

</head>  
      
<body>  
<div style="text-align:center"><br/>
 	<h2>Uitslagen Thuiswedstrijden <?php echo $clubnaam; ?></h2> </div> 
<div style="text-align:center">
<table class="uitslagen"> 
<tr> 
<th class="left" style="width:55px">Datum</th> 
<th class="left" style="width:170px">Thuis</th> 
<th class="left" style="width:170px">Uit</th> 
<th class="left" style="width:70px">Uitslag</th> 
</tr> 

<?php 
$rowclass = 0; 
$i=0; 
while ($i < $num1) { 

$datum=makeNiceDate(mysql_result($result1,$i,"Datum")); 
$thuis=mysql_result($result1,$i,"Thuis"); 
$uit=mysql_result($result1,$i,"Uit"); 
$uitslag=mysql_result($result1,$i,"Uitslag"); 
 
?> 

<tr> 
<td class="row<?php echo $rowclass ?>"><?php echo $datum; ?></td>  
<td class="row<?php echo $rowclass ?>"><?php echo $thuis; ?></td> 
<td class="row<?php echo $rowclass ?>"><?php echo $uit; ?></td> 
<td class="row<?php echo $rowclass ?>"><?php echo $uitslag; ?></td> 
</tr> 

<?php 
$i++; 
$rowclass = 1 - $rowclass; 

} 
IF ($num1==0) { ?>
<tr> 
<td class="center" colspan="4"><br /><b><?php echo 'Er zijn geen uitslagen bekend'; ?></b><br /></td>
</tr>
<?php }

echo "</table>"; 
?>

<h2>Uitslagen Uitwedstrijden <?php echo $clubnaam; ?></h2> </div> 
<div style="text-align:center">
<table class="uitslagen"> 
<tr> 
<th class="left" style="width:55px">Datum</th> 
<th class="left" style="width:170px">Thuis</th> 
<th class="left" style="width:170px">Uit</th> 
<th class="left" style="width:70px">Uitslag</th>  
</tr> 

<?php 
$rowclass = 0; 
$i=0; 
while ($i < $num2) { 

$datum=makeNiceDate(mysql_result($result2,$i,"Datum")); 
$thuis=mysql_result($result2,$i,"Thuis"); 
$uit=mysql_result($result2,$i,"Uit"); 
$uitslag=mysql_result($result2,$i,"Uitslag"); 

?>  
 
<tr> 
<td class="row<?php echo $rowclass ?>"><?php echo $datum; ?></td>  
<td class="row<?php echo $rowclass ?>"><?php echo $thuis; ?></td> 
<td class="row<?php echo $rowclass ?>"><?php echo $uit; ?></td> 
<td class="row<?php echo $rowclass ?>"><?php echo $uitslag; ?></td> 
</tr> 

<?php 
$i++; 
$rowclass = 1 - $rowclass; 

} 
IF ($num2==0) { ?>
<tr> 
<td class="center" colspan="4"><br /><b><?php echo 'Er zijn geen uitslagen bekend'; ?></b><br /></td>
</tr>
<?php }

echo "</table>"; 
?> 
<table class="uitslagen"> 
<tr>
<td>
<?php if ($numrows5  > '0') echo "<td class='small'><br />Uitslagen met een * zijn nog niet verwerkt. <br /></td>"; ?>
</td>    
</tr>
</table>
<table class="uitslagen"> 
    <tr> 
            <td class="small"><br />Bijgewerkt op: <?php  
            setlocale(LC_ALL, 'nl_NL'); 
            echo strftime('%d/%m/%y - %H:%M', strtotime(mysql_result($result4,0,'Update_time'))); ?></td> 
    </tr> 
      <tr>
    	 <td class="left"><br />Bron: <a href='http://www.voetbal.nl' target='_blank'>Voetbal.nl</a></td>
    </tr>   

</table>

</div> 
</body> 
</html>