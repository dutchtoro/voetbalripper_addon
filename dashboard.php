<?php session_start();
// dashboard.php 1.9.7.1 (07-11-2012) Bugfixes en kleine cosmetische aanpassingen.
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel, janneman

error_reporting(E_ERROR | E_WARNING | E_PARSE);


include("config.php");
include("functies.php");
// mysql_connect($server,$username,$password);
// @mysql_select_db($database) or die( "Unable to select database");

global $dbprefix;
$tbl_name=$dbprefix."teamlinks"; // Table name
$filter = "";


if (isset($_GET["orderdb"])) $orderdb = @$_GET["orderdb"];
if (isset($_GET["typedb"])) $ordtypedb = @$_GET["typedb"];

if (isset($_POST["filter"])) $filter = @$_POST["filter"];
if (isset($_POST["filter_field"])) $filterfield = @$_POST["filter_field"];
$wholeonly = false;
if (isset($_POST["wholeonly"])) $wholeonly = @$_POST["wholeonly"];

if (!isset($orderdb) && isset($_SESSION["orderdb"])) $orderdb = $_SESSION["orderdb"];
if (!isset($ordtypedb) && isset($_SESSION["typedb"])) $ordtypedb = $_SESSION["typedb"];
if (!isset($filter) && isset($_SESSION["filter"])) $filter = $_SESSION["filter"];
if (!isset($filterfield) && isset($_SESSION["filter_field"])) $filterfield = $_SESSION["filter_field"];
if (isset($_POST['Submit']))
{	//echo "<p>submit pressed";


	foreach($_POST['TeamID'] as $id)
	{

		$_POST["Naam".$id] = htmlentities($_POST["Naam".$id], ENT_QUOTES);
		$sql1="UPDATE ".$tbl_name." SET Teamcode='".$_POST["Teamcode".$id]."', AltTeamcode='".$_POST["AltTeamcode".$id]."', Naam='".$_POST["Naam".$id]."', Klasse='".$_POST["Klasse".$id]."', Wedstrijdtype='".$_POST["Wedstrijdtype".$id]."', Wedstrijdduur='".$_POST["Wedstrijdduur".$id]."', Periode='".$_POST["Periode".$id]."', Ophalen='".$_POST["Ophalen".$id]."', GroupID='".$_POST["GroupID".$id]."' WHERE TeamID='".$id."'";
		//$result1=mysqli_query($sql1) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error());
		// mysqli_query($sql1) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error());
		$result1=mysqli_query($con,$sql1) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
	}
}
?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<script type='text/javascript'>
function viewprog()
{
	window.open("programma.php","_blank","toolbar=no, location=no, menubar=no, resizable=yes, scrollbars=Yes, adresbar=no, status=no");
}
function updateprog()
{
	window.open("index.php?teamID=0","_blank","toolbar=no, location=no, menubar=no, resizable=yes, scrollbars=Yes, adresbar=no, status=no");
}
</script>

<script type='text/javascript'>
function viewuitslag()
{
	window.open("clubuitslagen.php","_blank","toolbar=no, location=no, menubar=no, resizable=yes, scrollbars=Yes, adresbar=no, status=no");
}
function updateuitslag()
{
	window.open("index.php?teamID=999","_blank","toolbar=no, location=no, menubar=no, resizable=yes, scrollbars=Yes, adresbar=no, status=no");
}
</script>

<script type='text/javascript'>
function editgid(group)
{
        window.open('index.php?groupID='+group, '_blank', 'toolbar=no, location=no, menubar=no, resizable=yes, scrollbars=yes, adresbar=no, status=no');
}
function editgid3(group)
{
        window.open('teamindindex.php?groupID='+group, '_blank', 'toolbar=no, location=no, menubar=no, resizable=yes, scrollbars=yes, adresbar=no, status=no');
}
function viewteam(team)
{
	window.open('team.php?teamID='+team, '_blank', 'toolbar=no, location=no, menubar=no, resizable=yes, scrollbars=yes, adresbar=no, status=no');
}
function updateteam(team)
{
	window.open('index.php?teamID='+team, '_blank', 'toolbar=no, location=no, menubar=no, resizable=yes, scrollbars=yes, adresbar=no, status=no');
}
function viewteamindeling(team)
{
	window.open('teamindeling.php?teamID='+team, '_blank', 'toolbar=no, location=no, menubar=no, resizable=yes, scrollbars=yes, adresbar=no, status=no');
}
function updateteamindeling(team)
{
        window.open('teamindindex.php?teamID='+team, '_blank', 'toolbar=no, location=no, menubar=no, resizable=yes, scrollbars=yes, adresbar=no, status=no');
}
</script>
<title>Club DashBoard <?php echo $clubnaam ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php echo "<link rel=\"stylesheet\" type=\"text/css\" href='$CSSadmin'>"; ?>
</head>
<body>
<table class="bdadmin" style="width:100%"><tr><td class="hr"><h2>Club DashBoard <?php echo $clubnaam ?></h2></td></tr></table>

<?php
if (!login()) exit;
?>
<div style="float: right"><a href="dashboard.php?a=logout">[ Uitloggen ]</a></div>
<br>
<?php
$con = connect();
$showrecs = 30;
$pagerange = 10;

$a = @$_GET["a"];
if ($a == "import")
{
	echo "Importeren";
	$ch = curl_init();


	// Inloggen op voetbal.nl
	login_voetbalnl($gebruikersnaam, $wachtwoord);

	// Nu bekijken of we een melding krijgen dat er ingelogd moet worden als we een url aanroepen. Ja dan is het inloggen fout gegaan.
	$inlog = check_inlog("http://pupillen.voetbal.nl/clubs-competities/html/clubs/programma/".$club);
	if ($inlog == "Log in of registreer gratis!")
	{
		echo "Probleem met Voetbal.nl ripper.<br><br>";
		echo "Het inloggen op de site is niet gelukt. <strong>Script is gestopt.</strong><br>";
		die;
	}else
	{
		//Geen probleem met inloggen dus doorgaan.
	}

	//teams ophalen
	//  $teams = get_teams("http://pupillen.voetbal.nl/clubs-competities/html/clubs/teams/".$club);
	$teams = get_teams("http://pupillen.voetbal.nl/clubs-competities/mijn/poules/");


	//teams opslaan
	opslaan_teams($teams);


}

if ($a == "reorder")
{
	reorderdb($dbprefix."teamlinks","TeamID");
}

if ($a == "cleartabel")
{
	leegmakendbcp($dbprefix."teamlinks");
}


$recid = @$_GET["recid"];
$page = @$_GET["page"];
if (!isset($page)) $page = 1;

$sql = @$_POST["sql"];

switch ($sql) {
case "insert":
	sql_insert();
	break;
case "update":
	sql_update();
	break;
case "delete":
	sql_delete();
	break;
}

switch ($a) {
case "add":
	addrec();
	break;
case "edit":
	editrec($recid);
	break;
case "del":
	deleterec($recid);
	break;
default:
	select();
	break;
}

if (isset($orderdb)) $_SESSION["orderdb"] = $orderdb;
if (isset($ordtypedb)) $_SESSION["typedb"] = $ordtypedb;
if (isset($filter)) $_SESSION["filter"] = $filter;
if (isset($filterfield)) $_SESSION["filter_field"] = $filterfield;
if (isset($wholeonly)) $_SESSION["wholeonly"] = $wholeonly;

mysqli_close($con);
?>
<table class="bdadmin" style="width:100%"><tr><td class="hr">Gemaakt door Yarro/Johnvs</td></tr></table>
<div style="float: left"><a href="instellingen.php">[ Instellingen ]</a></div><br>
<div style="float: left"><a href="admin.php">[ Beheer Teams ]</a></div><br>
<div style="float: left"><a href="groupid.php">[ Group ID accounts ]</a></div><br>
<div style="float: left"><a href="oefenprogramma.php">[ Oefen Programma/Uitslagen ]</a></div><br>
<div style="float: left"><a href="extra.php">[ Extra informatie Club Programma ]</a></div><br>
<div style="float: left"><a href="uitslag.php">[ Uitslagen invoeren voor Club / Team ]</a></div><br>
<div style="float: left"><a href="kantine.php">[ Kantinebezetting beheren ]</a></div><br>

</body>
</html>

<?php function select()
{
	global $a;
	global $showrecs;
	global $page;
	global $filter;
	global $filterfield;
	global $wholeonly;
	global $orderdb;
	global $ordtypedb;


	if ($a == "reset") {
		$filter = "";
		$filterfield = "";
		$wholeonly = "";
		$orderdb = "";
		$ordtypedb = "";
	}

	$checkstr = "";
	if ($wholeonly) $checkstr = " checked";
	if ($ordtypedb == "asc") { $ordtypedbstr = "desc"; } else { $ordtypedbstr = "asc"; }
	$res = sql_select();
	$count = sql_getrecordcount();
	if ($count % $showrecs != 0) {
		$pagecount = intval($count / $showrecs) + 1;
	}
	else {
		$pagecount = intval($count / $showrecs);
	}
	$startrec = $showrecs * ($page - 1);
	if ($startrec < $count) {mysqli_data_seek($res, $startrec);}
	$reccount = min($showrecs * $page, $count);


	?>

	<table class="bdadmin">
	</table>
	<br>
	<?php
	if ($page < 2)
	{
		include("config.php");
		?>
		<table class="bdadmin" style="width:100%"><tr><td class="hr"><h3>Club Programma / Uitslagen</h3></td></tr></table>
		<table class="tbl" style="width:100%">
		<tr>
		<td class="hr">&nbsp;</td>
		<td class="hr">&nbsp;</td>
		<td class="hr"><a class="hr"><?php echo htmlspecialchars("(Team)ID") ?></a></td>
		<td class="hr"><a class="hr"><?php echo htmlspecialchars("Naam") ?></a></td>
		<td class="hr"><a class="hr"><?php echo htmlspecialchars("Clubcode") ?></a></td>
		<td class="hr"><a class="hr"><?php echo htmlspecialchars("AltClubcode") ?></a></td>
		</tr>

		<?php
		$style = "dr";
		$bstyle = "style=\"background-color: #FFFFFF; border: 1px solid #D0D0D0;\"";
		?>
		<tr>
		<td class="<?php echo $style ?>"><a href="#" onclick="viewprog();">Bekijk</a></td>
		<td class="<?php echo $style ?>"><a href="#" onclick="updateprog();">Update</a></td>

		<td class="<?php echo $style ?>">0</td>
		<td class="<?php echo $style ?>">Programma - <?php echo $clubnaam ?> </td>
		<td class="<?php echo $style ?>"><?php echo $club ?> </td>
		<td class="<?php echo $style ?>"><?php echo $altclub ?> </td>
		</tr>

		<tr>

		<?php

		$style = "sr";
		$bstyle = "style=\"background-color: #A6D2FF; border: 1px solid #D0D0D0;\"";
		?>

		<td class="<?php echo $style ?>"><a class="<?php echo $style ?>" href="#" onclick="viewuitslag();">Bekijk</a></td>
		<td class="<?php echo $style ?>"><a class="<?php echo $style ?>" href="#" onclick="updateuitslag();">Update</a></td>

		<td class="<?php echo $style ?>">999</td>
		<td class="<?php echo $style ?>">Uitslagen - <?php echo $clubnaam ?> </td>
		<td class="<?php echo $style ?>"><?php echo $club ?> </td>
		<td class="<?php echo $style ?>"><?php echo $altclub ?> </td>

		</table>
		<hr />
		<br>

		<table class="bdadmin" style="width:100%"><tr><td class="hr"><h3>Group ID</h3></td></tr></table>
		<table class="tbl" style="width:100%">
		<tr>
		<td class="hr"><a class="hr"><?php echo htmlspecialchars("Team Uitslagen en Programma") ?></a></td>
		<td class="hr"><a class="hr"><?php echo htmlspecialchars("Team Indeling en Speelronde") ?></a></td>
		<td class="hr"><a class="hr"><?php echo htmlspecialchars("GroupID") ?></a></td>
		</tr>

		<?php

		// --------
		global $con;
		global $orderdb;
		global $ordtypedb;
		global $filter;
		global $filterfield;
		global $wholeonly;
		global $dbprefix;




		$query2 = "SELECT GroupID, COUNT(TeamCode) FROM `".$dbprefix."teamlinks` GROUP BY GroupID";
		$result2 = mysqli_query($con,$query2) or die(mysqli_error($con));

		// Print out result



		$result2num=mysqli_num_rows($result2);


		for ($i2 = 0; $i2 < $result2num; $i2++)
		{
			$row = mysqli_fetch_assoc($result2);
			$style = "dr";
			$bstyle = "style=\"background-color: #FFFFFF; border: 1px solid #D0D0D0;\"";
			if ($i2 % 2 != 0) {
				$style = "sr";
				$bstyle = "style=\"background-color: #A6D2FF; border: 1px solid #D0D0D0;\"";

			}


			?>
			<tr>
			<td class="<?php echo $style ?>"><a href="#" onclick="editgid(<?php echo $row['GroupID'] ?>);">Update</a></td>
			<td class="<?php echo $style ?>"><a href="#" onclick="editgid3(<?php echo $row['GroupID'] ?>);">Update</a></td>
			<td class="<?php echo $style ?>"><?php echo "GroupID - ". $row['GroupID'] ?><br /></td>


			</tr>
			<?php
		}

	}



	?>

	</table>
	<hr />
	&nbsp;

	<table class="bdadmin" style="width:100%"><tr><td class="hr"><h3>Teams</h3></td></tr></table>

	<form action="dashboard.php" method="post">
	<table class="bdadmin">
	<tr>
	<td><b>Filter</b>&nbsp;</td>
	<td><input type="text" name="filter" value="<?php echo $filter ?>"></td>
	<td><select name="filter_field">
	<option value="">Alle velden</option>
	<option value="<?php echo "TeamID" ?>"<?php if ($filterfield == "TeamID") { echo "selected"; } ?>><?php echo htmlspecialchars("TeamID") ?></option>
	<option value="<?php echo "Teamcode" ?>"<?php if ($filterfield == "Teamcode") { echo "selected"; } ?>><?php echo htmlspecialchars("Teamcode") ?></option>
	<option value="<?php echo "AltTeamcode" ?>"<?php if ($filterfield == "AltTeamcode") { echo "selected"; } ?>><?php echo htmlspecialchars("AltTeamcode") ?></option>
	<option value="<?php echo "Naam" ?>"<?php if ($filterfield == "Naam") { echo "selected"; } ?>><?php echo htmlspecialchars("Naam") ?></option>
	<option value="<?php echo "GroupID" ?>"<?php if ($filterfield == "GroupID") { echo "selected"; } ?>><?php echo htmlspecialchars("GroupID") ?></option>
	<option value="<?php echo "Datum/Tijd-Update" ?>"<?php if ($filterfield == "DatumTijd-Update") { echo "selected"; } ?>><?php echo htmlspecialchars("Datum/Tijd-Update") ?></option>
	</select></td>
	<td><input type="checkbox" name="wholeonly"<?php echo $checkstr ?>>Alleen hele woorden</td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	<td><input type="submit" name="action" value="Filter toepassen"></td>
	<td><a href="dashboard.php?a=reset">Reset Filter</a></td>
	</tr>
	</table>
	</form>

	<?php showpagenav($page, $pagecount); ?>
	<br>
	<form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF'];  ?>">
	<table class="tbl" style="width:100%">
	<tr>
	<td class="hr">Team</td>
	<td class="hr">Team</td>
	<td class="hr">Indeling-Ronde</td>
	<td class="hr">Indeling-Ronde</td>
	<td class="hr"><a class="hr" href="dashboard.php?orderdb=<?php echo "TeamID" ?>&amp;typedb=<?php echo $ordtypedbstr ?>"><?php echo htmlspecialchars("TeamID") ?></a></td>
	<td class="hr"><a class="hr" href="dashboard.php?orderdb=<?php echo "Naam" ?>&amp;typedb=<?php echo $ordtypedbstr ?>"><?php echo htmlspecialchars("Naam") ?></a></td>
	<td class="hr"><a class="hr" href="dashboard.php?orderdb=<?php echo "Klasse" ?>&amp;typedb=<?php echo $ordtypedbstr ?>"><?php echo htmlspecialchars("Klasse") ?></a></td>
	<td class="hr"><a class="hr" href="dashboard.php?orderdb=<?php echo "GroupID" ?>&amp;typedb=<?php echo $ordtypedbstr ?>"><?php echo htmlspecialchars("GroupID") ?></a></td>
	<td class="hr"><a class="hr" href="dashboard.php?orderdb=<?php echo "Teamcode" ?>&amp;typedb=<?php echo $ordtypedbstr ?>"><?php echo htmlspecialchars("Teamcode") ?></a></td>
	<td class="hr"><a class="hr" href="dashboard.php?orderdb=<?php echo "AltTeamcode" ?>&amp;typedb=<?php echo $ordtypedbstr ?>"><?php echo htmlspecialchars("AltTeamcode") ?></a></td>
	<td class="hr"><a class="hr" href="dashboard.php?orderdb=<?php echo "DatumTijd-Update" ?>&amp;typedb=<?php echo $ordtypedbstr ?>"><?php echo htmlspecialchars("DatumTijd-Update") ?></a></td>

	</tr>

	<?php  include("config.php"); ?>






	<?php
	for ($i = $startrec; $i < $reccount; $i++)
	{
		$row = mysqli_fetch_assoc($res);
		$style = "dr";
		$bstyle = "style=\"background-color: #FFFFFF; border: 1px solid #D0D0D0;\"";
		if ($i % 2 != 0) {
			$style = "sr";
			$bstyle = "style=\"background-color: #A6D2FF; border: 1px solid #D0D0D0;\"";

		}
		?>



		<tr>
		<td class="<?php echo $style ?>"><a href="#" onclick="viewteam(<?php echo $row["TeamID"] ?>);">Bekijk</a></td>
		<td class="<?php echo $style ?>"><a href="#" onclick="updateteam(<?php echo $row["TeamID"] ?>);">Update</a></td>
		<td class="<?php echo $style ?>"><a href="#" onclick="viewteamindeling(<?php echo $row["TeamID"] ?>);">Bekijk</a></td>
		<td class="<?php echo $style ?>"><a href="#" onclick="updateteamindeling(<?php echo $row["TeamID"] ?>);">Update</a></td>

		<td class="<?php echo $style ?>"><input type="hidden" name="TeamID[]" value="<?php echo $row['TeamID']; ?>" /><?php echo $row['TeamID']; ?></td>
		<td class="<?php echo $style ?>"><?php echo ($row["Naam"]) ?></td>
		<td class="<?php echo $style ?>"><?php echo ($row["Klasse"]) ?></td>
		<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["GroupID"]) ?></td>
		<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Teamcode"]) ?></td>
		<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["AltTeamcode"]) ?></td>
		
		<td class="<?php echo $style ?>"><?php echo htmlspecialchars(date("d/m/y : H:i:s",($row["DatumTijd-Update"]))) ?></td>
		</tr>
		<?php
	}

	mysqli_free_result($res);
	?>
	<tr>
	<td class="center" colspan="8"></td>
	</tr>

	</table>
	</form>
	<br />

	<?php showpagenav($page, $pagecount); ?>
	<?php } ?>

<?php function login()
{
	include("config.php");
	global $_POST;
	global $_SESSION;

	global $_GET;
	if (isset($_GET["a"]) && ($_GET["a"] == 'logout')) $_SESSION["logged_in_dashboard"] = false;
	if (!isset($_SESSION["logged_in_dashboard"])) $_SESSION["logged_in_dashboard"] = false;
	if (!$_SESSION["logged_in_dashboard"]) {
		$login = "";
		$password2 = "";
		if (isset($_POST["login"])) $login = @$_POST["login"];
		if (isset($_POST["password"])) $password2 = @$_POST["password"];

		if (($login != "") && ($password2 != "")) {
			if (($login == $Dbusername) && ($password2 == $Dbpassword)) {
				$_SESSION["logged_in_dashboard"] = true;
			}
			else {
				?>
				<p><b><font color="-1">De combinatie gebruikersnaam/wachtwoord is onjuist</font></b></p>
				<?php } } }if (isset($_SESSION["logged_in_dashboard"]) && (!$_SESSION["logged_in_dashboard"])) { ?>
		<form action="dashboard.php" method="post">
		<table class="bdadmin">
		<tr>
		<td>Gebruikersnaam</td>
		<td><input type="text" name="login" value="<?php echo $login ?>"></td>
		</tr>
		<tr>
		<td>Wachtwoord</td>
		<td><input type="password" name="password" value="<?php echo $password2 ?>"></td>
		</tr>
		<tr>
		<td><input type="submit" name="action" value="Inloggen"></td>
		</tr>
		</table>
		</form>
		<?php
	}
	if (!isset($_SESSION["logged_in_dashboard"])) $_SESSION["logged_in_dashboard"] = false;
	return $_SESSION["logged_in_dashboard"];
} ?>

<?php function showrow($row, $recid)
{
	?>
	<table class="tbl" style="width:50%">
	<tr>
	<td class="hr"><?php echo htmlspecialchars("TeamID")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["TeamID"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Teamcode")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Teamcode"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("AltTeamcode")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["AltTeamcode"]) ?></td>
	</tr>

	<tr>
	<td class="hr"><?php echo htmlspecialchars("Naam")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Naam"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Klasse")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Klasse"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Wedstrijdtype")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Wedstrijdtype"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Wedstrijdduur")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Wedstrijdduur"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Periode")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Periode"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Ophalen")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Ophalen"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("GroupID")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["GroupID"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("DatumTijd-Update")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars(date("d/m/y : H:m:s",($row["DatumTijd-Update"]))) ?></td>
	</tr>
	</table>
	<?php } ?>

<?php function showroweditor($row, $iseditmode)
{
	global $con;
	?>
	<table class="tbl" style="width:50%">
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Teamcode")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="4" name="Teamcode"><?php echo str_replace('"', '&quot;', trim($row["Teamcode"])) ?></textarea></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("AltTeamcode")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="4" name="AltTeamcode"><?php echo str_replace('"', '&quot;', trim($row["AltTeamcode"])) ?></textarea></td>
	</tr>

	<tr>
	<td class="hr"><?php echo htmlspecialchars("Naam")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="4" name="Naam" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Naam"])) ?></textarea></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Klasse")."&nbsp;" ?></td>
	<td class="dr"><input type="text" name="Klasse" maxlength="400" value="<?php echo str_replace('"', '&quot;', trim($row["Klasse"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Wedstrijdtype")."&nbsp;" ?></td>
<?php if ($row["Wedstrijdtype"] == "Competitie") { ?>
<td class="dr"><select name="Wedstrijdtype">
<option value="Competitie" selected>Competitie</option><option value="Beker">Beker</option></select>
<?php } else { ?>
<?php } ?>
<?php if ($row["Wedstrijdtype"] == "Beker") { ?>
<td class="dr"><select name="Wedstrijdtype">
<option value="Competitie">Competitie</option><option value="Beker" selected>Beker</option></select>
<?php } else { ?>
<?php } ?>
<?php if ($row["Wedstrijdtype"] != "Competitie" and $row["Wedstrijdtype"] != "Beker") { ?>
<td class="dr"><select name="Wedstrijdtype">
<option value="Competitie" selected>Competitie</option><option value="Beker">beker</option></select>
<?php } else { ?>
<?php } ?>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Wedstrijdduur")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Wedstrijdduur" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Wedstrijdduur"])) ?>"> In minuten incl. rust</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Periode")."&nbsp;" ?></td>
	<?php if ($row["Periode"] == "ja") { ?>
		<td class="dr"><input type="checkbox" CHECKED name="Periode" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Periode"])) ?>"> Indien aangevinkt worden de periode standen voor het team opgehaald. </td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Periode" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Periode"])) ?>"> Indien aangevinkt worden de periode standen voor het team opgehaald. </td>
		<?php } ?>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Ophalen")."&nbsp;" ?></td>
	<?php if ($row["Ophalen"] == "ja") { ?>
		<td class="dr"><input type="checkbox" CHECKED name="Ophalen" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Ophalen"])) ?>"> Indien aangevinkt worden uitslagen, standen etc. voor het team opgehaald. </td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Ophalen" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Ophalen"])) ?>"> Indien aangevinkt worden uitslagen, standen etc. voor het team opgehaald. </td>
		<?php } ?>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("GroupID")."&nbsp;" ?></td>
	<td class="dr"><input type="text" name="GroupID" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["GroupID"])) ?>">  Alleen numerieke waarde. Indien ophalen per Group wordt gezet in config.php kan via index.php?groupID=* alle gegevens van de teams met de zelfde groupID worden opgehaald.</td>
</tr>


</table>
<?php } ?>

<?php function showpagenav($page, $pagecount)
{
?>
<table class="bdadmin">
<tr>

<?php if ($page > 1) { ?>
<td><a href="dashboard.php?page=<?php echo $page - 1 ?>">&lt;&lt;&nbsp;Vorige</a>&nbsp;</td>
<?php } ?>
<?php
global $pagerange;

if ($pagecount > 1) {

if ($pagecount % $pagerange != 0) {
	$rangecount = intval($pagecount / $pagerange) + 1;
}
else {
	$rangecount = intval($pagecount / $pagerange);
}
for ($i = 1; $i < $rangecount + 1; $i++) {
	$startpage = (($i - 1) * $pagerange) + 1;
	$count = min($i * $pagerange, $pagecount);

	if ((($page >= $startpage) && ($page <= ($i * $pagerange)))) {
	for ($j = $startpage; $j < $count + 1; $j++) {
		if ($j == $page) {
?>
<td><b><?php echo $j ?></b></td>
<?php } else { ?>
<td><a href="dashboard.php?page=<?php echo $j ?>"><?php echo $j ?></a></td>
<?php } } } else { ?>
<td><a href="dashboard.php?page=<?php echo $startpage ?>"><?php echo $startpage ."..." .$count ?></a></td>
<?php } } } ?>
<?php if ($page < $pagecount) { ?>
<td>&nbsp;<a href="dashboard.php?page=<?php echo $page + 1 ?>">Volgende&nbsp;&gt;&gt;</a>&nbsp;</td>
<?php } ?>
</tr>
</table>
<?php } ?>

<?php function showrecnav($a, $recid, $count)
{
?>
<table class="bdadmin">
<tr>
<td><a href="dashboard.php">Index Pagina</a></td>
<?php if ($recid > 0) { ?>
<td><a href="dashboard.php?a=<?php echo $a ?>&recid=<?php echo $recid - 1 ?>">Vorige team</a></td>
<?php } if ($recid < $count - 1) { ?>
<td><a href="dashboard.php?a=<?php echo $a ?>&recid=<?php echo $recid + 1 ?>">Volgende team</a></td>
<?php } ?>
</tr>
</table>
<hr />
<?php } ?>

<?php function addrec()
{
?>
<table class="bdadmin">
<tr>
<td><a href="dashboard.php">Index Pagina</a></td>
</tr>
</table>
<hr />
<form enctype="multipart/form-data" action="dashboard.php" method="post">
<p><input type="hidden" name="sql" value="insert"></p>
<?php
$row = array(
"TeamID" => "",
"Teamcode" => "",
"AltTeamcode" => "",
"Naam" => "",
"Klasse" => "",
"Wedstrijdtype" => "",
"Wedstrijdduur" => "");
showroweditor($row, false);
?>
<p><input type="submit" name="action" value="Toevoegen"></p>
</form>
<?php } ?>

<?php function deleterec($recid)
{
echo "xxxxxxxxxx";
echo $recid;
?>
<br>

<?php

} ?>


<?php function connect()
{
include("config.php");
//$con = mysql_connect($server,$username,$password);
//mysql_select_db($database);
$con=mysqli_connect($server,$username,$password,$database);
return $con;
}

function sqlvalue($val, $quote)
{
if ($quote)
	$tmp = sqlstr($val);
else
	$tmp = $val;
if ($tmp == "")
	$tmp = "NULL";
elseif ($quote)
	$tmp = "'".$tmp."'";
return $tmp;
}

function sqlstr($val)
{
return str_replace("'", "''", $val);
}

function sql_select()
{
global $con;
global $orderdb;
global $ordtypedb;
global $filter;
global $filterfield;
global $wholeonly;
global $dbprefix;

$filterstr = sqlstr($filter);
if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
$sql = "SELECT `TeamID`, `Teamcode`, `AltTeamcode`, `Naam`, `Klasse`, `Wedstrijdtype`, `Wedstrijdduur`, `Periode`,`Ophalen`, `GroupID`, `DatumTijd-Update` FROM `".$dbprefix."teamlinks`";
if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
	$sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
} elseif (isset($filterstr) && $filterstr!='') {
	$sql .= " where (`TeamID` like '" .$filterstr ."') or (Teamcode like '" .$filterstr ."') or (AltTeamcode like '" .$filterstr ."') or (`Naam` like '" .$filterstr ."') or (`Klasse` like '" .$filterstr ."') or (`Wedstrijdtype` like '" .$filterstr ."') or (`Wedstrijdduur` like '" .$filterstr ."') or (`Periode` like '" .$filterstr ."') or (`Ophalen` like '" .$filterstr ."') or (`GroupID` like '" .$filterstr ."') or (`DatumTijd-Update` like '" .$filterstr ."')";
}
if (isset($orderdb) && $orderdb!='') $sql .= " order by `" .sqlstr($orderdb) ."`";
if (isset($ordtypedb) && $ordtypedb!='') $sql .= " " .sqlstr($ordtypedb);
$res = mysqli_query($con,$sql) or die(mysqli_error($con));
return $res;
}

function sql_getrecordcount()
{
global $con;
global $orderdb;
global $ordtypedb;
global $filter;
global $filterfield;
global $wholeonly;
global $dbprefix;

$filterstr = sqlstr($filter);
if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
$sql = "SELECT COUNT(*) FROM `".$dbprefix."teamlinks`";
if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
	$sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
} elseif (isset($filterstr) && $filterstr!='') {
	$sql .= " where (`TeamID` like '" .$filterstr ."') or (Teamcode like '" .$filterstr ."') or (AltTeamcode like '" .$filterstr ."') or (`Naam` like '" .$filterstr ."') or (`Klasse` like '" .$filterstr ."') or (`Wedstrijdtype` like '" .$filterstr ."') or (`Wedstrijdduur` like '" .$filterstr ."') or (`Periode` like '" .$filterstr ."') or (`Ophalen` like '" .$filterstr ."') or (`GroupID` like '" .$filterstr ."') or (`DatumTijd-Update` like '" .$filterstr ."')";
}
$res = mysqli_query($con,$sql) or die(mysqli_error($con));
$row = mysqli_fetch_assoc($res);
reset($row);
return current($row);
}

function sql_insert()
{
if(isset($_POST["Periode"])){
	$_POST["Periode"] = "ja";
}
else{
	$_POST["Periode"] = "nee";
}

if(isset($_POST["Ophalen"])){
	$_POST["Ophalen"] = "ja";
}
else{
	$_POST["Ophalen"] = "nee";
}

global $con;
global $_POST;
global $dbprefix;
$dt="1230768001";

$sql = "insert into `".$dbprefix."teamlinks` (`Teamcode`, `AltTeamcode`, `Naam`, `Klasse`, `Wedstrijdtype`, `Wedstrijdduur`, `Periode`, `Ophalen`, `GroupID`, `DatumTijd-Update`) values (" .sqlvalue(@$_POST["Teamcode"], true).", " .sqlvalue(@$_POST["AltTeamcode"], true).", " .sqlvalue(@$_POST["Naam"], true).", " .sqlvalue(@$_POST["Klasse"], true).", " .sqlvalue(@$_POST["Wedstrijdtype"], true).", "  .sqlvalue(@$_POST["Wedstrijdduur"], true).", "  .sqlvalue(@$_POST["Periode"], true).", " .sqlvalue(@$_POST["Ophalen"], true).", " .sqlvalue(@$_POST["GroupID"], true).", " .$dt.")";
mysqli_query($con,$sql) or die(mysqli_error($con));
}

function sql_update()
{
if(isset($_POST["Periode"])){
$_POST["Periode"] = "ja";
}
else{
$_POST["Periode"] = "nee";
}

if(isset($_POST["Ophalen"])){
$_POST["Ophalen"] = "ja";
}
else{
$_POST["Ophalen"] = "nee";
}

global $con;
global $_POST;
global $dbprefix;
$dt="1230768001";

$sql = "update `".$dbprefix."teamlinks` set Teamcode=" .sqlvalue(@$_POST["Teamcode"], true).", AltTeamcode=" .sqlvalue(@$_POST["AltTeamcode"], true).", `Naam`=" .sqlvalue(@$_POST["Naam"], true).", `Klasse`=" .sqlvalue(@$_POST["Klasse"], true).", `Wedstrijdtype`=" .sqlvalue(@$_POST["Wedstrijdtype"], true).",  `Wedstrijdduur`=" .sqlvalue(@$_POST["Wedstrijdduur"], true).",  `Periode`=" .sqlvalue(@$_POST["Periode"], true) .", `Ophalen`=" .sqlvalue(@$_POST["Ophalen"], true) .", `GroupID`=" .sqlvalue(@$_POST["GroupID"], true) .", `DatumTijd-Update`=" .$dt ." where " .primarykeycondition();
mysqli_query($con,$sql) or die(mysqli_error($con));
}

function sql_delete()
{
global $con;
global $dbprefix;

$sql = "delete from `".$dbprefix."teamlinks` where " .primarykeycondition();
//  mysqli_query($sql, $con) or die(mysqli_error());
echo $sql;
}
function primarykeycondition()
{
global $_POST;
$pk = "";
$pk .= "(`TeamID`";
if (@$_POST["xTeamID"] == "") {
	$pk .= " IS NULL";
}else{
$pk .= " = " .sqlvalue(@$_POST["xTeamID"], false);
};
$pk .= ")";
return $pk;
}
?>
