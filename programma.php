<?php 
// programma.php
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl 
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl 
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van 
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel
 

// programma volledige lijst, uit en thuis gesplitst, sortering optie met Filter

ini_set('display_errors', 1);
//Voorkom vreemde resultaten door netjes te programmeren.
//error_reporting(E_ALL);
error_reporting(E_ERROR | E_WARNING | E_PARSE);

include("config.php"); 
include('functies.php');
mysql_connect($server,$username,$password); 
@mysql_select_db($database) or die( "Unable to select database");

  if (isset($_GET["order"])) $order = @$_GET["order"];
  if (isset($_GET["type"])) $ordtype = @$_GET["type"];

  if (isset($_POST["filter"])) $filter = @$_POST["filter"];
  if (isset($_POST["filter_field"])) $filterfield = @$_POST["filter_field"];
  $wholeonly = false;
  if (isset($_POST["wholeonly"])) $wholeonly = @$_POST["wholeonly"];

  if (!isset($order) && isset($_SESSION["order"])) $order = $_SESSION["order"];
  if (!isset($ordtype) && isset($_SESSION["type"])) $ordtype = $_SESSION["type"];
  if (!isset($filter) && isset($_SESSION["filter"])) $filter = $_SESSION["filter"];
  if (!isset($filterfield) && isset($_SESSION["filter_field"])) $filterfield = $_SESSION["filter_field"]; 

// $querya = "SELECT Thuis FROM `".$dbprefix."clubprogramma` WHERE Thuis like '$club1'";

$querya = "SELECT DISTINCT Thuis, '' AS Uit FROM `".$dbprefix."clubprogramma` WHERE Thuis Like '$club1' 
UNION
SELECT DISTINCT '' AS  Thuis, Uit FROM `".$dbprefix."clubprogramma` WHERE Uit like '$club1'";


$result=mysql_query($querya) or die(mysql_error());
$numr=mysql_numrows($result);

IF ($numr>0) {

while( $row = mysql_fetch_array($result) )
{
$array[] = $row[0];
$array[] = $row[1];
}
$array = array_unique($array);
sort($array);
}
else
{ }
  global $filter;
  global $filterfield;
 if (isset($filter)) $_SESSION["filter"] = $filter;
  if (isset($filterfield)) $_SESSION["filter_field"] = $filterfield;

function sqlstr($val)
{
  return str_replace("'", "''", $val);
}

 $filterstr = sqlstr($filter);

If ($filterfield == "") $filterfield = $club1;
$filterfield = htmlentities($filterfield, ENT_QUOTES); // voor het geval een apastrofe in de naam zit


If ($SortCP == "Datum-Tijd-Team") $outputsortTh = "Datum, Tijd, Thuis ASC"; // Sorteer op Datum, Tijd, Teamnaam
If ($SortCP == "Datum-Tijd-Team") $outputsortUi = "Datum, Tijd, Uit ASC"; // Sorteer op Datum, Tijd, Teamnaam

If ($SortCP == "Team-Datum-Tijd") $outputsortTh = "Thuis, Datum, Tijd ASC"; // Sorteer op Teamnaam, Datum, Tijd
If ($SortCP == "Team-Datum-Tijd") $outputsortUi = "uit, Datum, Tijd ASC"; // Sorteer op Teamnaam, Datum, Tijd

If ($SortCP == "Datum-Team-Tijd") $outputsortTh = "Datum, Thuis, Tijd ASC"; // Sorteer op Datum, Teamnaam, Tijd
If ($SortCP == "Datum-Team-Tijd") $outputsortUi = "Datum, Uit, Tijd ASC"; // Sorteer op Datum, Teamnaam, Tijd

if ($CombCpAfg == "Uit")
// Alleen wedstrijden uit clubprogramma en oefenprogramma worden getoond
{

$query1 = "SELECT DISTINCT Tijd, Thuis, Uit, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Datum, '' AS Vertrekverzameltijd, '' AS KlkThuis, '' AS KlkUit, '' AS Veld
FROM `".$dbprefix."clubprogramma` WHERE Thuis Like '$filterfield' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP'
UNION
SELECT DISTINCT Tijd, Thuis, Uit, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Datum, Vertrekverzameltijd, KlkThuis, KlkUit, Veld
FROM `".$dbprefix."oefenprogramma` WHERE Thuis like '$filterfield' AND Uitslag Like 'ng' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP'
ORDER BY $outputsortTh";

$result1=mysql_query($query1); 
$num1=mysql_numrows($result1);

$query2 = "SELECT DISTINCT  Tijd,  Thuis,  Uit,  Type,  Accommodatie,  Wedstrijdnr,  Scheidsrechter, Status, Datum, '' AS Vertrekverzameltijd,'' AS KlkThuis, '' AS KlkUit, '' AS Veld
FROM `".$dbprefix."clubprogramma` WHERE Uit Like '$filterfield' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP'
UNION
SELECT DISTINCT Tijd, Thuis,  Uit,  Type,  Accommodatie,  Wedstrijdnr,  Scheidsrechter, Status, Datum, Vertrekverzameltijd, KlkThuis, KlkUit, Veld
FROM `".$dbprefix."oefenprogramma` WHERE Uit LIKE '$filterfield' AND Uitslag Like 'ng' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP'
ORDER BY $outputsortUi"; 

$result2=mysql_query($query2); 
$num2=mysql_numrows($result2);
}

else

// naast dat wedstrijden uit clubprogramma en oefenprogramma worden getoond worden ook de afgelastingen getoond
{
$query1 = "SELECT DISTINCT Tijd, Thuis, Uit, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Datum, '' AS Vertrekverzameltijd, '' AS KlkThuis, '' AS KlkUit, '' AS Veld
FROM `".$dbprefix."clubprogramma` WHERE Thuis Like '$filterfield' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP'
UNION
SELECT DISTINCT Tijd, Thuis, Uit, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Datum, Vertrekverzameltijd, KlkThuis, KlkUit, Veld
FROM `".$dbprefix."oefenprogramma` WHERE Thuis Like '$filterfield' AND Uitslag Like 'ng' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP' 
UNION
SELECT DISTINCT Tijd, Thuis, Uit, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Datum, '' AS Vertrekverzameltijd, '' AS KlkThuis, '' AS KlkUit, '' AS Veld
FROM `".$dbprefix."afgelasting` WHERE Thuis Like '$filterfield' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP'
ORDER BY $outputsortTh";
 
$result1=mysql_query($query1); 
$num1=mysql_numrows($result1);


$query2 = "SELECT DISTINCT  Tijd,  Thuis,  Uit,  Type,  Accommodatie,  Wedstrijdnr,  Scheidsrechter, Status, Datum, '' AS Vertrekverzameltijd, '' AS KlkThuis, '' AS KlkUit, '' AS Veld
FROM `".$dbprefix."clubprogramma` WHERE Uit Like '$filterfield' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP'
UNION
SELECT DISTINCT Tijd, Thuis,  Uit,  Type,  Accommodatie,  Wedstrijdnr,  Scheidsrechter, Status, Datum, Vertrekverzameltijd, KlkThuis, KlkUit, Veld
FROM `".$dbprefix."oefenprogramma` WHERE Uit Like '$filterfield' AND Uitslag Like 'ng' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP'
UNION
SELECT DISTINCT Tijd, Thuis, Uit, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Datum, '' AS Vertrekverzameltijd, '' AS KlkThuis, '' AS KlkUit, '' AS Veld
FROM `".$dbprefix."afgelasting` WHERE Uit Like '$filterfield' and to_days(datum) - to_days(now()) between -'$MinDagCP' and '$PlusDagCP'
ORDER BY $outputsortUi"; 


$result2=mysql_query($query2); 
$num2=mysql_numrows($result2); 
}


$query3 = "SELECT * FROM clubnaam WHERE ClubID='1'"; 
$result3=mysql_query($query3); 

$query4 = "SHOW TABLE STATUS from ".$database." LIKE '".$dbprefix."clubprogramma'"; 
$result4=mysql_query($query4);

// In dit gedeelte halen we extra informatie op zoals zelf bepaalde scheidsrechter en Vertrek of verzameltijd
$query6 = "SELECT DISTINCT EWedstrijdnr, EScheidsrechter, VertrekVerzameltijd, KlkThuis, KlkUit, Veld, EAfgelast FROM ".$dbprefix."extraprogramma"; 
$result6=mysql_query($query6) or die(mysql_error());
$num6=mysql_numrows($result6); 
 

mysql_close(); 


?> 
<!DOCTYPE html> 

<html xmlns="http://www.w3.org/1999/xhtml">  

    <head>  

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />  

        <title>Programma <?php echo $clubnaam; ?></title>  

<?php echo "<link rel=\"stylesheet\" type=\"text/css\" href='$CSSweergave' />"; ?>


    </head>  
      
<body>  
<div style="text-align:center"><br/>
<?php
if ($FilterCP == "Aan") 
{
?>
<form action="programma.php" method="post">
<table class="bd">
<tr>


<td> Team : <select name="filter_field">
<option value="">Alle teams</option>
<?php
	foreach($array as $array) 
	{
		if ($array != "")
		{
			?>
			<option value="<?php echo $array ?>"<?php if ($filterfield == $array) { echo "selected"; } ?>><?php echo html_entity_decode($array) ?></option>
			<?php
		}
	}

?>
</select>  <input type="submit" name="action"  value="Selecteer" /> <a href="programma.php?a=reset"> Reset Filter</a></td>

</tr>
</table>
</form>
<?php
}
else
{
}
?>
 	<h2>Thuiswedstrijden <?php echo $clubnaam; ?></h2> </div> 
<div style="text-align:center">
<table class="clubprogramma"> 
<tr> 
<th class="left" style="width:50px">Datum</th> 
<th class="left" style="width:50px">Tijd</th> 
<th class="left" style="width:170px">Thuis</th> 
<?php if ($Klkamertonen == "Aan") { ?> <th class="left" style="width:75px">Kl.kamer</th> <?php } ?>
<th class="left" style="width:170px">Uit</th> 
<?php if ($Klkamertonen == "Aan") { ?> <th class="left" style="width:75px">Kl.kamer</th> <?php } ?>
<?php if ($Veldtonen == "Aan") { ?> <th class="left" style="width:50px">Veld</th> <?php } ?>
<th class="left" style="width:50px">Type</th> 
<th class="left" style="width:60px">Wed<br />Nr</th> 
<?php if ($ClubprogrammaScheids == "Aan") { ?> <th class="left" style="width:150px">Scheidsrechter</th> <?php } ?>
<?php if ($ClubprogrammaVVtijd == "Aan") { ?> <th class="left" style="width:45px">Verzamel Vertrek tijd</th>  <?php } ?>
</tr> 

<?php 
$rowclass = 0; 
$i=0; 

while ($i < $num1) { 
$KlkThuis="";
$KlkUit="";
$Veld="";
$datum=makeNiceDate(mysql_result($result1,$i,"Datum")); 
$tijd=mysql_result($result1,$i,"Tijd"); 
$thuis=mysql_result($result1,$i,"Thuis"); 
$uit=mysql_result($result1,$i,"Uit"); 
$type=mysql_result($result1,$i,"Type"); 
$accommodatie=mysql_result($result1,$i,"Accommodatie"); 
$wedstrijdnr=mysql_result($result1,$i,"Wedstrijdnr");
$scheidsrechter=mysql_result($result1,$i,"Scheidsrechter");
$status=mysql_result($result1,$i,"Status");
 
$Oefvertrekverzameltijd=mysql_result($result1,$i,"Vertrekverzameltijd");
$OefKlkThuis=mysql_result($result1,$i,"KlkThuis");
$OefKlkUit=mysql_result($result1,$i,"KlkUit");
$OefVeld=mysql_result($result1,$i,"Veld");


$i2=0;
$escheidsrechter="";
$vertrekverzameltijd="";
$KlkThuis="";
$KlkUit="";
$Veld="";

while ($i2 < $num6) { 
$ewedstrijdnr=mysql_result($result6,$i2,"EWedstrijdnr"); 
$escheidsrechter1=mysql_result($result6,$i2,"EScheidsrechter"); 
$vertrekverzameltijd1=mysql_result($result6,$i2,"VertrekVerzameltijd");
$eKlkThuis=mysql_result($result6,$i2,"KlkThuis");
$eKlkUit=mysql_result($result6,$i2,"KlkUit");
$eVeld=mysql_result($result6,$i2,"Veld");
$EAfgelast=mysql_result($result6,$i2,"EAfgelast");

if ($ewedstrijdnr == $wedstrijdnr) 
{

$KlkThuis=$eKlkThuis;
$KlkUit=$eKlkUit;
$Veld=$eVeld;


$escheidsrechter=$escheidsrechter1;
if ($escheidsrechter !== "")
{
$scheidsrechter = $escheidsrechter;
}

$vertrekverzameltijd=$vertrekverzameltijd1;
if ($status !== "afgelast") $status=$EAfgelast;
}
$i2++;
}
 
?> 

<tr> 
<td class="row<?php echo $rowclass ?>"><?php echo $datum; ?></td>  
<td class="row<?php echo $rowclass ?>"><?php echo $tijd; ?></td> 
<td class="row<?php echo $rowclass ?>"><?php echo $thuis; ?></td> 
<?php if ($Klkamertonen == "Aan") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $KlkThuis; echo $OefKlkThuis; ?></td> <?php } ?>
<td class="row<?php echo $rowclass ?>"><?php echo $uit; ?></td> 
<?php if ($Klkamertonen == "Aan") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $KlkUit; echo $OefKlkUit; ?></td> <?php } ?>
<?php if ($Veldtonen == "Aan") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $Veld; echo $OefVeld; ?></td> <?php } ?>
<td class="row<?php echo $rowclass ?>"><?php echo $type; ?></td>  
<?php if ($status == "afgelast") { ?> <td class="row<?php echo $rowclass ?>"><?php echo "<div class=\"afgelastred\">".$status."</div>"; ?></td> <?php } ?>
<?php if ($status !== "afgelast") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $wedstrijdnr; ?></td> <?php } ?> 
<?php if ($ClubprogrammaScheids == "Aan") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $scheidsrechter; ?></td> <?php } ?>
<?php if ($ClubprogrammaVVtijd == "Aan") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $vertrekverzameltijd; echo $Oefvertrekverzameltijd; ?></td>  <?php } ?>

</tr> 

<?php 
$i++; 
$rowclass = 1 - $rowclass; 

} 
IF ($num1==0) { ?>
<tr> 
<td class="center" colspan="4"><br /><b><?php echo 'Er is geen actueel programma bekend'; ?></b><br /></td>
</tr>
<?php }

echo "</table>"; 
?>

<h2>Uitwedstrijden <?php echo $clubnaam; ?></h2> </div> 
<div style="text-align:center">
<table class="clubprogramma"> 
<tr> 
<th class="left" style="width:50px">Datum</th> 
<th class="left" style="width:50px">Tijd</th> 
<th class="left" style="width:170px">Thuis</th> 
<?php if ($Klkamertonen == "Aan") { ?> <th class="left" style="width:75px">Kl.kamer</th> <?php } ?>
<th class="left" style="width:170px">Uit</th> 
<?php if ($Klkamertonen == "Aan") { ?> <th class="left" style="width:75px">Kl.kamer</th> <?php } ?>
<?php if ($Veldtonen == "Aan") { ?> <th class="left" style="width:50px">Veld</th> <?php } ?>
<th class="left" style="width:50px">Type</th> 
<th class="left" style="width:60px">Wed<br />Nr</th> 
<?php if ($ClubprogrammaScheids == "Aan") { ?> <th class="left" style="width:150px">Scheidsrechter</th> <?php } ?>
<?php if ($ClubprogrammaVVtijd == "Aan") { ?> <th class="left" style="width:45px">Verzamel Vertrek tijd</th>  <?php } ?>
</tr> 

<?php 
$rowclass = 0; 
$i=0; 
while ($i < $num2) { 
$KlkThuis="";
$KlkUit="";
$Veld="";
$datum=makeNiceDate(mysql_result($result2,$i,"Datum")); 
$tijd=mysql_result($result2,$i,"Tijd"); 
$thuis=mysql_result($result2,$i,"Thuis"); 
$uit=mysql_result($result2,$i,"Uit"); 
$type=mysql_result($result2,$i,"Type"); 
$accommodatie=mysql_result($result2,$i,"Accommodatie"); 
$wedstrijdnr=mysql_result($result2,$i,"Wedstrijdnr");
$scheidsrechter=mysql_result($result2,$i,"Scheidsrechter");
$status=mysql_result($result2,$i,"Status"); 
$Oefvertrekverzameltijd=mysql_result($result2,$i,"Vertrekverzameltijd");
$OefKlkThuis=mysql_result($result2,$i,"KlkThuis");
$OefKlkUit=mysql_result($result2,$i,"KlkUit");
$OefVeld=mysql_result($result2,$i,"Veld");



$i2=0;
$escheidsrechter="";
$vertrekverzameltijd="";
$KlkThuis="";
$KlkUit="";
$Veld="";
while ($i2 < $num6) { 

$ewedstrijdnr=mysql_result($result6,$i2,"EWedstrijdnr"); 
$escheidsrechter1=mysql_result($result6,$i2,"EScheidsrechter"); 
$vertrekverzameltijd1=mysql_result($result6,$i2,"VertrekVerzameltijd");
$eKlkThuis=mysql_result($result6,$i2,"KlkThuis");
$eKlkUit=mysql_result($result6,$i2,"KlkUit");
$eVeld=mysql_result($result6,$i2,"Veld");
$EAfgelast=mysql_result($result6,$i2,"EAfgelast"); 

if ($ewedstrijdnr == $wedstrijdnr) 
{

$KlkThuis=$eKlkThuis;
$KlkUit=$eKlkUit;
$Veld=$eVeld;

$escheidsrechter=$escheidsrechter1;
if ($escheidsrechter !== "")
{
$scheidsrechter = $escheidsrechter;
}

$vertrekverzameltijd=$vertrekverzameltijd1;

if ($status !== "afgelast") $status=$EAfgelast;

}

$i2++;
}

?>  

<tr> 
<td class="row<?php echo $rowclass ?>"><?php echo $datum; ?></td>  
<td class="row<?php echo $rowclass ?>"><?php echo $tijd; ?></td> 
<td class="row<?php echo $rowclass ?>"><?php echo $thuis; ?></td> 
<?php if ($Klkamertonen == "Aan") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $KlkThuis; echo $OefKlkThuis; ?></td> <?php } ?>
<td class="row<?php echo $rowclass ?>"><?php echo $uit; ?></td> 
<?php if ($Klkamertonen == "Aan") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $KlkUit; echo $OefKlkUit; ?></td> <?php } ?>
<?php if ($Veldtonen == "Aan") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $Veld; echo $OefVeld; ?></td> <?php } ?>
<td class="row<?php echo $rowclass ?>"><?php echo $type; ?></td>  
<?php if ($status == "afgelast") { ?> <td class="row<?php echo $rowclass ?>"><?php echo "<div class=\"afgelastred\">".$status."</div>"; ?></td> <?php } ?> 
<?php if ($status !== "afgelast") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $wedstrijdnr; ?></td> <?php } ?> 
<?php if ($ClubprogrammaScheids == "Aan") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $scheidsrechter; ?></td> <?php } ?>
<?php if ($ClubprogrammaVVtijd == "Aan") { ?> <td class="row<?php echo $rowclass ?>"><?php echo $vertrekverzameltijd; echo $Oefvertrekverzameltijd; ?></td>  <?php } ?>

</tr> 

<?php 
$i++; 
$rowclass = 1 - $rowclass; 

} 
IF ($num2==0) { ?>
<tr> 
<td class="center" colspan="4"><br /><b><?php echo 'Er is geen actueel programma bekend'; ?></b><br /></td>
</tr>
<?php }

echo "</table>"; 
?> 

<table class="clubprogramma"> 
    <tr> 
        <td class="small"><br />Bijgewerkt op: <?php  
            setlocale(LC_ALL, 'nl_NL'); 
            echo strftime('%d/%m/%y - %H:%M', strtotime(mysql_result($result4,0,'Update_time'))); ?></td> 
    </tr> 
      <tr>
    	 <td class="left"><br />Bron: <a href='http://www.voetbal.nl' target='_blank'>Voetbal.nl</a></td>
    </tr>   

</table>

</div> 
</body> 
</html>