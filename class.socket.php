<?php
/*******************************************
*  Socket Class Made By Lucas van Dijk 
* * * * * * * * * * *
*  This class can be used for ANYTHING related to socket
*  programming.
*
*  Includes some examples on how the socket class can be
*  implented. (the HttpGetRequest and HttpPostRequest class)
* * * * * * * * * * * * * * * * * * * * * *
* Revision 1.3
* Revision 1.3.1 Killerbee proxy support added
*
* This version is used within the voetbal.nl Ripper 1.9.6 door Johnvs 
* Datum: 21-05-12
* 
********************************************/

class Socket
{
    /**
    * @desc the main socket
    * @scope protected
    * @type resource
    */
    var $socket;
      
    /**
    * @desc IP of the server
    * @scope protected
    * @type string
    */
    var $ip;
      
    /**
    * @desc port of the server
    * @scope protected
    * @type int
    */
    var $port;
      
    /**
    * @desc Timeout
    * @scope protected
    * @type int
    */
    var $timeout;
      
    /**
    * @desc Contains an error message
    * @scope protected
    * @type string
    */
    var $error;
      
    /**
    * @desc Contains the data returned
    * @scope protected
    * @type string
    */
    var $contents;
      
    /**
    * @desc Constructor, inits some vars
    * @scope
    */
    function Socket()
    {
        $this -> reset();
    }
      
    /**
    * @desc resets all vars
    * @scope
    */
    function reset()
    {
        $this -> timeout = 10;
        $this -> ip = '';
        $this -> port = 0;
        $this -> contents = '';
    }
      
    /**
    * @desc sets the IP and port
    * @scope
    * @param the IP
    * @param the port
    */
    function set_ip($ip, $port)
    {
        $this -> ip = (string) $ip;
        $this -> port = (int) $port;
    }
      
    /**
    * @desc sets the timeout
    * @scope
    * @param the timeout
    */
    function set_timeout($timeout)
    {
        $this -> timeout = (int) $timeout;
    }
      
    /**
    * @desc gets the error message
    * @scope
    * @return the error message
    */
    function get_error()
    {
        return $this -> error;
    }
      
    /**
    * @desc gets the data returned
    * @scope
    * @return the data
    */
    function get_contents()
    {
        return $this -> contents;
    }
      
    /**
    * @desc connects to the server
    * @scope
    * @return true on success else false
    */
    function connect()
    {
        return $this -> socket = fsockopen($this -> ip, $this -> port, $errno, $this -> error, $this -> timeout);
    }
      
    /**
    * @desc makes the actual request to the server
    * @scope
    * @param the request
    * @return the contents returned by server, false on fail
    */
    function request($request)
    {          
        // Connect to IP and Port
        $this -> connect();
          
        // Check if connected
        if(is_resource($this -> socket))
        {
            fwrite($this -> socket, $request);
              
            // Get contents
            $buffer = '';
            while($buffer = fgets($this -> socket, 4096))
            {
                $this -> contents .= $buffer;
            }
              
            return $this -> contents;
        }
        else
        {
            // Error!
            return false;
        }
    }
}

class HttpGetRequest extends Socket
{
    /**
    * @desc determines if we should include headers or not
    * @scope protected
    * @type boolean
    */
    var $include_headers;
      
    /**
    * @desc determines if we should redirect, when a Location header was found
    * @scope protected
    * @type boolean
    */
    var $redirect;
      
    /**
    * @desc an array with all the headers to be sent
    * @scope protected
    * @type array
    */
    var $headers;
      
    /**
    * @desc the file to request
    * @scope protected
    * @type string
    */
    var $file;
      
    /**
    * @desc an array with all cookies setted by the web page
    * @scope protected
    * @type array
    */
    var $cookies;
      
    /**
    * @desc Cookies to set with the next request
    * @scope protected
    * @type array
    */
    var $cookies_to_set;
      
    /**
    * @desc constructor, inits some vars
    * @scope
    */
    function HttpGetRequest()
    {
        $this -> reset();
    }
      
    /**
    * @desc resets all vars
    * @scope
    */
    function reset()
    {
        $this -> timeout = 10;
        $this -> include_headers = false;
        $this -> redirect = true;
        $this -> headers = array();
        $this -> file = '';
        $this -> cookies = array();
        $this -> cookies_to_set = array();
        $this -> redirect = true;
    }          
      
    /**
    * @desc sets if we include headers or not
    * @scope
    * @param true or false, should we include headers?
    */
    function include_headers($yes_no = true)
    {
        $this -> include_headers = (bool) $yes_no;
    }
      
    /**
    * @desc determines if we should redirect, when a Location header was found
    * @scope
    * @param true or false, should we redirect?
    */
    function use_redirect($yes_no = true)
    {
        $this -> redirect = (bool) $yes_no;
    }
      
    /**
    * @desc adds a header
    * @scope
    * @param the header
    */
    function add_header($header)
    {
        $this -> headers[] = (string) $header."\r\n";
    }
      
    /**
    * @desc set the file to request
    * @scope
    * @param the file
    */
    function set_file($file)
    {
        $this -> file = (string) $file;
    }
      
    /**
    * @desc gets the array woth cookies
    * @scope
    * @return array with all the cookies
    */
    function get_cookies()
    {
        return $this -> cookies;
    }
      
    /**
    * @desc send request to the webpage
    * @scope
    * @param the file
    * @return the contents, or false on fail
    */
    function request()
    {
        $contents = '';
          
        // Connect to IP and Port
        $this -> connect();
          
        // Check if connected
        if(is_resource($this -> socket))
        {
            // Build cookie header
            $cookies = '';
            foreach($this -> cookies_to_set as $name => $value)
            {
                $cookies .= $name."=".$value.";";
            }
            $this -> add_header("GET ".parse_url($this -> file, PHP_URL_PATH)." HTTP/1.1");
            $this -> add_header("Host: ".parse_url($this -> file, PHP_URL_HOST));
			$this -> add_header("Connection: Close");
            $this -> add_header("User-Agent: Socket PHP class (incompatible; IE sucks)");
            if(count($this -> cookies_to_set))
            {
                $this -> add_header("Cookie: ".$cookies);
            }
            $this -> add_header("");
              
            $this -> contents = $this -> do_request();
              
            $this -> headers = array();
              
            $this -> grab_cookies();
              
            return $this -> contents;
        }
        else
        {
            // Error!
            return false;
        }
    }
      
    /**
    * @desc does the actual request, builds headers etc.
    * @scope protected
    * @return the content
    */
    function do_request()
    {
        $contents = '';
          
        // Build request to send
        $request = '';
        foreach($this -> headers as $header)
        {
            $request .= $header;
        }
        
        fwrite($this -> socket, $request);
          
        if($this -> include_headers == false)
        {
            $line = '';
            while(!($line == "\r\n"))
            {
                $line = fgets($this -> socket, 128);
                  
                if($this -> redirect)
                {
                    if(preg_match("#Location\: (.*?)\\r\\n#si", $line, $matches))
                    {
                        $this -> set_file($matches[1]);
                          
                        // Clear all headers
                        $this -> headers = array();
                        return $this -> request();
                    }
                }
            }    // strip out the header
        }
              
        // Get contents
        $buffer = '';
        while($buffer = fgets($this -> socket, 4096))
        {
            $contents .= $buffer;
        }      
          
        return $contents;
    }
      
    /**
    * @desc gets the cookies setted by the webserver
    * @scope protected
    * @return array with the cookies
    */
    function grab_cookies()
    {
        if($this -> include_headers)
        {
            preg_match_all("/Set-Cookie: (.*?)\r\n/si", $this -> contents, $matches, PREG_SET_ORDER);
              
            foreach($matches as $match)
            {
                $cookie = explode(";", $match[1]);
                list($cookie_name, $cookie_value) = explode("=", $cookie[0], 2);
                $this -> cookies[urldecode($cookie_name)] = urldecode($cookie_value);
            }
        }
    }
      
    /**
    * @desc sets a cookie
    * @scope
    * @param cookie name
    * @param cookie value
    */
    function set_cookie($cookie_name, $cookie_value)
    {
        $this -> cookies_to_set[$cookie_name] = $cookie_value;
    }
      
    /**
    * @desc removes a cookie
    * @scope
    * @param cookie name
    */
    function remove_cookie($cookie_name)
    {
        unset($this -> cookies_to_set[$cookie_name]);
    }
      
    /**
    * @desc sets all cookies grabbed by grab_cookies();
    * @scope
    */
    function set_all_cookies()
    {
        foreach($this -> cookies as $name => $value)
        {
            $this -> set_cookie($name, $value);
        }
    }          
}

class HttpPostRequest extends HttpGetRequest
{
    /**
    * @desc Post variables to send
    * @scope protected
    * @type array
    */
    var $post_vars;
      
    /**
    * @desc Constructor, inits some vars
    * @scope
    */
    function HttpPostRequest()
    {
        $this -> reset();
    }
      
    /**
    * @desc resets all vars
    * @scope
    */
    function reset()
    {
        $this -> post_vars = array();
        $this -> timeout = 10;
        $this -> include_headers = false;
        $this -> redirect = true;
        $this -> headers = array();
        $this -> file = '';
        $this -> cookies = array();
        $this -> redirect = true;
    }          
      
    /**
    * @desc adds a post var
    * @scope
    * @param name of the var
    * @param value of the var
    */
    function add_var($name, $value)
    {
        $this -> post_vars[$name] = $value;
    }
      
    /**
    * @desc makes the actual request to the webpage
    * @scope
    * @param the file
    * @return the contents, or false on fail
    */
    function request()
    {          
        // Connect to IP and Port
        $this -> connect();
          
        // Check if connected
        if(is_resource($this -> socket))
        {
            // Build post vars
            $vars = '';
            foreach($this -> post_vars as $name => $value)
            {
                $vars .= "&".$name."=".urlencode($value);
            }
            $vars = substr($vars, 1);
            $this -> add_header("POST ".parse_url($this -> file, PHP_URL_PATH)." HTTP/1.1");
            $this -> add_header("Host: ".parse_url($this -> file, PHP_URL_HOST));
            $this -> add_header("Connection: Close");
            $this -> add_header("User-Agent: Socket PHP class (incompatible; IE sucks)");
            $this -> add_header("Content-type: application/x-www-form-urlencoded");
            $this -> add_header("Content-Length: ".strlen($vars));
            $this -> add_header('');
            $this -> add_header($vars);
            $this -> add_header('');              
              
            $this -> contents = $this -> do_request();
              
            $this -> grab_cookies();
              
            return $this -> contents;
        }
        else
        {
            // Error!
            return false;
        }
    }
}
?> 