<?php
// index.php 1.9.7.4 (09-01-2013) Aanpassing in geval clubprogramma wel thuis maar geen uitwedstrijden bevat.
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel


// ER ZIJN GEEN AANPASINGEN NODIG IN INDEX.PHP

// ////////////////////////////////////////////////////////
// Het is mogelijk dat er bv in accommodatie of scheidsrechter verkeerde karakters verschijnen
// deze worden zoveel mogelijk al afgevangen indien nodig staat
// in functie.php bij de opslaan functies code om karaters te vervangen
// ////////////////////////////////////////////////////////

ini_set('display_errors', 1);
//Voorkom vreemde resultaten door netjes te programmeren.
//error_reporting(E_ALL);
error_reporting(E_ERROR | E_WARNING | E_PARSE);

#insert database data
include("config.php");
include("functies3.php");


if (isset($_POST['Submit'])){	

	?>
	<!DOCTYPE html>

	<html xmlns="http://www.w3.org/1999/xhtml">


	<head>
	<title>Gegevens Opslaan</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="no-cache">
<meta http-equiv="Expires" content="-1">
<meta http-equiv="Cache-Control" content="no-cache">
	<link rel="stylesheet" type="text/css" href="opmaak.css">
	</head>
	<body>
	<table class="bd" width="100%"><tr><td class="hr"><h2>Gegevens Opslaan</h2></td></tr></table>

	<?php

include("config.php");
	global $_POST;
	global $_SESSION;
	global $_GET;

	if(isset($_POST['teamID']) && $_POST['soort'] == "stand")
	{

		$teamID = filter_var($_POST['teamID'], FILTER_VALIDATE_INT); //alleen numerieke waarde toegstaan - voorkomt mogelijkheid tot sql injection

		$standen = get_stand($_POST['html'],"normaal");
		if(empty($standen))
		{
			// geen resulaten dus we verwijderen de resultaten uit de tabel niet en proberen het ook niet op te slaan
			$sprobleem = "Probleem met het ophalen of opslaan van de Standen. ";
			if ($UserDebug == 'Aan') echo "Standen kunnen niet worden opgeslagen omdat het leeg is: -$standen-<br />";
		}
		else
		{
			// Leegmaken van de database
			leegmakendb($dbprefix."stand",$_POST['teamID']);
			//Standen opslaan in database
			opslaan_stand($standen,$_POST['teamID'],$dbprefix."stand");
			echo "Standen opgeslagen voor teamID: " .$_POST['teamID'];
		}
	} else if (isset($_POST['teamID']) && $_POST['soort'] == "uitslag") {
		if ($_POST['teamID'] == 0) {	
			$clubuitslag = get_clubuitslag($_POST['html']);
			
			if (empty($clubuitslag))
			{
				// geen resulaten dus we verwijderen de resultaten uit de tabel niet en proberen het ook niet op te slaan
				$cpprobleem = "Probleem met het ophalen of opslaan van het Club Uitslagen. ";
				if ($UserDebug == 'Aan') echo "Club uitslagen kunnen niet worden opgeslagen omdat het leeg is: -$clubuitslag-<br />";
			}
			else
			{
				if ($_POST['cleanTable']){
					// Leegmaken van de database. Ook als er geen clubuitslagen zijn gevonden maar het ophalen goed is gegaan.
					leegmakendbcp($dbprefix."clubuitslagen");
				}
				// Club Uitslagen opslaan in database alleen als er periode gegevens gevonden zijn.
				opslaan_clubuitslagen($clubuitslag);
				echo "ClubUitslagen opgeslagen voor teamID: " .$_POST['teamID'];
			}
		} else {
			$uitslagen = get_uitslag($_POST['html']);
			if(empty($uitslagen))
			{
				// geen resulaten dus we verwijderen de resultaten uit de tabel niet en proberen het ook niet op te slaan
				$uprobleem = "Probleem met het ophalen of opslaan van de Uitslagen. ";
				if ($UserDebug == 'Aan') echo "Uitslagen kunnen niet worden opgeslagen omdat het leeg is: -$uitslagen-<br />";
			}
			else
			{
				leegmakendb($dbprefix."uitslag",$_POST['teamID']);
				opslaan_uitslagen($uitslagen,$_POST['teamID']);
				echo "Uitslagen opgeslagen voor teamID: " .$_POST['teamID'];
			}		
		}
	} else if (isset($_POST['teamID']) && $_POST['soort'] == "programma") {
		if ($_POST['teamID'] == 0) {	
			$clubprog = get_clubprog($_POST['html']);
			
			if (empty($clubprog))
			{
				// geen resulaten dus we verwijderen de resultaten uit de tabel niet en proberen het ook niet op te slaan
				$cpprobleem = "Probleem met het ophalen of opslaan van het Club Programma. ";
				if ($UserDebug == 'Aan') echo "Club programma kan niet worden opgeslagen omdat het leeg is: -$clubprog-<br />";
			}
			else
			{
				if ($_POST['cleanTable']){
					// Leegmaken van de database. Ook als er geen programmagegevens zijn gevonden maar het ophalen goed is gegaan.
					leegmakendbcp($dbprefix."clubprogramma");
				}
				// Club Programma opslaan in database alleen als er periode programmagegevens gevonden zijn.
				opslaan_clubprog($clubprog);
				echo "ClubProgramma opgeslagen voor teamID: " .$_POST['teamID'];
			}
		} else {

			$programma = get_programma($_POST['html']);
			if(empty($programma) or $programma == 'leegprogramma')
			{
				// Probleem met ophalen programma gegevens dus we verwijderen de resultaten uit de tabel niet en proberen het ook niet op te slaan
				$pprobleem = "Probleem met het ophalen van programma gegevens. ";
				if ($UserDebug == 'Aan') echo "programma kan niet worden opgeslagen omdat het programma leeg is: -$programma-<br />";
			}
			else
			{
				// Leegmaken van de database. Ook als er geen programmagegevens zijn gevonden maar het ophalen goed is gegaan.
				leegmakendb($dbprefix."programma",$_POST['teamID']);
				opslaan_programma($programma,$_POST['teamID']);
				echo "Programma opgeslagen voor teamID: " .$_POST['teamID'];
			}
		}
	}	
} else {
	
		?>
	<!DOCTYPE html>

	<html xmlns="http://www.w3.org/1999/xhtml">


	<head>
	<title>Gegevens Ophalen</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="no-cache">
<meta http-equiv="Expires" content="-1">
<meta http-equiv="Cache-Control" content="no-cache">
	<link rel="stylesheet" type="text/css" href="opmaak.css">
	</head>
	<body>		
		<form name="standform" method="post" action="">
		<table>
		<tr><td>TeamID</td><td><input type="text" name="teamID" value="<?php echo $_GET['teamID']; ?>"></td></tr>
		<tr><td>CleanupTable</td><td><input type="checkbox" name="cleanTable" checked="checked"></td></tr>
		<tr><td>Soort</td><td><input type="text" name="soort" value="<?php echo $_GET['soort']; ?>"></td></tr>
		<tr style="heigth: 100%;"><td>Html</td><td><textarea name="html" style="width: 800px; heigth: 800px;" value=""></textarea></td></tr></table>
		<input type="submit" name="Submit" value="Submit"><br>
		</form>
<?php
}
?>		
