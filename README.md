Voetbal.nl Ripper aangepast voor gebruik met PHP7, en de nieuwe wijzigingen van voetbal.nl.  


Installatie vereisten:
-----
Webserver (Apache, nginx)  
PHP (min. versie 5.6) met de MySQLi en cURL extensies  
^- Er is geen ondersteuning voor de ingebouwde "socket" code  
MySQL/MariaDB database server  
^- Een database gebruiker met rechten tot minstens SELECT, UPDATE, INSERT, ALTER en CREATE

Installatie en gebruik:
-----
Al bestaande installatie van de ripper:
- Maak een backup van de oude bestanden en overschrijf daarna met de geüpdate bestanden

Nieuwe installatie van de ripper:

1. Upload de PHP bestanden naar een aparte map op de webserver (Bijv. /ripper)  
2. Wijzig de gegevens voor de database in config.php.  
3. Voor het eerste gebruik moet tabellenmaken.php worden aangeroepen, deze zal de database opstellen voor gebruik.  
4. Daarna kan via het admin paneel alles worden ingesteld.  
5. Na het instellen van de teamcode kan index.php worden aangeroepen, deze zal dan zoveel mogelijk gegevens binnen halen.  

Lees ook de leesmij.txt na, deze is nog actueel voor deze versie.

Zoeken van clubcodes en teamcodes
-----
1. Zoek je clubcode en teamcode  

Ga naar https://www.voetbal.nl/profiel/teams => voeg een team toe, zoek je club op  => bekijk teams  
in de url staat dan je clubcode => https://www.voetbal.nl/club/CLUBCODE/teams  

Bij de teams staan de teamcodes per team, bijv. https://www.voetbal.nl/team/T9999999990/overzicht, waarbij T9999999990 de teamcode is  



Opmerkingen:
-----
- Werkt met de database van voetbalripper 1.9.7, deze word geupdate bij het eerste draaien. Oorspronkelijke code blijft compatibel
- Houdt geen rekening met foutmeldingen
- Sommige data is niet beschikbaar
- Er word geen ondersteuning geleverd
- Code is naar eigen inzicht aan te passen en te gebruiken
- We zijn niet verantwoordelijk als deze code je hamster laat ontsnappen, eten voor je kookt, je huis verbouwt of andere vreemde dingen.
