<?php
// config.php
// Voetbal.nl Ripper 2.0 door Syphere en dutchtoro
// In deze versie zijn de functies om gegevens de schrapen van voetbal.nl aangepast om te werken op
// de nieuwe versie ervan. Ook is deze aangepast voor PHP7


/// Oorspronkelijk script v1.9.7 door Johnvs
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel

// Pas de volgende gegevens aan voor verbinding met eigen database
// In index.php hoeven geen wijzigingen gemaakt te worden.
// Alle andere instellingen zijn te wijzigen via instellingen.php. Behalve proxy instellingen.
//
// Indien nodig kunnen variablen onder aan in het script worden gewijzigd of toegevoegd.
$con = false;                               # De variabel die de connectie met de database bevat. NEDERLANDS
$username = "";            # Gebruikersnaam voor de eigen database
$password = "";            # Wachtwoord voor de eigen database
$database = "";            # Naam van de eigen database
$server = "localhost";              # Hostnaam van de server, is meestal localhost of 127.0.0.1
$dbprefix = "vrip_";		     # prefix voor de tabellen in de DataBase // Door deze te wijzigen en nog een installatie te doen in een andere direcory
                                 # kunnen er meerdere versies naast elkaar draaien.

$useproxy  = 'false'; 		     # true gebruik onderstaande proxyhost als proxy
$proxyhost = '10.10.10.10';      # Let op zonder http:// dus alleen de hostnaam of ip adres!
$proxyport = '8080';		     # Proxy poort. Meestal 8080


$debug =  'false';
$devdebug = false;
$debug_programma_html = '';


//Verbinding maken met database en query om instellingen op te halen
if( !function_exists('connect_db') ) {
    function connect_db() {
        static $_con;
        global $server, $username, $password, $database, $con;

        $con = ( $_con ) ? $_con : $con = mysqli_connect($server,$username,$password,$database) or die( "Er ging iets mis. Ververs de pagina om het opnieuw te proberen.");

        return $con;
    }
}

if( !function_exists('mysqli_reset') ) {
    function mysqli_reset() {
        global $con;

        if( $con )
            mysqli_close($con);

        $con = connect_db();
    }
}

if( !function_exists('_mysqli_num_rows') ) {
    function _mysqli_num_rows( $resource ) {
        if( !$resource )
            return 0;

        return mysqli_num_rows( $resource );
    }
}

//Reset if connection exists, connect if not.
$con = connect_db();

// Deze variabel is belangrijk als er updates gemaakt worden aan de database de db_update.php script goed uitgevoerd kan worden.
$scriptDBVersion = 2;

include_once("db_update.php");
checkVersion();

//Query om de instellingen op te halen
$instellingenquery = "SELECT * from ${dbprefix}instellingen";
$instellingen=mysqli_query($con,$instellingenquery) or die(mysqli_error($con));

//mysql_query($instellingenquery);

$numrinstellingen=_mysqli_num_rows($instellingen);
$iinfo= mysqli_fetch_array($instellingen);

// Variabelen zetten
$i=0;
$clubnaam = $iinfo[1];//mysql_result($instellingen, 0,  "Clubnaam");
$clubverkort = $iinfo[2];//mysql_result($instellingen, 0, "Club1");
$club1 = $iinfo[2];//mysql_result($instellingen, 0, "Club1");
$club = $iinfo[4];//mysql_result($instellingen, 0,  "Clubcode");
$altclub = $iinfo[5];//mysql_result($instellingen, 0,  "AltClubcode");
$Clubprogrammakeuze = $iinfo[6];//mysql_result($instellingen, 0,  "Clubprogramma");
$Clubprogrammaophalen = $iinfo[15];//mysql_result($instellingen, 0, "CPophalen");
$Clubuitslagenophalen = $iinfo[16];//mysql_result($instellingen, 0, "CUophalen");
$DelwdstrdCPCU = $iinfo[18];//mysql_result($instellingen, 0, "DelwdstrdCPCU");
$DelwdstrdCPCUtxt = $iinfo[17];//mysql_result($instellingen, 0, "DelwdstrdCPCUtxt");
$ClubprogrammaScheids = $iinfo[19];//mysql_result($instellingen, 0, "ScheidsinCP");
$ClubprogrammaVVtijd = $iinfo[20];//mysql_result($instellingen, 0, "VVinCP");
//$Programmadetails = $iinfo[21];//mysql_result($instellingen, 0, "Pdetails");
$Programmadetails = true;
$Wedstrijddetailstonen = $iinfo[22];//mysql_result($instellingen, 0, "Pdetailstonen");
$Klkamertonen = $iinfo[23];//mysql_result($instellingen, 0, "Klkamertonen");
$Veldtonen = $iinfo[24];//mysql_result($instellingen, 0, "Veldtonen");
$Oefeninteam = $iinfo[25];//mysql_result($instellingen, 0, "Oefeninteam");
$Tindelinginteam = $iinfo[26];//mysql_result($instellingen, 0, "Tindelinginteam");
$CSSadmin = $iinfo[28];//mysql_result($instellingen, 0, "CSSadmin");
$CSSweergave = $iinfo[27];//mysql_result($instellingen, 0, "CSSweergave");
$OphViaAcode = $iinfo[29];//mysql_result($instellingen, 0, "OphViaAcode");
$Wacht = $iinfo[31];//mysql_result($instellingen, 0, "Wacht");
$Wacht = "Uit";
$Opschonen = $iinfo[32];//mysql_result($instellingen, 0, "Opschonen");
$Meldingen = $iinfo[33];//mysql_result($instellingen, 0, "Meldingen");
$Curl = $iinfo[34];//mysql_result($instellingen, 0, "Curl");
$Phpsafemode = $iinfo[35];//mysql_result($instellingen, 0, "Phpsafemode");
$Phpmaxtime = $iinfo[36];//mysql_result($instellingen, 0, "Phpmaxtime");
$gebruikersnaam = $iinfo[37];//mysql_result($instellingen, 0, "Gebruikersnaam");
$wachtwoord = $iinfo[38];//mysql_result($instellingen, 0, "Wachtwoord");
$Emailvoormeldingen = $iinfo[39];//mysql_result($instellingen, 0, "Emailvoormeldingen");
$Beveiligindex = $iinfo[40];//mysql_result($instellingen, 0, "Beveiligindex");
$UserDebug = $iinfo[41];//mysql_result($instellingen, 0, "UserDebug");
//$UserDebug = "Uit";
$Indexusername = $iinfo[42];//mysql_result($instellingen, 0, "Igebruikersnaam");
$Indexpassword = $iinfo[43];//mysql_result($instellingen, 0, "Iwachtwoord");
$Adminusername = $iinfo[44];//mysql_result($instellingen, 0, "Agebruikersnaam");
$Adminpassword = $iinfo[45];//mysql_result($instellingen, 0, "Awachtwoord");
$Oefenusername = $iinfo[46];//mysql_result($instellingen, 0, "Ogebruikersnaam");
$Oefenpassword = $iinfo[47];//mysql_result($instellingen, 0, "Owachtwoord");
$Extrausername = $iinfo[48];//mysql_result($instellingen, 0, "Egebruikersnaam");
$Extrapassword = $iinfo[49];//mysql_result($instellingen, 0, "Ewachtwoord");
$Groupidusername = $iinfo[50];//mysql_result($instellingen, 0, "Ggebruikersnaam");
$Groupidpassword = $iinfo[51];//mysql_result($instellingen, 0, "Gwachtwoord");
$Uitslagusername = $iinfo[52];//mysql_result($instellingen, 0, "Ugebruikersnaam");
$Uitslagpassword = $iinfo[53];//mysql_result($instellingen, 0, "Uwachtwoord");
$Instusername = $iinfo[54];//mysql_result($instellingen, 0, "Instgebruikersnaam");
$Instpassword = $iinfo[55];//mysql_result($instellingen, 0, "Instwachtwoord");
$Dbusername = $iinfo[56];//mysql_result($instellingen, 0, "Dbgebruikersnaam");
$Dbpassword = $iinfo[57];//mysql_result($instellingen, 0, "Dbwachtwoord");
$CombCpAfg = $iinfo[13];//mysql_result($instellingen, 0, "CombCpAfg");
$Clubnaamcheck = $iinfo[14];//mysql_result($instellingen, 0, "Clubnaamcheck");
$SortCP = $iinfo[7];//mysql_result($instellingen, 0, "SortCP");
$FilterCP = $iinfo[8];//mysql_result($instellingen, 0, "FilterCP");
$MinDagCP = $iinfo[9];//mysql_result($instellingen, 0, "MinDagCP");
$PlusDagCP = $iinfo[10];//mysql_result($instellingen, 0, "PlusDagCP");
$MinDagUitslagen = $iinfo[11];//mysql_result($instellingen, 0, "MinDagUitslagen");
$PlusDagUitslagen = $iinfo[12];//mysql_result($instellingen, 0, "PlusDagUitslagen");

//Sommige variabelen hebben een aanpassing nodig.
$Clubprogrammakeuze = str_replace(' ', '%20', $Clubprogrammakeuze);
$club1 = "%".$club1."%";
$club2 = $club1."%";

?>
