<?php session_start();
// uitslag.php
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel


include("config.php");
include("functies.php");
//mysql_connect($server,$username,$password);
//@mysql_select_db($database) or die( "Unable to select database");

global $dbprefix;
//    $ufilter = "";
//    $ufilterfield = "";
//    $uwholeonly = "";



if (isset($_GET["orderuits"])) $orderuits = @$_GET["orderuits"];
if (isset($_GET["typeuits"])) $ordtypeuits = @$_GET["typeuits"];

if (isset($_POST["ufilter"])) $ufilter = @$_POST["ufilter"];
if (isset($_POST["ufilter_field"])) $ufilterfield = @$_POST["ufilter_field"];
$uwholeonly = false;
if (isset($_POST["wholeonly"])) $uwholeonly = @$_POST["wholeonly"];

if (!isset($orderuits) && isset($_SESSION["orderuits"])) $orderuits = $_SESSION["orderuits"];
if (!isset($ordtypeuits) && isset($_SESSION["typeuits"])) $ordtypeuits = $_SESSION["typeuits"];
if (!isset($ufilter) && isset($_SESSION["ufilter"])) $ufilter = $_SESSION["ufilter"];
if (!isset($ufilterfield) && isset($_SESSION["ufilter_field"])) $ufilterfield = $_SESSION["ufilter_field"];
if (isset($_POST['Submit']))
{ //	echo "<p>submit pressed";
	if ($ufilterfield == "Team Uitslagen" or $ufilterfield == "Team Uitslagen Club")
	{

		foreach($_POST['UitslagID'] as $id)
		{
			if (TRIM($_POST["Uitslag".$id]) !== "-" and !preg_match('/[*]/',$_POST["Uitslag".$id]))
			{
				$sql1 = "update `".$dbprefix."uitslag` set `Uitslag`='" .$_POST["Uitslag".$id]."*"."' where UitslagID='".$id."'";

				mysqli_query($con,$sql1) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
			}
		}
	}
	else
	{

		foreach($_POST['ClubUitslagID'] as $id)
		{
			if (TRIM($_POST["Uitslag".$id]) !== "-" and !preg_match('/[*]/',$_POST["Uitslag".$id]))
			{
				$sql1 = "update `".$dbprefix."clubuitslagen` set `Uitslag`='" .$_POST["Uitslag".$id]."*"."' where ClubUitslagID='".$id."'";

				mysqli_query($con,$sql1) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
			}
		}
	}
}
?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>Wedstrijd uitslagen invoeren toevoegen</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php echo "<link rel=\"stylesheet\" type=\"text/css\" href='$CSSadmin'>"; ?>
</head>
<body>
<table class="bdadmin" style="width:100%"><tr><td class="hr"><h2>Wedstrijd uitslagen invoeren</h2></td></tr></table>
<?php
if (!login()) exit;
?>
<div style="float: right"><a href="uitslag.php?a=logout">[ Uitloggen ]</a></div>
<br>
<?php
$con = connect();
$showrecs = 30;
$pagerange = 10;

$a = @$_GET["a"];
$recid = @$_GET["recid"];
$page = @$_GET["page"];
if (!isset($page)) $page = 1;

if ($a == "opschonen")
{
	global $con;

	$today = date("Y-m-d");
	$sql = "delete from `".$dbprefix."extraprogramma` where Edatum<'".$today."'";
	mysqli_query($con,$sql) or die(mysqli_error($con));

}

$sql = @$_POST["sql"];

switch ($sql) {
case "insert":
	sql_insert();
	break;
case "update":
	sql_update();
	break;
case "delete":
	sql_delete();
	break;
}

switch ($a) {
case "add":
	addrec();
	break;
case "view":
	viewrec($recid);
	break;
case "edit":
	editrec($recid);
	break;
case "del":
	deleterec($recid);
	break;
default:
	select();
	break;
}

if (isset($orderuits)) $_SESSION["orderuits"] = $orderuits;
if (isset($ordtypeuits)) $_SESSION["typeuits"] = $ordtypeuits;
if (isset($ufilter)) $_SESSION["ufilter"] = $ufilter;
if (isset($ufilterfield)) $_SESSION["ufilter_field"] = $ufilterfield;
if (isset($uwholeonly)) $_SESSION["wholeonly"] = $uwholeonly;

mysqli_close($con);
?>
<table class="bdadmin" style="width:100%"><tr><td class="hr">Opties - Gemaakt door Yarro/Johnvs</td></tr></table>
<div style="float: left"><a href="dashboard.php">[ DashBoard ]</a></div><br />
<div style="float: left"><a href="instellingen.php">[ Instellingen ]</a></div><br>
<div style="float: left"><a href="admin.php">[ Beheer Teams ]</a></div><br>
<div style="float: left"><a href="groupid.php">[ Group ID accounts ]</a></div><br>
<div style="float: left"><a href="oefenprogramma.php">[ Oefen Programma/Uitslagen ]</a></div><br>
<div style="float: left"><a href="extra.php">[ Extra informatie Club Programma ]</a></div><br>
<div style="float: left"><a href="kantine.php">[ Kantinebezetting beheren ]</a></div><br>

</body>
</html>

<?php function select()
{
	global $a;
	global $showrecs;
	global $page;
	global $ufilter;
	global $ufilterfield;
	global $uwholeonly;
	global $orderuits;
	global $ordtypeuits;
	global $dbprefix;


	if ($a == "reset") {
		$ufilter = "";
		$ufilterfield = "";
		$uwholeonly = "";
		$orderuits = "";
		$ordtypeuits = "";
	}

	$checkstr = "";
	if ($uwholeonly) $checkstr = " checked";
	if ($ordtypeuits == "asc") { $ordtypeuitsstr = "desc"; } else { $ordtypeuitsstr = "asc"; }
	$res = sql_select();
	$count = sql_getrecordcount();
	if ($count % $showrecs != 0) {
		$pagecount = intval($count / $showrecs) + 1;
	}
	else {
		$pagecount = intval($count / $showrecs);
	}
	$startrec = $showrecs * ($page - 1);
	if ($startrec < $count) {mysqli_data_seek($res, $startrec);}
	$reccount = min($showrecs * $page, $count);
	?>
	<table class="bdadmin">
	<tr><td>Getoonde wedstrijden <?php echo $startrec + 1 ?> - <?php echo $reccount ?>  van  <?php echo $count. "<br /><br />Alleen wedstrijden zonder uitslag en niet zijn afgelast worden getoond.<br />Een handmatige ingevoerde uitslag wordt voorzien van een '*'.<br />Bij een update worden handmatige toegevoegde uitslagen verwijderd." ?></td></tr>
	</table>
	<hr />
	<form action="uitslag.php" method="post">
	<table class="bdadmin">
	<tr>
	<td><select name="ufilter_field">
	<option value="">Clubuitslagen</option>
	<option value="<?php echo "Team Uitslagen" ?>"<?php if ($ufilterfield == "Team Uitslagen") { echo "selected"; } ?>><?php echo htmlspecialchars("Team Uitslagen") ?></option>
	<option value="<?php echo "Team Uitslagen Club" ?>"<?php if ($ufilterfield == "Team Uitslagen Club") { echo "selected"; } ?>><?php echo htmlspecialchars("Team Uitslagen Club") ?></option>
	</select></td>

	<tr><td><input type="submit" name="action" value="Filter toepassen"> <a href="uitslag.php?a=reset">&nbsp;Reset Filter</a></td>
	</tr>
	</table>
	</form>

	<hr />
	<?php showpagenav($page, $pagecount); ?>
	<br>
	<table class="tbl" style="width:100%">
	<tr>
	<td class="hr">&nbsp;</td>
	<td class="hr">&nbsp;</td>
	<td class="hr">ID</td>
	<td class="hr"><a class="hr" href="uitslag.php?orderuits=<?php echo "Datum" ?>&amp;typeuits=<?php echo $ordtypeuitsstr ?>"><?php echo htmlspecialchars("Datum") ?></a></td>
	<td class="hr"><a class="hr" href="uitslag.php?orderuits=<?php echo "Thuis" ?>&amp;typeuits=<?php echo $ordtypeuitsstr ?>"><?php echo htmlspecialchars("Thuis") ?></a></td>
	<td class="hr"><a class="hr" href="uitslag.php?orderuits=<?php echo "Uit" ?>&amp;typeuits=<?php echo $ordtypeuitsstr ?>"><?php echo htmlspecialchars("Uit") ?></a></td>
	<td class="hr"><a class="hr" href="uitslag.php?orderuits=<?php echo "Uitslag" ?>&amp;typeuits=<?php echo $ordtypeuitsstr ?>"><?php echo htmlspecialchars("Uitslag - Invoeren als *<spatie>-<spatie>*") ?></a></td>
	</tr>




	<?php
	for ($i = $startrec; $i < $reccount; $i++)
	{
		$row = mysqli_fetch_assoc($res);
		$style = "dr";
		$bstyle = "style=\"background-color: #FFFFFF; border: 1px solid #D0D0D0;\"";
		if ($i % 2 != 0) {
			$style = "sr";
			$bstyle = "style=\"background-color: #A6D2FF; border: 1px solid #D0D0D0;\"";

		}
		?>


		<form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF'];  ?>">
		<tr>


		<?php if ($ufilterfield == "Team Uitslagen" or $ufilterfield == "Team Uitslagen Club"){ ?>
			<td class="<?php echo $style ?>"><a href="uitslag.php?a=view&recid=<?php echo $i ?>">Bekijk</a></td>
			<td class="<?php echo $style ?>"><a href="uitslag.php?a=edit&recid=<?php echo $i ?>">Wijzig</a></td>
			<td class="<?php echo $style ?>"><input type="hidden" name="UitslagID[]" value="<?php echo $row['UitslagID']; ?>" /><?php echo $row['UitslagID']; ?></td>
			<td class="<?php echo $style ?>"><input type="hidden" name="Datum[]" value="<?php echo $row['Datum']; ?>" /><?php echo $row['Datum']; ?></td>
			<td class="<?php echo $style ?>"><input type="hidden" name="Thuis[]" value="<?php echo $row['Thuis']; ?>" /><?php echo $row['Thuis']; ?></td>
			<td class="<?php echo $style ?>"><input type="hidden" name="uit[]" value="<?php echo $row['Uit']; ?>" /><?php echo $row['Uit']; ?></td>
			<td class="<?php echo $style ?>"><input name="Uitslag<?php echo $row['UitslagID']; ?>" type="text" size=10 class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="Uitslag" value="<?php echo $row['Uitslag']; ?>"></td>
			<?php } else { ?>
			<td class="<?php echo $style ?>"><a href="uitslag.php?a=view&recid=<?php echo $i ?>">Bekijk</a></td>
			<td class="<?php echo $style ?>"><a href="uitslag.php?a=edit&recid=<?php echo $i ?>">Wijzig</a></td>
			<td class="<?php echo $style ?>"><input type="hidden" name="ClubUitslagID[]" value="<?php echo $row['ClubUitslagID']; ?>" /><?php echo $row['ClubUitslagID']; ?></td>
			<td class="<?php echo $style ?>"><input type="hidden" name="Datum[]" value="<?php echo $row['Datum']; ?>" /><?php echo $row['Datum']; ?></td>
			<td class="<?php echo $style ?>"><input type="hidden" name="Thuis[]" value="<?php echo $row['Thuis']; ?>" /><?php echo $row['Thuis']; ?></td>
			<td class="<?php echo $style ?>"><input type="hidden" name="uit[]" value="<?php echo $row['Uit']; ?>" /><?php echo $row['Uit']; ?></td>
			<td class="<?php echo $style ?>"><input name="Uitslag<?php echo $row['ClubUitslagID']; ?>" type="text" size=10 class="<?php echo $style; ?>" <?php echo $bstyle; ?>  id="Uitslag" value="<?php echo $row['Uitslag']; ?>"></td>
			<?php } ?>
		</tr>
		<?php
	}

	mysqli_free_result($res);
	?>
	<tr>
	<td class="center" colspan="7"><input type="submit" name="Submit" value="Wijzigingen aanbrengen" /></td>
	</tr>

	</table>
	<br>
	<?php showpagenav($page, $pagecount); ?>
	<?php }  ?>

<?php function login()
{
	include("config.php");
	global $_POST;
	global $_SESSION;

	global $_GET;
	if (isset($_GET["a"]) && ($_GET["a"] == 'logout')) $_SESSION["logged_in_uitslag"] = false;
	if (!isset($_SESSION["logged_in_uitslag"])) $_SESSION["logged_in_uitslag"] = false;
	if (!$_SESSION["logged_in_uitslag"]) {
		$login = "";
		$password2 = "";

		if (isset($_POST["login"])) $login = @$_POST["login"];
		if (isset($_POST["password"])) $password2 = @$_POST["password"];

		if (($login != "") && ($password2 != "")) {
			if (($login == $Uitslagusername) && ($password2 == $Uitslagpassword)) {
				$_SESSION["logged_in_uitslag"] = true;
			}
			else {
				?>
				<p><b><font color="-1">De combinatie gebruikersnaam/wachtwoord is onjuist</font></b></p>
				<?php } } }if (isset($_SESSION["logged_in_uitslag"]) && (!$_SESSION["logged_in_uitslag"])) { ?>
		<form action="uitslag.php" method="post">
		<table class="bdadmin">
		<tr>
		<td>Gebruikersnaam</td>
		<td><input type="text" name="login" value="<?php echo $login ?>"></td>
		</tr>
		<tr>
		<td>Wachtwoord</td>
		<td><input type="password" name="password" value="<?php echo $password2 ?>"></td>
		</tr>
		<tr>
		<td><input type="submit" name="action" value="Inloggen"></td>
		</tr>
		</table>
		</form>
		<?php
	}
	if (!isset($_SESSION["logged_in_uitslag"])) $_SESSION["logged_in_uitslag"] = false;
	return $_SESSION["logged_in_uitslag"];
} ?>

<?php function showrow($row, $recid)
{
	echo $hetfilter;
	if (isset($ufilterfield) && $ufilterfield!='') echo "xxxxx";
	?>
	<table class="tbl" style="width:50%">
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Datum")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Datum"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Thuis")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Thuis"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Uit")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Uit"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Uitslag")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Uitslag"]) ?></td>
	</tr>
	</table>
	<?php } ?>

<?php function showroweditor($row, $iseditmode)
{
	global $con;
	?>
	<table class="tbl" style="width:50%">
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Wedstrijdnr")."&nbsp;" ?></td>
	<td class="dr"><input type="text" name="Wedstrijdnr" value="<?php echo str_replace('"', '&quot;', trim($row["Wedstrijdnr"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Eigen Scheidsrechter")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="EScheidsrechter" maxlength="300" value="<?php echo str_replace('"', '&quot;', trim($row["EScheidsrechter"])) ?>"></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Vertrekverzameltijd")."&nbsp;" ?></td>
	<td class="dr"><input type="text" name="Vertrekverzameltijd" maxlength="5" value="<?php echo str_replace('"', '&quot;', trim($row["Vertrekverzameltijd"])) ?>"></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Zelf Afgelast")."&nbsp;" ?></td>
<?php if ($row["EAfgelast"] == "afgelast") { ?>
<td class="dr"><input type="checkbox" CHECKED name="EAfgelast" maxlength="10" value="<?php echo str_replace('"', '&quot;', trim($row["EAfgelast"])) ?>"> Indien aangevinkt is wedstrijd afgelast door Club.</td>
	<?php } else { ?>
	<td class="dr"><input type="checkbox" name="EAfgelast" maxlength="10" value="<?php echo str_replace('"', '&quot;', trim($row["EAfgelast"])) ?>"> Indien aangevinkt is wedstrijd afgelast door Club.</td>
<?php } ?>

</tr>

</table>
<?php } ?>

<?php function showroweditor2($row, $iseditmode)
{
global $con;
?>
<table class="tbl" style="width:50%">
<tr>
</tr>
<td class="<?php echo $style ?>"><input type="hidden" name="ClubUitslagID" value="<?php echo $row['ClubUitslagID']; ?>" /></td>
<td class="<?php echo $style ?>"><input type="hidden" name="UitslagID" value="<?php echo $row['UitslagID']; ?>" /></td>
<td class="<?php echo $style ?>"><input type="hidden" name="TeamID" value="<?php echo $row['TeamID']; ?>" /></td>
<tr>
<td class="hr"><?php echo htmlspecialchars("Datum")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Datum"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Thuis")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Thuis"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Uit")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Uit"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Uitslag")."&nbsp;" ?></td>
<td class="dr"><?php echo htmlspecialchars($row["Uitslag"]) ?></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Uitslag")."&nbsp;" ?></td>
<td class="dr"><input type="text" name="Uitslag" maxlength="10" value="<?php echo str_replace('"', '&quot;', trim($row["Uitslag"])) ?>">  Uitslag als * - *</td>
	</tr>
	</table>
	<?php } ?>

<?php function showpagenav($page, $pagecount)
{
	?>
	<table class="bdadmin">

	<?php if ($page > 1) { ?>
		<td><a href="uitslag.php?page=<?php echo $page - 1 ?>">&lt;&lt;&nbsp;Vorige</a>&nbsp;</td>
		<?php } ?>
	<?php
	global $pagerange;

	if ($pagecount > 1) {

		if ($pagecount % $pagerange != 0) {
			$rangecount = intval($pagecount / $pagerange) + 1;
		}
		else {
			$rangecount = intval($pagecount / $pagerange);
		}
		for ($i = 1; $i < $rangecount + 1; $i++) {
			$startpage = (($i - 1) * $pagerange) + 1;
			$count = min($i * $pagerange, $pagecount);

			if ((($page >= $startpage) && ($page <= ($i * $pagerange)))) {
				for ($j = $startpage; $j < $count + 1; $j++) {
					if ($j == $page) {
						?>
						<td><b><?php echo $j ?></b></td>
						<?php } else { ?>
						<td><a href="uitslag.php?page=<?php echo $j ?>"><?php echo $j ?></a></td>
						<?php } } } else { ?>
				<td><a href="uitslag.php?page=<?php echo $startpage ?>"><?php echo $startpage ."..." .$count ?></a></td>
				<?php } } } ?>
	<?php if ($page < $pagecount) { ?>
		<td>&nbsp;<a href="uitslag.php?page=<?php echo $page + 1 ?>">Volgende&nbsp;&gt;&gt;</a>&nbsp;</td>
		<?php } ?>

	</table>
	<?php } ?>

<?php function showrecnav($a, $recid, $count)
{
	?>
	<table class="bdadmin">
	<tr>
	<td><a href="uitslag.php">Index Pagina</a></td>
	<?php if ($recid > 0) { ?>
		<td><a href="uitslag.php?a=<?php echo $a ?>&recid=<?php echo $recid - 1 ?>">Vorige wedstrijd</a></td>
		<?php } if ($recid < $count - 1) { ?>
		<td><a href="uitslag.php?a=<?php echo $a ?>&recid=<?php echo $recid + 1 ?>">Volgende wedstrijd</a></td>
		<?php } ?>
	</tr>
	</table>
	<hr />
	<?php } ?>

<?php function addrec()
{
	?>
	<table class="bdadmin">
	<tr>
	<td><a href="uitslag.php">Index Pagina</a></td>
	</tr>
	</table>
	<hr />
	<form enctype="multipart/form-data" action="uitslag.php" method="post">
	<p><input type="hidden" name="sql" value="insert"></p>
	<?php
	$row = array(
	"Uitslag" => "");
	showroweditor($row, false);
	?>
	<p><input type="submit" name="action" value="Toevoegen"></p>
	</form>
	<?php } ?>

<?php function viewrec($recid)
{
	$res = sql_select();
	$count = sql_getrecordcount();
	mysqli_data_seek($res, $recid);
	$row = mysqli_fetch_assoc($res);
	showrecnav("view", $recid, $count);
	?>
	<br>
	<?php showrow($row, $recid) ?>
	<br>
	<hr />
	<table class="bdadmin">
	<tr>
	<td><a href="uitslag.php?a=edit&recid=<?php echo $recid ?>">Wedstrijd wijzigen</a></td>
	</tr>
	</table>
	<?php
	mysqli_free_result($res);
} ?>

<?php function editrec($recid)
{
	$res = sql_select();
	$count = sql_getrecordcount();
	mysqli_data_seek($res, $recid);
	$row = mysqli_fetch_assoc($res);
	showrecnav("edit", $recid, $count);
	?>
	<br>
	<form enctype="multipart/form-data" action="uitslag.php" method="post">
	<input type="hidden" name="sql" value="update">
	<input type="hidden" name="xExtraProgrammaID" value="<?php echo $row["ClubUitslagID"] ?>">
	<?php showroweditor2($row, true); ?>
	<p><input type="submit" name="action" value="Toevoegen"></p>
	</form>
	<?php
	mysqli_free_result($res);
} ?>

<?php function deleterec($recid)
{
	$res = sql_select();
	$count = sql_getrecordcount();
	mysqli_data_seek($res, $recid);
	$row = mysqli_fetch_assoc($res);
	showrecnav("del", $recid, $count);
	?>
	<br>
	<form action="uitslag.php" method="post">
	<input type="hidden" name="sql" value="delete">
	<input type="hidden" name="xExtraProgrammaID" value="<?php echo $row["ClubUitslagID"] ?>">
	<?php showrow($row, $recid) ?>
	<p><input type="submit" name="action" value="Bevestigen"></p>
	</form>
	<?php
	mysqli_free_result($res);
} ?>

<?php function connect()
{
	include("config.php");
	//$con = mysql_connect($server,$username,$password);
	//mysql_select_db($database);
	return $con;
}

function sqlvalue($val, $quote)
{
	if ($quote)
	$tmp = sqlstr($val);
	else
	$tmp = $val;
	if ($tmp == "")
	$tmp = "NULL";
	elseif ($quote)
	$tmp = "'".$tmp."'";
	return $tmp;
}

function sqlstr($val)
{
	return str_replace("'", "''", $val);
}

function sql_select()
{

	global $con;
	global $orderuits;
	global $ordtypeuits;
	global $ufilter;
	global $ufilterfield;
	global $uwholeonly;
	global $club1;
	global $dbprefix;

	$ufilterstr = sqlstr($ufilter);

	if ($ufilterfield == "Team Uitslagen")
	{
		$sql = "SELECT `UitslagID`, `TeamID`, `Datum`, `Thuis`, `Uit`, `Uitslag` FROM `".$dbprefix."uitslag` where TRIM(Uitslag) = '-' or `Uitslag` LIKE '%*'";
	}
	else
	{
	}

	if ($ufilterfield == "Team Uitslagen Club")
	{
		$sql = "SELECT `UitslagID`, `TeamID`, `Datum`, `Thuis`, `Uit`, `Uitslag` FROM `".$dbprefix."uitslag` WHERE (Thuis LIKE '$club1' or Uit LIKE '$club1')  AND (TRIM(Uitslag) = '-' or `Uitslag` LIKE '%*')";
	}
	else
	{
	}


	if ($ufilterfield == "")
	{
		$sql = "SELECT `ClubUitslagID`, `Datum`, `Thuis`, `Uit`, `Uitslag` FROM `".$dbprefix."clubuitslagen` where TRIM(Uitslag) = '-' or `Uitslag` LIKE '%*'";
	}
	else
	{
	}

	$res = mysqli_query($con,$sql) or die(mysqli_error($con));
	return $res;
}


function sql_getrecordcount()
{
	global $con;
	global $orderuits;
	global $ordtypeuits;
	global $ufilter;
	global $ufilterfield;
	global $uwholeonly;
	global $club1;
	global $dbprefix;

	if ($ufilterfield == "Team Uitslagen")
	{
		$sql = "SELECT COUNT(*) FROM `".$dbprefix."uitslag` where TRIM(Uitslag) = '-' or `Uitslag` LIKE '%*'";
	}
	else
	{
	}

	if ($ufilterfield == "Team Uitslagen Club")
	{
		$sql = "SELECT COUNT(*) FROM `".$dbprefix."uitslag` WHERE (Thuis LIKE '$club1' or Uit LIKE '$club1') AND (TRIM(Uitslag) = '-' or `Uitslag` LIKE '%*')";
	}
	else
	{
	}

	if ($ufilterfield == "")
	{
		$sql = "SELECT COUNT(*) FROM `".$dbprefix."clubuitslagen` where TRIM(Uitslag) = '-' or `Uitslag` LIKE '%*'";
	}
	else
	{
	}

	$res = mysqli_query($con,$sql) or die(mysqli_error($con));
	$row = mysqli_fetch_assoc($res);
	reset($row);
	return current($row);
}

function sql_insert()
{
	global $con;
	global $_POST;
	global $dbprefix;
	// Geen insert functie

}

function sql_update()
{
	global $con;
	global $orderuits;
	global $ordtypeuits;
	global $ufilter;
	global $ufilterfield;
	global $uwholeonly;
	global $dbprefix;

	//  global $_POST;
	//echo $ufilterfield;
	if ($ufilterfield == "Team Uitslagen")
	{
		$sql = "update `".$dbprefix."uitslag` set `Uitslag`=" .sqlvalue(@$_POST["Uitslag"]."*", true) ." where UitslagID=" .sqlvalue(@$_POST["UitslagID"], true);
	}
	else
	{
		$sql = "update `".$dbprefix."clubuitslagen` set `Uitslag`=" .sqlvalue(@$_POST["Uitslag"]."*", true) ." where ClubUitslagID=" .sqlvalue(@$_POST["ClubUitslagID"], true);
	}

	mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
}

function sql_delete()
{
	global $con;
	global $dbprefix;
	// Geen delete functie

}
function primarykeycondition()
{
	global $_POST;

	if ($ufilterfield == "Team Uitslagen")
	{
		$pk = "";
		$pk .= "(`UitslagID`";
		if (@$_POST["xUitslagID"] == "") {
			$pk .= " IS NULL";
		}else{
			$pk .= " = " .sqlvalue(@$_POST["xUitslagID"], false);
		};
		$pk .= ")";
	}
	else
	{

		$pk = "";
		$pk .= "(`ClubUitslagID`";
		if (@$_POST["xClubUitslagID"] == "") {
			$pk .= " IS NULL";
		}else{
			$pk .= " = " .sqlvalue(@$_POST["xClubUitslagID"], false);
		};
		$pk .= ")";
	}
	return $pk;
}
?>