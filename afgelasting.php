<?php
// afgelasting.php
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl 
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl 
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van 
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel

// Weergave van gegevens behorende bij Voetbal.nl Ripper

include("config.php");

mysql_connect($server,$username,$password); 
@mysql_select_db($database) or die( "Unable to select database"); 

$query1 = "SELECT DISTINCT Tijd, Thuis, Uit, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Datum, STR_TO_DATE(Datum, '%d %b' ) AS date_for_sort 
FROM `".$dbprefix."afgelasting` WHERE Thuis Like '$club1'
UNION
SELECT DISTINCT Tijd, Thuis, Uit, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Datum, STR_TO_DATE(Datum, '%d %b' ) AS date_for_sort 
FROM `".$dbprefix."oefenprogramma` WHERE Thuis Like '$club1' AND Status Like 'afgelast'
ORDER BY date_for_sort, Thuis DESC"; 
$result1=mysql_query($query1); 
$num1=mysql_numrows($result1);

$query2 = "SELECT DISTINCT 
 Tijd, Thuis, Uit, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Datum, STR_TO_DATE(Datum, '%d %b' ) AS date_for_sort  
 FROM `".$dbprefix."afgelasting` WHERE Uit Like '$club2'
 UNION
 SELECT DISTINCT Tijd, Thuis, Uit, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Datum, STR_TO_DATE(Datum, '%d %b' ) AS date_for_sort 
 FROM `".$dbprefix."oefenprogramma` WHERE Thuis NOT LIKE '$club1' AND Status Like 'afgelast'
 ORDER BY date_for_sort, Uit"; 
$result2=mysql_query($query2); 
$num2=mysql_numrows($result2); 

$query3 = "SELECT * FROM clubnaam WHERE ClubID='1'"; 
$result3=mysql_query($query3); 

$query4 = "SHOW TABLE STATUS from ".$database." LIKE '".$dbprefix."clubprogramma'"; 
$result4=mysql_query($query4); 

mysql_close(); 


?> 
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">  

    <head>  

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />  

        <title>Afgelasting <?php echo $clubnaam; ?></title>  

<?php echo "<link rel=\"stylesheet\" type=\"text/css\" href='$CSSweergave' />"; ?>

    </head>  
      
<body>  
<div style="text-align:center"><br/>
 	<h2>Afgelasting Thuiswedstrijden <?php echo $clubnaam; ?></h2> </div> 
<div style="text-align:center">
<table class="clubprogramma"> 
<tr> 
<th class="left" style="width:50px">Datum</th> 
<th class="left" style="width:50px">Tijd</th> 
<th class="left" style="width:170px">Thuis</th> 
<th class="left" style="width:170px">Uit</th> 
<th class="left" style="width:30px">T</th> 
<th class="left" style="width:230px">Accommodatie</th> 
<th class="left" style="width:50px">Wed<br />Nr</th> 
<th class="left" style="width:50px">Status</th> 
</tr> 

<?php 
$rowclass = 0; 
$i=0; 
while ($i < $num1) { 

$datum=mysql_result($result1,$i,"Datum"); 
$tijd=mysql_result($result1,$i,"Tijd"); 
$thuis=mysql_result($result1,$i,"Thuis"); 
$uit=mysql_result($result1,$i,"Uit"); 
$type=mysql_result($result1,$i,"Type"); 
$accommodatie=mysql_result($result1,$i,"Accommodatie"); 
$wedstrijdnr=mysql_result($result1,$i,"Wedstrijdnr");
$status=mysql_result($result1,$i,"Status"); 
?> 

<tr> 
<td class="row<?= $rowclass ?>"><?php echo $datum; ?></td>  
<td class="row<?= $rowclass ?>"><?php echo $tijd; ?></td> 
<td class="row<?= $rowclass ?>"><?php echo $thuis; ?></td> 
<td class="row<?= $rowclass ?>"><?php echo $uit; ?></td> 
<td class="row<?= $rowclass ?>"><?php echo $type; ?></td> 
<td class="row<?= $rowclass ?>"><?php echo $accommodatie; ?></td> 
<td class="row<?= $rowclass ?>"><?php echo $wedstrijdnr; ?></td> 
<td class="row<?= $rowclass ?>"><?php echo $status; ?></td> 
</tr> 

<?php 
$i++; 
$rowclass = 1 - $rowclass; 

} 
IF ($num1==0) { ?>
<tr> 
<td class="center" colspan="8"><br /><b><?php echo 'Er zijn geen afgelaste thuiswedstrijden bekend'; ?></b><br /></td>
</tr>
<?php }

echo "</table>"; 
?>

<h2>Afgelasting Uitwedstrijden <?php echo $clubnaam; ?></h2> </div> 
<div style="text-align:center">
<table class="clubprogramma"> 
<tr> 
<th class="left" style="width:50px">Datum</th> 
<th class="left" style="width:50px">Tijd</th> 
<th class="left" style="width:170px">Thuis</th> 
<th class="left" style="width:170px">Uit</th> 
<th class="left" style="width:30px">T</th> 
<th class="left" style="width:230px">Accommodatie</th> 
<th class="left" style="width:50px">Wed<br />Nr</th> 
<th class="left" style="width:50px">Status</th>  
</tr> 

<?php 
$rowclass = 0; 
$i=0; 
while ($i < $num2) { 

$datum=mysql_result($result2,$i,"Datum"); 
$tijd=mysql_result($result2,$i,"Tijd"); 
$thuis=mysql_result($result2,$i,"Thuis"); 
$uit=mysql_result($result2,$i,"Uit"); 
$type=mysql_result($result2,$i,"Type"); 
$accommodatie=mysql_result($result2,$i,"Accommodatie"); 
$wedstrijdnr=mysql_result($result2,$i,"Wedstrijdnr"); 
$status=mysql_result($result2,$i,"Status"); 
?> 

<tr> 
<td class="row<?= $rowclass ?>"><?php echo $datum; ?></td>  
<td class="row<?= $rowclass ?>"><?php echo $tijd; ?></td> 
<td class="row<?= $rowclass ?>"><?php echo $thuis; ?></td> 
<td class="row<?= $rowclass ?>"><?php echo $uit; ?></td> 
<td class="row<?= $rowclass ?>"><?php echo $type; ?></td> 
<td class="row<?= $rowclass ?>"><?php echo $accommodatie; ?></td> 
<td class="row<?= $rowclass ?>"><?php echo $wedstrijdnr; ?></td> 
<td class="row<?= $rowclass ?>"><?php echo $status; ?></td> 
</tr> 

<?php 
$i++; 
$rowclass = 1 - $rowclass; 

} 
IF ($num2==0) { ?>
<tr> 
<td class="center" colspan="8"><br /><b><?php echo 'Er zijn geen afgelaste uitwedstrijden bekend'; ?></b><br /></td>
</tr>
<?php }

echo "</table>"; 
?> 

<table class="clubprogramma"> 
    <tr> 
        <td class="small"><br />Bijgewerkt op: <?php  
            setlocale(LC_ALL, 'nl_NL'); 
            echo strftime('%d/%m/%y - %H:%M', strtotime(mysql_result($result4,0,'Update_time'))); ?></td>  
    </tr> 
      <tr>
    	 <td class="left"><br />Bron: <a href='http://www.voetbal.nl' target='_blank'>Voetbal.nl</a></td>
    </tr>   

</table>

</div> 
</body> 
</html>