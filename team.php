<?php
// team.php
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel


// Weergave van team gegevens behorende bij Voetbal.nl Ripper

include("config.php");
include("functies.php");

mysql_connect($server,$username,$password);
@mysql_select_db($database) or die( "Unable to select database");


$teamID = filter_var($_GET['teamID'], FILTER_VALIDATE_INT); //alleen numerieke waarde toegstaan - voorkomt mogelijkheid tot sql injection
If ($teamID == "") $teamID = 3;

//Query om de uitslagen per team op te halen
$query1 = "SELECT DISTINCT Uit, Thuis, Uitslag, Datum
FROM `".$dbprefix."uitslag` WHERE teamID=$teamID and to_days(datum) - to_days(now()) between - '$MinDagUitslagen' AND '$PlusDagUitslagen' ORDER BY Datum, Thuis, Uit ";

$result1=mysql_query($query1);
$num1=mysql_numrows($result1);


//Query om de stand per team op te halen
$query2 = "SELECT * FROM ".$dbprefix."stand WHERE teamID=$teamID ORDER BY Plaats ASC";

$result2=mysql_query($query2);
$num2=mysql_numrows($result2);

//Query om teamnaam en klasse op te halen
$query3 = "SELECT * FROM ".$dbprefix."teamlinks WHERE teamID=$teamID";
$result3=mysql_query($query3);
$checkid=mysql_num_rows($result3);
$klasse=mysql_result($result3,0,"Klasse");
$naam=mysql_result($result3,0,"Naam");
$wedstrijdduur=mysql_result($result3,0,"Wedstrijdduur");
//$sponsor=mysql_result($result3,$i,"Sponsor");

//Controle op juiste teamID
if ($checkid < 1)
{
	echo "Team ID niet gevonden. Gebruik team.php?teamID=* op de plaats van * moet een geldige teamID staan.";
	die;
}
else
{
}

//Query om programma per team op te halen

if ($Programmadetails == "Aan" AND $Oefeninteam == "Aan")
{
	$query4= "SELECT DISTINCT Tijd, '' AS Thuis, '' AS Uit, Wedstrijd, Type, Accommodatie, Datum, Wedstrijdnr, Status, Adres, Postcode, Plaats, Telefoon
FROM `".$dbprefix."programma` WHERE teamID=$teamID
UNION
SELECT DISTINCT Tijd, Thuis, Uit, '' AS Wedstrijd, Type,  Accommodatie,  Datum, Wedstrijdnr AS Type,  Status, Adres, Postcode, Plaats, Telefoon
FROM `".$dbprefix."oefenprogramma` WHERE Thuis Like '$naam' OR UIT Like '$naam' AND Uitslag Like 'ng'
ORDER BY Datum, Tijd";
}

if ($Programmadetails == "Aan" AND $Oefeninteam == "Uit")
{
	$query4= "SELECT DISTINCT Tijd, '' AS Thuis, '' AS Uit, Wedstrijd, Type, Accommodatie, Datum, Wedstrijdnr, Status, Adres, Postcode, Plaats, Telefoon
FROM `".$dbprefix."programma` WHERE teamID=$teamID
ORDER BY Datum, Tijd";
}

if ($Programmadetails == "Uit" AND $Oefeninteam == "Aan")
{
	$query4= "SELECT DISTINCT Tijd, '' AS Thuis, '' AS Uit, Wedstrijd, Type, Accommodatie, Datum, Wedstrijdnr, Status
FROM `".$dbprefix."programma` WHERE teamID=$teamID
UNION
SELECT DISTINCT Tijd, Thuis, Uit, '' AS Wedstrijd, Type,  Accommodatie,  Datum, Wedstrijdnr,  Status
FROM `".$dbprefix."oefenprogramma` WHERE Thuis Like '$naam' OR UIT Like '$naam' AND Uitslag Like 'ng'
ORDER BY Datum, Tijd";
}

if ($Programmadetails == "Uit" AND $Oefeninteam == "Uit")
{
	$query4= "SELECT DISTINCT Tijd, '' AS Thuis, '' AS Uit, Wedstrijd, Type, Accommodatie, Datum, Wedstrijdnr, Status
FROM `".$dbprefix."programma` WHERE teamID=$teamID
ORDER BY Datum, Tijd";
}




$result4=mysql_query($query4);
$num4=mysql_numrows($result4);
// kijken of statusvelden zijn gevuld in de programma tabel
$statquery = "SELECT * FROM ".$dbprefix."programma where teamID=$teamID AND Status <> ''";
$statresult = mysql_query($statquery);
$statresult = mysql_num_rows($statresult);


//Aantal uitslagen per klasse tellen
$result5 = mysql_query("SELECT Uitslag FROM ".$dbprefix."alle WHERE teamID=$teamID");
$numrows5 = mysql_num_rows($result5);

//Queries om Alle Uitslagen per klasse op te halen
$a = round($numrows5/2); //Bepalen van aantal uitslagen die in de linkertabel moeten komen
$query6 = "SELECT * FROM ".$dbprefix."alle WHERE teamID=$teamID ORDER BY Wedstrijd";
$result6 = mysql_query($query6);
$num6=mysql_numrows($result6);

// Query om de datum van laatste update op te halen
$query7 = "SHOW TABLE STATUS from ".$database." LIKE 'uitslag'";
$result7=mysql_query($query7);

//Aantal uitslagen tellen met *
$result8  = mysql_query("SELECT `Uitslag` FROM `".$dbprefix."uitslag` WHERE teamID=$teamID and `Uitslag` like '%*' and Datum >= DATE_SUB(CURRENT_DATE, INTERVAL '$MinDagUitslagen' DAY)");
$numrows8 = mysql_num_rows($result8);

//Aantal teams per indeling
$result15 = mysql_query("SELECT * FROM ".$dbprefix."teamindeling WHERE teamID=$teamID");
$numrows15 = mysql_num_rows($result15);

//Queries om teamindeling op te halen
$aa = round($numrows15/2); //Bepalen van aantal team die in de linkertabel moeten komen
$query16 = "SELECT * FROM ".$dbprefix."teamindeling WHERE teamID=$teamID";
$result16 = mysql_query($query16);
$num16=mysql_numrows($result16);


$result15a = mysql_query("SELECT * FROM ".$dbprefix."speelronde WHERE teamID=$teamID and Wedstrijd like '%speelronde%'");
$numrows15a = mysql_num_rows($result15a);
if($numrows15a % 2) $numrows15a = $numrows15a+"1";
$aantalteams= round($numrows15/2);
$aantalteams= $aantalteams + "1";
$berek= $numrows15a * $aantalteams;

$a2 = round($berek/2); //Bepalen van aantal indelingen die in de linkertabel moeten komen
$query16a = "SELECT * FROM ".$dbprefix."speelronde WHERE teamID=$teamID";
$result16a = mysql_query($query16a);
$num16a=mysql_numrows($result16a);

// Query om de datum van laatste update op te halen
$query17 = "SHOW TABLE STATUS from ".$database." LIKE 'speelronde'";
$result17=mysql_query($query17);


mysql_close();

?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?php echo $naam; ?></title>

<?php echo "<link rel=\"stylesheet\" type=\"text/css\" href='$CSSweergave' />"; ?>

</head>

<body>
<div style="text-align:center"><br/>
<h2><?php echo $naam; ?></h2> </div>
<h3><?php echo $klasse; ?></h3>

<h3>Uitslagen</h3>
<table class="stand">
<tr>
<th class="left">Datum</th>
<th class="left">Thuis</th>
<th class="left">Uit</th>
<th class="center">Uitslag</th>
</tr>

<?php
$i=0;
while ($i < $num1) {

	$datum=makeNiceDate(mysql_result($result1,$i,"Datum"));
	$thuis=mysql_result($result1,$i,"Thuis");
	$uit=mysql_result($result1,$i,"Uit");
	$uitslag=mysql_result($result1,$i,"Uitslag");
	If ($uitslag == 'afgelast'){
		$uitslag =  "<div class=\"afgelastblack\">".$uitslag."</div>";
	}
	?>

	<tr>
	<?php IF (strstr($uit,$clubnaam) or strstr($thuis,$clubnaam)) { ?>
		<td class="left3"><?php echo $datum; ?></td>
		<td class="left3"><?php echo $thuis; ?></td>
		<td class="left3"><?php echo $uit; ?></td>
		<td class="center3"><?php echo $uitslag; ?></td>
		</tr>
		<?php }
	ELSE { ?>
		<td class="left"><?php echo $datum; ?></td>
		<td class="left"><?php echo $thuis; ?></td>
		<td class="left"><?php echo $uit; ?></td>
		<td class="center"><?php echo $uitslag; ?></td>
		</tr>
		<?php } ?>

	<?php
	$i++;
} ?>
</table>
<table class="uitslagen">

<?php if ($numrows8  > '0') echo "<td class='small'><br />Uitslagen met een * zijn nog niet verwerkt. <br /></td>"; ?>
<tr>
<td>
</td>
</tr>

<?php IF ($num1==0) { ?>
	<tr>
	<td class="center" colspan="4"><br /><b><?php echo 'Er zijn geen actuele uitslagen bekend'; ?></b><br /></td>
	</tr>


	<?php }

echo "</table>";
?>

<h3>Stand</h3>
<table  class="stand">
<tr>
<th class="left" style="width:30px">Pl</th>
<th class="left" style="width:170px">Elftal</th>
<th class="center" style="width:30px">G</th>
<th class="center" style="width:30px">W</th>
<th class="center" style="width:30px">GL</th>
<th class="center" style="width:30px">V</th>
<th class="center" style="width:30px">P</th>
<th class="center" style="width:30px">DPV</th>
<th class="center" style="width:30px">DPT</th>
<th class="center" style="width:30px">PM</th>
</tr>

<?php
$i=0;
while ($i < $num2) {

	$plaats=mysql_result($result2,$i,"Plaats");
	$elftal=mysql_result($result2,$i,"Elftal");
	$g=mysql_result($result2,$i,"G");
	$w=mysql_result($result2,$i,"W");
	$gw=mysql_result($result2,$i,"GW");
	$v=mysql_result($result2,$i,"V");
	$p=mysql_result($result2,$i,"P");
	$dpv=mysql_result($result2,$i,"DPV");
	$dpt=mysql_result($result2,$i,"DPT");
	$pm=mysql_result($result2,$i,"PM");

	?>

	<tr>
	<?php IF (strstr($elftal,$clubnaam)) { ?>
		<td class="left3" style="width:30px"><?php echo $plaats; ?></td>
		<td class="left3"><?php echo $elftal; ?></td>
		<td class="center3" style="width:30px"><b><?php echo $g; ?></b></td>
		<td class="center3" style="width:30px"><?php echo $w; ?></td>
		<td class="center3" style="width:30px"><?php echo $gw; ?></td>
		<td class="center3" style="width:30px"><?php echo $v; ?></td>
		<td class="center3" style="width:30px"><b><?php echo $p; ?></b></td>
		<td class="center3" style="width:30px"><?php echo $dpv; ?></td>
		<td class="center3" style="width:30px"><?php echo $dpt; ?></td>
		<td class="center3" style="width:30px"><?php echo $pm; ?></td>
		<?php }
	ELSE { ?>
		<td class="left" style="width:30px"><?php echo $plaats; ?></td>
		<td class="left"><?php	echo $elftal; ?></td>
		<td class="center" style="width:30px"><b><?php echo $g; ?></b></td>
		<td class="center" style="width:30px"><?php echo $w; ?></td>
		<td class="center" style="width:30px"><?php echo $gw; ?></td>
		<td class="center" style="width:30px"><?php echo $v; ?></td>
		<td class="center" style="width:30px"><b><?php echo $p; ?></b></td>
		<td class="center" style="width:30px"><?php echo $dpv; ?></td>
		<td class="center" style="width:30px"><?php echo $dpt; ?></td>
		<td class="center" style="width:30px"><?php echo $pm; ?></td>
		<?php } ?>
	</tr>

	<?php
	$i++;
}

echo "</table>";
?>
<table style="width:600px" class="stand">
<tr>
<td class="small">| G: gespeeld | W: gewonnen | GL: gelijk | V: verloren | P: punten |<br />
|DPV: doelpunten voor | DPT: doelpunten tegen | PM: punten in mindering |
</td>
</tr></table>

<h3>Programma</h3>

<table class="programma">
<tr>
<th class="left">Datum</th>
<th class="left">Tijd</th>
<th class="left">Wedstrijd</th>
<th class="left">Accommodatie</th>
<?php if ($Programmadetails == 'Aan') echo "<th class='left'>Wed<br />Nr</th>"; ?>
<?php if ($statresult  > '0') echo "<th class='left'>Status</th>"; ?>
</tr>

<?php

$i=0;
while ($i < $num4) {

	$datum=makeNiceDate(mysql_result($result4,$i,"Datum"));
	$rdatum=mysql_result($result4,$i,"Datum");
	$tijd=mysql_result($result4,$i,"Tijd");
	$status=mysql_result($result4,$i,"Status");
	$type=mysql_result($result4,$i,"Type");

	if ($type == "oe")
	{
		$thuis=mysql_result($result4,$i,"Thuis");
		$uit=mysql_result($result4,$i,"Uit");
		$wedstrijd=$thuis." - ".$uit;

		$wedstrijdnr=mysql_result($result4,$i,"Wedstrijdnr");
		$accommodatie=mysql_result($result4,$i,"Accommodatie");
		$accommodatie=$accommodatie." - ".$wedstrijdnr;

	}
	else
	{
		$wedstrijd=mysql_result($result4,$i,"Wedstrijd");
		$accommodatie=mysql_result($result4,$i,"Accommodatie");

	}


	if ($Programmadetails == "Aan")
	{
		$wedstrijdnr=mysql_result($result4,$i,"Wedstrijdnr");
		$adres=mysql_result($result4,$i,"Adres");
		$postcode=mysql_result($result4,$i,"Postcode");
		$plaats=mysql_result($result4,$i,"Plaats");
		$telefoon=mysql_result($result4,$i,"Telefoon");
	}

	$uitwedstrijd =strstr ($wedstrijd, " - ");
	if(empty($status))
	{
		// Status is leeg
	}
	else
	{
		// Status nietleeg dus afgelast
		$status  =  "<div class=\"afgelastblack\">".$status."</div>";
		$accommodatie = "<div class=\"afgelastred\">".$status."</div>";




	}


	?>

	<tr>
	<?php IF (strstr($wedstrijd,$clubnaam)) { ?>
		<td class="left3"><?php echo $datum; ?></td>
		<td class="left3"><?php echo $tijd; ?></td>
		<td class="left3"><?php echo $wedstrijd; ?></td>
		<?php if ($statresult  == '0' AND $Wedstrijddetailstonen == "Uit") echo "<td class='left3'>$accommodatie</td></tr>";?>
		<?php if ($statresult  > '0' AND $Wedstrijddetailstonen == "Uit" ) echo "<td class='left3'>$accommodatie</td><td class='left3'>$status</td></tr>";?>

		<?php if ($Wedstrijddetailstonen == "Aan") { ?>

			<?php if ($statresult  == '0') echo "<td class='left3'>$accommodatie</td><td class='left3'>$wedstrijdnr</td></tr>";?>
			<?php if ($statresult  > '0') echo "<td class='left3'>$accommodatie</td><td class='left3'>$wedstrijdnr</td><td class='left3'>$status</td></tr>";?>

			<tr>
			<td class="left3"><?php echo $adres; ?></td>
			<td class="left3"><?php echo $postcode; ?></td>
			<td class="left3"><?php echo $plaats; ?></td>
			<td class="left3"><?php echo $telefoon; ?></td>
			<?php
			$icsarg= urlencode ("datum=$rdatum&amp;tijd=$tijd&amp;wedstrijd=$wedstrijd&amp;wedstrijdduur=$wedstrijdduur&amp;accommodatie=$accommodatie
&amp;wedstrijdnr=$wedstrijdnr&amp;adres=$adres&amp;postcode=$postcode&amp;plaats=$plaats&amp;telefoon=$telefoon");
			//echo $icsarg;
			?>
			<td class="left3"><?php echo "<a href=\"ics.php?datum=$rdatum&amp;tijd=$tijd&amp;wedstrijd=". urlencode($wedstrijd). "&amp;wedstrijdduur=$wedstrijdduur&amp;accommodatie=". rawurlencode($accommodatie).
			"&amp;wedstrijdnr=$wedstrijdnr&amp;adres=". urlencode($adres). "&amp;postcode=". urlencode($postcode). "&amp;plaats=". urlencode($plaats). "&amp;telefoon=". urlencode($telefoon). "\">iCalendar</a></td>"; ?><?php if ($statresult  > '0') echo "<td class='left3'>&nbsp;</td>";?>
			</tr>


			<?php } ?>
		<?php }
	ELSE { ?>

		<td class="left"><?php echo $datum; ?></td>
		<td class="left"><?php echo $tijd; ?></td>
		<td class="left"><?php echo $wedstrijd; ?></td>

		<?php if ($statresult  == '0' AND $Wedstrijddetailstonen == "Uit") echo "<td class='left'>$accommodatie</td></tr>";?>
		<?php if ($statresult  == '0' AND $Wedstrijddetailstonen == "Aan") echo "<td class='left'>$accommodatie</td><td class='left'>$wedstrijdnr</td><td class='left'>$status</td></tr>";?>
		<?php if ($statresult  > '0' AND $Wedstrijddetailstonen == "Uit" ) echo "<td class='left'>$accommodatie</td><td class='left'>$status</td></tr>";?>
		<?php if ($statresult  > '0' AND $Wedstrijddetailstonen == "Aan" ) echo "<td class='left'>$accommodatie</td><td class='left'>$wedstrijdnr</td><td class='left'>$status</td></tr>";?>
		<?php } ?>
	<?php

	$i++;
}


IF ($num4==0) { ?>
	<tr>
	<td class="center" colspan="5"><br /><b><?php echo 'Er is geen actueel programma bekend'; ?></b><br /></td>
	</tr>
	<?php }
echo "</table>";

?>

<h3>Alle Uitslagen</h3>
<table class="alle">

<tr><td style="width:350px; vertical-align:top;">
<table style="width:350px">
<tr>
<th class="left" style="width:300px">Wedstrijd</th>
<th class="center" style="width:50px">Uitslag</th>
</tr>
<?php
$i=0;
while ($i < $a) {
	$wedstrijd=mysql_result($result6,$i,"Wedstrijd");
	$uitslag=mysql_result($result6,$i,"Uitslag");
	?>
	<tr>
	<?php IF (strstr($wedstrijd,$clubnaam)) { ?>
		<td class="left23" style="width:295px"><?php echo $wedstrijd; ?></td>
		<td class="center23" style="width:50px"><?php echo $uitslag; ?></td>
		</tr>
		<?php }
	ELSE { ?>

		<td class="left2" style="width:295px"><?php echo $wedstrijd; ?></td>
		<td class="center2" style="width:50px"><?php echo $uitslag; ?></td>
		</tr>
		<?php } ?>
	<?php
	$i++;
}
echo "</table></td>"; ?>


<td style="width:350px; vertical-align:top;">
<table style="width:350px">
<tr>
<th class="left" style="width:300px">Wedstrijd</th>
<th class="center" style="width:50px">Uitslag</th>
</tr>
<?php
$i=$a;
while ($i < $num6)  {
	$wedstrijd2=mysql_result($result6,$i,"Wedstrijd");
	$uitslag2=mysql_result($result6,$i,"Uitslag");
	?>
	<tr>
	<?php IF (strstr($wedstrijd2,$clubnaam)) { ?>
		<td class="left23" style="width:300px"><?php echo $wedstrijd2; ?></td>
		<td class="center23" style="width:50px"><?php echo $uitslag2; ?></td>
		</tr>
		<?php }
	ELSE { ?>
		<td class="left2" style="width:300px"><?php echo $wedstrijd2; ?></td>
		<td class="center2" style="width:50px"><?php echo $uitslag2; ?></td>
		</tr>
		<?php } ?>
	<?php
	$i++;
}
echo "</table></td>";
echo "</tr></table>";



?>

<?php if ($Tindelinginteam == "Aan") { ?>

	<h3>Team Indeling</h3>
	<table class="alle">

	<tr><td style="width:350px; vertical-align:top;">
	<table style="width:350px">
	<tr>
	<th class="left" style="width:25px">nr</th>
	<th class="left" style="width:300px">Team</th>
	<th class="center" style="width:50px">Voorkeurstijd</th>
	</tr>
	<?php
	$i=0;
	while ($i < $aa) {
		$teamnr=mysql_result($result16,$i,"Teamnr");
		$naam=mysql_result($result16,$i,"Naam");
		$vktijd=mysql_result($result16,$i,"Vktijd");

		?>
		<tr>
		<?php IF (strstr($naam,$clubnaam)) { ?>
			<td class="left23" style="width:25px"><?php echo $teamnr; ?></td>
			<td class="left23" style="width:295px"><?php echo $naam; ?></td>
			<td class="center23" style="width:50px"><?php echo $vktijd; ?></td>
			</tr>
			<?php }
		ELSE { ?>
			<td class="left2" style="width:25px"><?php echo $teamnr; ?></td>
			<td class="left2" style="width:295px"><?php echo $naam; ?></td>
			<td class="center2" style="width:50px"><?php echo $vktijd; ?></td>
			</tr>
			<?php } ?>
		<?php
		$i++;
	}
	echo "</table></td>"; ?>


	<td style="width:350px; vertical-align:top;">
	<table style="width:350px">
	<tr>
	<th class="left" style="width:25px">nr</th>
	<th class="left" style="width:300px">Team</th>
	<th class="center" style="width:50px">Voorkeurstijd</th>
	</tr>
	<?php
	$i=$aa;
	while ($i < $num16)  {
		$teamnr2=mysql_result($result16,$i,"Teamnr");
		$naam2=mysql_result($result16,$i,"Naam");
		$vktijd2=mysql_result($result16,$i,"Vktijd");
		?>
		<tr>
		<?php IF (strstr($naam2,$clubnaam)) { ?>
			<td class="left23" style="width:25px"><?php echo $teamnr2; ?></td>
			<td class="left23" style="width:300px"><?php echo $naam2; ?></td>
			<td class="center23" style="width:50px"><?php echo $vktijd2; ?></td>
			</tr>
			<?php }
		ELSE { ?>
			<td class="left2" style="width:25px"><?php echo $teamnr2; ?></td>
			<td class="left2" style="width:300px"><?php echo $naam2; ?></td>
			<td class="center2" style="width:50px"><?php echo $vktijd2; ?></td>
			</tr>
			<?php } ?>
		<?php
		$i++;
	}
	echo "</table></td>";
	echo "</tr></table>";



	?>

	<h3>Speelronde</h3>
	<table class="alle">

	<tr><td style="width:350px; vertical-align:top;">
	<table style="width:350px">
	<tr>
	<th class="left" style="width:400px">Team</th>
	</tr>
	<?php
	$i=0;
	while ($i < $a2) {
		$speelrondenr=mysql_result($result16a,$i,"Speelrondenr");
		$wedstrijd=mysql_result($result16a,$i,"Wedstrijd");

		?>
		<tr>
		<?php IF (strstr($wedstrijd,"speelronde")) $wedstrijd = "<div class=\"speelronde\">".$wedstrijd."</div>"; ?>
		<?php IF (strstr($wedstrijd,$clubnaam)) { ?>

			<td class="left23" style="width:300px"><?php echo $wedstrijd; ?></td>

			</tr>
			<?php }
		ELSE { ?>

			<td class="left2" style="width:300px"><?php echo $wedstrijd; ?></td>

			</tr>
			<?php } ?>
		<?php
		$i++;
	}
	echo "</table></td>"; ?>


	<td style="width:350px; vertical-align:top;">
	<table style="width:350px">
	<tr>
	<th class="left" style="width:300px">Team</th>
	</tr>
	<?php
	$i=$a2;
	while ($i < $num16a)  {
		$speelrondenr2=mysql_result($result16a,$i,"Speelrondenr");
		$wedstrijd2=mysql_result($result16a,$i,"Wedstrijd");

		?>
		<tr>
		<?php IF (strstr($wedstrijd2,"speelronde")) $wedstrijd2 = "<div class=\"speelronde\">".$wedstrijd2."</div>"; ?>
		<?php IF (strstr($wedstrijd2,$clubnaam)) { ?>

			<td class="left23" style="width:300px"><?php echo $wedstrijd2; ?></td>

			</tr>
			<?php }
		ELSE { ?>

			<td class="left2" style="width:300px"><?php echo $wedstrijd2; ?></td>

			</tr>
			<?php } ?>
		<?php
		$i++;
	}
	echo "</table></td>";
	echo "</tr></table>";



	?>

	<?php }
ELSE { }?>

<table class="alle">
<tr>
<td class="small"><br />Bijgewerkt op: <?php
/* Set locale to Dutch */
if(getOS() == 'linux')
{
	setlocale(LC_ALL, array('nl_NL'));
}else{
	setlocale(LC_ALL, array('nld_nld'));
}
//          echo strftime('%d %B %Y', strtotime(mysql_result($result7,0,'Update_time')));
echo date('d/m/y : H:i', mysql_result($result3,0,'DatumTijd-Update'));

?>
</td>
</tr>
<tr>
<td class="left"><br />Bron: <a href='http://www.voetbal.nl' target='_blank'>Voetbal.nl</a></td>
</tr>
</table>

</body>
</html>