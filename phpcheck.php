<?php
// phpcheck.php
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>PHPCheck voor Voetbal.nl Ripper</title>
</head>

<body>

<?php


ob_start();
phpinfo();
$phpinfo = array('phpinfo' => array());
if(preg_match_all('#(?:<h2>(?:<a name=".*?">)?(.*?)(?:</a>)?</h2>)|(?:<tr(?: class=".*?")?><t[hd](?: class=".*?")?>(.*?)\s*</t[hd]>(?:<t[hd](?: class=".*?")?>(.*?)\s*</t[hd]>(?:<t[hd](?: class=".*?")?>(.*?)\s*</t[hd]>)?)?</tr>)#s', ob_get_clean(), $matches, PREG_SET_ORDER))
foreach($matches as $match)
if(strlen($match[1]))
$phpinfo[$match[1]] = array();
elseif(isset($match[3]))
$phpinfo[end(array_keys($phpinfo))][$match[2]] = isset($match[4]) ? array($match[3], $match[4]) : $match[3];
else
$phpinfo[end(array_keys($phpinfo))][] = $match[2];

echo "<b>Instellingen voor Voetbal.nl Ripper:</b>.<br />\n";
echo "Controleer de minimaal eisen en neem instellingen over in instellingen.php.<br />\n";
echo "<br />\n";
echo "Als geen waarde wordt weergegeven moet deze handmatig op de pagina worden opgezocht.<br />\n";
echo "<br />\n";
echo "PHP version moet minimaal 7.0<br />\n";
echo "<br />\n";
echo "Indien cURL support <b>NIET</b> enabled is werkt voetbal.nl ripper <b>NIET</b>.<br />\n";
echo "Tenzij in instellingen.php wordt aangegeven dat Curl niet enabled is. Curl wordt aanbevolen.<br />\n";
echo "<br />\n";
echo "Mysqli support moet enabled zijn.<br />\n";
echo "<br />\n";
echo "Indien een waarde 'no value' is kan men aanemen dat het uit is.<br />\n";
echo "<br />\n";
echo "PHP Version:<b> {$phpinfo['Core']['PHP Version']}</b><br />\n";
echo "cURL Support:<b> {$phpinfo['curl']['cURL support']}</b><br />\n";
echo "PHP Mysqli Support:<b> {$phpinfo['mysqli']['MysqlI Support']}</b><br />\n";
echo "PHP safe_mode:<b> {$phpinfo['Core']['safe_mode'][0]}</b><br />\n";
echo "PHP open_basedir:<b> {$phpinfo['Core']['open_basedir'][0]}</b><br />\n";
echo "PHP max_execution_time:<b> {$phpinfo['Core']['max_execution_time'][0]}</b><br />\n";
echo " <br />\n";
phpinfo();
?>
</body>
</html>

