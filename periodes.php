<?php
// periodes.php
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl 
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl 
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van 
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel

 
// Weergave van gegevens behorende bij Voetbal.nl Ripper

include("config.php");

mysql_connect($server,$username,$password); 
@mysql_select_db($database) or die( "Unable to select database"); 

$teamID = filter_var($_GET['teamID'], FILTER_VALIDATE_INT); //alleen numerieke waarde toegstaan - voorkomt mogelijkheid tot sql injection


$query1 = "SELECT * FROM ".$dbprefix."standp1 WHERE teamID=$teamID"; 
$result1=mysql_query($query1);

$query2 = "SELECT * FROM ".$dbprefix."standp2 WHERE teamID=$teamID"; 
$result2=mysql_query($query2);  

$query3 = "SELECT * FROM ".$dbprefix."standp3 WHERE teamID=$teamID"; 
$result3=mysql_query($query3);    

$query4 = "SELECT * FROM ".$dbprefix."teamlinks WHERE teamID=$teamID"; 
$result4=mysql_query($query4); 
$checkid=mysql_num_rows($result4);  


if ($checkid < 1)  
{  
echo "Team ID niet gevonden. Gebruik periodes.php?teamID=* op de plaats van * moet een geldige teamID staan.";  
die;  
}  
else  
{  
}

$query5 = "SHOW TABLE STATUS from ".$database." LIKE '".$dbprefix."standp1'"; 
$result5=mysql_query($query5); 

$num1=mysql_numrows($result1); 
$num2=mysql_numrows($result2);
$num3=mysql_numrows($result3); 
$num4=mysql_numrows($result4); 
$klasse=mysql_result($result4,$i,"Klasse"); 
$naam=mysql_result($result4,$i,"Naam");
//$sponsor=mysql_result($result4,$i,"Sponsor");
//$alt=mysql_result($result4,$i,"Alt");

mysql_close(); 


?> 
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">  

    <head>  

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />  

        <title><?php echo $naam; ?></title>  

<?php echo "<link rel=\"stylesheet\" type=\"text/css\" href='$CSSweergave' />"; ?>

</head>  
      
<body>  
<div style="text-align:center"><br/>
<h2><?php echo $naam; ?></h2> 
<h3>Stand 1e periode <?php echo $klasse; ?></h3>  
<table class="stand">
<tr> 
<th class="left" style="width:30px">Pl</th> 
<th class="left" style="width:170px">Elftal</th> 
<th class="center" style="width:30px">G</th> 
<th class="center" style="width:30px">W</th> 
<th class="center" style="width:30px">GL</th> 
<th class="center" style="width:30px">V</th> 
<th class="center" style="width:30px">P</th> 
<th class="center" style="width:30px">DPV</th> 
<th class="center" style="width:30px">DPT</th> 
<th class="center" style="width:30px">PM</th> 
</tr>  

<?php 
$i=0; 
while ($i < $num1) { 

$plaats=mysql_result($result1,$i,"Plaats"); 
$elftal=mysql_result($result1,$i,"Elftal"); 
$g=mysql_result($result1,$i,"G"); 
$w=mysql_result($result1,$i,"W"); 
$gw=mysql_result($result1,$i,"GW"); 
$v=mysql_result($result1,$i,"V"); 
$p=mysql_result($result1,$i,"P"); 
$dpv=mysql_result($result1,$i,"DPV"); 
$dpt=mysql_result($result1,$i,"DPT"); 
$pm=mysql_result($result1,$i,"PM"); 

?> 

<tr>
<?php IF ($elftal==$naam) { ?> 
<td class="left3" style="width:30px"><?php echo $plaats; ?></td> 
<td class="left3" style="width:170px"><?php echo $elftal; ?></td> 
<td class="center3" style="width:30px"><b><?php echo $g; ?></b></td> 
<td class="center3" style="width:30px"><?php echo $w; ?></td> 
<td class="center3" style="width:30px"><?php echo $gw; ?></td> 
<td class="center3" style="width:30px"><?php echo $v; ?></td> 
<td class="center3" style="width:30px"><b><?php echo $p; ?></b></td> 
<td class="center3" style="width:30px"><?php echo $dpv; ?></td> 
<td class="center3" style="width:30px"><?php echo $dpt; ?></td> 
<td class="center3" style="width:30px"><?php echo $pm; ?></td> 
<?php }	
ELSE { ?>
<td class="left" style="width:30px"><?php echo $plaats; ?></td> 
<td class="left" style="width:170px"><?php echo $elftal; ?></td> 
<td class="center" style="width:30px"><b><?php echo $g; ?></b></td> 
<td class="center" style="width:30px"><?php echo $w; ?></td> 
<td class="center" style="width:30px"><?php echo $gw; ?></td> 
<td class="center" style="width:30px"><?php echo $v; ?></td> 
<td class="center" style="width:30px"><b><?php echo $p; ?></b></td> 
<td class="center" style="width:30px"><?php echo $dpv; ?></td> 
<td class="center" style="width:30px"><?php echo $dpt; ?></td> 
<td class="center" style="width:30px"><?php echo $pm; ?></td>
<?php } ?> 
	
</tr> 

<?php 
$i++; 
} 

echo "</table>"; 
?>

<h3>Stand 2e periode <?php echo $klasse; ?></h3>  
<table class="stand">
<tr> 
<th class="left" style="width:30px">Pl</th> 
<th class="left" style="width:170px">Elftal</th> 
<th class="center" style="width:30px">G</th> 
<th class="center" style="width:30px">W</th> 
<th class="center" style="width:30px">GL</th> 
<th class="center" style="width:30px">V</th> 
<th class="center" style="width:30px">P</th> 
<th class="center" style="width:30px">DPV</th> 
<th class="center" style="width:30px">DPT</th> 
<th class="center" style="width:30px">PM</th>  
</tr>  

<?php 
$i=0; 
while ($i < $num2) { 

$plaats=mysql_result($result2,$i,"Plaats"); 
$elftal=mysql_result($result2,$i,"Elftal"); 
$g=mysql_result($result2,$i,"G"); 
$w=mysql_result($result2,$i,"W"); 
$gw=mysql_result($result2,$i,"GW"); 
$v=mysql_result($result2,$i,"V"); 
$p=mysql_result($result2,$i,"P"); 
$dpv=mysql_result($result2,$i,"DPV"); 
$dpt=mysql_result($result2,$i,"DPT"); 
$pm=mysql_result($result2,$i,"PM"); 

?> 

<tr>
<?php IF ($elftal==$naam) { ?>	 
<td class="left3" style="width:30px"><?php echo $plaats; ?></td> 
<td class="left3" style="width:170px"><?php echo $elftal; ?></td> 
<td class="center3" style="width:30px"><b><?php echo $g; ?></b></td> 
<td class="center3" style="width:30px"><?php echo $w; ?></td> 
<td class="center3" style="width:30px"><?php echo $gw; ?></td> 
<td class="center3" style="width:30px"><?php echo $v; ?></td> 
<td class="center3" style="width:30px"><b><?php echo $p; ?></b></td> 
<td class="center3" style="width:30px"><?php echo $dpv; ?></td> 
<td class="center3" style="width:30px"><?php echo $dpt; ?></td> 
<td class="center3" style="width:30px"><?php echo $pm; ?></td> 
<?php }	
ELSE { ?>
<td class="left" style="width:30px"><?php echo $plaats; ?></td> 
<td class="left" style="width:170px"><?php echo $elftal; ?></td> 
<td class="center" style="width:30px"><b><?php echo $g; ?></b></td> 
<td class="center" style="width:30px"><?php echo $w; ?></td> 
<td class="center" style="width:30px"><?php echo $gw; ?></td> 
<td class="center" style="width:30px"><?php echo $v; ?></td> 
<td class="center" style="width:30px"><b><?php echo $p; ?></b></td> 
<td class="center" style="width:30px"><?php echo $dpv; ?></td> 
<td class="center" style="width:30px"><?php echo $dpt; ?></td> 
<td class="center" style="width:30px"><?php echo $pm; ?></td>
<?php } ?> 
</tr> 

<?php 
$i++; 
} 

echo "</table>"; 
?> 

<h3>Stand 3e periode <?php echo $klasse; ?></h3>  
<table class="stand">
<tr> 
<th class="left" style="width:30px">Pl</th> 
<th class="left" style="width:170px">Elftal</th> 
<th class="center" style="width:30px">G</th> 
<th class="center" style="width:30px">W</th> 
<th class="center" style="width:30px">GL</th> 
<th class="center" style="width:30px">V</th> 
<th class="center" style="width:30px">P</th> 
<th class="center" style="width:30px">DPV</th> 
<th class="center" style="width:30px">DPT</th> 
<th class="center" style="width:30px">PM</th> 
</tr>   

<?php 
$i=0; 
while ($i < $num3) { 

$plaats=mysql_result($result3,$i,"Plaats"); 
$elftal=mysql_result($result3,$i,"Elftal"); 
$g=mysql_result($result3,$i,"G"); 
$w=mysql_result($result3,$i,"W"); 
$gw=mysql_result($result3,$i,"GW"); 
$v=mysql_result($result3,$i,"V"); 
$p=mysql_result($result3,$i,"P"); 
$dpv=mysql_result($result3,$i,"DPV"); 
$dpt=mysql_result($result3,$i,"DPT"); 
$pm=mysql_result($result3,$i,"PM"); 

?> 

<tr>
<?php IF ($elftal==$naam) { ?>	 
<td class="left3" style="width:30px"><?php echo $plaats; ?></td> 
<td class="left3" style="width:170px"><?php echo $elftal; ?></td> 
<td class="center3" style="width:30px"><b><?php echo $g; ?></b></td> 
<td class="center3" style="width:30px"><?php echo $w; ?></td> 
<td class="center3" style="width:30px"><?php echo $gw; ?></td> 
<td class="center3" style="width:30px"><?php echo $v; ?></td> 
<td class="center3" style="width:30px"><b><?php echo $p; ?></b></td> 
<td class="center3" style="width:30px"><?php echo $dpv; ?></td> 
<td class="center3" style="width:30px"><?php echo $dpt; ?></td> 
<td class="center3" style="width:30px"><?php echo $pm; ?></td> 
<?php }	
ELSE { ?>
<td class="left" style="width:30px"><?php echo $plaats; ?></td> 
<td class="left" style="width:170px"><?php echo $elftal; ?></td> 
<td class="center" style="width:30px"><b><?php echo $g; ?></b></td> 
<td class="center" style="width:30px"><?php echo $w; ?></td> 
<td class="center" style="width:30px"><?php echo $gw; ?></td> 
<td class="center" style="width:30px"><?php echo $v; ?></td> 
<td class="center" style="width:30px"><b><?php echo $p; ?></b></td> 
<td class="center" style="width:30px"><?php echo $dpv; ?></td> 
<td class="center" style="width:30px"><?php echo $dpt; ?></td> 
<td class="center" style="width:30px"><?php echo $pm; ?></td>	
<?php } ?> 
</tr> 

<?php 
$i++; 
} 

echo "</table>"; 
?> 
 
<table style="width:600px" class="stand"> 
    <tr> 
        <td class="small">| G: gespeeld | W: gewonnen | GL: gelijk | V: verloren | P: punten |<br /> 
|DPV: doelpunten voor | DPT: doelpunten tegen | PM: punten in mindering | 
        </td> 
    </tr>
    <tr> 
        <td class="small"><br />Bijgewerkt op: <?php  
            setlocale(LC_ALL, 'nl_NL'); 
            echo strftime('%d/%m/%y - %H:%M', strtotime(mysql_result($result5,0,'Update_time'))); ?></td> 
    </tr> 
      <tr>
    	 <td class="left"><br />Bron: <a href='http://www.voetbal.nl' target='_blank'>Voetbal.nl</a></td>
    </tr>
</table> 

</div> 
</body> 
</html>