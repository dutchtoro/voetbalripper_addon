<?php session_start();
// instellingen.php 1.9.7.1 (07-11-2012) Bugfixes en kleine cosmetische aanpassingen.
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel


include("config.php");

?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>Instellingen en Club gegevens</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php echo "<link rel=\"stylesheet\" type=\"text/css\" href='$CSSadmin'>"; ?>
</head>
<body>
<table class="bdadmin" style="width:100%"><tr><td class="hr"><h2>Instellingen en Club gegevens</h2></td></tr></table>
<?php
if (!login()) exit;
?>
<div style="float: right"><a href="instellingen.php?a=logout">[ Uitloggen ]</a></div>
<br>
<?php
$con = connect();
$showrecs = 30;
$pagerange = 10;

$a = @$_GET["a"];
$recid = @$_GET["recid"];
$page = @$_GET["page"];
if (!isset($page)) $page = 1;

$sql = @$_POST["sql"];

switch ($sql) {
case "update":
	sql_update();
	break;

}

switch ($a) {
case "view":
	viewrec($recid);
	break;
case "edit":
	editrec($recid);
default:
	select();
	break;
}



mysqli_close($con);
?>
<table class="bdadmin" style="width:100%"><tr><td class="hr">Gemaakt door Yarro/Johnvs voor voetbal.nl ripper van Johnvs</td></tr></table>
<div style="float: left"><a href="dashboard.php">[ DashBoard ]</a></div><br />
<div style="float: left"><a href="admin.php">[ Beheer Teams ]</a></div><br>
<div style="float: left"><a href="groupid.php">[ Group ID accounts ]</a></div><br>
<div style="float: left"><a href="oefenprogramma.php">[ Oefen Programma/Uitslagen ]</a></div><br>
<div style="float: left"><a href="extra.php">[ Extra informatie Club Programma ]</a></div><br>
<div style="float: left"><a href="uitslag.php">[ Uitslagen invoeren voor Club / Team ]</a></div><br>

</body>
</html>

<?php function select()
{
	global $a;
	global $showrecs;
	global $page;
	global $filter;
	global $filterfield;
	global $wholeonly;
	global $order;
	global $ordtype;


	if ($a == "reset") {
		$filter = "";
		$filterfield = "";
		$wholeonly = "";
		$order = "";
		$ordtype = "";
	}

	$checkstr = "";
	if ($wholeonly) $checkstr = " checked";
	if ($ordtype == "asc") { $ordtypestr = "desc"; } else { $ordtypestr = "asc"; }
	$res = sql_select();
	$count = sql_getrecordcount();
	if ($count % $showrecs != 0) {
		$pagecount = intval($count / $showrecs) + 1;
	}
	else {
		$pagecount = intval($count / $showrecs);
	}
	$startrec = $showrecs * ($page - 1);
	if ($startrec < $count) {mysqli_data_seek($res, $startrec);}
	$reccount = min($showrecs * $page, $count);
	?>
	<table class="bdadmin">
	<tr><td>Tabel: instellingen</td></tr>
	</table>
	<hr />
	<form action="instellingen.php" method="post">
	<table class="bdadmin">

	</table>
	</form>
	<hr />
	<?php showpagenav($page, $pagecount); ?>
	<br>
	<table class="tbl" style="width:100%">
	<tr>

	<td class="hr">&nbsp;</td>
	<td class="hr">&nbsp;</td>
	<td class="hr"><a class="hr" href="instellingen.php?order=<?php echo "Clubnaam" ?>&amp;type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Clubnaam") ?></a></td>
	<td class="hr"><a class="hr" href="instellingen.php?order=<?php echo "Clubcode" ?>&amp;type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("Clubcode") ?></a></td>
	<td class="hr"><a class="hr" href="instellingen.php?order=<?php echo "AltClubcode" ?>&amp;type=<?php echo $ordtypestr ?>"><?php echo htmlspecialchars("AltClubcode") ?></a></td>
	</tr>
	<?php
	for ($i = $startrec; $i < $reccount; $i++)
	{
		$row = mysqli_fetch_assoc($res);
		$style = "dr";
		if ($i % 2 != 0) {
			$style = "sr";
		}
		?>
		<tr>
		<td class="<?php echo $style ?>"><a href="instellingen.php?a=view&amp;recid=<?php echo $i ?>">Bekijk</a></td>
		<td class="<?php echo $style ?>"><a href="instellingen.php?a=edit&amp;recid=<?php echo $i ?>">Wijzig</a></td>
		<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Clubnaam"]) ?></td>
		<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["Clubcode"]) ?></td>
		<td class="<?php echo $style ?>"><?php echo htmlspecialchars($row["AltClubcode"]) ?></td>
		</tr>
		<?php
	}
	mysqli_free_result($res);
	?>
	</table>
	<br>
	<?php showpagenav($page, $pagecount); ?>
	<?php } ?>

<?php function login()
{
	include("config.php");
	global $_POST;
	global $_SESSION;

	global $_GET;
	if (isset($_GET["a"]) && ($_GET["a"] == 'logout')) $_SESSION["logged_in_inst"] = false;
	if (!isset($_SESSION["logged_in_inst"])) $_SESSION["logged_in_inst"] = false;
	if (!$_SESSION["logged_in_inst"]) {
		$login = "";
		$password2 = "";

		if (isset($_POST["login"])) $login = @$_POST["login"];
		if (isset($_POST["password"])) $password2 = @$_POST["password"];

		if (($login != "") && ($password2 != "")) {
			if (($login == $Instusername) && ($password2 == $Instpassword)) {
				$_SESSION["logged_in_inst"] = true;
			}
			else {
				?>
				<p><b><font color="-1">De combinatie gebruikersnaam/wachtwoord is onjuist</font></b></p>
				<?php } } }if (isset($_SESSION["logged_in_inst"]) && (!$_SESSION["logged_in_inst"])) { ?>
		<form action="instellingen.php" method="post">
		<table class="bdadmin">
		<tr>
		<td>Gebruikersnaam</td>
		<td><input type="text" name="login" value="<?php echo $login ?>"></td>
		</tr>
		<tr>
		<td>Wachtwoord</td>
		<td><input type="password" name="password" value="<?php echo $password2 ?>"></td>
		</tr>
		<tr>
		<td><input type="submit" name="action" value="Inloggen"></td>
		</tr>
		</table>
		</form>
		<?php
	}
	if (!isset($_SESSION["logged_in_inst"])) $_SESSION["logged_in_inst"] = false;
	return $_SESSION["logged_in_inst"];
} ?>

<?php function showrow($row, $recid)
{
	?>
	<table class="tbl" style="width:50%">
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Clubnaam")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Clubnaam"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Club1")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Club1"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Clubcode")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Clubcode"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("AltClubcode")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["AltClubcode"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Gegevens ophalen via alternatieve Club/Team code")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["OphViaAcode"]) ?></td>
	</tr>
	<tr>
	<td><hr /></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Clubprogramma")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Clubprogramma"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Clubnaam Check")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Clubnaamcheck"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Combineer Club Prog/Afgelasting")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["CombCpAfg"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Aantal +dagen Clubprogramma")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["PlusDagCP"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Aantal -dagen Clubprogramma")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["MinDagCP"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Club Programma Sortering")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["SortCP"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Toon Team Filter in Club Programma")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["FilterCP"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Clubprogramma Ophalen")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["CPophalen"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Bepaalde wedstrijden verwijderen uit Clubprogramma en Clubuitslagen")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["DelwdstrdCPCU"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Verwijderen van wedstrijden uit Clubprogramma en Clubuitslagen met het woord (indien vorige instelling aan staat)")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["DelwdstrdCPCUtxt"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Scheidsrechter in Club Programma")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["ScheidsinCP"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Verzamel - Vertrek tijd in Club Programma")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["VVinCP"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Kleedkamer tonen in Club Programma")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Klkamertonen"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Veld tonen in Club Programma")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Veldtonen"]) ?></td>
	</tr>
	<tr>
	<td><hr /></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Aantal -dagen Uitslagen")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["MinDagUitslagen"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Aantal +dagen Uitslagen")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["PlusDagUitslagen"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Clubuitslagen Ophalen")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["CUophalen"]) ?></td>
	</tr>
	<tr>
	<td><hr /></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Team Programmadetails Ophalen")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Pdetails"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Team Programmadetails tonen")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Pdetailstonen"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Oefenwedstrijden tonen in Team Programm")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Oefeninteam"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Team indeling en speel ronde tonen in Team Programm")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Tindelinginteam"]) ?></td>
	</tr>
	<tr>
	<td><hr /></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("CSS voor admin")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["CSSadmin"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("CSS voor weergave")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["CSSweergave"]) ?></td>
	</tr>
	<tr>
	<td><hr /></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Opschonen")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Opschonen"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Meldingen")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Meldingen"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Curl")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Curl"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Phpsafemode")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Phpsafemode"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Phpmaxtime")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Phpmaxtime"]) ?></td>
	</tr>
	<tr>
	<td><hr /></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Gebruikersnaam"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Wachtwoord"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Emailvoormeldingen")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Emailvoormeldingen"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Beveiligindex")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Beveiligindex"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("UserDebug")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["UserDebug"]) ?></td>
	</tr>
	<tr>
	<td><hr /></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Index gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Igebruikersnaam"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Index wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Iwachtwoord"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Admin gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Agebruikersnaam"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Admin wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Awachtwoord"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Oefen gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Ogebruikersnaam"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Oefen wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Owachtwoord"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Extra gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Egebruikersnaam"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Extra wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Ewachtwoord"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Group ID account gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Ggebruikersnaam"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Group ID account wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Gwachtwoord"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Uitslagen gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Ugebruikersnaam"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Uitslagen wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Uwachtwoord"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("DashBoard gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Dbgebruikersnaam"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("DashBoard wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Dbwachtwoord"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Instellingen gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Instgebruikersnaam"]) ?></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Instellingen wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><?php echo htmlspecialchars($row["Instwachtwoord"]) ?></td>
	</tr>
	</table>
	<?php } ?>

<?php function showroweditor($row, $iseditmode)
{
	global $con;
	?>
	<table class="tbl" style="width:100%">
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Clubnaam")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="1" name="Clubnaam"><?php echo str_replace('"', '&quot;', trim($row["Clubnaam"])) ?></textarea> Clubnaam zo als gebruikt op voetbal.nl</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Club1")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Club1" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Club1"])) ?></textarea> Deel van clubnaam wat altijd voorkomt in de naam. Zonder speciale tekens plus hoofd Letters. bv: Swift ipv Swift 'Boys. Wordt gebruikt voor filtering en controle.</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Clubcode")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="1" name="Clubcode" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Clubcode"])) ?></textarea> Club code -7 karakters- Login op voetbal.nl en ga naar mijn clubs, beweeg de muis over je club en je ziet in de adresbalk onder in het beeld de code achter /mijn/clubs/team/xxxxxxx</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("AltClubcode")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="1" name="AltClubcode" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["AltClubcode"])) ?></textarea> Alt Club code -24+ karakters- Login op voetbal.nl en ga naar mijn clubs, beweeg de muis over je club en je ziet in de adresbalk onder in het beeld de code achter /mijn/clubs/team/xxxxxxx</td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Gegevens ophalen via alternatieve Club/Team code")."&nbsp;" ?></td>
<?php if ($row["OphViaAcode"] == "Aan") { ?>
<td class="dr"><input type="checkbox" CHECKED name="OphViaAcode" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["OphViaAcode"])) ?>"> Indien aangevinkt worden Club/Team gegevens opgehaald via de alternatieve Club/Team code</td>
	<?php } else { ?>
	<td class="dr"><input type="checkbox" name="OphViaAcode" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["OphViaAcode"])) ?>"> Indien aangevinkt worden Club/Team gegevens opgehaald via de alternatieve Club/Team code</td>
<?php } ?>
</tr>

<tr>
<td><hr /></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Clubprogramma")."&nbsp;" ?></td>
<?php if ($row["Clubprogramma"] == "periode") { ?>
<td class="dr"><select name="Clubprogramma">
<option value="periode" selected>periode</option><option value="deze week">deze week</option><option value="volgende week">volgende week</option></select>
<?php } else { ?>
<?php } ?>
<?php if ($row["Clubprogramma"] == "deze week") { ?>
<td class="dr"><select name="Clubprogramma">
<option value="periode">periode</option><option value="deze week" selected>deze week</option><option value="volgende week">volgende week</option></select>
<?php } else { ?>
<?php } ?>
<?php if ($row["Clubprogramma"] == "volgende week") { ?>
<td class="dr"><select name="Clubprogramma">
<option value="periode">periode</option><option value="deze week">deze week</option><option value="volgende week" selected>volgende week</option></select>
<?php } else { ?>
<?php } ?>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Clubnaam check")."&nbsp;" ?></td>
<?php if ($row["Clubnaamcheck"] == "Aan") { ?>
<td class="dr"><input type="checkbox" CHECKED name="Clubnaamcheck" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Clubnaamcheck"])) ?>"> Uitschakelen indien teams van verschillende clubs wordt opgehaald. Minder controle op fouten bij ophalen programma. </td>
	<?php } else { ?>
	<td class="dr"><input type="checkbox" name="Clubnaamcheck" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Clubnaamcheck"])) ?>"> Uitschakelen indien teams van verschillende clubs wordt opgehaald. Minder controle op fouten bij ophalen programma. </td>
<?php } ?>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Combineer Club Programma en Afgelasting")."&nbsp;" ?></td>
<?php if ($row["CombCpAfg"] == "Aan") { ?>
<td class="dr"><input type="checkbox" CHECKED name="CombCpAfg" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["CombCpAfg"])) ?>"> Indien aangevinkt worden afgelastingen getoond in het Club Programmameldingen. </td>
	<?php } else { ?>
	<td class="dr"><input type="checkbox" name="CombCpAfg" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["CombCpAfg"])) ?>"> Indien aangevinkt worden afgelastingen getoond in het Club Programmameldingen. </td>
<?php } ?>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Aantal +dagen Club Programma")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="1" name="PlusDagCP" maxlength="4"><?php echo str_replace('"', '&quot;', trim($row["PlusDagCP"])) ?></textarea> Aantal toekomstige dagen in Club Programma. Beste is dan om Club Programma op Periode te zetten. </td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Aantal -dagen Club Programma")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="1" name="MinDagCP" maxlength="4"><?php echo str_replace('"', '&quot;', trim($row["MinDagCP"])) ?></textarea> Aantal historische dagen in Club Programma. Geen min (-) of plus (+) teken gebruiken. </td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Sortering Clubprogramma")."&nbsp;" ?></td>
<?php if ($row["SortCP"] == "Datum-Tijd-Team") { ?>
<td class="dr"><select name="SortCP">
<option value="Datum-Tijd-Team" selected>Datum-Tijd-Team</option><option value="Team-Datum-Tijd">Team-Datum-Tijd</option><option value="Datum-Team-Tijd">Datum-Team-Tijd</option></select>
<?php } else { ?>
<?php } ?>
<?php if ($row["SortCP"] == "Team-Datum-Tijd") { ?>
<td class="dr"><select name="SortCP">
<option value="Datum-Tijd-Team">Datum-Tijd-Team</option><option value="Team-Datum-Tijd" selected>Team-Datum-Tijd</option><option value="Datum-Team-Tijd">Datum-Team-Tijd</option></select>
<?php } else { ?>
<?php } ?>
<?php if ($row["SortCP"] == "Datum-Team-Tijd") { ?>
<td class="dr"><select name="SortCP">
<option value="Datum-Tijd-Team">Datum-Tijd-Team</option><option value="Team-Datum-Tijd">Team-Datum-Tijd</option><option value="Datum-Team-Tijd" selected>Datum-Team-Tijd</option></select>
<?php } else { ?>
<?php } ?>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Toon team filter Clubprogramma")."&nbsp;" ?></td>
<?php if ($row["FilterCP"] == "Aan") { ?>
<td class="dr"><input type="checkbox" CHECKED name="FilterCP" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["FilterCP"])) ?>"> Indien aangevinkt wordt het team filter getoond in het Club Programma. </td>
	<?php } else { ?>
	<td class="dr"><input type="checkbox" name="FilterCP" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["FilterCP"])) ?>"> Indien aangevinkt wordt het team filter getoond in het Club Programma. </td>
<?php } ?>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Clubprogramma ophalen")."&nbsp;" ?></td>
<?php if ($row["CPophalen"] == "Aan") { ?>
<td class="dr"><input type="checkbox" CHECKED name="CPophalen" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["CPophalen"])) ?>"> Indien aangevinkt wordt het Club Programma opgehaald. </td>
	<?php } else { ?>
	<td class="dr"><input type="checkbox" name="CPophalen" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["CPophalen"])) ?>"> Indien aangevinkt wordt het Club Programma opgehaald. </td>
<?php } ?>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Bepaalde wedstrijden verwijderen uit Clubprogramma en Clubuitslagen")."&nbsp;" ?></td>
<?php if ($row["DelwdstrdCPCU"] == "Aan") { ?>
<td class="dr"><input type="checkbox" CHECKED name="DelwdstrdCPCU" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["DelwdstrdCPCU"])) ?>"> Indien aangevinkt worden wedstrijden met het woord in de instelling hier onder verwijderd uit clubprogramma en clubuitslagen. </td>
	<?php } else { ?>
	<td class="dr"><input type="checkbox" name="DelwdstrdCPCU" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["DelwdstrdCPCU"])) ?>"> Indien aangevinkt worden wedstrijden met het woord in de instelling hier onder verwijderd uit clubprogramma en clubuitslagen </td>
<?php } ?>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Verwijderen van wedstrijden met het woord (indien vorige instelling aan staat")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="1" name="DelwdstrdCPCUtxt" maxlength="4"><?php echo str_replace('"', '&quot;', trim($row["DelwdstrdCPCUtxt"])) ?></textarea> Verwijderen van wedstrijden uit Clubprogramma en Clubuitslagen met het woord (indien vorige instelling aan staat). </td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Scheids tonen in Clubprogramma")."&nbsp;" ?></td>
<?php if ($row["ScheidsinCP"] == "Aan") { ?>
<td class="dr"><input type="checkbox" CHECKED name="ScheidsinCP" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["ScheidsinCP"])) ?>"> Indien aangevinkt wordt scheidsrechter getoond in Club Programma. </td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="ScheidsinCP" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["ScheidsinCP"])) ?>"> Indien aangevinkt wordt scheidsrechter getoond in Club Programma. </td>
<?php } ?>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Verzamel-Vertrek tijd tonen in Clubprogramma")."&nbsp;" ?></td>
<?php if ($row["VVinCP"] == "Aan") { ?>
<td class="dr"><input type="checkbox" CHECKED name="VVinCP" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["VVinCP"])) ?>"> Indien aangevinkt wordt Verzamel-Vertrek tijd getoond in Club Programma. </td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="VVinCP" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["VVinCP"])) ?>"> Indien aangevinkt wordt Verzamel-Vertrek tijd getoond in Club Programma. </td>
<?php } ?>
</tr>
<td class="hr"><?php echo htmlspecialchars("Kleedkamer tonen in Clubprogramma")."&nbsp;" ?></td>
<?php if ($row["Klkamertonen"] == "Aan") { ?>
<td class="dr"><input type="checkbox" CHECKED name="Klkamertonen" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Klkamertonen"])) ?>"> Indien aangevinkt wordt Kleedkamer getoond in Clubprogramma. Invoeren via extra.php. </td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Klkamertonen" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Klkamertonen"])) ?>"> Indien aangevinkt wordt Kleedkamer getoond in Clubprogramma. Invoeren via extra.php. </td>
<?php } ?>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Veld tonen in Clubprogramma")."&nbsp;" ?></td>
<?php if ($row["Veldtonen"] == "Aan") { ?>
<td class="dr"><input type="checkbox" CHECKED name="Veldtonen" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Veldtonen"])) ?>"> Indien aangevinkt wordt Veld getoond in Clubprogramma. Invoeren via extra.php. </td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Veldtonen" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Veldtonen"])) ?>"> Indien aangevinkt wordt Veld getoond in Clubprogramma. Invoeren via extra.php. </td>
<?php } ?>
</tr>
<tr>
<td><hr /></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Aantal -dagen Uitslagen")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="1" name="MinDagUitslagen" maxlength="4"><?php echo str_replace('"', '&quot;', trim($row["MinDagUitslagen"])) ?></textarea> Aantal historische dagen in uitslagen. Geen min (-) of plus (+) teken gebruiken. </td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Aantal +dagen Uitslagen")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="1" name="PlusDagUitslagen" maxlength="4"><?php echo str_replace('"', '&quot;', trim($row["PlusDagUitslagen"])) ?></textarea> Aantal toekomstige dagen in uitslagen. Geen min (-) of plus (+) teken gebruiken. </td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Clubuitslagen ophalen")."&nbsp;" ?></td>
<?php if ($row["CUophalen"] == "Aan") { ?>
<td class="dr"><input type="checkbox" CHECKED name="CUophalen" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["CUophalen"])) ?>"> Indien aangevinkt worden de Club Uitslagen opgehaald. </td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="CUophalen" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["CUophalen"])) ?>"> Indien aangevinkt worden de Club Uitslagen opgehaald. </td>
<?php } ?>
</tr>
<tr>
<td><hr /></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Programma details ophalen")."&nbsp;" ?></td>
<?php if ($row["Pdetails"] == "Aan") { ?>
<td class="dr"><input type="checkbox" CHECKED name="Pdetails" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Pdetails"])) ?>"> Indien aangevinkt worden programma details (voor het team) opgehaald. Kans op account blokkade op voetbal.nl is aanzienlijk groter. </td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Pdetails" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Pdetails"])) ?>"> Indien aangevinkt worden programma details (voor het team) opgehaald. Kans op account blokkade op voetbal.nl is aanzienlijk groter. </td>
<?php } ?>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Programma details tonen")."&nbsp;" ?></td>
<?php if ($row["Pdetailstonen"] == "Aan") { ?>
<td class="dr"><input type="checkbox" CHECKED name="Pdetailstonen" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Pdetailstonen"])) ?>"> Indien aangevinkt worden programma details (voor het team) getoond in team programma. </td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Pdetailstonen" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Pdetailstonen"])) ?>"> Indien aangevinkt worden programma details (voor het team) getoond in team programma. </td>
<?php } ?>
</tr>
<tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Oefenwedstrijden tonen in team.php")."&nbsp;" ?></td>
<?php if ($row["Oefeninteam"] == "Aan") { ?>
<td class="dr"><input type="checkbox" CHECKED name="Oefeninteam" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Oefeninteam"])) ?>"> Indien aangevinkt worden oefenwedstrijden getoond in programma van team.php </td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Oefeninteam" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Oefeninteam"])) ?>"> Indien aangevinkt worden oefenwedstrijden getoond in programma van team.php </td>
<?php } ?>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("Teamindeling en speel ronde tonen in team.php")."&nbsp;" ?></td>
<?php if ($row["Tindelinginteam"] == "Aan") { ?>
<td class="dr"><input type="checkbox" CHECKED name="Tindelinginteam" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Tindelinginteam"])) ?>"> Indien aangevinkt worden Team indeling en speel ronde getoond in programma van team.php </td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Tindelinginteam" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Tindelinginteam"])) ?>"> Indien aangevinkt worden Team indeling en speel ronde getoond in programma van team.php </td>
<?php } ?>
</tr>
<tr>
<td><hr /></td>
</tr>
<tr>
<td class="hr"><?php echo htmlspecialchars("CSSadmin")."&nbsp;" ?></td>
<td class="dr"><textarea cols="35" rows="1" name="CSSadmin" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["CSSadmin"])) ?></textarea> CSS voor admin programma's</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("CSSweergave")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="1" name="CSSweergave" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["CSSweergave"])) ?></textarea> CSS voor weergave. team.php, programma.php etc</td>
	</tr>
	<tr>
	<td><hr /></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Wacht")."&nbsp;" ?></td>
	<?php if ($row["Wacht"] == "Aan") { ?>
		<td class="dr"><input type="checkbox" CHECKED name="Wacht" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Wacht"])) ?>"> Indien aangevinkt wordt er een pauze ingelast voordat de gegevens van het volgende team worden opgehaald. </td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Wacht" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Wacht"])) ?>"> Indien aangevinkt wordt er een pauze ingelast voordat de gegevens van het volgende team worden opgehaald. </td>
		<?php } ?>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Opschonen")."&nbsp;" ?></td>
	<?php if ($row["Opschonen"] == "Aan") { ?>
		<td class="dr"><input type="checkbox" CHECKED name="Opschonen" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Opschonen"])) ?>"> Indien aangevinkt worden tabellen opgeschoond.</td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Opschonen" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Opschonen"])) ?>"> Indien aangevinkt worden tabellen opgeschoond.</td>
		<?php } ?>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Meldingen")."&nbsp;" ?></td>
	<?php if ($row["Meldingen"] == "Scherm") { ?>
		<td class="dr"><select name="Meldingen">
		<option value="Scherm" selected>Scherm</option><option value="Email">Email</option><option value="Beide">Beide</option></select>
		<?php } else { ?>
		<?php } ?>
	<?php if ($row["Meldingen"] == "Email") { ?>
		<td class="dr"><select name="Meldingen">
		<option value="Scherm">Scherm</option><option value="Email" selected>Email</option><option value="Beide">Beide</option></select>
		<?php } else { ?>
		<?php } ?>
	<?php if ($row["Meldingen"] == "Beide") { ?>
		<td class="dr"><select name="Meldingen">
		<option value="Scherm">Scherm</option><option value="Email">Email</option><option value="Beide" selected>Beide</option></select>
		<?php } else { ?>
		<?php } ?>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Curl")."&nbsp;" ?></td>
	<?php if ($row["Curl"] == "Aan") { ?>
		<td class="dr"><input type="checkbox" CHECKED name="Curl" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Curl"])) ?>"> Indien aangevinkt wordt Curl gebruikt om data op te halen. Gebruiken als Curl enabled is.</td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Curl" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Curl"])) ?>"> Indien aangevinkt wordt Curl gebruikt om data op te halen. Gebruiken als Curl enabled is.</td>
		<?php } ?>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Phpsafemode")."&nbsp;" ?></td>
	<?php if ($row["Phpsafemode"] == "Aan") { ?>
		<td class="dr"><input type="checkbox" CHECKED name="Phpsafemode" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Phpsafemode"])) ?>"> Aanvinken als Safemode aan staat en/of open_basedir gezet is.</td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Phpsafemode" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Phpsafemode"])) ?>"> Aanvinken als Safemode aan staat en/of open_basedir gezet is..</td>
		<?php } ?>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Phpmaxtime")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Phpmaxtime" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Phpmaxtime"])) ?></textarea> Getal invullen wat lager is dan de waarde die phpcheck.php geeft.  Indien phpcheck.php 0 geeft 0 invullen.</td>
	</tr>
	<tr>
	<td><hr /></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Gebruikersnaam" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Gebruikersnaam"])) ?></textarea> Gebruikersnaam voor voetbal.nl die gebruikt wordt voor het ophalen van 'Alle','teamID' of wanneer groupID '0' is.</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Wachtwoord" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Wachtwoord"])) ?></textarea> Wachtwoord voor voetbal.nl die gebruikt wordt voor het ophalen van 'Alle','teamID' of wanneer groupID '0' is.</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Emailvoormeldingen")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Emailvoormeldingen" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Emailvoormeldingen"])) ?></textarea> Email adres voor meldingen van foutmeldingen.</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Beveiligindex")."&nbsp;" ?></td>
	<?php if ($row["Beveiligindex"] == "Aan") { ?>
		<td class="dr"><input type="checkbox" CHECKED name="Beveiligindex" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Beveiligindex"])) ?>"> Aanvinken username en wachtwoord gevraagd moeten worden wanneer index.php/teamindindex.php gestart wordt.</td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="Beveiligindex" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["Beveiligindex"])) ?>"> Aanvinken indien username en wachtwoord gevraagd moeten worden wanneer index.php/teamindindex.php gestart wordt.</td>
		<?php } ?>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("UserDebug")."&nbsp;" ?></td>
	<?php if ($row["UserDebug"] == "Aan") { ?>
		<td class="dr"><input type="checkbox" CHECKED name="UserDebug" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["UserDebug"])) ?>"> Aanvinken indien er fouten gemeld worden om meer details te krijgen.</td>
<?php } else { ?>
<td class="dr"><input type="checkbox" name="UserDebug" maxlength="3" value="<?php echo str_replace('"', '&quot;', trim($row["UserDebug"])) ?>"> Aanvinken indien er fouten gemeld worden om meer details te krijgen</td>
		<?php } ?>
	</tr>
	<tr>
	<td><hr /></td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Index gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Igebruikersnaam" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Igebruikersnaam"])) ?></textarea> Gebruikersnaam voor index.php/teamindindex.php.</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Index wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Iwachtwoord" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Iwachtwoord"])) ?></textarea> Wachtwoord voor index.php/teamindindex.php.</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Admin gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Agebruikersnaam" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Agebruikersnaam"])) ?></textarea> Gebruikersnaam voor admin.php.</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Admin wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Awachtwoord" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Awachtwoord"])) ?></textarea> Wachtwoord voor admin.php.</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Oefen gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Ogebruikersnaam" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Ogebruikersnaam"])) ?></textarea> Gebruikersnaam voor oefenprogramma.php.</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Oefen wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Owachtwoord" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Owachtwoord"])) ?></textarea> Wachtwoord voor oefenprogramma.php.</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Extra gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Egebruikersnaam" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Egebruikersnaam"])) ?></textarea> Gebruikersnaam voor extra.php.</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Extra wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Ewachtwoord" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Ewachtwoord"])) ?></textarea> Wachtwoord voor extra.php.</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Group ID account gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Ggebruikersnaam" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Ggebruikersnaam"])) ?></textarea> Gebruikersnaam voor groupid.php.</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Group ID account wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Gwachtwoord" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Gwachtwoord"])) ?></textarea> Wachtwoord voor groupid.php.</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Uitslagen gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Ugebruikersnaam" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Ugebruikersnaam"])) ?></textarea> Gebruikersnaam voor invoeren uitslagen.php.</td>
	</tr><tr>
	<td class="hr"><?php echo htmlspecialchars("Uitslagen wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Uwachtwoord" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Uwachtwoord"])) ?></textarea> Wachtwoord voor invoeren uitslagen.php.</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("DashBoard gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Dbgebruikersnaam" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Dbgebruikersnaam"])) ?></textarea> Gebruikersnaam voor DashBoard.php.</td>
	</tr><tr>
	<td class="hr"><?php echo htmlspecialchars("DashBoard wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Dbwachtwoord" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Dbwachtwoord"])) ?></textarea> Wachtwoord voor invoeren DashBoard.php.</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Instellingen gebruikersnaam")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Instgebruikersnaam" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Instgebruikersnaam"])) ?></textarea> Gebruikersnaam voor instellingen.php.</td>
	</tr>
	<tr>
	<td class="hr"><?php echo htmlspecialchars("Instellingen wachtwoord")."&nbsp;" ?></td>
	<td class="dr"><textarea cols="35" rows="2" name="Instwachtwoord" maxlength="65535"><?php echo str_replace('"', '&quot;', trim($row["Instwachtwoord"])) ?></textarea> Wachtwoord voor instellingen.php.</td>
	</tr>

	</table>
	<?php } ?>

<?php function showpagenav($page, $pagecount)
{
	?>
	<table class="bdadmin">

	<?php if ($page > 1) { ?>
		<td><a href="instellingen.php?page=<?php echo $page - 1 ?>">&lt;&lt;&nbsp;Vorige</a>&nbsp;</td>
		<?php } ?>
	<?php
	global $pagerange;

	if ($pagecount > 1) {

		if ($pagecount % $pagerange != 0) {
			$rangecount = intval($pagecount / $pagerange) + 1;
		}
		else {
			$rangecount = intval($pagecount / $pagerange);
		}
		for ($i = 1; $i < $rangecount + 1; $i++) {
			$startpage = (($i - 1) * $pagerange) + 1;
			$count = min($i * $pagerange, $pagecount);

			if ((($page >= $startpage) && ($page <= ($i * $pagerange)))) {
				for ($j = $startpage; $j < $count + 1; $j++) {
					if ($j == $page) {
						?>
						<td><b><?php echo $j ?></b></td>
						<?php } else { ?>
						<td><a href="instellingen.php?page=<?php echo $j ?>"><?php echo $j ?></a></td>
						<?php } } } else { ?>
				<td><a href="instellingen.php?page=<?php echo $startpage ?>"><?php echo $startpage ."..." .$count ?></a></td>
				<?php } } } ?>
	<?php if ($page < $pagecount) { ?>
		<td>&nbsp;<a href="instellingen.php?page=<?php echo $page + 1 ?>">Volgende&nbsp;&gt;&gt;</a>&nbsp;</td>
		<?php } ?>

	</table>
	<?php } ?>

<?php function showrecnav($a, $recid, $count)
{
	?>
	<table class="bdadmin">
	<tr>
	<td><a href="instellingen.php">Index Pagina</a></td>
	<?php if ($recid > 0) { ?>
		<td><a href="instellingen.php?a=<?php echo $a ?>&recid=<?php echo $recid - 1 ?>">Vorige </a></td>
		<?php } if ($recid < $count - 1) { ?>
		<td><a href="instellingen.php?a=<?php echo $a ?>&recid=<?php echo $recid + 1 ?>">Volgende </a></td>
		<?php } ?>
	</tr>
	</table>
	<hr />
	<?php } ?>



<?php function viewrec($recid)
{
	$res = sql_select();
	$count = sql_getrecordcount();
	mysqli_data_seek($res, $recid);
	$row = mysqli_fetch_assoc($res);
	showrecnav("view", $recid, $count);
	?>
	<br>
	<?php showrow($row, $recid) ?>
	<br>
	<hr />
	<table class="bdadmin">
	<tr>
	<td><a href="instellingen.php?a=edit&recid=<?php echo $recid ?>">Gegevens wijzigen</a></td>
	</tr>
	</table>
	<?php
	mysqli_free_result($res);
} ?>

<?php function editrec($recid)
{
	$res = sql_select();
	$count = sql_getrecordcount();
	mysqli_data_seek($res, $recid);
	$row = mysqli_fetch_assoc($res);
	showrecnav("edit", $recid, $count);
	?>
	<br>
	<form enctype="multipart/form-data" action="instellingen.php" method="post">
	<input type="hidden" name="sql" value="update">
	<input type="hidden" name="xInstellingenID" value="<?php echo $row["InstellingenID"] ?>">
	<?php showroweditor($row, true); ?>
	<p><input type="submit" name="action" value="Wijzigen"></p>
	</form>
	<?php
	mysqli_free_result($res);
} ?>



<?php function connect()
{
	include("config.php");
	//$con = mysql_connect($server,$username,$password);
	//mysql_select_db($database);
	return $con;
}

function sqlvalue($val, $quote)
{
	if ($quote)
	$tmp = sqlstr($val);
	else
	$tmp = $val;
	if ($tmp == "")
	$tmp = "NULL";
	elseif ($quote)
	$tmp = "'".$tmp."'";
	return $tmp;
}

function sqlstr($val)
{
	return str_replace("'", "''", $val);
}

function sql_select()
{
	global $con;
	global $order;
	global $ordtype;
	global $filter;
	global $filterfield;
	global $wholeonly;
	global $dbprefix;

	$filterstr = sqlstr($filter);
	if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
	$sql = "SELECT * FROM `".$dbprefix."instellingen`";
	if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
		$sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
	} elseif (isset($filterstr) && $filterstr!='') {
		$sql .= " where (`InstellingenID` like '" .$filterstr ."') or (Clubcode like '" .$filterstr ."') or (AltClubcode like '" .$filterstr ."') or (`Naam` like '" .$filterstr ."') or (`Klasse` like '" .$filterstr ."') or (`Periode` like '" .$filterstr ."') or (`DatumTijd-Update` like '" .$filterstr ."')";
	}
	if (isset($order) && $order!='') $sql .= " order by `" .sqlstr($order) ."`";
	if (isset($ordtype) && $ordtype!='') $sql .= " " .sqlstr($ordtype);
	$res = mysqli_query($con,$sql) or die(mysqli_error($con));
	return $res;
}

function sql_getrecordcount()
{
	global $con;
	global $order;
	global $ordtype;
	global $filter;
	global $filterfield;
	global $wholeonly;
	global $dbprefix;

	$filterstr = sqlstr($filter);
	if (!$wholeonly && isset($wholeonly) && $filterstr!='') $filterstr = "%" .$filterstr ."%";
	$sql = "SELECT COUNT(*) FROM `".$dbprefix."instellingen`";
	if (isset($filterstr) && $filterstr!='' && isset($filterfield) && $filterfield!='') {
		$sql .= " where " .sqlstr($filterfield) ." like '" .$filterstr ."'";
	} elseif (isset($filterstr) && $filterstr!='') {
		$sql .= " where (`InstellingenID` like '" .$filterstr ."') or (Clubcode like '" .$filterstr ."') or (AltClubcode like '" .$filterstr ."') or (`Naam` like '" .$filterstr ."') or (`Klasse` like '" .$filterstr ."') or (`Periode` like '" .$filterstr ."') or (`DatumTijd-Update` like '" .$filterstr ."')";
	}
	$res = mysqli_query($con,$sql) or die(mysqli_error($con));
	$row = mysqli_fetch_assoc($res);
	reset($row);
	return current($row);
}



function sql_update()
{
	if(isset($_POST["Clubnaamcheck"])){
		$_POST["Clubnaamcheck"] = "Aan";
	}
	else{
		$_POST["Clubnaamcheck"] = "Uit";
	}

	if(isset($_POST["CombCpAfg"])){
		$_POST["CombCpAfg"] = "Aan";
	}
	else{
		$_POST["CombCpAfg"] = "Uit";
	}

	if(isset($_POST["FilterCP"])){
		$_POST["FilterCP"] = "Aan";
	}
	else{
		$_POST["FilterCP"] = "Uit";
	}

	if(isset($_POST["CPophalen"])){
		$_POST["CPophalen"] = "Aan";
	}
	else{
		$_POST["CPophalen"] = "Uit";
	}

	if(isset($_POST["DelwdstrdCPCU"])){
		$_POST["DelwdstrdCPCU"] = "Aan";
	}
	else{
		$_POST["DelwdstrdCPCU"] = "Uit";
	}

	if(isset($_POST["CUophalen"])){
		$_POST["CUophalen"] = "Aan";
	}
	else{
		$_POST["CUophalen"] = "Uit";
	}

	if(isset($_POST["VVinCP"])){
		$_POST["VVinCP"] = "Aan";
	}
	else{
		$_POST["VVinCP"] = "Uit";
	}

	if(isset($_POST["Pdetails"])){
		$_POST["Pdetails"] = "Aan";
	}
	else{
		$_POST["Pdetails"] = "Uit";
	}

	if(isset($_POST["Pdetailstonen"])){
		$_POST["Pdetailstonen"] = "Aan";
	}
	else{
		$_POST["Pdetailstonen"] = "Uit";
	}

	if(isset($_POST["Klkamertonen"])){
		$_POST["Klkamertonen"] = "Aan";
	}
	else{
		$_POST["Klkamertonen"] = "Uit";
	}

	if(isset($_POST["Veldtonen"])){
		$_POST["Veldtonen"] = "Aan";
	}
	else{
		$_POST["Veldtonen"] = "Uit";
	}

	if(isset($_POST["Oefeninteam"])){
		$_POST["Oefeninteam"] = "Aan";
	}
	else{
		$_POST["Oefeninteam"] = "Uit";
	}

	if(isset($_POST["Tindelinginteam"])){
		$_POST["Tindelinginteam"] = "Aan";
	}
	else{
		$_POST["Tindelinginteam"] = "Uit";
	}

	if(isset($_POST["ScheidsinCP"])){
		$_POST["ScheidsinCP"] = "Aan";
	}
	else{
		$_POST["ScheidsinCP"] = "Uit";
	}

	if(isset($_POST["OphViaAcode"])){
		$_POST["OphViaAcode"] = "Aan";
	}
	else{
		$_POST["OphViaAcode"] = "Uit";
	}

	if(isset($_POST["Wacht"])){
		$_POST["Wacht"] = "Aan";
	}
	else{
		$_POST["Wacht"] = "Uit";
	}

	if(isset($_POST["Opschonen"])){
		$_POST["Opschonen"] = "Aan";
	}
	else{
		$_POST["Opschonen"] = "Uit";
	}

	if(isset($_POST["Curl"])){
		$_POST["Curl"] = "Aan";
	}
	else{
		$_POST["Curl"] = "Uit";
	}

	if(isset($_POST["Phpsafemode"])){
		$_POST["Phpsafemode"] = "Aan";
	}
	else{
		$_POST["Phpsafemode"] = "Uit";
	}

	if(isset($_POST["Beveiligindex"])){
		$_POST["Beveiligindex"] = "Aan";
	}
	else{
		$_POST["Beveiligindex"] = "Uit";
	}

	if(isset($_POST["UserDebug"])){
		$_POST["UserDebug"] = "Aan";
	}
	else{
		$_POST["UserDebug"] = "Uit";
	}

	global $con;
	global $_POST;
	global $dbprefix;

	$clubnaam = mysqli_real_escape_string($_POST['Clubnaam']);

	$sql = "update `".$dbprefix."instellingen` set Clubcode=" .sqlvalue(@$_POST["Clubcode"], true).", `Clubnaam`='$clubnaam',
AltClubcode=" .sqlvalue(@$_POST["AltClubcode"], true).",
`Club1`=" .sqlvalue(@$_POST["Club1"], true).",
`Clubprogramma`=" .sqlvalue(@$_POST["Clubprogramma"], true).",
`Clubnaamcheck`=" .sqlvalue(@$_POST["Clubnaamcheck"], true).",
`CombCpAfg`=" .sqlvalue(@$_POST["CombCpAfg"], true).",
`MinDagUitslagen`=" .sqlvalue(@$_POST["MinDagUitslagen"], true).",
`PlusDagUitslagen`=" .sqlvalue(@$_POST["PlusDagUitslagen"], true).",
`PlusDagCP`=" .sqlvalue(@$_POST["PlusDagCP"], true).",
`MinDagCP`=" .sqlvalue(@$_POST["MinDagCP"], true).",
`SortCP`=" .sqlvalue(@$_POST["SortCP"], true).",
`FilterCP`=" .sqlvalue(@$_POST["FilterCP"], true).",
`CPophalen`=" .sqlvalue(@$_POST["CPophalen"], true).",
`CUophalen`=" .sqlvalue(@$_POST["CUophalen"], true).",
`DelwdstrdCPCU`=" .sqlvalue(@$_POST["DelwdstrdCPCU"], true).",
`DelwdstrdCPCUtxt`=" .sqlvalue(@$_POST["DelwdstrdCPCUtxt"], true).",
`ScheidsinCP`=" .sqlvalue(@$_POST["ScheidsinCP"], true).", `VVinCP`=" .sqlvalue(@$_POST["VVinCP"], true).",
`Pdetails`=" .sqlvalue(@$_POST["Pdetails"], true).",   `Pdetailstonen`=" .sqlvalue(@$_POST["Pdetailstonen"], true).",
`Klkamertonen`=" .sqlvalue(@$_POST["Klkamertonen"], true).",     `Veldtonen`=" .sqlvalue(@$_POST["Veldtonen"], true).",
`Oefeninteam`=" .sqlvalue(@$_POST["Oefeninteam"], true).",
`Tindelinginteam`=" .sqlvalue(@$_POST["Tindelinginteam"], true).",
`CSSadmin`=" .sqlvalue(@$_POST["CSSadmin"], true).",   `CSSweergave`=" .sqlvalue(@$_POST["CSSweergave"], true).",
`OphViaAcode`=" .sqlvalue(@$_POST["OphViaAcode"], true).",
`Wacht`=" .sqlvalue(@$_POST["Wacht"], true).", `Opschonen`=" .sqlvalue(@$_POST["Opschonen"], true).",
`Meldingen`=" .sqlvalue(@$_POST["Meldingen"], true).", `Curl`=" .sqlvalue(@$_POST["Curl"], true).",
`Phpsafemode`=" .sqlvalue(@$_POST["Phpsafemode"], true).", `Phpmaxtime`=" .sqlvalue(@$_POST["Phpmaxtime"], true).", `Gebruikersnaam`=" .sqlvalue(@$_POST["Gebruikersnaam"], true).",
`Wachtwoord`=" .sqlvalue(@$_POST["Wachtwoord"], true).", `Emailvoormeldingen`=" .sqlvalue(@$_POST["Emailvoormeldingen"], true).",
`Beveiligindex`=" .sqlvalue(@$_POST["Beveiligindex"], true).", `UserDebug`=" .sqlvalue(@$_POST["UserDebug"], true).",
`Igebruikersnaam`=" .sqlvalue(@$_POST["Igebruikersnaam"], true).", `Iwachtwoord`=" .sqlvalue(@$_POST["Iwachtwoord"], true).",
`Agebruikersnaam`=" .sqlvalue(@$_POST["Agebruikersnaam"], true).", `Awachtwoord`=" .sqlvalue(@$_POST["Awachtwoord"], true).",
`Ogebruikersnaam`=" .sqlvalue(@$_POST["Ogebruikersnaam"], true).", `Owachtwoord`=" .sqlvalue(@$_POST["Owachtwoord"], true).",
`Egebruikersnaam`=" .sqlvalue(@$_POST["Egebruikersnaam"], true).", `Ewachtwoord`=" .sqlvalue(@$_POST["Ewachtwoord"], true).",
`Ugebruikersnaam`=" .sqlvalue(@$_POST["Ugebruikersnaam"], true).", `Uwachtwoord`=" .sqlvalue(@$_POST["Uwachtwoord"], true).",
`Ggebruikersnaam`=" .sqlvalue(@$_POST["Ggebruikersnaam"], true).", `Gwachtwoord`=" .sqlvalue(@$_POST["Gwachtwoord"], true).",
`Dbgebruikersnaam`=" .sqlvalue(@$_POST["Dbgebruikersnaam"], true).", `Dbwachtwoord`=" .sqlvalue(@$_POST["Dbwachtwoord"], true).",
`Instgebruikersnaam`=" .sqlvalue(@$_POST["Instgebruikersnaam"], true).", `Instwachtwoord`=" .sqlvalue(@$_POST["Instwachtwoord"], true)."
where " .primarykeycondition();
	mysqli_query($con,$sql) or die(mysqli_error($con));
	//mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysqli_error($con));;
}


function primarykeycondition()
{
	global $_POST;
	$pk = "";
	$pk .= "(`InstellingenID`";
	if (@$_POST["xInstellingenID"] == "") {
		$pk .= " IS NULL";
	}else{
		$pk .= " = " .sqlvalue(@$_POST["xInstellingenID"], false);
	};
	$pk .= ")";
	return $pk;
}
?>
