<?php
// index.php
// Voetbal.nl Ripper 2.0 door Syphere en dutchtoro
// Datum laatste aanpassing: 27-10-2017
// In deze versie zijn de functies om gegevens de schrapen van voetbal.nl aangepast om te werken op
// de nieuwe versie ervan. Ook zijn deze aangepast voor PHP7

/// Oorspronkelijk script v1.9.7 door Johnvs
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel


// ER ZIJN GEEN AANPASINGEN NODIG IN INDEX.PHP

// ////////////////////////////////////////////////////////
// Het is mogelijk dat er bv in accommodatie of scheidsrechter verkeerde karakters verschijnen
// deze worden zoveel mogelijk al afgevangen indien nodig staat
// in functie.php bij de opslaan functies code om karaters te vervangen
// ////////////////////////////////////////////////////////

ini_set('display_errors', 1);
//Voorkom vreemde resultaten door netjes te programmeren.
//error_reporting(E_ALL);
error_reporting(E_ERROR | E_WARNING | E_PARSE);

#insert database data
include_once("config.php");
include_once("functies.php");

if ($Beveiligindex == "Aan")
{
	?>
	<!DOCTYPE html>

	<html xmlns="http://www.w3.org/1999/xhtml">


	<head>
	<title>Gegevens Ophalen</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="no-cache">
	<meta http-equiv="Expires" content="-1">
	<meta http-equiv="Cache-Control" content="no-cache">
	<link rel="stylesheet" type="text/css" href="opmaak.css">
	</head>
	<body>
	<table class="bd" width="100%"><tr><td class="hr"><h2>Gegevens Ophalen</h2></td></tr></table>

	<?php


	if (!login()) exit;

}


function login()
{
	include("config.php");
	if (isset($_GET["a"]) && ($_GET["a"] == 'logout')) $_SESSION["logged_in"] = false;
	if (!isset($_SESSION["logged_in"])) $_SESSION["logged_in"] = false;
	if (!$_SESSION["logged_in"]) {
		$login = "";
		$password2 = "";
		if (isset($_POST["login"])) $login = @$_POST["login"];
		if (isset($_POST["password"])) $password2 = @$_POST["password"];

		if (($login != "") && ($password2 != "")) {
			if (($login == $Indexusername) && ($password2 == $Indexpassword)) {
				$_SESSION["logged_in"] = true;
			}

			else {
				?>

				<p><b><font color="-1">De combinatie gebruikersnaam/wachtwoord is onjuist</font></b></p>
				<?php } } }if (isset($_SESSION["logged_in"]) && (!$_SESSION["logged_in"])) { ?>
		<form method="post">
		<table class="bd" border="0" cellspacing="1" cellpadding="4">
		<tr>
		<td>Gebruikersnaam</td>
		<td><input type="text" name="login" value="<?php echo $login ?>"></td>
		</tr>
		<tr>
		<td>Wachtwoord</td>
		<td><input type="password" name="password" value="<?php echo $password2 ?>"></td>
		</tr>
		<tr>
		<td><input type="submit" name="action" value="Inloggen"></td>
		</tr>
		</table>
		</form>
		<?php

	}
	if (!isset($_SESSION["logged_in"])) $_SESSION["logged_in"] = false;
	return $_SESSION["logged_in"];
}


if ($Phpsafemode == 'Uit') set_time_limit($Phpmaxtime); //tijd dat het script mag draaien. Indien Safemode aan staat werkt dit niet en moet php.ini aangepast worden.

$mysqli = new mysqli($server, $username, $password, $database);
if(mysqli_connect_errno())
{
	trigger_error('Fout bij verbinding: '.$mysqli->error);
}
if(empty($club)) die("Club gegevens nog niet ingesteld! Lees de leesmij.txt door!<br />");

if(isset($_GET['teamID']))
{
    $teamID = filter_var($_GET['teamID'], FILTER_VALIDATE_INT); //alleen numerieke waarde toegstaan - voorkomt mogelijkheid tot sql injection
	//Nodig om gegevens van 1 team op te halen. aanroepen met index.php?teamID=*

	$Ophalen = "Team";
	$groupID = "Not-Set"; // Dit wordt gezet omdat er verderop gechecked wordt op groupID
	$sql = "SELECT * FROM ".$dbprefix."teamlinks WHERE TeamID=$teamID";  // gegevens van 1 team moet worden opgehaald.
}

if(isset($_GET['groupID']))
{
    $groupID = filter_var($_GET['groupID'], FILTER_VALIDATE_INT); //alleen numerieke waarde toegstaan - voorkomt mogelijkheid tot sql injection
	//Nodig om gegevens van een groep van teams op te halen. aanroepen met index.php?groupID=*

	$Ophalen = "Group";
	$teamID = "Not-Set"; // Dit wordt gezet omdat er verderop gechecked wordt op teamID
	$sql = "SELECT * FROM ".$dbprefix."teamlinks WHERE GroupID=$groupID AND ophalen = 'ja'";  // gegevens van een groep teams moet worden opgehaald.
	$sql2 = "SELECT Gebruikersnaam, Wachtwoord FROM `".$dbprefix."groupidaccount` WHERE GroupID=$groupID";
}

if (false == isset($_GET['teamID']) and false == isset($_GET['groupID']))
{
    $Ophalen = "Alle";
	$teamID = "";
	$groupID = "";
	$sql = "SELECT * FROM ".$dbprefix."teamlinks WHERE ophalen = 'ja'"; // gegevens van alle teams moet worden opgehaald.
}


if ($Curl == 'Aan') {
	$ch = curl_init(); // Curl wordt gebruikt
}


// Checken welke clubcode gebruikt moet worden
if ($OphViaAcode == "Uit")
{
	// gegevens worden opgehaald via clubcode.
	$myclub = $club;
}
else
{
	// gegevens worden opgehaald via Alternative clubcode.
	$myclub = $altclub;
}

if ($UserDebug == 'Aan') echo "---Gebruikte clubcode:$myclub---<br />";


if(!$Slinks = $mysqli->query($sql))
{
	trigger_error('Fout in query: '.$mysqli->error);
}
else
{

	// Inloggen op voetbal.nl om de informatie op te halen waarvoor inloggen vereist is.
	// Dit doen we hiet omdat anders dit gebeurt elke keer als de team informatie wordt opgehaald en dit maar 1 keer hoeft

	if($debug !== true)
	{
		if ($Ophalen == 'Group') // In config.php moet voor iedere groupID een gebruikersnaam/wachtwoord zijn.
		{
			// indien groupID niet 0 is wordt er een gebruikernaam/wachtwoord met nummer gebruikt.
			// Anders het algemene gebruikersnaam/wachtwoord.
			if ($groupID !== 0 AND $groupID !== 999)
			{
				$result2=mysqli_query($con,$sql2);// mysql_query($sql2); //opvragen Gebruikersnaam en wachtwoord die behoren bij de groupID
				mysqli_data_seek($result2, 0);
				$finfo = mysqli_fetch_row($result2);
				$gebruikersnaam = $finfo[0]; //mysql_result($result2,0,"Gebruikersnaam") or die(" ---Probleem met Gebruikersnaam voor GroupID---");
				$wachtwoord = $finfo[1]; //mysql_result($result2,0,"Wachtwoord") or die(" ---Probleem met Wachtwoord voor GroupID---");
			}
		}
		login_voetbalnl($gebruikersnaam, $wachtwoord);

		$teams = get_teams("https://www.voetbal.nl/club/$myclub/teams");

		opslaan_teams(get_teamsdetails($teams));
		//get_teamsdetails($teams, $teamspagina);

		// Nu bekijken of we een melding krijgen dat er ingelogd moet worden als we een url aanroepen. Ja dan is het inloggen fout gegaan.
		/*$inlog = check_inlog("https://www.voetbal.nl/club/".$myclub."/teams");

		if ($inlog == "Meld je aan")
		{
			//Email versturen
			if ($Meldingen == 'Email' or $Meldingen == 'Beide') mail($Emailvoormeldingen, "Probleem met Voetbal.nl ripper.", "\nProbleem met Voetbal.nl ripper.\n\nHet inloggen op de site is niet gelukt.\n\nScript is gestopt.");
			//Of melding op scherm
			if ($Meldingen == 'Scherm' or $Meldingen == 'Beide') echo "Probleem met Voetbal.nl ripper.<br /><br />Het inloggen op de site is niet gelukt. <strong>Script is gestopt.</strong><br> ";
			die;
		} else
		{
			//Geen probleem met inloggen dus doorgaan.

		}
		// Nu bekijken we of het account niet geblokkeerd is.
		if ($inlog == "\n.")
		{
			//Email versturen
			if ($Meldingen == 'Email' or $Meldingen == 'Beide') mail($Emailvoormeldingen, "Probleem met Voetbal.nl ripper.", "\nProbleem met Voetbal.nl ripper.\n\nHet account voor voetbal.nl is geblokkeerd.\n\nScript is gestopt.");
			//Of melding op scherm
			if ($Meldingen == 'Scherm' or $Meldingen == 'Beide') echo "Probleem met Voetbal.nl ripper.<br /><br />Het account voor voetbal.nl is geblokkeerd. <strong>Script is gestopt.</strong><br />";
			die;
		} else
		{
			//Geen probleem met inloggen dus doorgaan.
		}*/
	}


	if ($Wacht == 'Aan' && $debug !== true) Sleep(rand(3, 15));

	$cpprobleem  = "";
	$cuprobleem  = "";


	if ($Clubprogrammaophalen == "Aan" or $teamID == "0" or $groupID == "0")
	{

		//Club Programma ophalen.
        $url = "https://www.voetbal.nl/club/".$myclub."/programma";
		$progs = get_clubprog($url);
		//dp($progs);
		$clubprog = $progs;
		//$clubaf = $progs[1];

		if (empty($clubprog) or $clubprog == 'leegprogramma')
		{
			// geen resulaten dus we verwijderen de resultaten uit de tabel niet en proberen het ook niet op te slaan
			$cpprobleem = "Probleem met het ophalen of opslaan van het Club Programma.";
			if ($UserDebug == 'Aan') echo "Club programma kan niet worden opgeslagen omdat het leeg is: -$clubprog-<br />";
		}
		else
		{
			// Leegmaken van de database. Ook als er geen programmagegevens zijn gevonden maar het ophalen goed is gegaan.
			leegmakendbcp($dbprefix."clubprogramma");
			// Club Programma opslaan in database alleen als er periode programmagegevens gevonden zijn.
			if ($progs <> "Er zijn geen programmagegevens") opslaan_clubprog($clubprog);
		}


		if(empty($clubaf) or $clubprog == 'leegprogramma')
		{
			// Leegmaken van de database. Ook als er geen afgelastingen zijn gevonden.
			leegmakendbcp($dbprefix."afgelasting");
		}
		else
		{
			// Leegmaken van de database.
			leegmakendbcp($dbprefix."afgelasting");

			// Afgelastingen opslaan in database alleen als er gegevens gevonden zijn.
			//if ($progs <> "Er zijn geen programmagegevens") opslaan_clubaf($clubaf);

// Volgens simonroester zou bovenstaande regel vervangen moeten worden door onderstaand regel.
// Aangezien het clubprogramma opgeslagen wordt terwijl er alleen afgelastingen zijn
// Heb het niet kunnen testen dus wijziging is nog niet door gevoerd.
//			if(count($clubprog) > 0 && $clubprog != "E") { opslaan_clubprog($clubprog);
		}
	}


	if ($Clubuitslagenophalen == "Aan" or $teamID == "999" or $groupID == "999")
	{
		//Club Uitslagen ophalen.
		$clubuitslagen = get_clubuitslag("https://www.voetbal.nl/club/".$myclub."/uitslagen");
		if (empty($clubuitslagen) or $clubuitslagen == 'leeg')
		{
			// geen resulaten dus we verwijderen de resultaten uit de tabel niet en proberen het ook niet op te slaan
			$cuprobleem = "Probleem met het ophalen of opslaan van het Club Uitslagen. ";
			if ($UserDebug == 'Aan') echo "Club Uitslagen kunnen niet worden opgeslagen omdat ze leeg zijn: -$clubuitslagen-<br />";
			echo"probleem";
		}
		else
		{
			// Leegmaken van de database. Ook als er geen programmagegevens zijn gevonden maar het ophalen goed is gegaan.
			// Eerst handmatig ingevoerde uitslagen veilig stellen.
			$saveteamuitslag = $dbprefix."uitslag";
			$huitslagen=clubhanduitslagen($saveteamuitslag," ",$dbprefix."clubuitslagen");
			$huitslag = $huitslagen[0];
			$huitslagnum = $huitslagen[1];

			leegmakendbcp($dbprefix."clubuitslagen");
			// Club Uitslagen opslaan in database alleen als er gegevens gevonden zijn.
			if ($clubuitslagen <> "Er zijn geen thuis uitslagen") opslaan_clubuitslagen($clubuitslagen);
			if ($huitslagnum > '0') opslaan_chuitslagen($huitslag,$huitslagnum,$dbprefix."clubuitslagen");


		}
	}
	// ClubProgramma, ClubAfgelastingen, ClubUitslagen ophalen en opslaan moet hierboven gebeuren anders gebeurt het bij ieder team in de database.

	while($link = $Slinks->fetch_assoc())
	{
		if ($Phpsafemode == 'Uit') set_time_limit($Phpmaxtime); // reset van de time counter zodat voor ieder team opnieuw geteld wordt.

		$probleem    = "";
		//  $cpprobleem  = "";
		//  $cuprobleem  = "";
		$sprobleem   = "";
		$uprobleem   = "";
		$pprobleem   = "";
		$auprobleem  = "";
		$perprobleem = "";

		// aanroepen van functies om de informatie op te halen van voetbal.nl

		// Checken welke teamcode gebruikt moet worden
		//if ($OphViaAcode == "Uit")
		//{
			// gegevens worden opgehaald via Teamcode.
			$myteamcode = $link["Teamcode"];
		//}
		//else
		//{
			// gegevens worden opgehaald via Alternative Teamcode.
		//	$myteamcode = $link["AltTeamcode"];
		//}

		if ($UserDebug == 'Aan') echo "---Gebruikte Teamcode:$myteamcode---<br />";

		if ($link["Wedstrijdtype"] == "Competitie_NJ")        $basestandurl="https://www.voetbal.nl/team/$myteamcode/stand";
		if ($link["Wedstrijdtype"] == "Competitie")        $basestandurl="https://www.voetbal.nl/team/$myteamcode/stand";
		if ($link["Wedstrijdtype"] == "Competitie_NJ")			$baseurl="https://www.voetbal.nl/team/$myteamcode/programma/beker";
		if ($link["Wedstrijdtype"] == "Competitie")			$baseurl="https://www.voetbal.nl/team/$myteamcode/programma/competitie";




		if ($link["Wedstrijdtype"] != "Competitie" and $link["Wedstrijdtype"] != "Competitie_NJ")
		{
			echo "Probleem met Voetbal.nl ripper.<br /><br />";
			echo "Er is een probleem met de waarde 'Wedstrijdtype' in admin.php voor team: ".$myteamcode;
			echo "<br />De waarde moet 'Competitie' of Beker zijn. De waarde is nu: ".$link["Wedstrijdtype"];
			echo "<br /><strong>Script is gestopt.</strong><br />";
			die;
		}else
		{
			//Geen probleem met inloggen dus doorgaan.
		}

		//dp($baseurl);

		//Haal de teamgegevens op en sla deze op in $html.
		$html = get_teamdata($baseurl);
		$standhtml = get_teamdata($basestandurl);
		//Standen ophalen
		$standen = get_stand($standhtml,"normaal");

		/*//Periode Standen ophalen
		if ($link["Periode"] == "ja")
		{
			$standenp1 = get_stand($html,"p1");
			$standenp2 = get_stand($html,"p2");
			$standenp3 = get_stand($html,"p3");
		}*/

		//Uitslagen ophalen
		if ($link["Wedstrijdtype"] == "Competitie_NJ") {
			$baseurl="https://www.voetbal.nl/team/ajax/" .$myteamcode ."/uitslagen/competitie-najaar";
		} else {
			$baseurl="https://www.voetbal.nl/team/ajax/" .$myteamcode ."/uitslagen/competitie";
		}
		//$html = get_teamdata_ajax($baseurl);
		$uitslagen = get_uitslag($standhtml);
		//Programma ophalen
		// team programma ophalen met details - Scheidrechter adres etc
		$programma = get_programmadetailed($html, $myteamcode);

		//Alle Uitslagen ophalen
		//$alle = get_alle($html);


		if(empty($standen))
		{
			// geen resulaten dus we verwijderen de resultaten uit de tabel niet en proberen het ook niet op te slaan
			$sprobleem = "Probleem met het ophalen of opslaan van de Standen. ";
			if ($UserDebug == 'Aan') echo "Standen kunnen niet worden opgeslagen omdat het leeg is: -$standen-<br />";
		}
		else
		{
			// Leegmaken van de database
			leegmakendb($dbprefix."stand",$link["TeamID"]);
			//Standen opslaan in database
			opslaan_stand($standen,$link,$dbprefix."stand");
		}

		if(empty($uitslagen))
		{
			// geen resulaten dus we verwijderen de resultaten uit de tabel niet en proberen het ook niet op te slaan
			$uprobleem = "Probleem met het ophalen of opslaan van de Uitslagen. ";
			if ($UserDebug == 'Aan') echo "Uitslagen kunnen niet worden opgeslagen omdat het leeg is: -$uitslagen-<br />";
		}
		else
		{
			// Leegmaken van de database. Ook als er geen uitslagen zijn gevonden maar het ophalen goed is gegaan.
			// Eerst handmatig ingevoerde uitslagen veilig stellen.
			$saveteamuitslag = $dbprefix."uitslag";
			$huitslagen=teamhanduitslagen($saveteamuitslag,$link["TeamID"],$dbprefix."uitslag");
			$huitslag = $huitslagen[0];
			$huitslagnum = $huitslagen[1];


			leegmakendb($dbprefix."uitslag",$link["TeamID"]);
			//Uitslagen opslaan in database alleen als er uitslagen gevonden zijn.
			if ($uitslagen <> "Er zijn geen uitslagen") opslaan_uitslagen($uitslagen,$link);
			if ($huitslagnum > '0') opslaan_thuitslagen($huitslag,$huitslagnum,$dbprefix."uitslag");
		}
//
//		// ontdubbelen Afgelast/lege uitslagen
//		$afgelastteamuitslag = $dbprefix."uitslag";
//		$auitslagen=ontdubbeluitslagen($afgelastteamuitslag,$link["TeamID"],$dbprefix."uitslag");
//		$auitslag = $auitslagen[0];
//		$auitslagnum = $auitslagen[1];
//		if ($auitslagnum > '0') opslaan_afuitslagen($auitslag,$auitslagnum,$dbprefix."uitslag");
//
//
		if(empty($programma) or $programma == 'leegprogramma')
		{
			// Probleem met ophalen programma gegevens dus we verwijderen de resultaten uit de tabel niet en proberen het ook niet op te slaan
			$pprobleem = "Probleem met het ophalen van programma gegevens. ";
			if ($UserDebug == 'Aan') echo "programma kan niet worden opgeslagen omdat het programma leeg is: -$programma-<br />";
		}
		else
		{
			// Leegmaken van de database. Ook als er geen programmagegevens zijn gevonden maar het ophalen goed is gegaan.
			leegmakendb($dbprefix."programma",$link["TeamID"]);
			//Programma opslaan in database alleen als er programmagegevens gevonden zijn.
			//if ($Programmadetails == 'Aan')
			//{
			//	if ($programma <> "Er zijn geen programmagegevens") opslaan_programmadetailed($programma,$link);
			//}
			//else
			//{
				if ($programma <> "Er zijn geen programmagegevens") opslaan_programmadetailed($programma,$link);
			//}
		}
//
//		if(empty($alle))
//		{
//			// geen resulaten dus we verwijderen de resultaten uit de tabel niet en proberen het ook niet op te slaan
//			$auprobleem = "Probleem met het ophalen of opslaan van Alle Uitslagen. ";
//			if ($UserDebug == 'Aan') echo "Alle Uitslagen kunnen niet worden opgeslagen omdat het leeg is: -$alle-<br />";
//		}
//		else
//		{
//			// Leegmaken van de database
//			leegmakendb($dbprefix."alle",$link["TeamID"]);
//			//Alle Uitslagen opslaan in database
//			opslaan_alle($alle,$link);
//		}
//
//		// Ophalen van de periode standen als dit aangezet voor het team
//		if ($link["Periode"] == "ja")
//		{
//			if(empty($standenp1))
//			{
//				//Geen resulaten dus we verwijderen de resultaten uit de tabel niet en proberen het ook niet op te slaan
//				$perprobleem = "Probleem met het ophalen of opslaan van de Periode Standen. Of er is geen stand voor de periode.";
//				if ($UserDebug == 'Aan') echo "Periode Standen P1 kunnen niet worden opgeslagen omdat standen leeg zijn: P1:-$standenp1-<br />";
//			}
//			else
//			{
//				//Leegmaken van de database
//				leegmakendb($dbprefix."standp1",$link["TeamID"]);
//				//Standen opslaan in database
//				opslaan_stand($standenp1,$link,$dbprefix."standp1");
//			}
//			if(empty($standenp2))
//			{
//				//Geen resulaten dus we verwijderen de resultaten uit de tabel niet en proberen het ook niet op te slaan
//				$perprobleem = "Probleem met het ophalen of opslaan van de Periode Standen. Of er is geen stand voor de periode.";
//				if ($UserDebug == 'Aan') echo "Periode Standen P2 kunnen niet worden opgeslagen omdat standen leeg zijn: P2:-$standenp2-<br />";
//			}
//			else
//			{
//				//Leegmaken van de database
//				leegmakendb($dbprefix."standp2",$link["TeamID"]);
//				//Standen opslaan in database
//				opslaan_stand($standenp2,$link,$dbprefix."standp2");
//			}
//			if(empty($standenp3))
//			{
//				//Geen resulaten dus we verwijderen de resultaten uit de tabel niet en proberen het ook niet op te slaan
//				$perprobleem = "Probleem met het ophalen of opslaan van de Periode Standen. Of er is geen stand voor de periode.";
//				if ($UserDebug == 'Aan') echo "Periode Standen P3 kunnen niet worden opgeslagen omdat standen leeg zijn: P3:-$standenp3-<br />";
//			}
//			else
//			{
//				//Leegmaken van de database
//				leegmakendb($dbprefix."standp3",$link["TeamID"]);
//				//Standen opslaan in database
//				opslaan_stand($standenp3,$link,$dbprefix."standp3");
//			}
//		}
//
//		// Volgende regel actief maken als account wordt geblokkeerd door hoeveelheid data die wordt opgehaald.
//		if ($Wacht == 'Aan') Sleep(rand(4, 8));
//
//		// Updaten van datum en tijd
//		$cur_time = time();
//		include("config.php");
//		mysql_connect($server,$username,$password);
//		@mysql_select_db($database) or die( "Unable to select database to update date and time");
//		$sql="UPDATE ".$dbprefix."teamlinks SET `DatumTijd-Update` = $cur_time WHERE TeamID = ".$link["TeamID"];
//		mysql_query($sql)
//		or die(mysql_error());
//
//		// Mail versturen - bericht geven indien er problemen zijn
//		$probleem = $cpprobleem.$cuprobleem.$sprobleem.$uprobleem.$pprobleem.$auprobleem.$perprobleem;
//		if(empty($probleem))
//		{
//			// Er is blijkbaar niets mis gegaan dus we doen niets.
//		}
//		else
//		{
//			$teaminfo = "TeamID: ".$link["TeamID"]." Teamcode: ".$myteamcode." Teamnaam: ".$link["Naam"];
//			//Email versturen
//			if ($Meldingen == 'Email' or $Meldingen == 'Beide') mail($Emailvoormeldingen, "Probleem met Voetbal.nl ripper.", "<br />".$teaminfo."<br />".$probleem."\n\nDit kan betekenen dat er een wijziging was op voetbal.nl maar het kan ook zijn dat er geen nieuwe data is");
//			//Of melding op scherm
//			if ($Meldingen == 'Scherm' or $Meldingen == 'Beide') echo "Probleem met Voetbal.nl ripper."."<br />".$teaminfo."<br />".$cpprobleem."<br />".$cuprobleem."<br />".$sprobleem."<br />".$uprobleem."<br />".$pprobleem."<br />".$auprobleem."<br />".$perprobleem."<br />";
//		}
//
//
	}

}
// Opschonen van bestanden in de database indien keuze 'Aan' in config.php
if ($Opschonen == 'Aan')
{
	reorderdb($dbprefix."stand","StandID");
	reorderdb($dbprefix."uitslag","UitslagID");
	reorderdb($dbprefix."programma","ProgrammaID");
	reorderdb($dbprefix."alle","AlleID");
	reorderdb($dbprefix."standp1","StandID");
	reorderdb($dbprefix."standp2","StandID");
	reorderdb($dbprefix."standp3","StandID");
}
?>
