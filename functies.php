<?php
// functies.php
// Voetbal.nl Ripper 2.0 door Syphere en dutchtoro
// In deze versie zijn de functies om gegevens de schrapen van voetbal.nl aangepast om te werken op
// de nieuwe versie ervan. Ook is deze aangepast voor PHP7


/// Oorspronkelijk script v1.9.7 door Johnvs
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel

require("config.php");

if ($Curl == "Uit") {
	include("class.socket.php");
	$usesocket = 'true';
}

//Inloggen -----------------------------
function login_voetbalnl($Gebruikersnaam, $Wachtwoord)
{
	global $ch, $cookies, $usesocket, $site, $Phpsafemode, $useproxy, $proxyhost, $proxyport;

	if ($usesocket == 'true') {
		//Inloggen op voetbal.nl middels de socket class van Lucas
		$url  = "https://www.voetbal.nl/inloggen"; //"http://pupillen.voetbal.nl/home?destination=node%2F23942";
		if ($useproxy == 'true') {
			$site  = $proxyhost;
			$port  = $proxyport;
		} else {
			$site = "www.voetbal.nl";
			$port = 80;
		}
		$post = new HttpPostRequest();
		$post -> set_ip($site, $port);
		$post -> set_file($url);
		$post -> include_headers();
		$post -> use_redirect(false);
		$post -> add_var("email", $Gebruikersnaam); //add_var("name", $Gebruikersnaam);    // Je email
		$post -> add_var("password", $Wachtwoord);//add_var("pass", $Wachtwoord);        // je wachtwoord
		$post -> add_var("op", "Inloggen");
		$post -> add_var("form_id", "voetbal_login_login_form"); //"login_block_v3_login_form");
		$conected = $post -> request();
		$cookies = $post -> get_cookies();
		$result = $conected;
			} 
		else 
		{
		$site = 'https://www.voetbal.nl';
		$url = $site."/inloggen"; // simple scherm om in te loggen	 
		$ch = curl_init();

		//$httpagent = $_SERVER["HTTP_USER_AGENT"];
		$httpagent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36';
		curl_setopt($ch, CURLOPT_USERAGENT, $httpagent);		
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('Expect:'));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 1);
	//	if ($Phpsafemode == "Uit") curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		//curl_setopt($ch, CURLOPT_HEADERFUNCTION, "HeaderCallback");

		if ($useproxy == 'true') {
			curl_setopt($ch, CURLOPT_PROXY, $proxyhost);
			curl_setopt($ch, CURLOPT_PROXYPORT, $proxyport);
		}


		$logindata = file_get_contents("https://www.voetbal.nl/inloggen"); //hierin staat de form_build_id die bij iedere sessie anders is
	        $formbuilddata = explode('name=\"form_build_id\" id=\"',$logindata,2);

		
		// name is je gebruikersnaam
		// password is je wachtwoord
		$postdata=array(
			"email" => $Gebruikersnaam,    
			"password" => $Wachtwoord,   
			"op" => "Inloggen",
                        "form_build_id" => substr($formbuilddata[1],0,37),
			"form_id" => "voetbal_login_login_form"
		);

		curl_setopt ($ch, CURLOPT_POSTFIELDS, $postdata);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//error_reporting(E_ALL);
		if ($Phpsafemode == "Uit") {
			$result = curl_exec($ch);
			}
		else
		{
			$result = curl_exec_follow($ch);
		}
		

		preg_match_all('|Set-Cookie: (.*);|U', $result, $results);
		$cookies = implode(';', $results[1]);
	}
	if($devdebug) {
		echo "cookies zijn: $cookies <br />";
		echo "result is: $result <br />";
	}


	return $result;
}
// -------------------------------------



//Inlog check ----------------------
function check_inlog($url)
{
	$html = fetch_url($url);

	//Eerste kijken of er wel informatie is als er alleen een . staat is het account geblokkeerd. De "lege regel punt" opde volgende regel is belangrijk.
	$zoekstring = "\n.";
	if(strstr($html,$zoekstring)) return $zoekstring;

	// Kijken of er niet staat - Log in of registreer gratis! - anders is inloggen mislukt
	$zoekstring = "Meld je aan";
	if(strstr($html,$zoekstring)) return $zoekstring;
	
	//$regexp omschrijft nu hoe de pagina eruit moet zien
	//$regexp ='/<div><h1>(.*?)<.h1><.div>\s+/';
	$regexp = '/section-navigation-link active">(.*?)<.a>/';
	
	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Inlog'] = $value[1];
		$i++;
	}
	if(isset($value))
	{
		//Waarde terug geven
		return $value[1];
	}else{
		return NULL;
	}
}
// -------------------------------------

//Algemene functie om de resultaten van een url op te halen en deze als resultaat terug te geven.

function fetch_url($url)
{
	global $ch, $site, $cookies, $usesocket, $useproxy, $proxyhost, $proxyport;

	if ($usesocket == 'true') {
		//Haal de url op middels de socket class van Lucas
		$get = new HttpGetRequest();
		if ($useproxy == 'true') {
			$get -> set_ip($site, $proxyport);
		} else {
			$get -> set_ip($site, 80);
		}
		$get -> set_file($url);

		foreach($cookies as $name => $value) {
			$get -> set_cookie($name, $value);
		}
		if ($get -> request()) {
			$html = $get -> get_contents();
		}
	} else {
		//Haal de url op middels CURL

		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Expect:'));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_COOKIE,  $cookies);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		if ($useproxy == 'true') {
			curl_setopt($ch, CURLOPT_PROXY, $proxyhost);
			curl_setopt($ch, CURLOPT_PROXYPORT, $proxyport);
		}
		$html = curl_exec($ch);
	}
	return $html;
}

function HeaderCallback($curl, $header)
{
	echo $header ."<br />";
}

// -------------------------------------

//Alle Programma's ophalen voor Club ---
function get_clubprog($cpurl)
{
	include("config.php");

	$html = fetch_url($cpurl);
	//Er zijn wel gegevens gevonden dus gaan we door
	//$regexp omschrijft nu hoe de tabel eruit moet zien
	$regexp = '/(?:<span class="title">[\s\S]+?<span>(.*)<.span>[\s\S]+?)?<a href="(.*)" class="row.*"[\s\S]+?class="team">(.*)<.div>[\s\S]+?"value center">[\s\S]+?(.*)<.div>[\s\S]+?class="team">(.*)<.div>/';
	
	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);

	if(count($matches) < 1)
	{
		echo 'Fout bij het ophalen van het programma, lijkt fout te gaan met het ophalen van de html:<br /><br />'.$html;
	}
	
	$match = array();
	$afgmatch = array();
	$i = 0;
	$j = 0;
	$datum;
	foreach($matches AS $value)
	{
		if(!empty($value[1]))
		{
			$datum = $value[1];
		}
		else
		{
			$value[1] = $datum;
		}

			$match[$i]['Datum']   = $value[1];
			$match[$i]["Thuis"]   = trim($value[3]);
			$match[$i]["Uit"]     = trim($value[5]);
			$match[$i]['Tijd']	 = $value[4];
			$i++;

	}
	//Elke match teruggeven
	//if($devdebug) var_dump ($match);
	//return array($match,$afgmatch);
	return $match;
}


//TODO: Opschonen en MySQLi queries/Array goed mengen
function opslaan_clubprog($clubprog)
{
	include("config.php");

	foreach($clubprog AS $value)
	{	
		$datum = mysqli_real_escape_string($con, convertStringToDate(trim($value["Datum"])));
		$tijd = mysqli_real_escape_string($con, trim($value["Tijd"]));
		$thuis = mysqli_real_escape_string($con, trim($value["Thuis"]));
		$uit = mysqli_real_escape_string($con, trim($value["Uit"]));
		$sql = "INSERT INTO ${dbprefix}clubprogramma (Datum, Tijd, Thuis, Uit) VALUES 
				('$datum', '$tijd', '$thuis', '$uit');";
	
		if ($UserDebug == 'Aan') {
			mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
		}
		else
		{
			mysqli_query($con,$sql) or die(mysqli_error($con));
		}
	}
	// verwijderen van bepaalde wedstrijden volgens instellingen.php
	/*if ($DelwdstrdCPCU == 'Aan') {
		$DelwdstrdCPCUtxt = "%".$DelwdstrdCPCUtxt."%";
		$sql = "DELETE FROM `".$dbprefix."clubprogramma` WHERE Thuis Like '$DelwdstrdCPCUtxt' OR Uit Like '$DelwdstrdCPCUtxt'";
		if ($UserDebug == 'Aan') {
			mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno() . ") " . mysqli_error($con));
		}
		else
		{
			mysqli_query($con,$sql) or die(mysqli_error($con));
		}
	}*/

}
// -------------------------------------

//TODO:
function get_alle($html)
{

	//Tegenwoordig bevat de pagina meerdere tabellen met uitslagen. Neem de alles na de eerste
	//$htmlstripped = get_string_between($html,"id=\"ResultsContent3","");
	$htmlstripped = get_string_between($html,'<div name="ResultsContent" id="ResultsContent2" style="display: none;">',"");


	// Er zijn wel gegevens gevonden dus gaan we door //$regexp omschrijft nu hoe de tabel eruit moet zien
	$regexp = '/<div.class="datum-tijd">(.*?)<.div>\s+<div.class="wedstrijd"><div.class=".*?">(.*?)<.div><div.*?>-<.div><div.class=".*?">(.*?)<.div><.div>\s+<div.class="uitslag">(.*?)-(.*?)<.div>/';
	preg_match_all($regexp, $htmlstripped, $matches, PREG_SET_ORDER);

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Datum']   = $value[1];
		$match[$i]["Thuis"]   = trim($value[2]);
		$match[$i]["Uit"]     = trim($value[3]);
		$match[$i]['Uitslag'] = $value[4]." - ".$value[5];
		$i++;
	}
	//Elke match teruggeven
	$match = removeDuplicateMatches($match);
	return $match;
}


function get_teamdata($baseurl)
{
	$html = fetch_url($baseurl);
	return $html;
}
// -------------------------------------


// Stand ophalen -----------------------
function get_stand($html, $what)
{
	//Omdat de normale en de periode standen achter elkaar staan in $html kopieren we alleen het juiste deel in $htmlstripped

	//Stand periode 1 ophalen
	if ($what == "p1")
	{
		$htmlstripped = get_string_between($html,"id=\"RankingsContent2","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
	}

	//Stand periode 2 ophalen
	if ($what == "p2")
	{
		$htmlstripped = get_string_between($html,"id=\"RankingsContent3","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
		if(empty($htmlstripped))
		{
			// $htmlstripped = get_string_between($html,"id=\"RankingsContent3","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
		}
	}

	//Stand periode 3 ophalen, als deze er is. Anders nogmaals periode 2 ophalen, omdat er maar 2 zijn.
	if ($what == "p3")
	{
		$htmlstripped = get_string_between($html,"id=\"RankingsContent4","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
		if(empty($htmlstripped))
		{
			// $htmlstripped = get_string_between($html,"id=\"RankingsContent3","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
		}
	}

	//Normale stand ophalen
	if ($what == "normaal")
	{
		//Controleer of het team uberhaupt periode standen kent.
		// if (strpos($html,"id=\"RankingsContent2") <> 0)
		// {
		//	//Team kent periode standen
		//	$htmlstripped = get_string_between($html,"id=\"RankingsContent1","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
		//} else {
		//	//Team kent geen periode standen
		//	$htmlstripped = get_string_between($html,"id=\"RankingsContent1","");
		//}
		$htmlstripped = get_string_between($html,"<div class=\"table-wrapper table-standingstable\" data-printable-target>","<div class=\"table-captions-wrapper\">");
	}

	// echo $htmlstripped;
	// $regexp = '/<div class="nr">(.*?)<.div>\s+<div class="team.*?">(.*?)<.div>\s+<div class="g"><strong>(.*?)<.strong><.div>\s+<div class="w">(.*?)<.div>\s+<div class="gl">(.*?)<.div>\s+<div class="v">(.*?)<.div>\s+<div class="p"><strong>(.*?)<.strong><.div>\s+<div class="dvp">(.*?)<.div>\s+<div class="dpt">(.*?)<.div>\s+<div class="pm">(.*?)<.div>\s+<.div>\s+/';
	$regexp = '/<div class="value position">\s{2,}<span>(.*?)<.span>\s+<.div>.*\s.*\s.*<div class="value team">\s{2,}<span>(.*?)<.span>\s+<.div><div class="value">\s{2,}<span>(.*?)<.span>\s+<.div><div class="value">\s{2,}<span>(.*?)<.span>\s+<.div><div class="value">\s{2,}<span>(.*?)<.span>\s+<.div><div class="value">\s{2,}<span>(.*?)<.span>\s+<.div><div class="value points">\s{2,}<span>(.*?)<.span>\s+<.div><div class="value ">\s{2,}<span>(.*?)<.span>\s+<.div><div class="value">\s{2,}<span>(.*?)<.span>\s+<.div><div class="value">\s{2,}<span>(.*?)<.span>\s+/';

	//Waardes ophalen en opslaan in $matches
	preg_match_all($regexp, $htmlstripped, $matches, PREG_SET_ORDER);
	
	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		if(!empty($value[1]) && !empty($value[2]))
		{
			$match[$i]['#'] = trim($value[1]);
			$match[$i]['Elftal'] = trim($value[2]);
			$match[$i]['G'] = trim($value[3]);
			$match[$i]['W'] = trim($value[4]);
			$match[$i]['GW'] = trim($value[5]);
			$match[$i]['V'] = trim($value[6]);
			$match[$i]['P'] = trim($value[7]);
			$match[$i]['DPV'] = trim($value[8]);
			$match[$i]['DPT'] = trim($value[9]);
			$match[$i]['PM'] = trim($value[10]);
			$i++;
		}
	}
	//Elke match teruggeven
	return $match;
}
// -------------------------------------

//TODO: Regexp aanpassen naar nieuwe templates
function get_uitslag($html)
{
	//Tegenwoordig bevat de pagina meerdere tabellen met uitslagen. Neem de eerste. Aangenomen dat dit de laatste uitslagen zijn
	$htmlstripped = get_string_between($html,"id=\"showResults","id=\"showRankings");


	// eerste kijken of er wel informatie is
	$zoekstring = "Er zijn geen uitslagen";

	// Indien er geen gegevens zijn gevonden stoppen
	if(strstr($htmlstripped,$zoekstring)) return $zoekstring;


	//Er zijn wel gegevens gevonden dus gaan we door
	//$regexp omschrijft nu hoe de tabel eruit moet zien
	//Houd ook rekening met "higlight" voor als het een "mijn elftal" elftal is.
	$regexp ='/<div.class="datum-tijd">(.*?)<.div>\s+<div.class="wedstrijd"><div.class=".*?">(.*?)<.div><div.*?>-<.div><div.class=".*?">(.*?)<.div><.div>\s+<div.class="uitslag">(.*?)<.div>/';
	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Datum']   = $value[1];
		$match[$i]["Thuis"]   = trim($value[2]);
		$match[$i]["Uit"]     = trim($value[3]);
		//    $match[$i]['Uitslag'] = $value[4]." - ".$value[5];
		$match[$i]['Uitslag'] = $value[4];
		$i++;
	}
	//Elke match teruggeven
	return $match;
}

// Haalt de clubuitslagen op
// Haalt nu wel de gegevens, maar nog niet wanneer deze gestaakt is (geeft geen GST aan)
function get_clubuitslag($cuurl)
{
	include("config.php");

	$html = fetch_url($cuurl);

	// Er zijn wel gegevens gevonden dus gaan we door //$regexp omschrijft nu hoe de tabel eruit moet zien
	/*$regexp ='/<div.class="datum-tijd".style="margin-left:15px;">(.*?)<.div>\s+<div.class="wedstrijd"><div.class=".*?">(.*?)<.div><div.*?>-<.div><div.class=".*?">(.*?)<.div><.div>\s+<div.class="uitslag">(.*?)<.div>/';*/
	//$regexp = '/<span class="title">[\s\S]+?<span>(.*)<.span>[\s\S]+?<a href="(.*)"[\s\S]+?class="team">(.*)<.div>[\s\S]+?class="value center">\s+?(.*)<.div>[\s\S]+?class="team">(.*)<.div>/';
	$regexp = '/(?:<span class="title">[\s\S]+?<span>(.*)<.span>[\s\S]+?)?<a href="(.*)" class="row.*"[\s\S]+?class="team">(.*)<.div>[\s\S]+?"value center">[\s\S]+?(.*)<.div>[\s\S]+?class="team">(.*)<.div>/';
	
	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);
	
	$match = array();
	$i = 0;
	$datum = "";
	foreach($matches AS $value)
	{
		if(!empty($value[1]))
		{
			$datum = $value[1];
		}
		else
		{
			$value[1] = $datum;
		}
		$match[$i]['Datum']   = $value[1];
		$match[$i]["Thuis"]   = trim($value[3]);
		$match[$i]["Uit"]     = trim($value[5]);
		$match[$i]['Uitslag'] = $value[4];
		$i++;
	}
	//Elke match teruggeven
	//$match = removeDuplicateMatches($match);
	
	return $match;
}
// -------------------------------------

function get_programmadetailed($html, $teamcode)
{
	include("config.php");

	// Er zijn wel gegevens gevonden dus gaan we door
	//$regexp omschrijft nu hoe de tabel eruit moet zien
	/*$regexp = '/<div.class="datum-tijd">(.*?),.(.*?)<.div>\s+<div.class="status(\s(afgelast))*">\s+<div.class="wedstrijd"><div.class=".*?">(.*?)<.div><div..*?>-<.div><div.class=".*?">(.*?)<.div><.div>\s+<div.class="type">(.*?)<.div>\s+<div.class="accomodatie">(.*?)<.div>\s+<.div>\s+.*?\s+<div.class="details"><a.class="fancy-box".href="(.*?)\?iframe/';*/
	$regexp = '/(?:<span class="title">[\s\S]+?<span>(.*)<.span>[\s\S]+?)?<a href="(.*)" class="row.*"[\s\S]+?class="team">(.*)<.div>[\s\S]+?"value center">[\s\S]+?(.*)<.div>[\s\S]+?class="team">(.*)<.div>/';
	
	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);

	//dp($matches);
	//die;

	$match = array();
	$i = 0;
	$datum;
	foreach($matches AS $value)
	{
		if(!empty($value[1]))
		{
			$datum = $value[1];
		}
		else
		{
			$value[1] = $datum;
		}
		//$match[$i]['Datum']          = $value[1];
		//$match[$i]['Tijd']           = $value[2];
		//$match[$i]['Status']         = $value[4];
		//$match[$i]['Wedstrijd']      = $value[5]." - ".$value[6];
		//$match[$i]['Type']           = $value[7];
		
		$match[$i]['Datum']				= $value[1];
		$match[$i]['Wedstrijd']			= $value[3]." - ".$value[5];
		$match[$i]['Page']				= $value[2];
		$match[$i]['Tijd']				= trim($value[4]);

		//Bij een afgelasting staat er een plaatje in plaats van de accommodatie.
		/*if(stristr($value[8], 'AFGELAST') === FALSE)
		{
			$match[$i]['Accommodatie'] = $value[8];
		}else{
			$match[$i]['Accommodatie'] = '';
		}*/
		
		// Alle wedstrijd details staan nu in een aparte link. Deze wordt nu opgevraagd en alle details opgeslagen
		$htmlwdetails = fetch_url("http://voetbal.nl".$value[2]);
		
		

		
		/*$regexp = '/<div.class="naw.*><label>Wedstrijdnummer:<.label>.(.*?)<.div>\s+(<div.class="naw.*><label>Periode:<.label>.(.*?)<.div>\s+)?<div.class="naw.*><label>Scheidsrechter:<.label>.(.*?)<.div>\s+<div.class="naw.*><label>Sportpark:<.label>.(.*?)<.div>\s+<div.class="naw.*><label>Adres:<.label>.(.*?)<.div>\s+<div.class="naw.*><label>Postcode:<.label>.(.*?)<.div>\s+<div.class="naw.*><label>Plaats:<.label>.(.*?)<.div>\s+<div.class="naw.*><label>Telefoon:<.label>.(.*?)<.div>\s+/'; */
		//$regexp = '/class="column values">\s+?<span>(.*)<.span>[\s\S]+?(?:<span>(.*)<.span>[\s\S]+?)?(?:referee">\s+(.*)<.span>[\s\S]+?)?"park">(.*)<[\s\S]+?"street">(.*)<[\s\S]+?"zip">(.*) (.*)<[\s\S]+?"phone">(.*)</';
		$regexp = '/class="column values">\s+?<span>(.*)<.span>[\s\S]+?(?:<span>(.*)<.span>[\s\S]+?)?(?:referee">\s+(.*)<.span>[\s\S]+?)?"park">(.*)<[\s\S]+?"street">(.*)<[\s\S]+?"zip">(.*)<[\s\S]+?"phone">(.*)</';
		
		preg_match_all($regexp, $htmlwdetails, $dmatches, PREG_SET_ORDER);
		//$dmatch = array();
		$i1=0;
		foreach($dmatches AS $dvalue)
		{
			$addressArray = explode(' ', $dvalue[6], 2);
			$match[$i]['Wedstrijdnr']    = $dvalue[1];
			$match[$i]['Periode']    = $dvalue[2];
			$match[$i]['Scheidsrechter']    = $dvalue[3];
			$match[$i]['Sportpark']    = $dvalue[4];
			$match[$i]['Adres']    = $dvalue[5];
			$match[$i]['Postcode']    = $addressArray[0];
			$match[$i]['Plaats']    = $addressArray[1];
			$match[$i]['Telefoon']    = $dvalue[7];
			$i1++;
		}
		$i++;
	}
	
	//Elke match teruggeven
	return $match;
}
// Einde functie programma ophalen met details -------------------------------------

function opslaan_programmadetailed($programma,$link)
{
	include("config.php");
	$t = 0;
	$CountData = count($programma);
	foreach($programma AS $value)
	{
		/*if(!isset($value['Wedstrijd']))
		{
			$wedstrijd = '';
		}else{
			$value['Wedstrijd'] = htmlentities($value['Wedstrijd'], ENT_QUOTES);
		}
		$value['Accommodatie'] = htmlentities($value['Accommodatie'],ENT_QUOTES);
		$value['Sportpark'] = htmlentities($value['Sportpark'],ENT_QUOTES);
		$value['Adres'] = htmlentities($value['Adres'],ENT_QUOTES);
		$value['Plaats'] = htmlentities($value['Plaats'],ENT_QUOTES);
		if(!isset($value['Wedstrijdnr']))
		{
			$value['Wedstrijdnr'] = '';
		}
		if(!isset($value['Scheidsrechter']))
		{
			$value['Scheidsrechter'] = '';
		}else{
			$value['Scheidsrechter'] = htmlentities($value['Scheidsrechter'],ENT_QUOTES);
		}*/
		
		
		$teamid = mysqli_real_escape_string($con, trim($link["TeamID"]));
		$datum = mysqli_real_escape_string($con, convertStringToDate(trim($value["Datum"])));
		$tijd = mysqli_real_escape_string($con, trim($value["Tijd"]));
		$wedstrijd = mysqli_real_escape_string($con, trim($value["Wedstrijd"]));
		$type = mysqli_real_escape_string($con, trim($value["Type"]));
		$accommodatie = mysqli_real_escape_string($con, trim($value["Sportpark"]));
		$wedstrijdnr = mysqli_real_escape_string($con, trim($value["Wedstrijdnr"]));
		$scheidsrechter = mysqli_real_escape_string($con, trim($value["Scheidsrechter"]));
		$status = mysqli_real_escape_string($con, trim($value["Status"]));
		$sportpark = mysqli_real_escape_string($con, trim($value["Sportpark"]));
		$adres = mysqli_real_escape_string($con, trim($value["Adres"]));
		$postcode = mysqli_real_escape_string($con, trim($value["Postcode"]));
		$plaats = mysqli_real_escape_string($con, trim($value["Plaats"]));
		$telefoon = mysqli_real_escape_string($con, trim($value["Telefoon"]));
		$pagina = mysqli_real_escape_string($con, trim($value["Page"]));
		
		$sql = "INSERT INTO ${dbprefix}programma
				(TeamID, Datum, Tijd, Wedstrijd, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Sportpark, Adres, Postcode, Plaats, Telefoon, Pagina) VALUES 
				('$teamid', '$datum', '$tijd', '$wedstrijd', '$type', '$accommodatie', '$wedstrijdnr', '$scheidsrechter', '$status', '$sportpark', '$adres', '$postcode', '$plaats', '$telefoon', '$pagina')";
		
		mysqli_query($con,$sql) or die(mysqli_error($con));
		
		
		/*$sql = "INSERT INTO ".$dbprefix."programma
				(TeamID, Datum, Tijd, Wedstrijd, Type, Accommodatie, Wedstrijdnr, Scheidsrechter, Status, Sportpark, Adres, Postcode, Plaats, Telefoon) VALUES
				('".$link["TeamID"]."', '".($value['Datum'])."', '".$value['Tijd']."', '".$value['Wedstrijd']."', '".$value['Type']."', '".$value['Accommodatie']."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."', '".$value['Status']."', '".$value['Sportpark']."', '".$value['Adres']."', '".$value['Postcode']."', '".$value['Plaats']."', '".$value['Telefoon']."'),";*/
		

		//print($sql);

	}
	/*if ($UserDebug == 'Aan') {
		mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$sql) or die(mysqli_error($con));
	}*/

	// Verwijderen van lege entries
	$sql = "DELETE FROM `".$dbprefix."programma` WHERE Datum = '0000-00-00'";
	if ($UserDebug == 'Aan') {
		mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_query($con,$sql) or die(mysqli_error($con));
	}
	// Wijzigen van bepaalde tekens in Accomodatie. Dit wordt veroorzaakt in de html code van voetbal.nl
	// Indien nodig de // van de volgende 3 regels verwijderen en de codes veranderen. de eerste is wat vervangen moet worden door de tweede.
	// $sql = "UPDATE ".$dbprefix."programma SET Accommodatie = REPLACE(Accommodatie, '&Atilde;&cent;','&acirc;')";
	//  mysqli_query($con,$sql)
	//  or die(mysqli_error($con));


}
// Einde functie programma met details opslaan -------------------------------------



//Ophalen Handmatig ingevoerde club uitslagen --------
function clubhanduitslagen($saveteamuitslag,$teamid,$table)
{
	include("config.php");
	//mysql_connect($server,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database");

	$query1 = "SELECT DISTINCT Datum, Thuis, Uit, Uitslag
FROM $table WHERE Uitslag LIKE '%*' ";

	$huitslag=mysqli_query($con,$query1);
	$huitslagnum=mysqli_num_rows($huitslag);

	return array ($huitslag,$huitslagnum);
}
// -------------------------------------

//ClubUitslagen opslaan in database --------
function opslaan_clubuitslagen($clubuitslagen)
{
	include("config.php");

	$query = "
	INSERT INTO ".$dbprefix."clubuitslagen
	(
	Datum,
	Thuis,
	Uit,
	Uitslag
	)
	VALUES
";
	$t = 0;
	$CountData = count($clubuitslagen);
	foreach($clubuitslagen AS $value)
	{		
		$datum = mysqli_real_escape_string($con, convertStringToDate(trim($value["Datum"])));
		$uitslag = mysqli_real_escape_string($con, trim($value["Uitslag"]));
		$thuis = mysqli_real_escape_string($con, trim($value["Thuis"]));
		$uit = mysqli_real_escape_string($con, trim($value["Uit"]));
		$query = "INSERT INTO ".$dbprefix."clubuitslagen (Datum, Thuis, Uit, Uitslag) VALUES 
		('$datum', '$thuis', '$uit', '$uitslag')";
		if ($UserDebug == 'Aan') {
			mysqli_query($con,$query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
		}
		else
		{
			mysqli_query($con,$query) or die(mysqli_error($con));
		}

	}
	

	// verwijderen van bepaalde wedstrijden volgens instellingen.php

	if ($DelwdstrdCPCU == 'Aan') {
		$DelwdstrdCPCUtxt = "%".$DelwdstrdCPCUtxt."%";
		$sql = "DELETE FROM `".$dbprefix."clubuitslagen` WHERE Thuis Like '$DelwdstrdCPCUtxt' OR Uit Like '$DelwdstrdCPCUtxt'";
		if ($UserDebug == 'Aan') {
			mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
		}
		else
		{
			mysqli_query($con,$sql) or die(mysqli_error($con));
		}
	}


	// Verwijderen van lege entries
	//  $sql = "DELETE FROM `".$dbprefix."uitslag` WHERE Datum = '0000-00-00'";
	//  mysqli_query($con,$sql)
	//  or die(mysqli_error($con));
}

//Alle opgehaalde standen opslaan. Dit kan een korte periode duren.
function opslaan_stand($standen,$link,$tabel)
{
	include("config.php");
	
	$query = "
	DELETE FROM $tabel WHERE Elftal = '';
	INSERT INTO $tabel
	(
		TeamID,
		Elftal,
		Plaats,
		G,
		W,
		GW,
		V,
		P,
		DPV,
		DPT,
		PM
	)
	VALUES
";
	$sql;
	foreach($standen AS $value)
	{
		$value['Elftal'] = mysqli_real_escape_string($con, $value['Elftal']);

		if ($value['PM'] == '0'){
			$PM = '-';
		}else{
			$PM = $value['PM'];
		}
		$sql .= "$query ('${link['TeamID']}',
			'${value['Elftal']}',
			'${value['#']}',
			'${value['G']}',
			'${value['W']}',
			'${value['GW']}',
			'${value['V']}',
			'${value['P']}',
			'${value['DPV']}',
			'${value['DPT']}',
			'$PM');";
	}
	
	if ($UserDebug == 'Aan') {
		mysqli_multi_query($con,$query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
	}
	else
	{
		mysqli_multi_query($con, $sql) or die(mysqli_error($con));
	}
}
// -------------------------------------


function get_teams($teamsurl)
{

	$html = fetch_url($teamsurl);


	//Tegenwoordig bevat de pagina meerdere tabellen met uitslagen. Neem de alles na de eerste
	//$htmlstripped = get_string_between($html,"id=\"ResultsContent3","");
	//  $htmlstripped = get_string_between($html,'<div name="ResultsContent" id="ResultsContent2" style="display: none;">',"");

	// Er zijn wel gegevens gevonden dus gaan we door //$regexp omschrijft nu hoe de tabel eruit moet zien
	//$regexp ='/clubs-competities.mijn.poules.uitslagen.(.*?)".title="Selecteer poule">(.*?).nbsp.(.*?)<.a>/';
	$teamregexp = '/(?:<h4>(.*)<.h4>[\s\S]+?)?<a href=".+(T[0-9]+).+" class="team-follow-link?[\s\S]+?team-follow-name">(.*)<.p>/';
	preg_match_all($teamregexp, $html, $matches, PREG_SET_ORDER);
	
	

	$match = array();
	$i = 0;
	$teamnaam;
	foreach($matches AS $value)
	{
		if(!empty($value[1]))
		{
			$teamnaam = $value[1];
		}
		else
		{
			$value[1] = $teamnaam;
		}

		$match[$i]['Naam']		= $value[3];
		//$match[$i]['Klasse']   	= $value[2];
		$match[$i]['TeamCode']	= $value[2];
		$i++;
	}
	//Elke match teruggeven
	//  $match = removeDuplicateMatches($match);
	return $match;
}

function get_teamsdetails($teams)
{
	$teamregexp = '/<h4>(.*)<.h4>[\s\S]+?<a href="(.*)" class?/';

	//$CountData = count($teamgegevens);
	
	$i = 0;
	foreach($teams as $teamvalue)
	{
		$teampagina = "https://www.voetbal.nl/team/${teamvalue['TeamCode']}/overzicht";
		$html = fetch_url($teampagina);
	
		$teamregexp = '/class="dashboard-header-maintitle">(.*)<.h1>[\s\S]+?"dashboard-header-subtitle">(.*) - (.*)<.h2>/';
		preg_match_all($teamregexp, $html, $match, PREG_SET_ORDER);
		
		$teams[$i]['Naam']		= $match[0][1]. " / " .$match[0][2];
		$teams[$i]['Klasse']   	= $match[0][3];
		$teams[$i]['Teamcode']	= $teamvalue['TeamCode'];
		$teams[$i]['Wedstrijdduur'] = "0";
		$teams[$i]['Periode'] = "nee";
		$teams[$i]['Ophalen'] = "ja";
		$teams[$i]['GroupID'] = "1";
		$teams[$i]['DatumTijd-Update'] = "1";
		$i++;
	}
	

	//$teams = removeDuplicateMatches($match);
	return $teams;
}


function opslaan_teams($teams)
{
	include("config.php");
	//mysql_connect($server,$username,$password);
	//@mysql_select_db($database) or die( "Unable to select database to store Uitslagen");
	$query = "
	INSERT IGNORE INTO ${dbprefix}teamlinks
	(
	`Teamcode`,
	`Naam`,
	`Klasse`,
	`Wedstrijdduur`,
	`Periode`,
	`Ophalen`,
	`GroupID`,
	`DatumTijd-Update`
	)
	VALUES
";
	$i = 0;
	$CountData = count($teams);

	foreach($teams AS $value)
	{
		/*$value["Naam"] = htmlentities($value["Naam"], ENT_QUOTES);
		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$value["Teamcode"]."', '".$value["Naam"]."', '".$value['Klasse']."', '".$value['Wedstrijdduur']."', '".$value['Periode']."', '".$value['Ophalen']."', '".$value['GroupID']."', '".$value['DatumTijd-Update']."'),";
		}
		else
		{
			$query.="('".$value["Teamcode"]."', '".$value["Naam"]."', '".$value['Klasse']."', '".$value['Wedstrijdduur']."', '".$value['Periode']."', '".$value['Ophalen']."', '".$value['GroupID']."', '".$value['DatumTijd-Update']."')";
		}*/
		
		if(!empty($value['Naam']) && !empty($value['Klasse'])){
		
			$value["Naam"] = mysqli_real_escape_string($con, $value["Naam"]);
			$sql = "$query ('${value['TeamCode']}',
			'${value['Naam']}',
			'${value['Klasse']}',
			'${value['Wedstrijdduur']}',
			'${value['Periode']}',
			'${value['Ophalen']}',
			'${value['GroupID']}',
			'${value['DatumTijd-Update']}')";
				if ($UserDebug == 'Aan') {
					mysqli_query($con,$sql) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysqli_errno($con) . ") " . mysqli_error($con));
				}
				else
				{
					mysqli_query($con,$sql) or die("A MySQL error has occurred.");
				}
			$i++;
		}
	}


}


function convertStringToDate($string)
{
	setlocale(LC_TIME, 'NL_nl');
	$monthreplacement = array(
		'januari'=>'jan',
		'februari'=>'feb',
		'maart'=>'mar',
		'april'=>'apr',
		'mei'=>'may',
		'juni'=>'jun',
		'juli'=>'jul',
		'augustus'=>'aug',
		'september'=>'sep',
		'oktober'=>'oct',
		'november'=>'nov',
		'december'=>'dec'
	);
	$dayreplacement = array(
		'maandag'=>'mon',
		'dinsdag'=>'tue',
		'woensdag'=>'wed',
		'donderdag'=>'thu',
		'vrijdag'=>'fri',
		'zaterdag'=>'sat',
		'zondag'=>'sun'
	);
	
	$string = str_ireplace(array_keys($monthreplacement), $monthreplacement, $string);
	$string = str_ireplace(array_keys($dayreplacement), $dayreplacement, $string);
	/*$string = str_ireplace('okt','oct',$string);
	$string = str_ireplace('maa','mar',$string);
	$string = str_ireplace('mrt','mar',$string);
	$string = str_ireplace('mei','may',$string);*/

	//$datum  = date('Y-m-d',strtotime($string));
	//$datum = date('D d M Y',strtotime($string));
	$datum = date('Y-m-d', strtotime($string));
	$jul = date('Y-m-d',strtotime('01 jul'));

	//Is het vandaag voor of na 1 juli
	if (dateDiff(Date("Y-m-d"),$jul) < 0)
	{
		// Vandaag is na 1 juli: Ligt de gevraagde datum ervoor, dan 1 jaar erbij.
		If (dateDiff($jul,$datum) < 0)
		{
			//Tel er een jaar bij op
			$datum = date('Y-m-d',strtotime('+1 year',strtotime($datum)));
		}
	} else {
		// Vandaag is voor 1 juli: Ligt de gevraagde datum erna, dan 1 jaar eraf.
		If (dateDiff($jul,$datum) >= 0)
		{
			//Tel er een jaar bij op
			$datum = date('Y-m-d',strtotime('-1 year',strtotime($datum)));
		}
	}
	return $datum;
}

function dateDiff($startDate, $endDate)
{
	// Parse dates for conversion
	$startArry = date_parse($startDate);
	$endArry = date_parse($endDate);

	// Convert dates to Julian Days
	$start_date = gregoriantojd($startArry["month"],$startArry["day"], $startArry["year"]);
	$end_date = gregoriantojd($endArry["month"], $endArry["day"], $endArry["year"]);

	// Return difference
	return round(($end_date - $start_date), 0);
}


function removeDuplicateMatches($matches)
{
	$processed_matches = array();
	$result = array();
	foreach($matches as $match)
	{
		if(!in_array($match["Thuis"].'_'.$match["Uit"],$processed_matches))
		{
			$result[] = $match;
			$processed_matches[] = $match["Thuis"].'_'.$match["Uit"];
		}
	}
	return $result;
}

//TODO: Query schoonmaken
function ontdubbeluitslagen($afgelastteamuitslag,$teamid,$table)
{

	include("config.php");

	$query1 = "SELECT DISTINCT TeamID, Datum, Thuis, Uit, Uitslag
FROM $table WHERE TeamID=$teamid AND Uitslag LIKE '%afgelast%' ";

	$auitslag=mysqli_query($con,$query1);
	$auitslagnum=mysqli_num_rows($auitslag);

	return array ($auitslag,$auitslagnum);
}


function get_string_between($string, $start, $end)
{
	$string = " ".$string;
	$ini = strpos($string,$start);
	//Substring is niet gevonden
	if ($ini == 0) return "";
	//Bepaal de nieuwe startpositie voor het zoeken naar deel twee
	$ini += strlen($start);
	//Indien er geen $end is opgegeven geef de complete string vanaf $start terug
	if ($end == "") return substr($string,$ini);
	//Zoek deel twee en bepaal de lengte van het terug te geven deel.
	$len = strpos($string,$end,$ini) - $ini;
	//Geef de string tussen $start en $end terug.
	return substr($string,$ini,$len);
}


//Database leegmaken -------------------
function leegmakendbcp($table)
{
	include("config.php");

	mysqli_query($con,"TRUNCATE TABLE $table")
	or die(mysqli_error($con));
}
// -------------------------------------


//Database leegmaken -------------------
function leegmakendb($table,$teamid)
{
	include("config.php");
	if(!empty($teamid)) {
		mysqli_query($con,"DELETE FROM $table WHERE TeamID=$teamid")
		or die(mysqli_error($con));
		mysqli_query($con,"ALTER TABLE $table AUTO_INCREMENT =1")
		or die(mysqli_error($con));
	}
	else
	{
		mysqli_query($con,"TRUNCATE $table")
		or die(mysqli_error($con));
	}
}

function reorderdb($table,$tableID)
{
	include("config.php");


	mysqli_query($con,"ALTER TABLE $table DROP $tableID")
	or die(mysqli_error($con));
	mysqli_query($con,"OPTIMIZE TABLE $table")
	or die(mysqli_error($con));
	mysqli_query($con,"ALTER TABLE $table ADD $tableID INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST")
	or die(mysqli_error($con));
}

function makeNiceDate($date)
{
	if(!empty($date))
	{
		$epoch = mktime(0,0,0,substr($date,5,2),substr($date,8,2),substr($date,0,4));
		/* Set locale to Dutch */
		if(getOS() == 'linux')
		{
			setlocale(LC_ALL, array('nl_NL'));
		}else{
			setlocale(LC_ALL, array('nld_nld'));
		}
		$date=strftime('%d %b',$epoch);
	}
	return $date;
}

function getOS()
{
	if(isset($_SERVER['SERVER_SOFTWARE']))
	{
		if(strpos($_SERVER['SERVER_SOFTWARE'],'Win32') === false)
		{
			return 'linux';
		}else{
			return 'windows';
		}
	}else{
		return 'linux';
	}
}


function dp($input)
{
	if(is_array($input) || is_object($input))
	{
		echo 'Begin array: '.'<br />';
		echo '<pre>';
		print_r($input);
		echo '<br />'.'</pre>';
		echo '<br />';
		echo 'End array<br />';
	}else{
		echo 'Begin string: '.'<br />';
		echo '<pre>';
		echo '|||'.htmlentities($input).'|||';
		echo '<br />'.'</pre>'.'END string'.'<br />';
	}
}

// -------------------------------------

//Algemene functie om de resultaten van een url op te halen en deze als resultaat terug te geven.
function fetch_url_ajax($url)
{
	global $ch, $site, $cookies, $usesocket, $useproxy, $proxyhost, $proxyport;

	//curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
		//curl_setopt($ch,CURLOPT_HTTPHEADER,array('Expect:'));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_COOKIE,  $cookies);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$html = curl_exec($ch);

	$arr = json_decode($html,true);	
	// echo $arr["html"];
	return $arr["html"];
}
// -------------------------------------


			

?>
