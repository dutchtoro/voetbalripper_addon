<?php
// functies.php 1.9.7.9.2 (23-10-2014) Aanpassing ivm wijziging inloggen voetbal.nl. 
// Voetbal.nl Ripper 1.9.7 door Johnvs
// Datum: 23-10-12
// Vorige Datum: 22-05-12  1.9.6
// Dit script is gebaseerd op KNVB Ripper 1.0 door Redroest op wmcity.nl
// KNVB Ripper 1.0 was al aangepast door Yarro en johnvs om het werkend te krijgen na wijzigingen op KNVB.nl
// Delen van Voetbal.nl Ripper zijn afkomstig uit de KNVB Ripper versie en/of kunnen afkomstig zijn van
// Redroest, Yarro, patron2, FreddyHell, Killerbee, Pietjebel


If ($Curl == "Uit") {
	include("class.socket.php"); // Als Curl niet aan staat wordt de socket class gebruikt
	$usesocket = 'true';
}

//Functie om een substring op te halen
function get_string_between($string, $start, $end)
{
	$string = " ".$string;
	$ini = strpos($string,$start);
	//Substring is niet gevonden
	if ($ini == 0) return "";
	//Bepaal de nieuwe startpositie voor het zoeken naar deel twee
	$ini += strlen($start);
	//Indien er geen $end is opgegeven geef de complete string vanaf $start terug
	if ($end == "") return substr($string,$ini);
	//Zoek deel twee en bepaal de lengte van het terug te geven deel.
	$len = strpos($string,$end,$ini) - $ini;
	//Geef de string tussen $start en $end terug.
	return substr($string,$ini,$len);
}

//Functie om een substring op te halen
function get_string_between_number($string, $start, $end, $which)
{
	$string = " ".$string;
	$ini = 0;
	for ($x = 1; $x <= $which; $x++) {
		//echo "The number is: $x <br>";
		$ini = strpos($string,$start,$ini);
		$ini += strlen($start);
	} 	
	
	//Substring is niet gevonden
	if ($ini == 0) return "";
	//Bepaal de nieuwe startpositie voor het zoeken naar deel twee
	$ini += strlen($start);
	//Indien er geen $end is opgegeven geef de complete string vanaf $start terug
	if ($end == "") return substr($string,$ini);
	//Zoek deel twee en bepaal de lengte van het terug te geven deel.
	$len = strpos($string,$end,$ini) - $ini;
	//Geef de string tussen $start en $end terug.
	return substr($string,$ini,$len);
}

//Functie om twee datas van elkaar af te trekken en het verschil in dagen terug te geven
function dateDiff($startDate, $endDate)
{
	// Parse dates for conversion
	$startArry = date_parse($startDate);
	$endArry = date_parse($endDate);

	// Convert dates to Julian Days
	$start_date = gregoriantojd($startArry["month"],$startArry["day"], $startArry["year"]);
	$end_date = gregoriantojd($endArry["month"], $endArry["day"], $endArry["year"]);

	// Return difference
	return round(($end_date - $start_date), 0);
}

function makeNiceDate($date)
{
	if(!empty($date))
	{
		$epoch = mktime(0,0,0,substr($date,5,2),substr($date,8,2),substr($date,0,4));
		/* Set locale to Dutch */
		if(getOS() == 'linux')
		{
			setlocale(LC_ALL, array('nl_NL'));
		}else{
			setlocale(LC_ALL, array('nld_nld'));
		}
		$date=strftime('%d %b',$epoch);
	}
	return $date;
}

function getOS()
{
	if(isset($_SERVER['SERVER_SOFTWARE']))
	{
		if(strpos($_SERVER['SERVER_SOFTWARE'],'Win32') === false)
		{
			return 'linux';
		}else{
			return 'windows';
		}
	}else{
		return 'linux';
	}
}

//Debug print function
function dp($input)
{
	if(is_array($input) || is_object($input))
	{
		echo 'Begin array: '.'<br />';
		echo '<pre>';
		print_r($input);
		echo '<br />'.'</pre>';
		echo '<br />';
		echo 'End array<br />';
	}else{
		echo 'Begin string: '.'<br />';
		echo '<pre>';
		echo '|||'.htmlentities($input).'|||';
		echo '<br />'.'</pre>'.'END string'.'<br />';
	}
}
function removeDuplicateMatches($matches)
{
	$processed_matches = array();
	$result = array();
	foreach($matches as $match)
	{
		if(!in_array($match["Thuis"].'_'.$match["Uit"],$processed_matches))
		{
			$result[] = $match;
			$processed_matches[] = $match["Thuis"].'_'.$match["Uit"];
		}
	}
	return $result;
}

function convertStringToDate($string)
{
    $string = str_ireplace('maandag ','',$string);
    $string = str_ireplace('dinsdag ','',$string);
    $string = str_ireplace('woensdag ','',$string);
    $string = str_ireplace('donderdag ','',$string);
    $string = str_ireplace('vrijdag ','',$string);
    $string = str_ireplace('zaterdag ','',$string);
    $string = str_ireplace('zondag ','',$string);
	$string = str_ireplace('januari','jan',$string);
	$string = str_ireplace('februari','feb',$string);
	$string = str_ireplace('maart','mar',$string);
    $string = str_ireplace('april','apr',$string);
	$string = str_ireplace('mei','may',$string);
    $string = str_ireplace('juni','jun',$string);
    $string = str_ireplace('juli','jul',$string);
    $string = str_ireplace('augustus','aug',$string);
    $string = str_ireplace('september','sep',$string);
    $string = str_ireplace('oktober','oct',$string);
    $string = str_ireplace('november','nov',$string);
    $string = str_ireplace('december','dec',$string);

	$datum  = date('Y-m-d',strtotime($string));
	$jul = date('Y-m-d',strtotime('01 jul'));

	//Is het vandaag voor of na 1 juli
	if (datediff(Date("Y-m-d"),$jul) < 0)
	{
		// Vandaag is na 1 juli: Ligt de gevraagde datum ervoor, dan 1 jaar erbij.
		If (dateDiff($jul,$datum) < 0)
		{
			//Tel er een jaar bij op
			$datum = date('Y-m-d',strtotime('+1 year',strtotime($datum)));
		}
	} else {
		// Vandaag is voor 1 juli: Ligt de gevraagde datum erna, dan 1 jaar eraf.
		If (dateDiff($jul,$datum) >= 0)
		{
			//Tel er een jaar bij op
			$datum = date('Y-m-d',strtotime('-1 year',strtotime($datum)));
		}
	}
	return $datum;
}



//Algemene functie om de resultaten van een url op te halen en deze als resultaat terug te geven.
function fetch_url($url)
{
	global $ch, $site, $cookies, $usesocket, $useproxy, $proxyhost, $proxyport;

	If ($usesocket == 'true') {
		//Haal de url op middels de socket class van Lucas
		$get = new HttpGetRequest();
		if ($useproxy == 'true') {
			$get -> set_ip($site, $proxyport);
		} else {
			$get -> set_ip($site, 80);
		}
		$get -> set_file($url);

		foreach($cookies as $name => $value) {
			$get -> set_cookie($name, $value);
		}
		if ($get -> request()) {
			$html = $get -> get_contents();
		}
	} else {
		//Haal de url op middels CURL

		curl_setopt($ch,CURLOPT_HTTPHEADER,array('Expect:'));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_COOKIE,  $cookies);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		if ($useproxy == 'true') {
			curl_setopt($ch, CURLOPT_PROXY, $proxyhost);
			curl_setopt($ch, CURLOPT_PROXYPORT, $proxyport);
		}
		$html = curl_exec($ch);
	}

	return $html;
}
// -------------------------------------

//Algemene functie om de resultaten van een url op te halen en deze als resultaat terug te geven.
function fetch_url_ajax($url)
{
	global $ch, $site, $cookies, $usesocket, $useproxy, $proxyhost, $proxyport;

	//curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
		//curl_setopt($ch,CURLOPT_HTTPHEADER,array('Expect:'));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_COOKIE,  $cookies);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$html = curl_exec($ch);

	$arr = json_decode($html,true);	
	// echo $arr["html"];
	return $arr["html"];
}
// -------------------------------------


//Inloggen -----------------------------
function login_voetbalnl($Gebruikersnaam, $Wachtwoord)
{
	global $ch, $cookies, $usesocket, $site, $Phpsafemode, $useproxy, $proxyhost, $proxyport;

	If ($usesocket == 'true') {
		//Inloggen op voetbal.nl middels de socket class van Lucas
		$url  = "http://pupillen.voetbal.nl/home?destination=node%2F23942";
		if ($useproxy == 'true') {
			$site  = $proxyhost;
			$port  = $proxyport;
		} else {
			$site = "pupillen.voetbal.nl";
			$port = 80;
		}
		$post = new HttpPostRequest();
		$post -> set_ip($site, $port);
		$post -> set_file($url);
		$post -> include_headers();
		$post -> use_redirect(false);
		$post -> add_var("name", $Gebruikersnaam);    // Je email
		$post -> add_var("pass", $Wachtwoord);        // je wachtwoord
		$post -> add_var("op", "Inloggen");
		$post -> add_var("form_id", "login_block_v3_login_form");
		$connected = $post -> request();
		$cookies = $post -> get_cookies();
		$result = $connected;
    } else {
        // echo "stap 8 1 <br />";
        // $site = 'http://pupillen.voetbal.nl';
        $site = 'https://www.voetbal.nl/inloggen';
//		$url = $site."/clubs_comp/mijn-teams/competitie/"; // simple scherm om in te loggen
		$url = $site; // Hoofd scherm om in te loggen
		$ch = curl_init();

		$httpagent = $_SERVER["HTTP_USER_AGENT"]; 
		curl_setopt($ch, CURLOPT_USERAGENT, $httpagent);		
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('Expect:'));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 1);
	//	if ($Phpsafemode == "Uit") curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);

           // echo "stap 8 2 <br />";

		if ($useproxy == 'true') {
            // echo "stap 8 3 <br />";

            curl_setopt($ch, CURLOPT_PROXY, $proxyhost);
			curl_setopt($ch, CURLOPT_PROXYPORT, $proxyport);
		}

        $logindata = file_get_contents($site); //hierin staat de form_build_id die bij iedere sessie anders is
             //echo "logindata: $logindata <br />";
        $formbuilddata = explode('name="form_build_id" value="',$logindata,3);
            // echo "stap 8 4 <br />";
            // echo "login: ".substr($formbuilddata[2],0,48) ."<br />";

		//g-recaptcha-response  03AJIzXZ529rUTYBtU8r1eYIf6fCrBplCDQ3ot3hU_08QWglZ_SEeUfcmQWTFgpU7mQr-1ohWj_H8DYvX2251PIbfuQ_kXHw8YKoVNLyciLuIV70bpf4-qMdT3QjHOCuJrQEeoTxfRzAPjS_8ooftt-6Ng4WLkXmXDrtTbCvARrN4x3bHnXI9PUOKzER7rpnyvFUJOjbfOH4weritFewtrnLqkIHKal6TR7-96p89Jfy1rPDYOg8TaUDyn01EMUkrntOun73Vgb50DzHMxpg405_tXLbYo6wtXtJWs97ej4AuxuhE2fPlAsXa_pAKbIcFNye7uDzfS4QL6BsGZTRYKl3Hw8O0KQxsfmw
		//token
		// name is je gebruikersnaam
		// password is je wachtwoord
		$postdata=array(
			"email" => $Gebruikersnaam,
			"password" => $Wachtwoord,
			"op" => "inloggen",
                        "form_build_id" => substr($formbuilddata[2],0,48),
			"form_id" => "voetbal_login_login_form"
		);

		curl_setopt ($ch, CURLOPT_POSTFIELDS, $postdata);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17');
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_ENCODING,       'gzip,deflate'  );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		//error_reporting(E_ALL);
		if ($Phpsafemode == "Uit") {
            // echo "stap 8 5 <br />";

            // echo "stap 8 safemode uit <br />";
            $result = curl_exec($ch);
			}
		else
		{
            // echo "stap 8 6 <br />";
            $result = curl_exec_follow($ch);
		}

		preg_match_all('|Set-Cookie: (.*);|U', $result, $results);
		$cookies = implode(';', $results[1]);
	}
	// var_dump ($results);
	 //echo "cookies zijn: $cookies <br />";
	 //echo "result is: $result <br />";
    // die;
	return $result;
}
// -------------------------------------

// cURL follow location functie wanneer safe_mode aan staat en/of open_savedir gezet is ----------------------

function curl_exec_follow(/*resource*/ $ch, /*int*/ &$maxredirect = null) {
    echo "stap 8 safemode aan <br />";
    $mr = $maxredirect === null ? 5 : intval($maxredirect);
 

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false); 
        if ($mr > 0) { 
            $newurl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL); 
            $rch = curl_copy_handle($ch);

            curl_setopt($rch, CURLOPT_HEADER, true); 
            curl_setopt($rch, CURLOPT_NOBODY, true); 
            curl_setopt($rch, CURLOPT_FORBID_REUSE, false); 
            curl_setopt($rch, CURLOPT_RETURNTRANSFER, true); 
            do { 
                curl_setopt($rch, CURLOPT_URL, $newurl); 
                $header = curl_exec($rch); 
                if (curl_errno($rch)) { 
                    $code = 0; 
                } else { 
                    $code = curl_getinfo($rch, CURLINFO_HTTP_CODE); 
                    if ($code == 301 || $code == 302) { 
                        preg_match('/Location:(.*?)\n/', $header, $matches); 
                        $newurl = trim(array_pop($matches)); 
                    } else { 
                        $code = 0; 
                    } 
                } 
            } while ($code && --$mr); 
            //curl_close($rch); 
            if (!$mr) { 
                if ($maxredirect === null) { 
                    trigger_error('Too many redirects. When following redirects, libcurl hit the maximum amount.', E_USER_WARNING);
                 } else { 
                    $maxredirect = 0; 
                } 
                return false; 
            } 
            curl_setopt($ch, CURLOPT_URL, $newurl); 
        } 
    return curl_exec($ch); 
} 

// -------------------------------------


//Inlog check ----------------------
function check_inlog($url)
{
	$html = fetch_url($url);

	//Eerste kijken of er wel informatie is als er alleen een . staat is het account geblokkeerd. De "lege regel punt" opde volgende regel is belangrijk.
	$zoekstring = "\n.";
	if(strstr($html,$zoekstring)) return $zoekstring;

	// Kijken of er niet staat - Log in of registreer gratis! - anders is inloggen mislukt
	$zoekstring = "Log in of registreer gratis!";
	if(strstr($html,$zoekstring)) return $zoekstring;

	//$regexp omschrijft nu hoe de pagina eruit moet zien
	$regexp ='/<div><h1>(.*?)<.h1><.div>\s+/';

	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Inlog'] = $value[1];
		$i++;
	}
	if(isset($value))
	{
		//Waarde terug geven
		return $value[1];
	}else{
		return NULL;
	}
}
// -------------------------------------


//Database leegmaken -------------------
function leegmakendbcp($table)
{
	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to clear table");
	mysql_query("TRUNCATE TABLE $table")
	or die(mysql_error());
}
// -------------------------------------


//Database leegmaken -------------------
function leegmakendb($table,$teamid)
{
	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to clear table for teamid");
	mysql_query("DELETE FROM $table WHERE TeamID=$teamid")
	or die(mysql_error());
	mysql_query("ALTER TABLE $table AUTO_INCREMENT =1")
	or die(mysql_error());
}
// -------------------------------------

//Haal de teamgegevens op en sla deze op.
function get_teamdata($baseurl)
{
	$html = fetch_url($baseurl);
	//$html = file_get_contents($baseurl); 
	return $html;
}
// -------------------------------------

//Haal de teamgegevens op en sla deze op.
function get_teamdata_ajax($baseurl)
{
	$html = fetch_url_ajax($baseurl);
	//$html = file_get_contents($baseurl); 
	return $html;
}
// -------------------------------------


// Stand ophalen -----------------------
function get_stand($html, $what)
{
	//Omdat de normale en de periode standen achter elkaar staan in $html kopieren we alleen het juiste deel in $htmlstripped

	//Stand periode 1 ophalen
	if ($what == "p1")
	{
		$htmlstripped = get_string_between($html,"id=\"RankingsContent2","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
	}

	//Stand periode 2 ophalen
	if ($what == "p2")
	{
		$htmlstripped = get_string_between($html,"id=\"RankingsContent3","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
		if(empty($htmlstripped))
		{
			// $htmlstripped = get_string_between($html,"id=\"RankingsContent3","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
		}
	}

	//Stand periode 3 ophalen, als deze er is. Anders nogmaals periode 2 ophalen, omdat er maar 2 zijn.
	if ($what == "p3")
	{
		$htmlstripped = get_string_between($html,"id=\"RankingsContent4","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
		if(empty($htmlstripped))
		{
			// $htmlstripped = get_string_between($html,"id=\"RankingsContent3","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
		}
	}

	//Normale stand ophalen
	if ($what == "normaal")
	{
		//Controleer of het team uberhaupt periode standen kent.
		// if (strpos($html,"id=\"RankingsContent2") <> 0)
		// {
		//	//Team kent periode standen
		//	$htmlstripped = get_string_between($html,"id=\"RankingsContent1","<strong>G</strong> = gespeeld | <strong>W</strong> = gewonnen");
		//} else {
		//	//Team kent geen periode standen
		//	$htmlstripped = get_string_between($html,"id=\"RankingsContent1","");
		//}
		$htmlstripped = get_string_between($html,"<div class=\"table-wrapper table-standingstable\" data-printable-target","<div class=\"table-captions-wrapper\">");
	}
	// $regexp = '/<div class="nr">(.*?)<.div>\s+<div class="team.*?">(.*?)<.div>\s+<div class="g"><strong>(.*?)<.strong><.div>\s+<div class="w">(.*?)<.div>\s+<div class="gl">(.*?)<.div>\s+<div class="v">(.*?)<.div>\s+<div class="p"><strong>(.*?)<.strong><.div>\s+<div class="dvp">(.*?)<.div>\s+<div class="dpt">(.*?)<.div>\s+<div class="pm">(.*?)<.div>\s+<.div>\s+/';
	$regexp = '/<div class="value position">\s{2,}<span>(.*?)<.span>\s+<.div>.*\s.*\s.*<div class="value team">\s{2,}<span>(.*?)<.span>\s+<.div><div class="value">\s{2,}<span>(.*?)<.span>\s+<.div><div class="value">\s{2,}<span>(.*?)<.span>\s+<.div><div class="value">\s{2,}<span>(.*?)<.span>\s+<.div><div class="value">\s{2,}<span>(.*?)<.span>\s+<.div><div class="value points">\s{2,}<span>(.*?)<.span>\s+<.div><div class="value ">\s{2,}<span>(.*?)<.span>\s+<.div><div class="value">\s{2,}<span>(.*?)<.span>\s+<.div><div class="value">\s{2,}<span>(.*?)<.span>\s+/';

	//Waardes ophalen en opslaan in $matches
	preg_match_all($regexp, $htmlstripped, $matches, PREG_SET_ORDER);
	
	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		if(!empty($value[1]) && !empty($value[2]))
		{
			$match[$i]['#'] = $value[1];
			$match[$i]['Elftal'] = $value[2];
			$match[$i]['G'] = $value[3];
			$match[$i]['W'] = $value[4];
			$match[$i]['GW'] = $value[5];
			$match[$i]['V'] = $value[6];
			$match[$i]['P'] = $value[7];
			$match[$i]['DPV'] = $value[8];
			$match[$i]['DPT'] = $value[9];
			$match[$i]['PM'] = $value[10];
			$i++;
		}
	}
	//Elke match teruggeven
	return $match;
}
// -------------------------------------


//Uitslag ophalen ----------------------
function get_uitslag($html)
{
	// echo 'get uitslag';
	//Tegenwoordig bevat de pagina meerdere tabellen met uitslagen. Neem de eerste. Aangenomen dat dit de laatste uitslagen zijn
	//$htmlstripped = get_string_between($html,"id=\"showResults","id=\"showRankings");
	$htmlstripped = $html;

	//echo $htmlstripped;
	// eerste kijken of er wel informatie is
	//$zoekstring = "Er zijn geen uitslagen";

	// Indien er geen gegevens zijn gevonden stoppen
	//if(strstr($htmlstripped,$zoekstring)) return $zoekstring;


	//Er zijn wel gegevens gevonden dus gaan we door
	//$regexp omschrijft nu hoe de tabel eruit moet zien
	//Houd ook rekening met "higlight" voor als het een "mijn elftal" elftal is.                             //  ? weggehaald tussen *>
	// $regexp ='/<div.class="datum-tijd">(.*?)<.div>\s+<div.class="wedstrijd"><div.class=".*?">(.*?)<.div><div.*>-<.div><div.class=".*?">(.*?)<.div><.div>\s+<div.class="uitslag">(.*?)<.div>/';
	//preg_match_all($regexp, $htmlstripped, $matches, PREG_SET_ORDER);
	
	//new eerst blokje per gespeelde datum
	//$regexp ='/<div.class=\\"table-wrapper.table-timetable.*?<span>(.*?)<.span/';
	//$regexp = '/div.*?table-wrapper.table-timetable.*?target>.*?\s{2,}.* // ? >\s{2,}.* ? >.*?\s.*\s+<span>(.*)<.span>/';
	$regexp = '/class="table-wrapper.table-timetable.*\s.*\s.*\s.*\s<span.*\s.*<span>(.*)<.span>/';
	preg_match_all($regexp, $htmlstripped, $play_dates, PREG_SET_ORDER);
	
	// echo 'length of play_dates ' .count($play_dates); 
	$play_date = array();
	$i = 0;
	foreach($play_dates AS $value)
	{
		$play_date[$i] = $value[1];
		$i++;
	}

	$i = 0;
	$j = 0;
	$match = array();
	foreach($play_date AS $value)
	{
		// echo 'Datum ' .$i .' ' .$value;
		if (count($play_date) != $i + 1)  {
			$htmlstrippedPlayDate = get_string_between($html,$value,"table-wrapper table-timetable");
		} else {
			$htmlstrippedPlayDate = get_string_between($html,$value,"");
		}
		if ($i == 0) {
			// echo "<br>" . "wat" . "<br>";
			//echo "<br><pre>" . $htmlstrippedPlayDate ."</pre><br>";
		}
		// $regexp2 = '/<div.class="value home.*">\s+<div class="team">(.*)<.div>\s.*?<div.*?\s+<img.*?\s+<.div>\s+<.div>\s+.*?center">\s(.*)<.div>\s+.*>\s+.*>\s+.*.>\s+.*\s+<div class="team">(.*)<.div>/';
		// $regexp2 = '/class="value home.*\s+<div class="team">(.*)<.div>\s.*\s.*\s.*\s.*\s.*center">\s(.*)<.div>\s.*\s.*\s.*\s.*\s+<div class="team">(.*)<.div>/';
		// <div class="team">Suawoude 1</div>
		$regexp2 = '/class="value home.*\s+<div class="team">(.*)<.div>\s.*\s.*\s.*\s.*\s.*\s.*\s(.*)\s.*\s.*\s.*\s.*\s+<div class="team">(.*)<.div>/';
		preg_match_all($regexp2, $htmlstrippedPlayDate, $matches, PREG_SET_ORDER);
		// echo "aantal matches: " .count($matches);
		foreach($matches AS $value2) {
			$match[$j]['Datum']   = $value;
			$match[$j]["Thuis"]   = trim($value2[1]);
			$match[$j]["Uit"]     = trim($value2[3]);
			$match[$j]['Uitslag'] = trim($value2[2]);
			// echo $value ." " .trim($value2[1]) ." - " .trim($value2[3]) ." " .trim($value2[2]);     
			$j++;
		}
		$i++;
	}
	return $match;
}
// -------------------------------------


//Programma ophalen -------------------- Programma met details is de functie hieronder
function get_programma($html)
{
	include("config.php");
	$htmlstripped = $html;

	// Er zijn wel gegevens gevonden dus gaan we door
	//$regexp omschrijft nu hoe de tabel eruit moet zien                                                                                                       -? tussen*>
	//$regexp = '/<div.class="datum-tijd">(.*?),.(.*?)<.div>\s+<div.class="status(\s(afgelast))*">\s+<div.class="wedstrijd"><div.class=".*?">(.*?)<.div><div..*>-<.div><div.class=".*?">(.*?)<.div><.div>\s+<div.class="type">(.*?)<.div>\s+<div.class="accomodatie">(.*?)<.div>\s+<.div>/';
	//preg_match_all($regexp, $htmlstripped, $matches, PREG_SET_ORDER);
	// $regexp = '/div.*?table-wrapper.table-timetable.*?target>.*?\s{2,}.*? >\s{2,}.*? >.*?\s.*\s+<span>(.*)<.span>/';
	$regexp = '/class="table-wrapper.table-timetable.*\s.*\s.*\s.*\s<span.*\s.*<span>(.*)<.span>/';
	preg_match_all($regexp, $htmlstripped, $play_dates, PREG_SET_ORDER);
	
	// echo 'length of play_dates ' .count($play_dates); 
	$play_date = array();
	$i = 0;
	foreach($play_dates AS $value)
	{
		// echo 'play_date: ' .$value[1] ."<br>";
		$play_date[$i] = $value[1];
		$i++;
	}
	
	$same_as_previous=false;
	$previous_date="";
	$same_date_number=1;
	$i = 0;
	$j = count($match);
	foreach($play_date AS $value)
	{
		//echo 'Datum ' .$i .' ' .$value;
		if ($previous_date == $value) {
			if (count($play_date) != $i + 1)  {
				$htmlstrippedPlayDate = get_string_between_number($html,$value,"table-wrapper table-timetable", $same_date_number + 1);
			} else {
				$htmlstrippedPlayDate = get_string_between_number($html,$value,"", $same_date_number +1);
			}
			$same_date_number++;
		} else {
			$same_date_number=1;
			if (count($play_date) != $i + 1)  {
				$htmlstrippedPlayDate = get_string_between($html,$value,"table-wrapper table-timetable");
			} else {
				$htmlstrippedPlayDate = get_string_between($html,$value,"");
			}
		}
		//echo $$htmlstrippedPlayDate;
		// $regexp2 = '/<div.class="value home.*">\s+<div class="team">(.*)<.div>\s.*?<div.*?\s+<img.*?\s+<.div>\s+<.div>\s+.*?center">\s(.*)<.div>\s+.*>\s+.*>\s+.*.>\s+.*\s+<div class="team">(.*)<.div>/';
		// $regexp2 = '/class="value home.*\s+<div class="team">(.*)<.div>\s.*\s.*\s.*\s.*\s.*\s.*\s(.*)\s.*\s.*\s.*\s.*\s+<div class="team">(.*)<.div>/';
		$regexp2 = '/class="value home.*\s+<div class="team">(.*)<.div>\s.*\s.*\s.*\s.*\s.*\s.*\s(.*)\s.*\s.*\s.*\s.*\s+<div class="team">(.*)<.div>/';
		preg_match_all($regexp2, $htmlstrippedPlayDate, $matches, PREG_SET_ORDER);
		foreach($matches AS $value2) {
			$match[$j]['Datum']   = $value;
			//$match[$j]["Thuis"]   = trim($value2[1]);
			//$match[$j]["Uit"]     = trim($value2[3]);
			$match[$j]['Wedstrijd'] = trim($value2[1])." - ".trim($value2[3]);
			$match[$j]['Tijd'] = trim($value2[2]);
			// echo $value ." " .trim($value2[1]) ." - " .trim($value2[3]) ." " .trim($value2[2]);     
			$j++;
		}
		$i++;
		$previous_date=$value;
	}
	//dp($matches);
	//die;

	//$match = array();
	//$i = 0;
	//foreach($matches AS $value)
	//{
	//	$match[$i]['Datum']          = $value[1];
	//	$match[$i]['Tijd']           = $value[2];
	//	$match[$i]['Status']         = $value[4];
	//	$match[$i]['Wedstrijd']      = $value[5]." - ".$value[6];
	//	$match[$i]['Type']           = $value[7];
	//	//Bij een afgelasting staat er een plaatje in plaats van de accommodatie.
	//	if(stristr($value[8], 'AFGELAST') === FALSE)
	//	{
	//		$match[$i]['Accommodatie'] = $value[8];
	//	}else{
	//		$match[$i]['Accommodatie'] = '';
	//	}
	//	//$match[$i]['Wedstrijdnr']    = $value[8];
	//	//$match[$i]['Scheidsrechter'] = $value[9];
	//
	//	$i++;
	//}
	//Elke match teruggeven
	return $match;
}
// Einde functie programma ophalen zonder details -------------------------------------


//Programma met details ophalen ---- Programma zonder details hierboven
function get_programmadetailed($html, $teamcode)
{

	include("config.php");

	//Tegenwoordig bevat de pagina meerdere tabellen met programma's. Neem de laatste
	//$zoekterm =  "id=\"FixturesContent";
	$zoekterm =  "id=\"FixturesContent".$teamcode."-3";
	$htmlstripped = get_string_between($html,$zoekterm,"");

	//Eerste kijken of er wel informatie is
	$zoekstring = "Er zijn geen programmagegevens";

	//Indien er geen gegevens zijn gevonden stoppen
	if(strstr($htmlstripped,$zoekstring)) return $zoekstring;

	if ($Clubnaamcheck == 'Aan')
	// Check op clubnaam of deze wel of niet voorkomt. Hogere betrouwbaarheid bij wegschrijven gegevens
	{
		//Nu kijken of er wel informatie in zit door te checken op de clubnaam
		$zoekstring = $clubverkort;
		$emptystring = "leegprogramma";

		//Indien clubnaam niet is gevonden stoppen

		$pos = strpos($html, $zoekstring);

		if ($pos === false) {
			if ($UserDebug == 'Aan') echo "Team Programma: Clubnaam  '$zoekstring' is niet gevonden in team programma.<br />";
			return $emptystring;
		} else {
			if ($UserDebug == 'Aan') echo "team programma: Clubnaam '$zoekstring' is gevonden in team programma.<br />";
		}
	}
	else
	{
		// Geen controle op clubnaam. Hierdoor kunnen de gegevens van meerdere club teams worden opgehaals maar mogelijk problemen met wegschrijven van gegevens
	}


	// Er zijn wel gegevens gevonden dus gaan we door
	//$regexp omschrijft nu hoe de tabel eruit moet zien
	$regexp = '/<div.class="datum-tijd">(.*?),.(.*?)<.div>\s+<div.class="status(\s(afgelast))*">\s+<div.class="wedstrijd"><div.class=".*?">(.*?)<.div><div..*?>-<.div><div.class=".*?">(.*?)<.div><.div>\s+<div.class="type">(.*?)<.div>\s+<div.class="accomodatie">(.*?)<.div>\s+<.div>\s+.*?\s+<div.class="details"><a.class="fancy-box".href="(.*?)\?iframe/';
	preg_match_all($regexp, $htmlstripped, $matches, PREG_SET_ORDER);

	//dp($matches);
	//die;

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Datum']          = $value[1];
		$match[$i]['Tijd']           = $value[2];
		$match[$i]['Status']         = $value[4];
		$match[$i]['Wedstrijd']      = $value[5]." - ".$value[6];
		$match[$i]['Type']           = $value[7];
		//Bij een afgelasting staat er een plaatje in plaats van de accommodatie.
		if(stristr($value[8], 'AFGELAST') === FALSE)
		{
			$match[$i]['Accommodatie'] = $value[8];
		}else{
			$match[$i]['Accommodatie'] = '';
		}

		// Alle wedstrijd details staan nu in een aparte link. Deze wordt nu opgevraagd en alle details opgeslagen
		$htmlwdetails = fetch_url("http://pupillen.voetbal.nl".$value[9]);

		$regexp = '/<div.class="naw.*><label>Wedstrijdnummer:<.label>.(.*?)<.div>\s+(<div.class="naw.*><label>Periode:<.label>.(.*?)<.div>\s+)?<div.class="naw.*><label>Scheidsrechter:<.label>.(.*?)<.div>\s+<div.class="naw.*><label>Sportpark:<.label>.(.*?)<.div>\s+<div.class="naw.*><label>Adres:<.label>.(.*?)<.div>\s+<div.class="naw.*><label>Postcode:<.label>.(.*?)<.div>\s+<div.class="naw.*><label>Plaats:<.label>.(.*?)<.div>\s+<div.class="naw.*><label>Telefoon:<.label>.(.*?)<.div>\s+/';
		preg_match_all($regexp, $htmlwdetails, $dmatches, PREG_SET_ORDER);

		$dmatch =array();
		$i1=0;
		foreach($dmatches AS $dvalue)
		{
			$match[$i]['Wedstrijdnr']    = $dvalue[1];
			$match[$i]['Periodestring']  = $dvalue[2];
			$match[$i]['Periode']        = $dvalue[3];
			$match[$i]['Scheidsrechter'] = $dvalue[4];
			$match[$i]['Sportpark']	 = $dvalue[5];
			$match[$i]['Adres'] 	 = $dvalue[6];
			$match[$i]['Postcode'] 	 = $dvalue[7];
			$match[$i]['Plaats'] 	 = $dvalue[8];
			$match[$i]['Telefoon'] 	 = $dvalue[9];
			$i1++;
		}

		$i++;
	}

	//Elke match teruggeven
	return $match;
}
// Einde functie programma ophalen met details -------------------------------------


//Alle Uitslagen ophalen ---------------
function get_alle($html)
{

	//Tegenwoordig bevat de pagina meerdere tabellen met uitslagen. Neem de alles na de eerste
	//$htmlstripped = get_string_between($html,"id=\"ResultsContent3","");
	$htmlstripped = get_string_between($html,'<div name="ResultsContent" id="ResultsContent2" style="display: none;">',"");


	// Er zijn wel gegevens gevonden dus gaan we door //$regexp omschrijft nu hoe de tabel eruit moet zien
	$regexp = '/<div.class="datum-tijd">(.*?)<.div>\s+<div.class="wedstrijd"><div.class=".*?">(.*?)<.div><div.*?>-<.div><div.class=".*?">(.*?)<.div><.div>\s+<div.class="uitslag">(.*?)-(.*?)<.div>/';
	preg_match_all($regexp, $htmlstripped, $matches, PREG_SET_ORDER);

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Datum']   = $value[1];
		$match[$i]["Thuis"]   = trim($value[2]);
		$match[$i]["Uit"]     = trim($value[3]);
		$match[$i]['Uitslag'] = $value[4]." - ".$value[5];
		$i++;
	}
	//Elke match teruggeven
	$match = removeDuplicateMatches($match);
	return $match;
}
// -------------------------------------


//Alle Programma's ophalen voor Club ---
function get_clubprog($html)
{

	include("config.php");

	global $debug,$debug_programma_html;

	// echo "html: $html <br />";
	// eerste kijken of er wel informatie is
 	$htmlexplode = explode('<div class="table-wrapper table-timetable ',$html);

    $match = array();

	$x = 0;
 	foreach($htmlexplode AS $blok) {
 	    if ($x > 0) {
            // echo "blok1: $htmlexplode[$x] <br />";
            $match = get_clubprog_date($htmlexplode[$x], $match);
 	        $x++;
        } else {
 	        $x++;
        }
         // echo "loop: $x <br />";
    }


    // echo "1e: $htmlexplode[0] <br /> ";

//	$thuischeck = $htmlexplode[2];
//	$uitcheck = $htmlexplode[3];
//	$zoekstring = 'Er zijn geen programmagegevens';
//
//	// Indien er geen gegevens zijn gevonden stoppen
//	if((strstr($thuischeck,$zoekstring)) and (strstr($uitcheck,$zoekstring))) return $zoekstring;


	//Nu kijken of er wel informatie in zit door te checken op de clubnaam
//	$zoekstring = $clubverkort;
//	$emptystring = "leegprogramma";
//
//	//Indien clubnaam niet is gevonden stoppen
//
//	$pos = strpos($html, $zoekstring);
//
//	if ($pos === false) {
//		if ($UserDebug == 'Aan') echo "Clubnaam  '$zoekstring' is niet gevonden in Club programma.<br />";
//		return array($emptystring,$emptystring);
//	} else {
//		if ($UserDebug == 'Aan') echo "Clubnaam '$zoekstring' is gevonden in Club programma.<br />";
//	}
//


//	//Er zijn wel gegevens gevonden dus gaan we door
//	//$regexp omschrijft nu hoe de tabel eruit moet zien
//	$regexp = '/<div.class="datum-tijd" style=".*?">(.*?),.(.*?)<.div>\s+<div style=".*?">.*?<.div>\s+<div.class="status(\s(afgelast))*">\s+<div.class="wedstrijd".style=".*?">\s+<div class="".style=".*?">(.*?)<.div>\s+<div style=".*?">.+?<.div>\s+<div class="".*?style=".*?">(.*?)<.div>\s+<.div>\s+<div class="type">(.*?)<.div>\s+<div class="wedstrijdnr".*?style=".*?">(.*?)<.div>\s+<.div>\s+<div class="details">.*?<.div>\s+<div.class="scheidsrechter".*?style=".*?">(.*?)<.div>\s+/';
//	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);
//
//	if(count($matches) < 1)
//	{
//		if($html != '')
//		{
//			echo 'Fout bij het ophalen van het programma, search string niet gevonden.';
//		}else{
//			echo 'Fout bij het ophalen van het programma, lijkt fout te gaan met het ophalen van de html:<br /><br />'.$html;
//		}
//	}
	$afgmatch = array();
	$i = 0;
	$j = 0;
//	foreach($matches AS $value)
//	{
//		if($value[4] == 'afgelast')
//		{
//			$afgmatch[$j]['Datum']  = $value[1];
//			$afgmatch[$j]['Tijd']   = $value[2];
//			$afgmatch[$j]['Status'] = $value[4];
//			$afgmatch[$j]["Thuis"]  = trim($value[5]);
//			$afgmatch[$j]["Uit"]    = trim($value[6]);
//			$afgmatch[$j]['Type']   = $value[7];
//			//Accomodatie bestaat niet meer dus vul hem maar met de thuis spelende club
//			$afgmatch[$j]['Accommodatie'] = trim($value[5]);
//			if(stristr($value[8], 'AFGELAST') === FALSE)
//			{
//				$afgmatch[$j]['Wedstrijdnr'] = $value[8];
//			}else{
//				$afgmatch[$j]['Wedstrijdnr'] = '';
//			}
//
//			$afgmatch[$j]['Scheidsrechter'] = $value[9];
//			$j++;
//		}else{
//			$match[$i]['Datum'] = $value[1];
//			$match[$i]['Tijd']  = $value[2];
//			$match[$i]["Thuis"] = trim($value[5]);
//			$match[$i]["Uit"]   = trim($value[6]);
//			$match[$i]['Type']  = $value[7];
//			//Accomodatie bestaat niet meer dus vul hem maar met de thuis spelende club
//			$match[$i]['Accommodatie'] = trim($value[5]);
//			$match[$i]['Wedstrijdnr']  = $value[8];
//			$match[$i]['Scheidsrechter'] = $value[9];
//			$i++;
//		}
//	}
	//Elke match teruggeven
    // var_dump ($match);
	return $match;
}
// -------------------------------------

function get_clubprog_date($datablok, $match)
{
    $i = count($match);
//	$regexp = '/<div.class="datum-tijd" style=".*?">(.*?),.(.*?)<.div>\s+<div style=".*?">.*?<.div>\s+<div.class="status(\s(afgelast))*">\s+<div.class="wedstrijd".style=".*?">\s+<div class="".style=".*?">(.*?)<.div>\s+<div style=".*?">.+?<.div>\s+<div class="".*?style=".*?">(.*?)<.div>\s+<.div>\s+<div class="type">(.*?)<.div>\s+<div class="wedstrijdnr".*?style=".*?">(.*?)<.div>\s+<.div>\s+<div class="details">.*?<.div>\s+<div.class="scheidsrechter".*?style=".*?">(.*?)<.div>\s+/';
    // $regexp = '/<div.class="table">\s{2,}<div.class="header">\s{2,}<span.class="title">\s{2,}<span>(.*?)<.span>\s+<div.class="team">(.*?)<.div>\s+<div.class="value.center">\s{2,}(.*?)\s{2,}<.div>\s+/';
    //$regexp = '/<div.class="table">\s{2,}<div.class="header">\s{2,}<span.class="title">\s{2,}<span>(.*?)<.span>\s+<div.class="team">(.*?)<.div>\s+/';
    // onderstaande volledige regexp geeft alleen het eerste record
    //$regexp = '/<div.class="table">\s{2,}<div.class="header">\s{2,}<span.class="title">\s{2,}<span>(.*?)<.span>\s+\s{2,}<.span>\s{2,}<span.class=".*".\s{2,}<.span>\s{2,}<.div>\s{2,}<a.href=".*">\s{2,}<div.class=".*">\s{2,}<div.class="team">(.*?)<.div>\s+<div.class=".*?">\s{2,}<img.class=".*".*\s{2,}<.div>\s{2,}<.div>\s{2,}<div.class="value.center">\s{2,}(.*?)\s{2,}<.div>\s{2,}<div.class=".*">\s{2,}<div.class=".*">\s{2,}<img.*\s{2,}<.div>\s{2,}<div.class=".*?">(.*?)<.div>\s+/';

    $regexpDate = '/<div.class="table">\s{2,}<div.class="header">\s{2,}<span.class="title">\s{2,}<span>(.*?)<.span>\s+/';
    preg_match_all($regexpDate, $datablok, $matchesDate, PREG_SET_ORDER);
    $regexp = '/<div.class="team">(.*?)<.div>\s+<div.class=".*?">\s{2,}<img.class=".*".*\s{2,}<.div>\s{2,}<.div>\s{2,}<div.class="value.center">\s{2,}(.*?)\s{2,}<.div>\s{2,}<div.class=".*">\s{2,}<div.class=".*">\s{2,}<img.*\s{2,}<.div>\s{2,}<div.class=".*?">(.*?)<.div>\s+/';
    preg_match_all($regexp, $datablok, $matches, PREG_SET_ORDER);
	if(count($matches) < 1)
	{
			echo 'Fout bij het ophalen van het programma, lijkt fout te gaan met het ophalen van de html:<br /><br />';
	}
    $wedstrijddatum = '';
    foreach($matchesDate AS $valueDate) {
        $wedstrijddatum = $valueDate[1];
    }

    //echo "Wedstrijddatum: $wedstrijddatum <br />";
    // $time = strtotime($wedstrijddatum);
    // $newformat = date('Y-m-d',$time);
    // echo "Datum bepalen: $newformat <br />";

    foreach($matches AS $value)
	{
	    //echo "value 1 is: $value[1] <br />";
        //echo "value 2 is: $value[2] <br />";
        //echo "value 3 is: $value[3] <br />";
        //echo "value 4 is: $value[4] <br />";
        //$match[$i]['Datum'] = $wedstrijddatum;
        $match[$i]['Datum'] = $wedstrijddatum;
        $match[$i]['Tijd']  = $value[2];
        $match[$i]["Thuis"] = trim($value[1]);
        $match[$i]["Uit"]   = trim($value[3]);
        $match[$i]['Type']  = '';
        //Accomodatie bestaat niet meer dus vul hem maar met de thuis spelende club
        $match[$i]['Accommodatie'] = '';
        $match[$i]['Wedstrijdnr']  = '';
        $match[$i]['Scheidsrechter'] = '';
			$i++;
	}

    return $match;
}

//Afgelastingen ophalen voor Club --- Deze functie wordt niet meer gebruikt.... Afgelasting zitten in functie hierboven
function get_clubaf($caurl)
{

	include("config.php");

	$html = fetch_url($caurl);

	// eerste kijken of er wel informatie is
	$zoekstring = "Er zijn geen programmagegevens";
	$zoekstring2 = "Er zijn geen programmagegevens";

	// Indien er geen gegevens zijn gevonden stoppen
	if((strstr($html,$zoekstring)) and (strstr($html,$zoekstring2))) return $zoekstring;

	//Nu kijken of er wel informatie in zit door te checken op de clubnaam
	$zoekstring = $clubverkort;
	$emptystring = "";

	//Indien clubnaam niet is gevonden stoppen
	if(strstr($html,$zoekstring)) return $emptystring;


	// Er zijn wel gegevens gevonden dus gaan we door
	// $regexp omschrijft nu hoe de tabel eruit moet zien
	$regexp = '#<div.class="calendar-month">(.*?)<.div>\s+
		<div.class="calendar-day">(.*?)<.div>\s+
		<.div>\s+
		<.td>\s+
		<td.class="time".style="padding:0px;.margin:0px;font-weight:normal;">(.*?)<.td>\s+
		<td.class="match".style="padding:0px;.margin:0px;font-weight:normal;">(.*?)&nbsp;&nbsp;-&nbsp;&nbsp;(.*?)<.td>\s+
		<td.class="where".style="padding:0px;.margin:0px;font-weight:normal;">(.*?)<.td>\s+
		<td.class="where".style="padding:0px;.margin:0px;font-weight:normal;">(.*?)<.td>\s+
		<td.class="where".style="padding:0px;.margin:0px;font-weight:normal;">(.*?)<.td>\s+
		<td.class="where".style="padding:0px;.margin:0px;font-weight:normal;">(.*?)<.td>\s+
		<td.class="where".style="padding:0px;.margin:0px;font-weight:normal;">(.*?)<.td>\s+#simx';

	//Waardes ophalen en opslaan in $matches

	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);
	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Datum']          = $value[2]." ".$value[1];
		$match[$i]['Tijd']           = $value[3];
		$match[$i]["Thuis"]          = trim($value[4]);
		$match[$i]["Uit"]            = trim($value[5]);
		$match[$i]['Type']           = $value[6];
		$match[$i]['Accommodatie']   = $value[7];
		$match[$i]['Wedstrijdnr']    = $value[8];
		$match[$i]['Scheidsrechter'] = $value[9];
		$match[$i]['Status']         = $value[10];
		$i++;
	}
	//Elke match teruggeven
	return $match;
}
// -------------------------------------


//Alle teams ophalen van clubpagina ---------------
function get_teams($teamsurl)
{

	$html = fetch_url($teamsurl);


	//Tegenwoordig bevat de pagina meerdere tabellen met uitslagen. Neem de alles na de eerste
	//$htmlstripped = get_string_between($html,"id=\"ResultsContent3","");
	//  $htmlstripped = get_string_between($html,'<div name="ResultsContent" id="ResultsContent2" style="display: none;">',"");

	// Er zijn wel gegevens gevonden dus gaan we door //$regexp omschrijft nu hoe de tabel eruit moet zien
	$regexp ='/clubs-competities.mijn.poules.uitslagen.(.*?)".title="Selecteer poule">(.*?).nbsp.(.*?)<.a>/';


	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Naam']		= $value[2];
		$match[$i]['Klasse']   	= $value[3];
		$match[$i]['Teamcode']	= $value[1];
		$match[$i]['Wedstrijdduur'] = "0";
		$match[$i]['Periode'] = "nee";
		$match[$i]['Ophalen'] = "ja";
		$match[$i]['GroupID'] = "1";
		$match[$i]['DatumTijd-Update'] = "1230768001";
		$i++;
	}
	//Elke match teruggeven
	//  $match = removeDuplicateMatches($match);
	return $match;
}
// -------------------------------------


//Teamindeling Ophalen ---------------
function get_teamindeling($html)
{


	//Nu kijken of er wel informatie in zit door te checken op de clubnaam
	$zoekstring = $clubverkort;
	$emptystring = "";

	//Indien clubnaam niet is gevonden stoppen
	if(strstr($html,$zoekstring)) return $emptystring;

	// Er zijn wel gegevens gevonden dus gaan we door //$regexp omschrijft nu hoe de tabel eruit moet zien

	$regexp ='/<div.*class="indeling-row.*">\s+<div.class="nr">(.*?)<.div>\s+<div class="elftal">(.*?)<.div>\s+<div class="voorkeurstijd">(.*?)<.div>\s+/';

	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Teamnr']		= $value[1];
		$match[$i]['Naam']   	= $value[2];
		$match[$i]['Vktijd']	= $value[3];
		//echo $value[1];
		//echo $value[2];
		//echo $value[3];
		$i++;
	}
	//Elke match teruggeven
	//  $match = removeDuplicateMatches($match);
	return $match;
}
// -------------------------------------


//Speelronde Ophalen ---------------
function get_speelronde($html)
{

	//Tegenwoordig bevat de pagina meerdere tabellen met uitslagen. Neem de eerste. Aangenomen dat dit de laatste uitslagen zijn

	$htmlstripped = get_string_between($html,"<div class=\"speelschema-topheader\">speelschema</div>","");

	$html=$htmlstripped;


	//Nu kijken of er wel informatie in zit door te checken op de clubnaam
	$zoekstring = $clubverkort;
	$emptystring = "";

	//Indien clubnaam niet is gevonden stoppen
	if(strstr($html,$zoekstring)) return $emptystring;

	// Er zijn wel gegevens gevonden dus gaan we door //$regexp omschrijft nu hoe de tabel eruit moet zien

	$regexp ='/<div.*class="nr">(.*?)<.div>\s+/';

	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);

	$match = array();
	$i = 0;
	foreach($matches AS $value)
	{
		$match[$i]['Speelronde']	= "00";
		$match[$i]['Wedstrijd']	= $value[1];
		$i++;
	}
	//Elke match teruggeven
	//  $match = removeDuplicateMatches($match);
	return $match;
}
// -------------------------------------


//Standen opslaan in database ----------
function opslaan_stand($standen,$teamID,$tabel)
{
	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to store Standen");
	$query = "
	INSERT INTO $tabel
	(
		TeamID,
		Elftal,
		Plaats,
		G,
		W,
		GW,
		V,
		P,
		DPV,
		DPT,
		PM
	)
	VALUES
";
	$t = 0;
	$CountData = count($standen);
	
	foreach($standen AS $value)
	{
		$value['Elftal'] = htmlentities($value['Elftal'], ENT_QUOTES);
		if ($value['PM'] == '0'){
			$PM = '-';
		}else{
			$PM = $value['PM'];
		}

		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$teamID."', '".$value['Elftal']."', '".$value['#']."', '".$value['G']."', '".$value['W']."', '".$value['GW']."', '".$value['V']."', '".$value['P']."', '".$value['DPV']."', '".$value['DPT']."', '".$PM."'),";
		}
		else
		{
			$query.="('".$teamID."', '".$value['Elftal']."', '".$value['#']."', '".$value['G']."', '".$value['W']."', '".$value['GW']."', '".$value['V']."', '".$value['P']."', '".$value['DPV']."', '".$value['DPT']."', '".$PM."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysql_query($query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($query) or die(mysql_error());
	}


	// Verwijderen van lege entries
	$sql = "DELETE FROM ".$tabel." WHERE Elftal = ''";
	if ($UserDebug == 'Aan') {
		mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($sql) or die(mysql_error());
	}
}
// -------------------------------------

//Alle Uitslagen ophalen voor Club ---
function get_clubuitslag($html)
{

	include("config.php");

    $htmlexplode = explode('<div class="table-wrapper table-timetable',$html);

    $match = array();

    $x = 0;
    foreach($htmlexplode AS $blok) {
        if ($x > 0) {
            //echo "blok1: $htmlexplode[$x] <br />";
            $match = get_clubuitslag_date($htmlexplode[$x], $match);
            $x++;
        } else {
            $x++;
        }
        // echo "loop: $x <br />";
    }

	// Er zijn wel gegevens gevonden dus gaan we door //$regexp omschrijft nu hoe de tabel eruit moet zien
/*	$regexp ='/<div.class="datum-tijd".style="margin-left:15px;">(.*?)<.div>\s+<div.class="wedstrijd"><div.class=".*?">(.*?)<.div><div.*?>-<.div><div.class=".*?">(.*?)<.div><.div>\s+<div.class="uitslag">(.*?)<.div>/';*/
//	preg_match_all($regexp, $html, $matches, PREG_SET_ORDER);
//
//	$match = array();
//	$i = 0;
//	foreach($matches AS $value)
//	{
//		$match[$i]['Datum']   = $value[1];
//		$match[$i]["Thuis"]   = trim($value[2]);
//		$match[$i]["Uit"]     = trim($value[3]);
//		$match[$i]['Uitslag'] = $value[4];
//		$i++;
//	}
//	//Elke match teruggeven
//	$match = removeDuplicateMatches($match);
	return $match;
}
// -------------------------------------

function get_clubuitslag_date($datablok, $match)
{
    $i = count($match);

    $regexpDate = '/<div.class="table">\s{2,}<div.class="header">\s{2,}<span.class="title">\s{2,}<span>(.*?)<.span>\s+/';
    preg_match_all($regexpDate, $datablok, $matchesDate, PREG_SET_ORDER);
    $regexp = '/<div.class="team">(.*?)<.div>\s+<div.class=".*?">\s{2,}<img.class=".*".*\s{2,}<.div>\s{2,}<.div>\s{2,}<div.class="value.center">\s{2,}(.*?)\s{2,}<.div>\s{2,}<div.class=".*">\s{2,}<div.class=".*">\s{2,}<img.*\s{2,}<.div>\s{2,}<div.class=".*?">(.*?)<.div>\s+/';
    preg_match_all($regexp, $datablok, $matches, PREG_SET_ORDER);
    if(count($matches) < 1)
    {
        echo 'Fout bij het ophalen van het programma, lijkt fout te gaan met het ophalen van de html:<br /><br />';
    }
    $wedstrijddatum = '';
    foreach($matchesDate AS $valueDate) {
        $wedstrijddatum = $valueDate[1];
    }

    // echo "Wedstrijddatum: $wedstrijddatum <br />";
    // $time = strtotime($wedstrijddatum);
    // $newformat = date('Y-m-d',$time);
    // echo "Datum bepalen: $newformat <br />";

    foreach($matches AS $value)
    {
        // echo "value 1 is: $value[1] <br />";
        // echo "value 2 is: $value[2] <br />";
        // echo "value 3 is: $value[3] <br />";
        // echo "value 4 is: $value[4] <br />";
        //$match[$i]['Datum'] = $wedstrijddatum;
        $match[$i]['Datum'] = $wedstrijddatum;
        $match[$i]["Thuis"] = trim($value[1]);
        $match[$i]["Uit"]   = trim($value[3]);
        $match[$i]['Uitslag']  = $value[2];
        $i++;
    }

    return $match;
}


//ophalen van afgelast/lege/dubbele team uitslag
function ontdubbeluitslagen($afgelastteamuitslag,$teamid,$table)
{

	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database");

	$query1 = "SELECT DISTINCT TeamID, Datum, Thuis, Uit, Uitslag
FROM $table WHERE TeamID=$teamid AND Uitslag LIKE '%afgelast%' ";

	$auitslag=mysql_query($query1);
	$auitslagnum=mysql_numrows($auitslag);

	return array ($auitslag,$auitslagnum);
}
// -------------------------------------

//Opslaan afgelast/lege/dubbele team uitslag

function opslaan_afuitslagen($auitslag,$auitslagnum,$table)
{
	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to store Uitslagen");

	$i=0;
	while ($i < $auitslagnum) {

		$TeamID = mysql_result($auitslag,$i,"TeamID");
		$Datum = mysql_result($auitslag,$i,"Datum");
		$Thuis = mysql_result($auitslag,$i,"Thuis");
		$Uit = mysql_result($auitslag,$i,"Uit");
		$Uitslag = mysql_result($auitslag,$i,"Uitslag");



		$sql = "SELECT * FROM ".$table." WHERE teamID='$TeamID' AND datum='$Datum' AND thuis='$Thuis' AND uit='$Uit' AND TRIM(uitslag) = \"-\"";
		$result=mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
		$num2=mysql_numrows($result);

		if($num2 >'0')
		{
			// De uitslag is nog steeds een '-' dus we verwijderen die entry en plaatsen de afgelaste uitslag weer in de tabel.

			$sql = "DELETE FROM ".$table." WHERE teamID='$TeamID' AND datum='$Datum' AND thuis='$Thuis' AND uit='$Uit' AND TRIM(uitslag) = \"-\"";
			mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());

			$sql= "INSERT INTO ".$table." (TeamID, Datum, Thuis, Uit, Uitslag) VALUES ('TeamID', '$Datum', '$Thuis', '$Uit', '$Uitslag')";
			mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());

		}
		else
		{
			// De uitslag lijkt dubbel in de tabel te zitten
		}

		$i++;
	}

}
// -------------------------------------



//Ophalen Handmatig ingevoerde team uitslagen --------
function teamhanduitslagen($saveteamuitslag,$teamid,$table)
{

	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database");

	$query1 = "SELECT DISTINCT TeamID, Datum, Thuis, Uit, Uitslag
FROM $table WHERE TeamID=$teamid AND Uitslag LIKE '%*' ";

	$huitslag=mysql_query($query1);
	$huitslagnum=mysql_numrows($huitslag);

	return array ($huitslag,$huitslagnum);
}
// -------------------------------------



//Opslaan Handmatig ingevoerde team uitslagen --------
function opslaan_thuitslagen($huitslag,$huitslagnum,$table)
{
	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to store Uitslagen");

	$i=0;
	while ($i < $huitslagnum) {

		$TeamID = mysql_result($huitslag,$i,"TeamID");
		$Datum = mysql_result($huitslag,$i,"Datum");
		$Thuis = mysql_result($huitslag,$i,"Thuis");
		$Uit = mysql_result($huitslag,$i,"Uit");
		$Uitslag = mysql_result($huitslag,$i,"Uitslag");

		$sql = "SELECT * FROM ".$table." WHERE teamID='$TeamID' AND datum='$Datum' AND thuis='$Thuis' AND uit='$Uit' AND TRIM(uitslag) = \"-\"";

		$result=mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
		$num2=mysql_numrows($result);



		if($num2 >'0')
		{
			// De uitslag is nog steeds een '-' dus we verwijderen die entry en plaatsen de handmatige uitslag weer in de tabel.

			$sql = "DELETE FROM ".$table." WHERE teamID='$TeamID' AND datum='$Datum' AND thuis='$Thuis' AND uit='$Uit' AND TRIM(uitslag) = \"-\"";
			mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());

			//$sql= 'INSERT INTO `'.$table.'` (TeamID, Datum, Thuis, Uit, Uitslag) VALUES ("$TeamID", "$Datum", "$Thuis", "$Uit", "$Uitslag")';
			$sql= "INSERT INTO ".$table." (TeamID, Datum, Thuis, Uit, Uitslag) VALUES ('TeamID', '$Datum', '$Thuis', '$Uit', '$Uitslag')";
			mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());}
		else
		{
			// De uitslag lijkt te zijn verwerkt dus we plaatsen de handmatige uitslag NIET in de tabel.
		}

		$i++;
	}

}
// -------------------------------------



//Ophalen Handmatig ingevoerde club uitslagen --------
function clubhanduitslagen($saveteamuitslag,$teamid,$table)
{

	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database");

	$query1 = "SELECT DISTINCT Datum, Thuis, Uit, Uitslag
FROM $table WHERE Uitslag LIKE '%*' ";

	$huitslag=mysql_query($query1);
	$huitslagnum=mysql_numrows($huitslag);

	return array ($huitslag,$huitslagnum);
}
// -------------------------------------

//Opslaan Handmatig ingevoerde club uitslagen --------
function opslaan_chuitslagen($huitslag,$huitslagnum,$table)
{
	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to store Uitslagen");

	$i=0;
	while ($i < $huitslagnum) {

		$Datum = mysql_result($huitslag,$i,"Datum");
		$Thuis = mysql_result($huitslag,$i,"Thuis");
		$Uit = mysql_result($huitslag,$i,"Uit");
		$Uitslag = mysql_result($huitslag,$i,"Uitslag");
		// Ook  deze aangepast
		$sql = 'SELECT * FROM `'.$table.'` WHERE Datum= "$Datum" AND Thuis="$Thuis" AND Uit="$Uit" AND TRIM(Uitslag) = "-"';
		$result=mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
		$num2=mysql_numrows($result);

		if($num2 >'0')
		{
			// De uitslag is nog steeds een '-' dus we verwijderen die entry en plaatsen de handmatige uitslag weer in de tabel.

			$sql = 'DELETE FROM `'.$table.'` WHERE Datum="$Datum" AND Thuis="$Thuis" AND Uit="$Uit" AND TRIM(Uitslag) = "-"';
			mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());

			$sql= 'INSERT INTO `'.$table.'` (Datum, Thuis, Uit, Uitslag) VALUES ("$Datum", "$Thuis", "$Uit", "$Uitslag")';
			mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());}
		else
		{
			// De uitslag lijkt te zijn verwerkt dus we plaatsen de handmatige uitslag NIET in de tabel.
		}

		$i++;
	}


}
// -------------------------------------


//Uitslagen opslaan in database --------
function opslaan_uitslagen($uitslagen,$teamID)
{
	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to store Uitslagen");


	$query = "
	INSERT INTO ".$dbprefix."uitslag
	(
	TeamID,
	Datum,
	Thuis,
	Uit,
	Uitslag
	)
	VALUES
";
	$t = 0;
	$CountData = count($uitslagen);
	foreach($uitslagen AS $value)
	{
		$value["Thuis"] = htmlentities($value["Thuis"], ENT_QUOTES);
		$value["Uit"] = htmlentities($value["Uit"], ENT_QUOTES);
		$value['Uitslag'] = str_replace('&nbsp;&nbsp;', ' ', $value['Uitslag']);
		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$teamID."', '".convertStringToDate($value["Datum"])."', '".$value["Thuis"]."', '".$value["Uit"]."', '".$value['Uitslag']."'),";
		}
		else
		{
			$query.="('".$teamID."', '".convertStringToDate($value["Datum"])."', '".$value["Thuis"]."', '".$value["Uit"]."', '".$value['Uitslag']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysql_query($query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($query) or die(mysql_error());
	}

	// Verwijderen van lege entries
	$sql = "DELETE FROM `".$dbprefix."uitslag` WHERE Datum = '0000-00-00'";
	if ($UserDebug == 'Aan') {
		mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($sql) or die(mysql_error());
	}
}
// -------------------------------------


//Programma zonder details opslaan in database -------- Programma opslaan met details zie functie hieronder
function opslaan_programma($programma,$teamID)
{
	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to store programma");

	$query = "
	INSERT INTO ".$dbprefix."programma
	(
	TeamID,
	Datum,
	Tijd,
	Wedstrijd,
	Type,
	Accommodatie,
	Wedstrijdnr,
	Scheidsrechter,
		Status
	)
	VALUES
";
	$t = 0;
	$CountData = count($programma);
	foreach($programma AS $value)
	{
		if(!isset($value['Wedstrijd']))
		{
			$wedstrijd = '';
		}else{
			$value['Wedstrijd'] = htmlentities($value['Wedstrijd'], ENT_QUOTES);
		}
		$value['Accommodatie'] = htmlentities($value['Accommodatie'],ENT_QUOTES);
		if(!isset($value['Wedstrijdnr']))
		{
			$value['Wedstrijdnr'] = '';
		}
		if(!isset($value['Scheidsrechter']))
		{
			$value['Scheidsrechter'] = '';
		}else{
			$value['Scheidsrechter'] = htmlentities($value['Scheidsrechter'],ENT_QUOTES);
		}

		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$teamID."', '".convertStringToDate($value['Datum'])."', '".$value['Tijd']."', '".$value['Wedstrijd']."', '".$value['Type']."', '".$value['Accommodatie']."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."', '".$value['Status']."'),";
		}
		else
		{
			$query.="('".$teamID."', '".convertStringToDate($value['Datum'])."', '".$value['Tijd']."', '".$value['Wedstrijd']."', '".$value['Type']."', '".$value['Accommodatie']."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."', '".$value['Status']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysql_query($query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($query) or die(mysql_error());
	}

	// Verwijderen van lege entries
	$sql = "DELETE FROM `".$dbprefix."programma` WHERE Datum = '0000-00-00'";
	if ($UserDebug == 'Aan') {
		mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($sql) or die(mysql_error());
	}

	// Wijzigen van bepaalde tekens in Accomodatie. Dit wordt veroorzaakt in de html code van voetbal.nl
	// Indien nodig de // van de volgende 3 regels verwijderen en de codes veranderen. de eerste is wat vervangen moet worden door de tweede.
	// $sql = "UPDATE ".$dbprefix."programma SET Accommodatie = REPLACE(Accommodatie, '&Atilde;&cent;','&acirc;')";
	//  mysql_query($sql)
	//  or die(mysql_error());



}
// Einde functie programma zonder details opslaan -------------------------------------


//Programma met details opslaan in database -------- Programma opslaan zonder details zie functie hierboven
function opslaan_programmadetailed($programma,$link)
{
	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to Store ");

	$query = "
	INSERT INTO ".$dbprefix."programma
	(
		TeamID,
		Datum,
		Tijd,
		Wedstrijd,
	Type,
		Accommodatie,
	Wedstrijdnr,
	Scheidsrechter,
	Status,
	Sportpark,
	Adres,
	Postcode,
	Plaats,
	Telefoon
	)
	VALUES
";
	$t = 0;
	$CountData = count($programma);
	foreach($programma AS $value)
	{
		if(!isset($value['Wedstrijd']))
		{
			$wedstrijd = '';
		}else{
			$value['Wedstrijd'] = htmlentities($value['Wedstrijd'], ENT_QUOTES);
		}
		$value['Accommodatie'] = htmlentities($value['Accommodatie'],ENT_QUOTES);
		$value['Sportpark'] = htmlentities($value['Sportpark'],ENT_QUOTES);
		$value['Adres'] = htmlentities($value['Adres'],ENT_QUOTES);
		$value['Plaats'] = htmlentities($value['Plaats'],ENT_QUOTES);
		if(!isset($value['Wedstrijdnr']))
		{
			$value['Wedstrijdnr'] = '';
		}
		if(!isset($value['Scheidsrechter']))
		{
			$value['Scheidsrechter'] = '';
		}else{
			$value['Scheidsrechter'] = htmlentities($value['Scheidsrechter'],ENT_QUOTES);
		}

		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$link["TeamID"]."', '".convertStringToDate($value['Datum'])."', '".$value['Tijd']."', '".$value['Wedstrijd']."', '".$value['Type']."', '".$value['Accommodatie']."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."', '".$value['Status']."', '".$value['Sportpark']."', '".$value['Adres']."', '".$value['Postcode']."', '".$value['Plaats']."', '".$value['Telefoon']."'),";
		}
		else
		{
			$query.="('".$link["TeamID"]."', '".convertStringToDate($value['Datum'])."', '".$value['Tijd']."', '".$value['Wedstrijd']."', '".$value['Type']."', '".$value['Accommodatie']."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."', '".$value['Status']."', '".$value['Sportpark']."', '".$value['Adres']."', '".$value['Postcode']."', '".$value['Plaats']."', '".$value['Telefoon']."')";
			//        $query.="('".$link["TeamID"]."', '".convertStringToDate($value['Datum'])."', '".$value['Tijd']."', '".$value['Wedstrijd']."', '".$value['Type']."', '".$value['Accommodatie']."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."', '".$value['Status']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysql_query($query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($query) or die(mysql_error());
	}

	// Verwijderen van lege entries
	$sql = "DELETE FROM `".$dbprefix."programma` WHERE Datum = '0000-00-00'";
	if ($UserDebug == 'Aan') {
		mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($sql) or die(mysql_error());
	}
	// Wijzigen van bepaalde tekens in Accomodatie. Dit wordt veroorzaakt in de html code van voetbal.nl
	// Indien nodig de // van de volgende 3 regels verwijderen en de codes veranderen. de eerste is wat vervangen moet worden door de tweede.
	// $sql = "UPDATE ".$dbprefix."programma SET Accommodatie = REPLACE(Accommodatie, '&Atilde;&cent;','&acirc;')";
	//  mysql_query($sql)
	//  or die(mysql_error());



}
// Einde functie programma met details opslaan -------------------------------------



//Alle uitslagen in database -----------
function opslaan_alle($alle,$link)
{
	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to store alle uitslagen");

	//Alle Uitslagen opslaan in database
	//Veld Datum aangehouden aangezien dit wellicht in de toekomst gebruikt wordt.
	//Ntb veld is nader te bepalen. Dit zo gehouden omdat de structuur dan het zelfde is als Uitslag.

	$query = "
	INSERT INTO ".$dbprefix."alle
	(
	TeamID,
	Datum,
	Wedstrijd,
	Ntb,
	Uitslag
	)
	VALUES
";
	$t = 0;
	$CountData = count($alle);
	foreach($alle AS $value)
	{
		if(!isset($value['Wedstrijd']) && isset($value["Thuis"]) && isset($value["Uit"]))
		{
			$Wedstrijd = htmlentities($value["Thuis"] . ' - '.$value["Uit"], ENT_QUOTES);
		}else{
			$Wedstrijd = htmlentities($value['Wedstrijd'], ENT_QUOTES);
		}
		$t++;
		#last row has , (comma)
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$link["TeamID"]."', '".convertStringToDate($value["Datum"])."', '".$Wedstrijd."', '', '".$value["Uitslag"]."'),";
		}else{
			$query.="('".$link["TeamID"]."', '".convertStringToDate($value["Datum"])."', '".$Wedstrijd."', '', '".$value["Uitslag"]."')";
		}
	}

	if ($UserDebug == 'Aan') {
		mysql_query($query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($query) or die(mysql_error());
	}

	// Verwijderen van lege entries
	$sql = "DELETE FROM `".$dbprefix."alle` WHERE Uitslag = ' * '";
	if ($UserDebug == 'Aan') {
		mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($sql) or die(mysql_error());
	}
}
// -------------------------------------


//Club Programma opslaan in database ---
function opslaan_clubprog($clubprog)
{
    // echo "stap 11 <br />";

    include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to store clubprogramma");

	$query = "
	INSERT INTO ".$dbprefix."clubprogramma
	(
	Datum,
	Tijd,
	Thuis,
	Uit,
	Type,
	Accommodatie,
	Wedstrijdnr,
	Scheidsrechter
	)
	VALUES
";
	$t = 0;
	$CountData = count($clubprog);
    // echo "count: $CountData <br />";

    foreach($clubprog AS $value)
	{
		$value["Thuis"] = htmlentities($value["Thuis"], ENT_QUOTES);
		$value["Uit"] = htmlentities($value["Uit"], ENT_QUOTES);
		$value['Scheidsrechter'] = htmlentities($value['Scheidsrechter'], ENT_QUOTES);

		// $value['Accommodatie'] = htmlentities($value['Accommodatie'], ENT_QUOTES);
		// Omdat de Accomodatie niet meer direct beschikbaar bij het clubprogramma is vul deze maar met Thuis of Uit.

		If (stristr(strtolower($value["Thuis"]),str_replace('%','',strtolower($club1)))) { $Accomodatie = "Thuis"; } else { $Accomodatie = "Uit"; }

		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".convertStringToDate($value["Datum"])."', '".$value['Tijd']."', '".$value["Thuis"]."', '".$value["Uit"]."', '".$value['Type']."', '".$Accomodatie."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."'),";
		}
		else
		{
			$query.="('".convertStringToDate($value["Datum"])."', '".$value['Tijd']."', '".$value["Thuis"]."', '".$value["Uit"]."', '".$value['Type']."', '".$Accomodatie."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."')";
		}
	}
    // echo "query: $query <br />";
	if ($UserDebug == 'Aan') {
		mysql_query($query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($query) or die(mysql_error());
	}

	// verwijderen van bepaalde wedstrijden volgens instellingen.php
	if ($DelwdstrdCPCU == 'Aan') {
		$DelwdstrdCPCUtxt = "%".$DelwdstrdCPCUtxt."%";
		$sql = "DELETE FROM `".$dbprefix."clubprogramma` WHERE Thuis Like '$DelwdstrdCPCUtxt' OR Uit Like '$DelwdstrdCPCUtxt'";
		if ($UserDebug == 'Aan') {
			mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
		}
		else
		{
			mysql_query($sql) or die(mysql_error());
		}
	}

	// Verwijderen van lege entries
	//  $sql = "DELETE FROM `".$dbprefix."clubprogramma` WHERE Datum = ''";
	//  mysql_query($sql)
	//  or die(mysql_error());

	// Wijzigen van bepaalde tekens in Accomodatie. Dit wordt veroorzaakt in de html code van voetbal.nl
	// Indien nodig de // van de volgende 3 regels verwijderen en de codes veranderen. de eerste is wat vervangen moet worden door de tweede.
	// $sql = "UPDATE ".$dbprefix."clubprogramma SET Accommodatie = REPLACE(Accommodatie, '&Atilde;&cent;','&acirc;')";
	//  mysql_query($sql)
	//  or die(mysql_error());
	// Queries die Maa en Okt vervangen door Mar en Oct
	//$sql = "update ".$dbprefix."clubprogramma set Datum = replace(Datum,'Maa','Mar')";
	//mysql_query($sql)
	//or die(mysql_error());
	//$sql = "update ".$dbprefix."clubprogramma set Datum = replace(Datum,'Okt','Oct')";
	//mysql_query($sql)
	//or die(mysql_error());

}
// -------------------------------------


//Afgelastingen opslaan in database ---
function opslaan_clubaf($clubaf)
{
	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to store afgelastingen");

	$query = "
	INSERT INTO ".$dbprefix."afgelasting
	(
	Datum,
	Tijd,
	Thuis,
	Uit,
	Type,
	Accommodatie,
	Wedstrijdnr,
	Scheidsrechter,
	Status
	)
	VALUES
";
	$t = 0;
	$CountData = count($clubaf);
	foreach($clubaf AS $value)
	{
		$value["Thuis"] = htmlentities($value["Thuis"], ENT_QUOTES);
		$value["Uit"] = htmlentities($value["Uit"], ENT_QUOTES);
		$value['Scheidsrechter'] = htmlentities($value['Scheidsrechter'], ENT_QUOTES);
		$value['Accommodatie'] = htmlentities($value['Accommodatie'], ENT_QUOTES);
		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".convertStringToDate($value["Datum"])."', '".$value['Tijd']."', '".$value["Thuis"]."', '".$value["Uit"]."', '".$value['Type']."', '".$value['Accommodatie']."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."', '".$value['Status']."'),";
		}
		else
		{
			$query.="('".convertStringToDate($value["Datum"])."', '".$value['Tijd']."', '".$value["Thuis"]."', '".$value["Uit"]."', '".$value['Type']."', '".$value['Accommodatie']."', '".$value['Wedstrijdnr']."', '".$value['Scheidsrechter']."', '".$value['Status']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysql_query($query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($query) or die(mysql_error());
	}

	// Verwijderen van lege entries
	$sql = "DELETE FROM `".$dbprefix."clubprogramma` WHERE Datum = '0000-00-00'";
	if ($UserDebug == 'Aan') {
		mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($sql) or die(mysql_error());
	}

	// Wijzigen van bepaalde tekens in Accomodatie. Dit wordt veroorzaakt in de html code van voetbal.nl
	// Indien nodig de // van de volgende 3 regels verwijderen en de codes veranderen. de eerste is wat vervangen moet worden door de tweede.
	// $sql = "UPDATE ".$dbprefix."afgelasting SET Accommodatie = REPLACE(Accommodatie, '&Atilde;&cent;','&acirc;')";
	//  mysql_query($sql)
	//  or die(mysql_error());
	// Queries die Maa en Okt vervangen door Mar en Oct
	//$sql = "update ".$dbprefix."afgelasting set Datum = replace(Datum,'Maa','Mar')";
	//mysql_query($sql)
	//or die(mysql_error());
	//$sql = "update ".$dbprefix."afgelasting set Datum = replace(Datum,'Okt','Oct')";
	//mysql_query($sql)
	//or die(mysql_error());
}
// -------------------------------------

//Alle teams opslaan in database --------
function opslaan_teams($teams)
{
	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to store Uitslagen");


	$query = "
	INSERT INTO ".$dbprefix."teamlinks
	(
	`Teamcode`,
	`Naam`,
	`Klasse`,
	`Wedstrijdduur`,
	`Periode`,
	`Ophalen`,
	`GroupID`,
	`DatumTijd-Update`
	)
	VALUES
";
	$t = 0;
	$CountData = count($teams);


	foreach($teams AS $value)
	{

		$value["Naam"] = htmlentities($value["Naam"], ENT_QUOTES);
		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$value["Teamcode"]."', '".$value["Naam"]."', '".$value['Klasse']."', '".$value['Wedstrijdduur']."', '".$value['Periode']."', '".$value['Ophalen']."', '".$value['GroupID']."', '".$value['DatumTijd-Update']."'),";
		}
		else
		{
			$query.="('".$value["Teamcode"]."', '".$value["Naam"]."', '".$value['Klasse']."', '".$value['Wedstrijdduur']."', '".$value['Periode']."', '".$value['Ophalen']."', '".$value['GroupID']."', '".$value['DatumTijd-Update']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysql_query($query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($query) or die(mysql_error());
	}


}
// -------------------------------------

//ClubUitslagen opslaan in database --------
function opslaan_clubuitslagen($clubuitslagen)
{
	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to store Uitslagen");


	$query = "
	INSERT INTO ".$dbprefix."clubuitslagen
	(
	Datum,
	Thuis,
	Uit,
	Uitslag
	)
	VALUES
";
	$t = 0;
	$CountData = count($clubuitslagen);
	foreach($clubuitslagen AS $value)
	{
		$value["Thuis"] = htmlentities($value["Thuis"], ENT_QUOTES);
		$value["Uit"] = htmlentities($value["Uit"], ENT_QUOTES);
		$value['Uitslag'] = str_replace('&nbsp;&nbsp;', ' ', $value['Uitslag']);
		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".convertStringToDate($value["Datum"])."', '".$value["Thuis"]."', '".$value["Uit"]."', '".$value['Uitslag']."'),";
		}
		else
		{
			$query.="('".convertStringToDate($value["Datum"])."', '".$value["Thuis"]."', '".$value["Uit"]."', '".$value['Uitslag']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysql_query($query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($query) or die(mysql_error());
	}

	// verwijderen van bepaalde wedstrijden volgens instellingen.php

	if ($DelwdstrdCPCU == 'Aan') {
		$DelwdstrdCPCUtxt = "%".$DelwdstrdCPCUtxt."%";
		$sql = "DELETE FROM `".$dbprefix."clubuitslagen` WHERE Thuis Like '$DelwdstrdCPCUtxt' OR Uit Like '$DelwdstrdCPCUtxt'";
		if ($UserDebug == 'Aan') {
			mysql_query($sql) or die("A MySQL error has occurred.<br />Your Query: " . $sql . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
		}
		else
		{
			mysql_query($sql) or die(mysql_error());
		}
	}


	// Verwijderen van lege entries
	//  $sql = "DELETE FROM `".$dbprefix."uitslag` WHERE Datum = '0000-00-00'";
	//  mysql_query($sql)
	//  or die(mysql_error());
}


// -------------------------------------

//Teamindeling opslaan in database --------
function opslaan_teamindeling($teamindeling,$link)
{
	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to store Uitslagen");


	$query = "
	INSERT INTO ".$dbprefix."teamindeling
	(
	TeamID,
	Teamnr,
	Naam,
	Vktijd
	)
	VALUES
";
	$t = 0;
	$CountData = count($teamindeling);
	foreach($teamindeling AS $value)
	{
		$value["Naam"] = htmlentities($value["Naam"], ENT_QUOTES);
		$value["Vktijd"] = htmlentities($value["Vktijd"], ENT_QUOTES);
		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$link["TeamID"]."', '".$value['Teamnr']."', '".$value['Naam']."', '".$value['Vktijd']."'),";
		}
		else
		{
			$query.="('".$link["TeamID"]."', '".$value['Teamnr']."', '".$value['Naam']."', '".$value['Vktijd']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysql_query($query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($query) or die(mysql_error());
	}


}
// -------------------------------------

//Speelronde opslaan in database --------
function opslaan_speelronde($speelronde,$link)
{
	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to store Uitslagen");


	$query = "
	INSERT INTO ".$dbprefix."speelronde
	(
	TeamID,
	Speelrondenr,
	Wedstrijd
	)
	VALUES
";
	$t = 0;
	$CountData = count($speelronde);
	foreach($speelronde AS $value)
	{
		$value["Wedstrijd"] = htmlentities($value["Wedstrijd"], ENT_QUOTES);
		$t++;
		#last row has , (comma)
		if($CountData != $t)
		{
			$query.="('".$link["TeamID"]."', '".$value['Speelrondenr']."', '".$value['Wedstrijd']."'),";
		}
		else
		{
			$query.="('".$link["TeamID"]."', '".$value['Speelrondenr']."', '".$value['Wedstrijd']."')";
		}
	}
	if ($UserDebug == 'Aan') {
		mysql_query($query) or die("A MySQL error has occurred.<br />Your Query: " . $query . "<br /> Error: (" . mysql_errno() . ") " . mysql_error());
	}
	else
	{
		mysql_query($query) or die(mysql_error());
	}


}
// -------------------------------------


//Reorder Database ---------------------
//Opschonen van bestanden
function reorderdb($table,$tableID)
{
	include("config.php");
	mysql_connect($server,$username,$password);
	@mysql_select_db($database) or die( "Unable to select database to reorder database");

	mysql_query("ALTER TABLE $table DROP $tableID")
	or die(mysql_error());
	mysql_query("OPTIMIZE TABLE $table")
	or die(mysql_error());
	mysql_query("ALTER TABLE $table ADD $tableID INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST")
	or die(mysql_error());
}
// -------------------------------------

?>